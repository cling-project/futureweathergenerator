/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator;

/**
 * Enumerates the months and returns the number of hours in the month and its
 * first and last hour.
 *
 * @author eugenio
 */
public class Months {

    /**
     * Calculates the number of hours of a month in the year, starting hour, and
     * last hour of the month.
     *
     * @param month month abbreviation
     * @return array with number of hours in the month, starting hour, and last
     * hour in the month
     */
    public static int[] getMonthRowIds(Abbreviation month) {
        int rows_in_month_days;
        int first_row_id;
        int last_row_id;
        switch (month) {
            case Jan -> {
                rows_in_month_days = 31 * 24;
                first_row_id = 0;
                last_row_id = first_row_id + rows_in_month_days;
            }
            case Feb -> {
                rows_in_month_days = 28 * 24;
                first_row_id = 31 * 24;
                last_row_id = first_row_id + rows_in_month_days;
            }
            case Mar -> {
                rows_in_month_days = 31 * 24;
                first_row_id = (31 + 28) * 24;
                last_row_id = first_row_id + rows_in_month_days;
            }
            case Apr -> {
                rows_in_month_days = 30 * 24;
                first_row_id = (31 + 28 + 31) * 24;
                last_row_id = first_row_id + rows_in_month_days;
            }
            case May -> {
                rows_in_month_days = 31 * 24;
                first_row_id = (31 + 28 + 31 + 30) * 24;
                last_row_id = first_row_id + rows_in_month_days;
            }
            case Jun -> {
                rows_in_month_days = 30 * 24;
                first_row_id = (31 + 28 + 31 + 30 + 31) * 24;
                last_row_id = first_row_id + rows_in_month_days;
            }
            case Jul -> {
                rows_in_month_days = 31 * 24;
                first_row_id = (31 + 28 + 31 + 30 + 31 + 30) * 24;
                last_row_id = first_row_id + rows_in_month_days;
            }
            case Aug -> {
                rows_in_month_days = 31 * 24;
                first_row_id = (31 + 28 + 31 + 30 + 31 + 30 + 31) * 24;
                last_row_id = first_row_id + rows_in_month_days;
            }
            case Sep -> {
                rows_in_month_days = 30 * 24;
                first_row_id = (31 + 28 + 31 + 30 + 31 + 30 + 31 + 31) * 24;
                last_row_id = first_row_id + rows_in_month_days;
            }
            case Oct -> {
                rows_in_month_days = 31 * 24;
                first_row_id = (31 + 28 + 31 + 30 + 31 + 30 + 31 + 31 + 30) * 24;
                last_row_id = first_row_id + rows_in_month_days;
            }
            case Nov -> {
                rows_in_month_days = 30 * 24;
                first_row_id = (31 + 28 + 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31) * 24;
                last_row_id = first_row_id + rows_in_month_days;
            }
            case Dec -> {
                rows_in_month_days = 31 * 24;
                first_row_id = (31 + 28 + 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31 + 30) * 24;
                last_row_id = first_row_id + rows_in_month_days;
            }
            default ->
                throw new UnsupportedOperationException("Error: Unknown month abbreviation: " + month.name());
        }
        return new int[]{rows_in_month_days, first_row_id, last_row_id};
    }

    /**
     * Enumerates the month abbreviations.
     */
    public enum Abbreviation {
        /**
         * January.
         */
        Jan,
        /**
         * February.
         */
        Feb,
        /**
         * March.
         */
        Mar,
        /**
         * April.
         */
        Apr,
        /**
         * May.
         */
        May,
        /**
         * June.
         */
        Jun,
        /**
         * July.
         */
        Jul,
        /**
         * August.
         */
        Aug,
        /**
         * September.
         */
        Sep,
        /**
         * October.
         */
        Oct,
        /**
         * November.
         */
        Nov,
        /**
         * December.
         */
        Dec
    };

}
