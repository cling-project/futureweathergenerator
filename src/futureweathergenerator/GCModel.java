/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator;

/**
 * Enumerates the implemented simulation results from general circulation
 * models.
 *
 * @author eugenio
 */
public enum GCModel {
    /**
     * Beijing Climate Center Climate System Model. URL: https://gmd.copernicus.org/articles/14/2977/2021/.
     */
    BCC_CSM2_MR,
    /**
     * The Canadian Earth System Model version 5. URL: https://www.wdc-climate.de/ui/cmip6?input=CMIP6.ScenarioMIP.CCCma.CanESM5.
     */
    CanESM5,
    /**
     * The Canadian Earth System Model version 5.1. URL: https://www.wdc-climate.de/ui/cmip6?input=CMIP6.ScenarioMIP.CCCma.CanESM5.1.
     */
    CanESM5_1,
    /**
     * The Canadian Earth System Model version Canadian Ocean Ecosystem. URL: https://www.wdc-climate.de/ui/cmip6?input=CMIP6.ScenarioMIP.CCCma.CanESM5-CanOE.
     */
    CanESM5_CanOE,
    /**
     * Centro Euro-Mediterraneo sui Cambiamenti Climatici. URL: https://www.cmcc.it/models/cmcc-esm-earth-system-model.
     */
    CMCC_ESM2,
    /**
     * Chinese Academy of Sciences Earth System Model version 2.0. URL: https://www.wdc-climate.de/ui/cmip6?input=CMIP6.CMIP.CAS.CAS-ESM2-0.
     */
    CAS_ESM2_0,
    /**
     * Centre National de Recherches Meteorologiques. URL: https://www.wdc-climate.de/ui/cmip6?input=CMIP6.HighResMIP.CNRM-CERFACS.CNRM-CM6-1-HR.
     */
    CNRM_CM6_1_HR,
    /**
     * Centre National de Recherches Meteorologiques. URL: https://www.wdc-climate.de/ui/cmip6?input=CMIP6.CMIP.CNRM-CERFACS.CNRM-ESM2-1.
     */
    CNRM_ESM2_1,
    /**
     * European Community Earth-System Model 3. URL: https://www.wdc-climate.de/ui/cmip6?input=CMIP6.ScenarioMIP.EC-Earth-Consortium.EC-Earth3.
     */
    EC_Earth3,
    /**
     * European Community Earth-System Model 3. URL: https://www.wdc-climate.de/ui/cmip6?input=CMIP6.ScenarioMIP.EC-Earth-Consortium.EC-Earth3-Veg.
     */
    EC_Earth3_Veg,
    /**
     * Flexible Global Ocean-Atmosphere-Land System Model. URL: https://agupubs.onlinelibrary.wiley.com/doi/full/10.1029/2019MS002012.
     */
    FGOALS_g3,
    /**
     * NASA, Goddard Institute for Space Studies. URL: https://www.wdc-climate.de/ui/cmip6?input=CMIP6.ISMIP6.NASA-GISS.GISS-E2-1-G.
     */
    GISS_E2_1_G,
    /**
     * NASA, Goddard Institute for Space Studies. URL: https://www.wdc-climate.de/ui/cmip6?input=CMIP6.CMIP.NASA-GISS.GISS-E2-1-H.
     */
    GISS_E2_1_H,
    /**
     * NASA, Goddard Institute for Space Studies. URL: https://www.wdc-climate.de/ui/cmip6?input=CMIP6.CMIP.NASA-GISS.GISS-E2-2-G.
     */
    GISS_E2_2_G,
    /**
     * Institut Pierre Simon Laplace. URL: https://cmc.ipsl.fr/ipsl-climate-models/ipsl-cm6/.
     */
    IPSL_CM6A_LR,
    /**
     * MIROC-Earth System Model Version 2 high resolution. URL: https://www.wdc-climate.de/ui/cmip6?input=CMIP6.GeoMIP.MIROC.MIROC-ES2H.
     */
    MIROC_ES2H,
    /**
     * MIROC-Earth System Model Version 2 low resolution. URL: https://www.wdc-climate.de/ui/cmip6?input=CMIP6.ScenarioMIP.MIROC.MIROC-ES2L.
     */
    MIROC_ES2L,
    /**
     * Center for Earth System Research and Sustainability. URL: https://cera-www.dkrz.de/WDCC/ui/cerasearch/cmip6?input=CMIP6.CMIP.MIROC.MIROC6.
     */
    MIROC6,
    /**
     * Meteorological Research Institute Earth System Model 2. URL: https://www.wdc-climate.de/ui/cmip6?input=CMIP6.CMIP.MRI.MRI-ESM2-0.
     */
    MRI_ESM2_0,
    /**
     * U.K. Earth System Model. URL: https://ukesm.ac.uk/wp-content/uploads/2022/06/UKESM1-0-LL.html.
     */
    UKESM1_0_LL;
}
