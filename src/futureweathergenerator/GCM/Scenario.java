/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.GCM;

import java.util.ArrayList;

/**
 * Creates a scenario with the monthly mean difference variables.
 *
 * @author eugenio
 */
public class Scenario extends InterpolatedVariablesScenario {

    private final Variable[] near_surface_air_temperature; // tas
    private final Variable[] daily_maximum_near_surface_air_temperature; // tasmax
    private final Variable[] daily_minimum_near_surface_air_temperature; // tasmin
    private final Variable[] precipitation; // pr
    private final Variable[] surface_downwelling_shortwave_radiation; // rsds
    private final Variable[] total_cloud_cover_percentage; // clt
    private final Variable[] snow_depth; // snd
    private final Variable[] near_surface_specific_humidity; // huss
    private final Variable[] air_pressure_at_mean_sea_level; // psl
    private final Variable[] near_surface_wind_speed; // sfcWind

    /**
     * Initiates the Scenario object.
     *
     * @param GRID_POINT_OPTION grid weighted distance option
     * @param timeframe timeframe name
     * @param scenario scenario name
     * @param latitude latitude of the location
     * @param longitude longitude of the location
     * @param four_nearest_points four nearest points
     * @param near_surface_air_temperature monthly change near surface air temperature
     * @param daily_maximum_near_surface_air_temperature monthly change daily maximum near surface air temperature
     * @param daily_minimum_near_surface_air_temperature monthly change daily minimum near surface air temperature
     * @param precipitation monthly change precipitation
     * @param surface_downwelling_shortwave_radiation monthly change surface downwelling shortwave radiation
     * @param total_cloud_cover_percentage monthly change total cloud cover percentage
     * @param snow_depth monthly change snow depth
     * @param near_surface_specific_humidity monthly change near surface specific humidity
     * @param air_pressure_at_mean_sea_level monthly change air pressure at mean sea level
     * @param near_surface_wind_speed monthly change near surface wind speed
     */
    public Scenario(int GRID_POINT_OPTION, String timeframe, String scenario, float latitude, float longitude, ArrayList<GridPoint> four_nearest_points, Variable[] near_surface_air_temperature, Variable[] daily_maximum_near_surface_air_temperature, Variable[] daily_minimum_near_surface_air_temperature, Variable[] precipitation, Variable[] surface_downwelling_shortwave_radiation, Variable[] total_cloud_cover_percentage, Variable[] snow_depth, Variable[] near_surface_specific_humidity, Variable[] air_pressure_at_mean_sea_level, Variable[] near_surface_wind_speed) {
        super(timeframe, scenario);
        this.near_surface_air_temperature = near_surface_air_temperature;
        this.daily_maximum_near_surface_air_temperature = daily_maximum_near_surface_air_temperature;
        this.daily_minimum_near_surface_air_temperature = daily_minimum_near_surface_air_temperature;
        this.precipitation = precipitation;
        this.surface_downwelling_shortwave_radiation = surface_downwelling_shortwave_radiation;
        this.total_cloud_cover_percentage = total_cloud_cover_percentage;
        this.snow_depth = snow_depth;
        this.near_surface_specific_humidity = near_surface_specific_humidity;
        this.air_pressure_at_mean_sea_level = air_pressure_at_mean_sea_level;
        this.near_surface_wind_speed = near_surface_wind_speed;

        System.out.println();
        switch (GRID_POINT_OPTION) {
            default:
            case 0:
                System.out.println("\tGrid: the weighted distance to the four nearest points will be used.");
                break;
            case 1:
                System.out.println("\tGrid: the average of the four nearest points will be used.");
                break;
            case 2:
                System.out.println("\tGrid: the nearest point will be used.");
                break;
        }

        int nearest_point_id = Grid.getNearestPoint(longitude, latitude, four_nearest_points);
        System.out.println();

        // Sets the variables values by or (a) averaging the four nearest points
        // when the location is nearer to the center of those four points
        // or (b) the nearest point of the four if it is nearer than the center.
        if (this.near_surface_air_temperature == null) {
            super.interpolated_near_surface_air_temperature = null;
        } else {
            super.interpolated_near_surface_air_temperature = new float[12];
            for (int i = 0; i < 12; i++) {
                float a = this.near_surface_air_temperature[i].getFour_nearest_points_var_values()[0];
                float b = this.near_surface_air_temperature[i].getFour_nearest_points_var_values()[1];
                float c = this.near_surface_air_temperature[i].getFour_nearest_points_var_values()[2];
                float d = this.near_surface_air_temperature[i].getFour_nearest_points_var_values()[3];
                switch (GRID_POINT_OPTION) {
                    default:
                    case 0:
                        super.interpolated_near_surface_air_temperature[i] = Grid.getWeightedValue(latitude, longitude, four_nearest_points, a, b, c, d);
                        break;
                    case 1:
                        super.interpolated_near_surface_air_temperature[i] = (a + b + c + d) / 4f;
                        break;
                    case 2:
                        super.interpolated_near_surface_air_temperature[i] = this.near_surface_air_temperature[i].getFour_nearest_points_var_values()[nearest_point_id];
                        break;
                }
            }
        }

        if (this.daily_maximum_near_surface_air_temperature == null) {
            super.interpolated_daily_maximum_near_surface_air_temperature = null;
        } else {
            super.interpolated_daily_maximum_near_surface_air_temperature = new float[12];
            for (int i = 0; i < 12; i++) {
                float a = this.daily_maximum_near_surface_air_temperature[i].getFour_nearest_points_var_values()[0];
                float b = this.daily_maximum_near_surface_air_temperature[i].getFour_nearest_points_var_values()[1];
                float c = this.daily_maximum_near_surface_air_temperature[i].getFour_nearest_points_var_values()[2];
                float d = this.daily_maximum_near_surface_air_temperature[i].getFour_nearest_points_var_values()[3];
                switch (GRID_POINT_OPTION) {
                    default:
                    case 0:
                        super.interpolated_daily_maximum_near_surface_air_temperature[i] = Grid.getWeightedValue(latitude, longitude, four_nearest_points, a, b, c, d);
                        break;
                    case 1:
                        super.interpolated_daily_maximum_near_surface_air_temperature[i] = (a + b + c + d) / 4f;
                        break;
                    case 2:
                        super.interpolated_daily_maximum_near_surface_air_temperature[i] = this.daily_maximum_near_surface_air_temperature[i].getFour_nearest_points_var_values()[nearest_point_id];
                        break;
                }
            }
        }

        if (this.daily_minimum_near_surface_air_temperature == null) {
            super.interpolated_daily_minimum_near_surface_air_temperature = null;
        } else {
            super.interpolated_daily_minimum_near_surface_air_temperature = new float[12];
            for (int i = 0; i < 12; i++) {
                float a = this.daily_minimum_near_surface_air_temperature[i].getFour_nearest_points_var_values()[0];
                float b = this.daily_minimum_near_surface_air_temperature[i].getFour_nearest_points_var_values()[1];
                float c = this.daily_minimum_near_surface_air_temperature[i].getFour_nearest_points_var_values()[2];
                float d = this.daily_minimum_near_surface_air_temperature[i].getFour_nearest_points_var_values()[3];
                switch (GRID_POINT_OPTION) {
                    default:
                    case 0:
                        super.interpolated_daily_minimum_near_surface_air_temperature[i] = Grid.getWeightedValue(latitude, longitude, four_nearest_points, a, b, c, d);
                        break;
                    case 1:
                        super.interpolated_daily_minimum_near_surface_air_temperature[i] = (a + b + c + d) / 4f;
                        break;
                    case 2:
                        super.interpolated_daily_minimum_near_surface_air_temperature[i] = this.daily_minimum_near_surface_air_temperature[i].getFour_nearest_points_var_values()[nearest_point_id];
                        break;
                }
            }
        }

        if (this.precipitation == null) {
            super.interpolated_precipitation = null;
        } else {
            super.interpolated_precipitation = new float[12];
            for (int i = 0; i < 12; i++) {
                float a = getFraction(this.precipitation[i].getFour_nearest_points_var_values()[0]);
                float b = getFraction(this.precipitation[i].getFour_nearest_points_var_values()[1]);
                float c = getFraction(this.precipitation[i].getFour_nearest_points_var_values()[2]);
                float d = getFraction(this.precipitation[i].getFour_nearest_points_var_values()[3]);
                switch (GRID_POINT_OPTION) {
                    default:
                    case 0:
                        super.interpolated_precipitation[i] = Grid.getWeightedValue(latitude, longitude, four_nearest_points, a, b, c, d);
                        break;
                    case 1:
                        super.interpolated_precipitation[i] = (a + b + c + d) / 4f;
                        break;
                    case 2:
                        super.interpolated_precipitation[i] = getFraction(this.precipitation[i].getFour_nearest_points_var_values()[nearest_point_id]);
                        break;
                }
            }
        }

        if (this.surface_downwelling_shortwave_radiation == null) {
            super.interpolated_surface_downwelling_shortwave_radiation = null;
        } else {
            super.interpolated_surface_downwelling_shortwave_radiation = new float[12];
            for (int i = 0; i < 12; i++) {
                float a = this.surface_downwelling_shortwave_radiation[i].getFour_nearest_points_var_values()[0];
                float b = this.surface_downwelling_shortwave_radiation[i].getFour_nearest_points_var_values()[1];
                float c = this.surface_downwelling_shortwave_radiation[i].getFour_nearest_points_var_values()[2];
                float d = this.surface_downwelling_shortwave_radiation[i].getFour_nearest_points_var_values()[3];
                switch (GRID_POINT_OPTION) {
                    default:
                    case 0:
                        super.interpolated_surface_downwelling_shortwave_radiation[i] = Grid.getWeightedValue(latitude, longitude, four_nearest_points, a, b, c, d);
                        break;
                    case 1:
                        super.interpolated_surface_downwelling_shortwave_radiation[i] = (a + b + c + d) / 4f;
                        break;
                    case 2:
                        super.interpolated_surface_downwelling_shortwave_radiation[i] = this.surface_downwelling_shortwave_radiation[i].getFour_nearest_points_var_values()[nearest_point_id];
                        break;
                }
            }
        }

        if (this.total_cloud_cover_percentage == null) {
            super.interpolated_total_cloud_cover_percentage = null;
        } else {
            super.interpolated_total_cloud_cover_percentage = new float[12];
            for (int i = 0; i < 12; i++) {
                float a = this.total_cloud_cover_percentage[i].getFour_nearest_points_var_values()[0];
                float b = this.total_cloud_cover_percentage[i].getFour_nearest_points_var_values()[1];
                float c = this.total_cloud_cover_percentage[i].getFour_nearest_points_var_values()[2];
                float d = this.total_cloud_cover_percentage[i].getFour_nearest_points_var_values()[3];
                switch (GRID_POINT_OPTION) {
                    default:
                    case 0:
                        super.interpolated_total_cloud_cover_percentage[i] = Grid.getWeightedValue(latitude, longitude, four_nearest_points, a, b, c, d);
                        break;
                    case 1:
                        super.interpolated_total_cloud_cover_percentage[i] = (a + b + c + d) / 4f;
                        break;
                    case 2:
                        super.interpolated_total_cloud_cover_percentage[i] = this.total_cloud_cover_percentage[i].getFour_nearest_points_var_values()[nearest_point_id];
                        break;
                }
            }
        }

        if (this.snow_depth == null) {
            super.interpolated_snow_depth = null;
        } else {
            super.interpolated_snow_depth = new float[12];
            for (int i = 0; i < 12; i++) {
                float a = getFraction(this.snow_depth[i].getFour_nearest_points_var_values()[0]);
                float b = getFraction(this.snow_depth[i].getFour_nearest_points_var_values()[1]);
                float c = getFraction(this.snow_depth[i].getFour_nearest_points_var_values()[2]);
                float d = getFraction(this.snow_depth[i].getFour_nearest_points_var_values()[3]);
                switch (GRID_POINT_OPTION) {
                    default:
                    case 0:
                        super.interpolated_snow_depth[i] = Grid.getWeightedValue(latitude, longitude, four_nearest_points, a, b, c, d);
                        break;
                    case 1:
                        super.interpolated_snow_depth[i] = (a + b + c + d) / 4f;
                        break;
                    case 2:
                        super.interpolated_snow_depth[i] = getFraction(this.snow_depth[i].getFour_nearest_points_var_values()[nearest_point_id]);
                        break;
                }
            }
        }

        if (this.near_surface_specific_humidity == null) {
            super.interpolated_near_surface_specific_humidity = null;
        } else {
            super.interpolated_near_surface_specific_humidity = new float[12];
            for (int i = 0; i < 12; i++) {
                float a = this.near_surface_specific_humidity[i].getFour_nearest_points_var_values()[0];
                float b = this.near_surface_specific_humidity[i].getFour_nearest_points_var_values()[1];
                float c = this.near_surface_specific_humidity[i].getFour_nearest_points_var_values()[2];
                float d = this.near_surface_specific_humidity[i].getFour_nearest_points_var_values()[3];
                switch (GRID_POINT_OPTION) {
                    default:
                    case 0:
                        super.interpolated_near_surface_specific_humidity[i] = Grid.getWeightedValue(latitude, longitude, four_nearest_points, a, b, c, d);
                        break;
                    case 1:
                        super.interpolated_near_surface_specific_humidity[i] = (a + b + c + d) / 4f;
                        break;
                    case 2:
                        super.interpolated_near_surface_specific_humidity[i] = this.near_surface_specific_humidity[i].getFour_nearest_points_var_values()[nearest_point_id];
                        break;
                }
            }
        }

        if (this.air_pressure_at_mean_sea_level == null) {
            super.interpolated_air_pressure_at_mean_sea_level = null;
        } else {
            super.interpolated_air_pressure_at_mean_sea_level = new float[12];
            for (int i = 0; i < 12; i++) {
                // Convert Pa to hPa
                float a = this.air_pressure_at_mean_sea_level[i].getFour_nearest_points_var_values()[0] / 100.0f;
                float b = this.air_pressure_at_mean_sea_level[i].getFour_nearest_points_var_values()[1] / 100.0f;
                float c = this.air_pressure_at_mean_sea_level[i].getFour_nearest_points_var_values()[2] / 100.0f;
                float d = this.air_pressure_at_mean_sea_level[i].getFour_nearest_points_var_values()[3] / 100.0f;
                switch (GRID_POINT_OPTION) {
                    default:
                    case 0:
                        super.interpolated_air_pressure_at_mean_sea_level[i] = Grid.getWeightedValue(latitude, longitude, four_nearest_points, a, b, c, d);
                        break;
                    case 1:
                        super.interpolated_air_pressure_at_mean_sea_level[i] = (a + b + c + d) / 4f;
                        break;
                    case 2:
                        super.interpolated_air_pressure_at_mean_sea_level[i] = this.air_pressure_at_mean_sea_level[i].getFour_nearest_points_var_values()[nearest_point_id] / 100.0f;
                        break;
                }
            }
        }

        if (this.near_surface_wind_speed == null) {
            super.interpolated_near_surface_wind_speed = null;
        } else {
            super.interpolated_near_surface_wind_speed = new float[12];
            for (int i = 0; i < 12; i++) {
                float a = getFraction(this.near_surface_wind_speed[i].getFour_nearest_points_var_values()[0]);
                float b = getFraction(this.near_surface_wind_speed[i].getFour_nearest_points_var_values()[1]);
                float c = getFraction(this.near_surface_wind_speed[i].getFour_nearest_points_var_values()[2]);
                float d = getFraction(this.near_surface_wind_speed[i].getFour_nearest_points_var_values()[3]);
                switch (GRID_POINT_OPTION) {
                    default:
                    case 0:
                        super.interpolated_near_surface_wind_speed[i] = Grid.getWeightedValue(latitude, longitude, four_nearest_points, a, b, c, d);
                        break;
                    case 1:
                        super.interpolated_near_surface_wind_speed[i] = (a + b + c + d) / 4f;
                        break;
                    case 2:
                        super.interpolated_near_surface_wind_speed[i] = getFraction(this.near_surface_wind_speed[i].getFour_nearest_points_var_values()[nearest_point_id]);
                        break;
                }
            }
        }
    }

    /**
     * Returns an array of monthly changes of surface downwelling shortwave radiation.
     *
     * @return array
     */
    public Variable[] getSurface_downwelling_shortwave_radiation() {
        return surface_downwelling_shortwave_radiation;
    }

    /**
     * Returns an array of monthly changes of precipitation.
     *
     * @return array
     */
    public Variable[] getPrecipitation() {
        return precipitation;
    }

    /**
     * Returns an array of monthly changes of snow depth.
     *
     * @return array
     */
    public Variable[] getSnow_depth() {
        return snow_depth;
    }

    /**
     * Returns an array of monthly changes of near surface specific humidity.
     *
     * @return array
     */
    public Variable[] getNear_surface_specific_humidity() {
        return near_surface_specific_humidity;
    }

    /**
     * Returns an array of monthly changes of near surface air temperature.
     *
     * @return array
     */
    public Variable[] getNear_surface_air_temperature() {
        return near_surface_air_temperature;
    }

    /**
     * Returns an array of monthly changes daily maximum near surface air temperature.
     *
     * @return array
     */
    public Variable[] getDaily_maximum_near_surface_air_temperature() {
        return daily_maximum_near_surface_air_temperature;
    }

    /**
     * Returns an array of monthly changes daily minimum near surface air temperature.
     *
     * @return array
     */
    public Variable[] getDaily_minimum_near_surface_air_temperature() {
        return daily_minimum_near_surface_air_temperature;
    }

    /**
     * Returns an array of monthly changes of near surface wind speed.
     *
     * @return array
     */
    public Variable[] getNear_surface_wind_speed() {
        return near_surface_wind_speed;
    }

    /**
     * Returns an array of monthly changes of air pressure at mean sea level.
     *
     * @return array
     */
    public Variable[] getAir_pressure_at_mean_sea_level() {
        return air_pressure_at_mean_sea_level;
    }

    /**
     * Returns an array of monthly changes of total cloud cover percentage.
     *
     * @return array
     */
    public Variable[] getTotal_cloud_cover_percentage() {
        return total_cloud_cover_percentage;
    }

    private float getFraction(float value) {
        return value == 100000002004087730000f ? 1 : value;
    }
}
