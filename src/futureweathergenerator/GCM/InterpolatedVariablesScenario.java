/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.GCM;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.Locale;

/**
 * Sets the average GCM model_type scenario.
 *
 * @author eugenio
 */
public class InterpolatedVariablesScenario {

    private final String scenario;
    private final String timeframe;

    /**
     * Monthly interpolated mean daily temperature (∆°C).
     */
    public float[] interpolated_near_surface_air_temperature; // tas
    /**
     * Monthly interpolated maximum daily temperature (∆°C).
     */
    public float[] interpolated_daily_maximum_near_surface_air_temperature; // tasmanx
    /**
     * Monthly interpolated minimum daily temperature (∆°C).
     */
    public float[] interpolated_daily_minimum_near_surface_air_temperature; // tasmin
    /**
     * Monthly interpolated precipitation (fraction). // pr
     */
    public float[] interpolated_precipitation;
    /**
     * Monthly interpolated total downward surface shortwave flux (∆W/m2).
     */
    public float[] interpolated_surface_downwelling_shortwave_radiation; // rsds
    /**
     * Monthly interpolated total cloud in longwave radiation (∆%).
     */
    public float[] interpolated_total_cloud_cover_percentage; // clt
    /**
     * Monthly interpolated snow depth (fraction).
     */
    public float[] interpolated_snow_depth; // snd
    /**
     * Monthly interpolated specific humidity (∆g/kg).
     */
    public float[] interpolated_near_surface_specific_humidity; // huss
    /**
     * Monthly interpolated mean sea level pressure (∆hPa).
     */
    public float[] interpolated_air_pressure_at_mean_sea_level; // psl
    /**
     * Monthly interpolated wind speed (fraction).
     */
    public float[] interpolated_near_surface_wind_speed; // sfcWind

    /**
     * Initiates the AverageScenario object.
     *
     * @param timeframe future timeframe
     * @param scenario final scenario
     */
    public InterpolatedVariablesScenario(String timeframe, String scenario) {
        this.timeframe = timeframe;
        this.scenario = scenario;
    }

    /**
     * Initiates the AverageScenario object from given scenarios.
     *
     * @param scenario scenario
     */
    public InterpolatedVariablesScenario(Scenario scenario) {
        this.timeframe = scenario.getTimeframe();
        this.scenario = scenario.getScenario();
        this.interpolated_near_surface_air_temperature = scenario.getInterpolated_mean_daily_temperature();
        this.interpolated_daily_maximum_near_surface_air_temperature = scenario.getInterpolated_maximum_daily_temperature();
        this.interpolated_daily_minimum_near_surface_air_temperature = scenario.getInterpolated_minimum_daily_temperature();
        this.interpolated_precipitation = scenario.getInterpolated_precipitation();
        this.interpolated_surface_downwelling_shortwave_radiation = scenario.getInterpolated_total_downward_surface_shortwave_flux();
        this.interpolated_total_cloud_cover_percentage = scenario.getInterpolated_total_cloud_in_longwave_radiation();
        this.interpolated_snow_depth = scenario.getInterpolated_snow_depth();
        this.interpolated_near_surface_specific_humidity = scenario.getInterpolated_specific_humidity();
        this.interpolated_air_pressure_at_mean_sea_level = scenario.getInterpolated_mean_sea_level_pressure();
        this.interpolated_near_surface_wind_speed = scenario.getInterpolated_wind_speed();
    }

    /**
     * Returns string name of the scenario.
     *
     * @return string
     */
    public String getScenario() {
        return scenario;
    }

    /**
     * Returns string name of the timeframe.
     *
     * @return string
     */
    public String getTimeframe() {
        return timeframe;
    }

    /**
     * Returns an array of monthly interpolated precipitation (fraction).
     *
     * @return array
     */
    public float[] getInterpolated_precipitation() {
        return interpolated_precipitation;
    }

    /**
     * Returns an array of monthly interpolated total downward surface shortwave
     * flux (∆w/m2).
     *
     * @return array
     */
    public float[] getInterpolated_total_downward_surface_shortwave_flux() {
        return interpolated_surface_downwelling_shortwave_radiation;
    }

    /**
     * Returns an array of monthly interpolated snow depth (fraction).
     *
     * @return array
     */
    public float[] getInterpolated_snow_depth() {
        return interpolated_snow_depth;
    }

    /**
     * Returns an array of monthly interpolated specific humidity (∆g/kg).
     *
     * @return array
     */
    public float[] getInterpolated_specific_humidity() {
        return interpolated_near_surface_specific_humidity;
    }

    /**
     * Returns an array of monthly interpolated mean daily temperature (∆°C).
     *
     * @return array
     */
    public float[] getInterpolated_mean_daily_temperature() {
        return interpolated_near_surface_air_temperature;
    }

    /**
     * Returns an array of monthly interpolated maximum daily temperature (∆°C).
     *
     * @return array
     */
    public float[] getInterpolated_maximum_daily_temperature() {
        return interpolated_daily_maximum_near_surface_air_temperature;
    }

    /**
     * Returns an array of monthly interpolated minimum daily temperature (∆°C).
     *
     * @return array
     */
    public float[] getInterpolated_minimum_daily_temperature() {
        return interpolated_daily_minimum_near_surface_air_temperature;
    }

    /**
     * Returns an array of monthly interpolated wind speed (fraction).
     *
     * @return array
     */
    public float[] getInterpolated_wind_speed() {
        return interpolated_near_surface_wind_speed;
    }

    /**
     * Returns an array of monthly interpolated mean sea level pressure (∆hPa).
     *
     * @return array
     */
    public float[] getInterpolated_mean_sea_level_pressure() {
        return interpolated_air_pressure_at_mean_sea_level;
    }

    /**
     * Returns an array of monthly interpolated total cloud in longwave
     * radiation (∆W/m2).
     *
     * @return array
     */
    public float[] getInterpolated_total_cloud_in_longwave_radiation() {
        return interpolated_total_cloud_cover_percentage;
    }

    /**
     * Saves a csv table of the GCM interpolated variables.
     *
     * @param model_type_string string of model type
     * @param printOutputs String buffer for echo outputs
     * @param errorsAndWarnings String buffer for errors and warnings
     * @param path_output path to csv file
     */
    public void saveTable_GCM_variables(String model_type_string, StringBuffer printOutputs, StringBuffer errorsAndWarnings, String path_output) {
        printOutputs.append(String.format(Locale.ROOT, "%n\tSaving ")).append(model_type_string).append(String.format(Locale.ROOT, " monthly variable values to file...%n\t")).append(path_output).append(String.format(Locale.ROOT, "%n"));
        StringBuilder sb = new StringBuilder();
        sb.append("# ").append(model_type_string).append(" ").append(this.scenario).append(" ").append(this.timeframe).append(String.format(Locale.ROOT, "%n"));
        sb.append(String.format(Locale.ROOT, "Variable,Abbreviation,Unit,Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec%n"));
        sb.append(String.format(Locale.ROOT, "Near surface air temperature,tas,∆°C,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f%n",
                interpolated_near_surface_air_temperature[0],
                interpolated_near_surface_air_temperature[1],
                interpolated_near_surface_air_temperature[2],
                interpolated_near_surface_air_temperature[3],
                interpolated_near_surface_air_temperature[4],
                interpolated_near_surface_air_temperature[5],
                interpolated_near_surface_air_temperature[6],
                interpolated_near_surface_air_temperature[7],
                interpolated_near_surface_air_temperature[8],
                interpolated_near_surface_air_temperature[9],
                interpolated_near_surface_air_temperature[10],
                interpolated_near_surface_air_temperature[11]));
        sb.append(String.format(Locale.ROOT, "Daily maximum near surface air temperature,tasmax,∆°C,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f%n",
                interpolated_daily_maximum_near_surface_air_temperature[0],
                interpolated_daily_maximum_near_surface_air_temperature[1],
                interpolated_daily_maximum_near_surface_air_temperature[2],
                interpolated_daily_maximum_near_surface_air_temperature[3],
                interpolated_daily_maximum_near_surface_air_temperature[4],
                interpolated_daily_maximum_near_surface_air_temperature[5],
                interpolated_daily_maximum_near_surface_air_temperature[6],
                interpolated_daily_maximum_near_surface_air_temperature[7],
                interpolated_daily_maximum_near_surface_air_temperature[8],
                interpolated_daily_maximum_near_surface_air_temperature[9],
                interpolated_daily_maximum_near_surface_air_temperature[10],
                interpolated_daily_maximum_near_surface_air_temperature[11]));
        sb.append(String.format(Locale.ROOT, "Daily minimum near surface air temperature,tasmin,∆°C,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f%n",
                interpolated_daily_minimum_near_surface_air_temperature[0],
                interpolated_daily_minimum_near_surface_air_temperature[1],
                interpolated_daily_minimum_near_surface_air_temperature[2],
                interpolated_daily_minimum_near_surface_air_temperature[3],
                interpolated_daily_minimum_near_surface_air_temperature[4],
                interpolated_daily_minimum_near_surface_air_temperature[5],
                interpolated_daily_minimum_near_surface_air_temperature[6],
                interpolated_daily_minimum_near_surface_air_temperature[7],
                interpolated_daily_minimum_near_surface_air_temperature[8],
                interpolated_daily_minimum_near_surface_air_temperature[9],
                interpolated_daily_minimum_near_surface_air_temperature[10],
                interpolated_daily_minimum_near_surface_air_temperature[11]));
        sb.append(String.format(Locale.ROOT, "Precipitation,pr,fraction,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f%n",
                interpolated_precipitation[0],
                interpolated_precipitation[1],
                interpolated_precipitation[2],
                interpolated_precipitation[3],
                interpolated_precipitation[4],
                interpolated_precipitation[5],
                interpolated_precipitation[6],
                interpolated_precipitation[7],
                interpolated_precipitation[8],
                interpolated_precipitation[9],
                interpolated_precipitation[10],
                interpolated_precipitation[11]));
        sb.append(String.format(Locale.ROOT, "Surface downwelling shortwave radiation,rsds,∆W/m²,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f%n",
                interpolated_surface_downwelling_shortwave_radiation[0],
                interpolated_surface_downwelling_shortwave_radiation[1],
                interpolated_surface_downwelling_shortwave_radiation[2],
                interpolated_surface_downwelling_shortwave_radiation[3],
                interpolated_surface_downwelling_shortwave_radiation[4],
                interpolated_surface_downwelling_shortwave_radiation[5],
                interpolated_surface_downwelling_shortwave_radiation[6],
                interpolated_surface_downwelling_shortwave_radiation[7],
                interpolated_surface_downwelling_shortwave_radiation[8],
                interpolated_surface_downwelling_shortwave_radiation[9],
                interpolated_surface_downwelling_shortwave_radiation[10],
                interpolated_surface_downwelling_shortwave_radiation[11]));
        sb.append(String.format(Locale.ROOT, "Total cloud cover percentage,clt,∆%%,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f%n",
                interpolated_total_cloud_cover_percentage[0],
                interpolated_total_cloud_cover_percentage[1],
                interpolated_total_cloud_cover_percentage[2],
                interpolated_total_cloud_cover_percentage[3],
                interpolated_total_cloud_cover_percentage[4],
                interpolated_total_cloud_cover_percentage[5],
                interpolated_total_cloud_cover_percentage[6],
                interpolated_total_cloud_cover_percentage[7],
                interpolated_total_cloud_cover_percentage[8],
                interpolated_total_cloud_cover_percentage[9],
                interpolated_total_cloud_cover_percentage[10],
                interpolated_total_cloud_cover_percentage[11]));
        if (interpolated_snow_depth != null) {
            sb.append(String.format(Locale.ROOT, "Snow depth,snd,fraction,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f%n",
                    interpolated_snow_depth[0],
                    interpolated_snow_depth[1],
                    interpolated_snow_depth[2],
                    interpolated_snow_depth[3],
                    interpolated_snow_depth[4],
                    interpolated_snow_depth[5],
                    interpolated_snow_depth[6],
                    interpolated_snow_depth[7],
                    interpolated_snow_depth[8],
                    interpolated_snow_depth[9],
                    interpolated_snow_depth[10],
                    interpolated_snow_depth[11]));
        }
        if (interpolated_near_surface_specific_humidity != null) {
            sb.append(String.format(Locale.ROOT, "Near surface specific humidity,huss,∆kg/kg,%.6f,%.6f,%.6f,%.6f,%.6f,%.6f,%.6f,%.6f,%.6f,%.6f,%.6f,%.6f%n",
                    interpolated_near_surface_specific_humidity[0],
                    interpolated_near_surface_specific_humidity[1],
                    interpolated_near_surface_specific_humidity[2],
                    interpolated_near_surface_specific_humidity[3],
                    interpolated_near_surface_specific_humidity[4],
                    interpolated_near_surface_specific_humidity[5],
                    interpolated_near_surface_specific_humidity[6],
                    interpolated_near_surface_specific_humidity[7],
                    interpolated_near_surface_specific_humidity[8],
                    interpolated_near_surface_specific_humidity[9],
                    interpolated_near_surface_specific_humidity[10],
                    interpolated_near_surface_specific_humidity[11]));
        }
        sb.append(String.format(Locale.ROOT, "Air pressure at mean sea level,psl,∆hPa,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f%n",
                interpolated_air_pressure_at_mean_sea_level[0],
                interpolated_air_pressure_at_mean_sea_level[1],
                interpolated_air_pressure_at_mean_sea_level[2],
                interpolated_air_pressure_at_mean_sea_level[3],
                interpolated_air_pressure_at_mean_sea_level[4],
                interpolated_air_pressure_at_mean_sea_level[5],
                interpolated_air_pressure_at_mean_sea_level[6],
                interpolated_air_pressure_at_mean_sea_level[7],
                interpolated_air_pressure_at_mean_sea_level[8],
                interpolated_air_pressure_at_mean_sea_level[9],
                interpolated_air_pressure_at_mean_sea_level[10],
                interpolated_air_pressure_at_mean_sea_level[11]));
        sb.append(String.format(Locale.ROOT, "Near surface wind speed,sfcWind,fraction,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f%n",
                interpolated_near_surface_wind_speed[0],
                interpolated_near_surface_wind_speed[1],
                interpolated_near_surface_wind_speed[2],
                interpolated_near_surface_wind_speed[3],
                interpolated_near_surface_wind_speed[4],
                interpolated_near_surface_wind_speed[5],
                interpolated_near_surface_wind_speed[6],
                interpolated_near_surface_wind_speed[7],
                interpolated_near_surface_wind_speed[8],
                interpolated_near_surface_wind_speed[9],
                interpolated_near_surface_wind_speed[10],
                interpolated_near_surface_wind_speed[11]));

        try (OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(path_output), StandardCharsets.UTF_8)) {
            writer.write(sb.toString());
            writer.close();
        } catch (FileNotFoundException ex) {
            String err = " ** WARNING ** File not found: %s %n";
            errorsAndWarnings.append(err.formatted(Locale.ROOT, ex));
            System.err.print(err);
        } catch (IOException ex) {
            String err = " ** WARNING ** Cannot write file: %s %n";
            errorsAndWarnings.append(err.formatted(Locale.ROOT, ex));
            System.err.print(err);
        }
    }

}
