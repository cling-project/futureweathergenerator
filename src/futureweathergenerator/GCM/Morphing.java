/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.GCM;

import futureweathergenerator.EPW.EPW;
import futureweathergenerator.functions.A1_Data_Source_And_Uncertainty_Flag;
import futureweathergenerator.functions.Comments;
import futureweathergenerator.functions.Design_Conditions;
import futureweathergenerator.functions.Ground_Temperatures;
import futureweathergenerator.functions.N10_Extraterrestrial_Horizontal_Radiation;
import futureweathergenerator.functions.N11_Extraterrestrial_Direct_Normal_Radiation;
import futureweathergenerator.functions.N12_Horizontal_Infrared_Radiation_Intensity;
import futureweathergenerator.functions.N13_Global_Horizontal_Radiation;
import futureweathergenerator.functions.N14_Direct_Normal_Radiation;
import futureweathergenerator.functions.N15_Diffuse_Horizontal_Radiation;
import futureweathergenerator.functions.N16_Global_Horizontal_Illuminance;
import futureweathergenerator.functions.N17_Direct_Normal_Illuminance;
import futureweathergenerator.functions.N18_Diffuse_Horizontal_Illuminance;
import futureweathergenerator.functions.N19_Zenith_Luminance;
import futureweathergenerator.functions.N1_Year;
import futureweathergenerator.functions.N20_Wind_Direction;
import futureweathergenerator.functions.N21_Wind_Speed;
import futureweathergenerator.functions.N22_Total_Sky_Cover;
import futureweathergenerator.functions.N23_Opaque_Sky_Cover;
import futureweathergenerator.functions.N24_Visibility;
import futureweathergenerator.functions.N25_Ceiling_Height;
import futureweathergenerator.functions.N26_Present_Weather_Observation;
import futureweathergenerator.functions.N27_Present_Weather_Code;
import futureweathergenerator.functions.N28_Precipitable_Water;
import futureweathergenerator.functions.N29_Aerosol_Optical_Depth;
import futureweathergenerator.functions.N30_Snow_Depth;
import futureweathergenerator.functions.N31_Days_Since_Last_Snowfall;
import futureweathergenerator.functions.N32_Albedo;
import futureweathergenerator.functions.N33_Liquid_Precipitation_Depth;
import futureweathergenerator.functions.N34_Liquid_Precipitation_Quantity;
import futureweathergenerator.functions.N6_Dry_Bulb_Temperature;
import futureweathergenerator.functions.N7_Dew_Point_Temperature;
import futureweathergenerator.functions.N8_Relative_Humidity;
import futureweathergenerator.functions.N9_Atmospheric_Pressure;
import futureweathergenerator.functions.Typical_Extreme_Periods;
import java.util.Locale;

/**
 * Morphs the current EPW data.
 * @author eugenio
 */
public class Morphing {

    /**
     * Morphs the current EPW file into the future EPW data.
     *
     * @param model_type_string string of climate model type
     * @param printOutputs String Buffer to store prints
     * @param epw current EPW file
     * @param scenario future scenario to use
     * @param morphedEPW morphed EPW data
     * @param numberOfHoursToSmooth number of hours to smooth month transition
     */
    public static void createMorphedEPWFile(String model_type_string, StringBuffer printOutputs, EPW epw, InterpolatedVariablesScenario scenario, EPW morphedEPW, float numberOfHoursToSmooth) {
        printOutputs.append(String.format(Locale.ROOT, "%n\tGenerating future EPW file...%n"));

        // Non-environment Fields
        printOutputs.append(String.format(Locale.ROOT, "\t\tSetting year, data source and uncertainty flag fields...%n"));
        N1_Year.set(scenario, morphedEPW);
        A1_Data_Source_And_Uncertainty_Flag.update(morphedEPW);

        // Removing design conditions
        printOutputs.append(String.format(Locale.ROOT, "\t\tRemoving design conditions...%n"));
        Design_Conditions.reset(morphedEPW);

        // Add comments about the generated data
        printOutputs.append(String.format(Locale.ROOT, "\t\tAdding comments...%n"));
        Comments.update(model_type_string, scenario, morphedEPW);

        // Environment Fields
        // WARNING: The order in which the Fields are morphed must be kept! //
        printOutputs.append(String.format(Locale.ROOT, "\t\tMorphing environment fields...%n"));

        // Independent Future Variables
        N6_Dry_Bulb_Temperature.morph(epw, scenario, morphedEPW, numberOfHoursToSmooth);
        N9_Atmospheric_Pressure.morph(epw, scenario, morphedEPW, numberOfHoursToSmooth);
        N10_Extraterrestrial_Horizontal_Radiation.calculate(morphedEPW);
        N11_Extraterrestrial_Direct_Normal_Radiation.calculate(morphedEPW);
        N13_Global_Horizontal_Radiation.morph(epw, scenario, morphedEPW, numberOfHoursToSmooth);
        N21_Wind_Speed.morph(epw, scenario, morphedEPW, numberOfHoursToSmooth);
        N22_Total_Sky_Cover.morph(epw, scenario, morphedEPW, numberOfHoursToSmooth);
        N30_Snow_Depth.morph(epw, scenario, morphedEPW, numberOfHoursToSmooth);
        N33_Liquid_Precipitation_Depth.morph(epw, scenario, morphedEPW, numberOfHoursToSmooth);
        // Fields that are kept the same and not morphed:
        N20_Wind_Direction.keepOriginal(epw, morphedEPW);
        N26_Present_Weather_Observation.keepOriginal(epw, morphedEPW);
        N27_Present_Weather_Code.keepOriginal(epw, morphedEPW);
        N34_Liquid_Precipitation_Quantity.keepOriginal(epw, morphedEPW);
        // Fields to be set as missing data:
        N24_Visibility.setMissing(morphedEPW);
        N25_Ceiling_Height.setMissing(morphedEPW);
        N28_Precipitable_Water.setMissing(morphedEPW);
        N29_Aerosol_Optical_Depth.setMissing(morphedEPW);
        N31_Days_Since_Last_Snowfall.setMissing(morphedEPW);
        N32_Albedo.setMissing(morphedEPW);

        // Dependent Future Variables
        N8_Relative_Humidity.morph(epw, scenario, morphedEPW, numberOfHoursToSmooth);
        N7_Dew_Point_Temperature.morph(epw, scenario, morphedEPW, numberOfHoursToSmooth);
        N12_Horizontal_Infrared_Radiation_Intensity.calculate(morphedEPW);
        N15_Diffuse_Horizontal_Radiation.calculate(morphedEPW);
        N14_Direct_Normal_Radiation.calculate(morphedEPW);
        // NOTICE:
        // CCWorldWeatherGen outputs different values for the 
        // illuminances and luminance (N16 to N19 fields).
        N16_Global_Horizontal_Illuminance.calculate(morphedEPW);
        N17_Direct_Normal_Illuminance.calculate(morphedEPW);
        N18_Diffuse_Horizontal_Illuminance.calculate(morphedEPW);
        N19_Zenith_Luminance.calculate(morphedEPW);
        N23_Opaque_Sky_Cover.calculate(epw, morphedEPW);

        // Ground Temperatures at different depths
        printOutputs.append(String.format(Locale.ROOT, "\t\tDetermining ground temperatures at different depths...%n"));
        Ground_Temperatures.morph(morphedEPW);
        
        // Typical/extreme periods
        printOutputs.append(String.format(Locale.ROOT, "\t\tDetermining typical/extreme periods...%n"));
        Typical_Extreme_Periods.calculate(morphedEPW);
    }

}
