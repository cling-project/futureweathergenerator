/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.GCM;

import futureweathergenerator.Months;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.CountDownLatch;

/**
 * Creates a thread to read climate model file.
 *
 * @author eugenio
 */
public class ReadFileThread implements Runnable {

    private final int i;
    private final String model_type_string;
    private final Variable[][] variables;
    private final ArrayList<Settings> variableSettings;
    private final CountDownLatch dS;

    /**
     * Initiates a thread for the climate model file reading.
     *
     * @param model_type_string model type string
     * @param i current month
     * @param variables array of variables_by_month
     * @param variableSettings variable settings
     * @param dS thread count down latch
     */
    public ReadFileThread(String model_type_string, int i, Variable[][] variables, ArrayList<Settings> variableSettings, CountDownLatch dS) {
        this.i = i;
        this.model_type_string = model_type_string;
        this.variables = variables;
        this.variableSettings = variableSettings;
        this.dS = dS;
    }

    /**
     * Runs this thread.
     */
    @Override
    public void run() {
        variables[i] = getVariable(
                variableSettings.get(i).getPath(),
                variableSettings.get(i).getFour_nearest_points(),
                variableSettings.get(i).getScenario(),
                variableSettings.get(i).getVariable(),
                variableSettings.get(i).getTimeframe()
        );
        this.dS.countDown();
    }

    private Variable[] getVariable(String PATH_TO_OUTPUT_FOLDER_MODEL, ArrayList<GridPoint> four_nearest_points, String scenario, String variable_abbreviation, String timeframe) {
        String filename = model_type_string + "-" + scenario + "-" + timeframe + "-" + variable_abbreviation+ ".csv";
        System.out.println("\tVariable File: " + filename);
        
        File file = new File(PATH_TO_OUTPUT_FOLDER_MODEL + filename);
        Scanner reader;
        try {
            reader = new Scanner(file);
            
            int pt1_row_id = four_nearest_points.get(0).getId();
            int pt2_row_id = four_nearest_points.get(1).getId();
            int pt3_row_id = four_nearest_points.get(2).getId();
            int pt4_row_id = four_nearest_points.get(3).getId();

            float[][] collectedMonthValues = new float[12][4];
            int id = 0;
            boolean breakWhile = false;
            int countPoints = 0;
            while (reader.hasNextLine() && !breakWhile) {
                String line = reader.nextLine();
                
                if(pt1_row_id == id || pt2_row_id == id || pt3_row_id == id || pt4_row_id == id) {
                    String[] columns_values_strings = line.split(",");
                    float jan = columns_values_strings[0].contains("Inf") ? 1 : Float.parseFloat(columns_values_strings[0]);
                    float feb = columns_values_strings[1].contains("Inf") ? 1 : Float.parseFloat(columns_values_strings[1]);
                    float mar = columns_values_strings[2].contains("Inf") ? 1 : Float.parseFloat(columns_values_strings[2]);
                    float apr = columns_values_strings[3].contains("Inf") ? 1 : Float.parseFloat(columns_values_strings[3]);
                    float may = columns_values_strings[4].contains("Inf") ? 1 : Float.parseFloat(columns_values_strings[4]);
                    float jun = columns_values_strings[5].contains("Inf") ? 1 : Float.parseFloat(columns_values_strings[5]);
                    float jul = columns_values_strings[6].contains("Inf") ? 1 : Float.parseFloat(columns_values_strings[6]);
                    float aug = columns_values_strings[7].contains("Inf") ? 1 : Float.parseFloat(columns_values_strings[7]);
                    float sep = columns_values_strings[8].contains("Inf") ? 1 : Float.parseFloat(columns_values_strings[8]);
                    float oct = columns_values_strings[9].contains("Inf") ? 1 : Float.parseFloat(columns_values_strings[9]);
                    float nov = columns_values_strings[10].contains("Inf") ? 1 : Float.parseFloat(columns_values_strings[10]);
                    float dec = columns_values_strings[11].contains("Inf") ? 1 : Float.parseFloat(columns_values_strings[11]);

                    int pt = -1;
                    if(pt1_row_id == id) {
                        pt = 0;
                    }
                    if(pt2_row_id == id) {
                        pt = 1;
                    }
                    if(pt3_row_id == id) {
                        pt = 2;
                    }
                    if(pt4_row_id == id) {
                        pt = 3;
                    }

                    collectedMonthValues[0][pt] = jan;
                    collectedMonthValues[1][pt] = feb;
                    collectedMonthValues[2][pt] = mar;
                    collectedMonthValues[3][pt] = apr;
                    collectedMonthValues[4][pt] = may;
                    collectedMonthValues[5][pt] = jun;
                    collectedMonthValues[6][pt] = jul;
                    collectedMonthValues[7][pt] = aug;
                    collectedMonthValues[8][pt] = sep;
                    collectedMonthValues[9][pt] = oct;
                    collectedMonthValues[10][pt] = nov;
                    collectedMonthValues[11][pt] = dec;
                    countPoints++;
                }
                id++;
                if(countPoints == 4) {
                    breakWhile = true;
                }
            }
                       
            if(countPoints == 4) {
                Variable[] variables_by_month = new Variable[12];
                for (int vi = 0; vi < Months.Abbreviation.values().length; vi++) {
                    Months.Abbreviation month = Months.Abbreviation.values()[vi];
                    variables_by_month[vi] = new Variable(
                            month,
                            scenario,
                            timeframe,
                            collectedMonthValues[vi]);
                }
                reader.close();
                return variables_by_month;
            } else {
                throw new UnsupportedOperationException(" ERROR: Monthly changes were not collected: " + filename);
            }
        } catch (FileNotFoundException ex) {
            throw new UnsupportedOperationException(" ** ERROR ** Unknown grid file name: " + filename);
        }
        
    }
    
}
