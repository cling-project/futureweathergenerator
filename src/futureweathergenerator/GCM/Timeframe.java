/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.GCM;

import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Creates a timeframe with the four SSP scenarios.
 *
 * @author eugenio
 */
public class Timeframe {

    private final Scenario ssp126;
    private final Scenario ssp245;
    private final Scenario ssp370;
    private final Scenario ssp585;

    /**
     * Initiates the timeframe.
     *
     * @param model_type_string climate model type string
     * @param NUMBER_OF_CPU_THREADS number of threads to read data
     * @param GRID_POINT_OPTION grid nearest points weighted distances option
     * @param PATH_TO_OUTPUT_FOLDER_MODEL path to output folder of the climate model
     * @param timeframe timeframe name
     * @param latitude latitude of the EPW
     * @param longitude longitude of the EPW
     * @param four_nearest_points list of grid points
     */
    public Timeframe(String model_type_string, int NUMBER_OF_CPU_THREADS, int GRID_POINT_OPTION, String PATH_TO_OUTPUT_FOLDER_MODEL, String timeframe, float longitude, float latitude, ArrayList<GridPoint> four_nearest_points) {
        this.ssp126 = getTimeframeVariables(model_type_string, NUMBER_OF_CPU_THREADS, GRID_POINT_OPTION, PATH_TO_OUTPUT_FOLDER_MODEL, longitude, latitude, four_nearest_points, timeframe, "ssp126");
        this.ssp245 = getTimeframeVariables(model_type_string, NUMBER_OF_CPU_THREADS, GRID_POINT_OPTION, PATH_TO_OUTPUT_FOLDER_MODEL, longitude, latitude, four_nearest_points, timeframe, "ssp245");
        this.ssp370 = getTimeframeVariables(model_type_string, NUMBER_OF_CPU_THREADS, GRID_POINT_OPTION, PATH_TO_OUTPUT_FOLDER_MODEL, longitude, latitude, four_nearest_points, timeframe, "ssp370");
        this.ssp585 = getTimeframeVariables(model_type_string, NUMBER_OF_CPU_THREADS, GRID_POINT_OPTION, PATH_TO_OUTPUT_FOLDER_MODEL, longitude, latitude, four_nearest_points, timeframe, "ssp585");
    }

    private futureweathergenerator.GCM.Scenario getTimeframeVariables(
            String model_type_string,
            int NUMBER_OF_CPU_THREADS, int GRID_POINT_OPTION, String PATH_TO_OUTPUT_FOLDER_MODEL, float longitude, float latitude, ArrayList<GridPoint> four_nearest_points, String timeframe, String scenario) {
        ArrayList<futureweathergenerator.GCM.Settings> variableSettings = new ArrayList<>();
        variableSettings.add(new futureweathergenerator.GCM.Settings(PATH_TO_OUTPUT_FOLDER_MODEL, four_nearest_points, scenario, timeframe, "tas"));
        variableSettings.add(new futureweathergenerator.GCM.Settings(PATH_TO_OUTPUT_FOLDER_MODEL, four_nearest_points, scenario, timeframe, "tasmax"));
        variableSettings.add(new futureweathergenerator.GCM.Settings(PATH_TO_OUTPUT_FOLDER_MODEL, four_nearest_points, scenario, timeframe, "tasmin"));
        variableSettings.add(new futureweathergenerator.GCM.Settings(PATH_TO_OUTPUT_FOLDER_MODEL, four_nearest_points, scenario, timeframe, "pr"));
        variableSettings.add(new futureweathergenerator.GCM.Settings(PATH_TO_OUTPUT_FOLDER_MODEL, four_nearest_points, scenario, timeframe, "rsds"));
        variableSettings.add(new futureweathergenerator.GCM.Settings(PATH_TO_OUTPUT_FOLDER_MODEL, four_nearest_points, scenario, timeframe, "clt"));
        variableSettings.add(new futureweathergenerator.GCM.Settings(PATH_TO_OUTPUT_FOLDER_MODEL, four_nearest_points, scenario, timeframe, "snd"));
        variableSettings.add(new futureweathergenerator.GCM.Settings(PATH_TO_OUTPUT_FOLDER_MODEL, four_nearest_points, scenario, timeframe, "huss"));
        variableSettings.add(new futureweathergenerator.GCM.Settings(PATH_TO_OUTPUT_FOLDER_MODEL, four_nearest_points, scenario, timeframe, "psl"));
        variableSettings.add(new futureweathergenerator.GCM.Settings(PATH_TO_OUTPUT_FOLDER_MODEL, four_nearest_points, scenario, timeframe, "sfcWind"));

        Variable[][] variables = threadsReadFiles(model_type_string, NUMBER_OF_CPU_THREADS, variableSettings);
        return new futureweathergenerator.GCM.Scenario(
                GRID_POINT_OPTION,
                timeframe,
                scenario,
                latitude,
                longitude,
                four_nearest_points,
                variables[0],
                variables[1],
                variables[2],
                variables[3],
                variables[4],
                variables[5],
                variables[6],
                variables[7],
                variables[8],
                variables[9]
        );
    }

    private static Variable[][] threadsReadFiles(String model_type_string, int NUMBER_OF_CPU_THREADS, ArrayList<futureweathergenerator.GCM.Settings> variableSettings) {
        Variable[][] variables = new Variable[variableSettings.size()][];
        ExecutorService pool = Executors.newFixedThreadPool(NUMBER_OF_CPU_THREADS);
        CountDownLatch dS = new CountDownLatch(variableSettings.size());
        for (int i = 0; i < variableSettings.size(); i++) {
            pool.execute(new futureweathergenerator.GCM.ReadFileThread(model_type_string, i, variables, variableSettings, dS));
        }
        try {
            dS.await();
        } catch (InterruptedException ex) {
            System.out.println(ex);
        }
        pool.shutdown();
        return variables;
    }

    /**
     * Returns the SSP1-2.6 scenario.
     *
     * @return scenario
     */
    public Scenario getSsp126() {
        return ssp126;
    }

    /**
     * Returns the SSP2-4.5 scenario.
     *
     * @return scenario
     */
    public Scenario getSsp245() {
        return ssp245;
    }

    /**
     * Returns the SSP3-7.0 scenario.
     *
     * @return scenario
     */
    public Scenario getSsp370() {
        return ssp370;
    }

    /**
     * Returns the SSP5-8.5 scenario.
     *
     * @return scenario
     */
    public Scenario getSsp585() {
        return ssp585;
    }

}
