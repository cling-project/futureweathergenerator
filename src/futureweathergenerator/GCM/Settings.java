/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.GCM;

import java.util.ArrayList;

/**
 * Initiates the variable settings.
 *
 * @author eugenio
 */
public class Settings {

    private final String path;
    private final ArrayList<GridPoint> four_nearest_points;
    private final String scenario;
    private final String variable;
    private final String timeframe;

    /**
     * Initiates the variable settings.
     *
     * @param PATH_TO_OUTPUT_FOLDER_MODEL path to the variable
     * @param four_nearest_points list of the four nearest points (location
     * only)
     * @param scenario scenario name
     * @param variable variable name
     * @param timeframe timeframe name
     */
    public Settings(String PATH_TO_OUTPUT_FOLDER_MODEL, ArrayList<GridPoint> four_nearest_points, String scenario, String timeframe, String variable) {
        this.path = PATH_TO_OUTPUT_FOLDER_MODEL;
        this.four_nearest_points = four_nearest_points;
        this.scenario = scenario;
        this.variable = variable;
        this.timeframe = timeframe;
    }

    /**
     * Returns path to variable resource.
     *
     * @return path
     */
    public String getPath() {
        return path;
    }

    /**
     * List of the four nearest points.
     *
     * @return list of points
     */
    public ArrayList<GridPoint> getFour_nearest_points() {
        return four_nearest_points;
    }

    /**
     * Returns scenario name.
     *
     * @return scenario name
     */
    public String getScenario() {
        return scenario;
    }

    /**
     * Returns variable name.
     *
     * @return variable name
     */
    public String getVariable() {
        return variable;
    }

    /**
     * Returns timeframe name.
     *
     * @return timeframe name
     */
    public String getTimeframe() {
        return timeframe;
    }

}
