/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.GCM;

import futureweathergenerator.Months;

/**
 * Sets a variable data.
 *
 * @author eugenio
 */
public class Variable {

    private Months.Abbreviation month;
    private String scenario;
    private String timeframe;
    private float[] four_nearest_points_var_values;

    /**
     * Initiates a variable object.
     *
     * @param month month abbreviation
     * @param scenario scenario name
     * @param timeframe timeframe name
     * @param four_nearest_points_var_values values in the grid
     */
    public Variable(Months.Abbreviation month, String scenario, String timeframe, float[] four_nearest_points_var_values) {
        this.month = month;
        this.scenario = scenario;
        this.timeframe = timeframe;
        this.four_nearest_points_var_values = four_nearest_points_var_values;
    }

    /**
     * Returns the abbreviation of the month.
     *
     * @return month abbreviation
     */
    public Months.Abbreviation getMonth() {
        return month;
    }

    /**
     * Returns scenario name.
     *
     * @return string
     */
    public String getScenario() {
        return scenario;
    }

    /**
     * Returns timeframe name.
     *
     * @return string
     */
    public String getTimeframe() {
        return timeframe;
    }

    /**
     * Returns variable values in the world grid.
     *
     * @return grid values
     */
    public float[] getFour_nearest_points_var_values() {
        return four_nearest_points_var_values;
    }

    /**
     * Sets month abbreviation.
     *
     * @param month month abbreviation
     */
    public void setMonth(Months.Abbreviation month) {
        this.month = month;
    }

    /**
     * Sets the scenario name.
     *
     * @param scenario string
     */
    public void setScenario(String scenario) {
        this.scenario = scenario;
    }

    /**
     * Sets the timeframe name.
     *
     * @param timeframe string
     */
    public void setTimeframe(String timeframe) {
        this.timeframe = timeframe;
    }

    /**
     * Sets the variable values in the world grid.
     *
     * @param four_nearest_points_var_values grid values
     */
    public void setFour_nearest_points_var_values(float[] four_nearest_points_var_values) {
        this.four_nearest_points_var_values = four_nearest_points_var_values;
    }

}
