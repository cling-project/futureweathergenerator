/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.GCM;

import futureweathergenerator.EPW.EPW;
import futureweathergenerator.FutureWeatherGenerator;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.Locale;
import java.util.concurrent.CountDownLatch;

/**
 * Initiates a timeframe thread.
 * 
 * @author eugenio
 */
public class TimeframeThread implements Runnable {

    private final boolean ENSEMBLE;
    private final String PATH_OUTPUT_FOLDER;
    private final boolean EPW_VARIABLE_LIMITS;
    private final String timeframe_trigger;
    private final String scenario_trigger;
    private final ClimateModel model;
    private final float numberOfHoursToSmooth;
    private final CountDownLatch dS;

    /**
     * Initializes the timeframe thread.
     *
     * @param ENSEMBLE is an ensemble of climate models
     * @param PATH_OUTPUT_FOLDER path to the output folder
     * @param EPW_VARIABLE_LIMITS apply the EPW variable limits
     * @param timeframe_trigger timeframe name
     * @param scenario_trigger scenario name
     * @param model climate model
     * @param numberOfHoursToSmooth number of hours to smooth month transition
     * @param dS thread count down latch
     */
    public TimeframeThread(boolean ENSEMBLE, String PATH_OUTPUT_FOLDER, boolean EPW_VARIABLE_LIMITS, String timeframe_trigger, String scenario_trigger, ClimateModel model, float numberOfHoursToSmooth, CountDownLatch dS) {
        this.ENSEMBLE = ENSEMBLE;
        this.PATH_OUTPUT_FOLDER = PATH_OUTPUT_FOLDER;
        this.EPW_VARIABLE_LIMITS = EPW_VARIABLE_LIMITS;
        this.timeframe_trigger = timeframe_trigger;
        this.scenario_trigger = scenario_trigger;
        this.model = model;
        this.numberOfHoursToSmooth = numberOfHoursToSmooth;
        this.dS = dS;
    }

    /**
     * Runs this thread.
     */
    @Override
    public void run() {
        StringBuffer errorsAndWarnings = new StringBuffer();
        StringBuffer printOutputs = new StringBuffer();
        printOutputs.append(String.format(Locale.ROOT, "%nGenerating ")).append(timeframe_trigger).append(String.format(Locale.ROOT, " timeframe...%n"));

        errorsAndWarnings.append(this.model.getEpw().getAdjustmentWanings());
        errorsAndWarnings.append(String.format(Locale.ROOT, "%n"));
        EPW morphedEPW = new EPW(errorsAndWarnings, 
                this.model.getEpw().getPath_EPW(), 
                this.model.getEpw().getSolarHourAdjustmentOption().ordinal(),
                this.model.getEpw().getDiffuseIrradiationModel().ordinal(),
                EPW_VARIABLE_LIMITS, false);
        morphedEPW.solarHourAdjustment = this.model.getEpw().solarHourAdjustment;
        morphedEPW.setUHIInfo(this.model.getEpw().getEpw_original_lcz(), this.model.getEpw().getTarget_uhi_lcz());

        // Selects the chosen timeframe
        Timeframe timeframe;
        switch (timeframe_trigger) {
            case "2050" ->
                timeframe = model.getTimeframe_2050();
            case "2080" ->
                timeframe = model.getTimeframe_2080();
            default ->
                throw new UnsupportedOperationException("Error: Unknown Timeframe: " + timeframe_trigger);
        }

        Scenario ssp;
        switch (scenario_trigger) {
            case "ssp126" ->
                ssp = timeframe.getSsp126();
            case "ssp245" ->
                ssp = timeframe.getSsp245();
            case "ssp370" ->
                ssp = timeframe.getSsp370();
            case "ssp585" ->
                ssp = timeframe.getSsp585();
            default ->
                throw new UnsupportedOperationException("Error: Unknown Scenario: " + scenario_trigger);
        }

        // Loads the SSPs
        printOutputs.append(String.format(Locale.ROOT, "\tLoading Shared Socioeconomic Pathway ")).append(scenario_trigger).append(String.format(Locale.ROOT, "...%n"));
        InterpolatedVariablesScenario scenario = new InterpolatedVariablesScenario(ssp);

        String path = PATH_OUTPUT_FOLDER
                + this.model.getEpw().getEpw_location().getA3_country().replace("/", "-").replace("\\", "-")
                + "_"
                + this.model.getEpw().getEpw_location().getA2_region().replace("/", "-").replace("\\", "-")
                + "_"
                + this.model.getEpw().getEpw_location().getA1_city().replace("/", "-").replace("\\", "-")
                + "_";
        
        // Saves model interpolated variables
        scenario.saveTable_GCM_variables(this.model.getModel_type_string(), printOutputs, errorsAndWarnings,
                path
                + this.model.getModel_type_string()
                + "_"
                + scenario_trigger
                + "_"
                + timeframe_trigger
                + "_"
                + FutureWeatherGenerator.OUTPUT_FILE_VARIABLES
                + ".csv");

        if(!ENSEMBLE) {
            // Generates future weather
            String models_string = !ENSEMBLE ? this.model.getModel_type_string() + " (" + this.model.getModels_string() + ")" : this.model.getModel_type_string();
            Morphing.createMorphedEPWFile(models_string, printOutputs, this.model.getEpw(), scenario, morphedEPW, numberOfHoursToSmooth);

            // Saves comparison table between original and morphed file
            morphedEPW.saveEPWComparison(printOutputs, errorsAndWarnings,
                    path
                    + this.model.getModel_type_string()
                    + morphedEPW.getFile_suffix()
                    + "_"
                    + scenario_trigger
                    + "_"
                    + timeframe_trigger
                    + "_"
                    + FutureWeatherGenerator.OUTPUT_FILE_COMPARISON    
                    + ".csv",
                    this.model.getEpw());

            // Saves morphed weather to file
            morphedEPW.saveEPW(printOutputs, errorsAndWarnings,
                    path
                    + this.model.getModel_type_string()
                    + morphedEPW.getFile_suffix()
                    + "_"
                    + scenario_trigger
                    + "_"
                    + timeframe_trigger
                    + ".epw");

            // Saves morphed STAT data
            morphedEPW.saveSTATFile(printOutputs, errorsAndWarnings,
                    path
                    + this.model.getModel_type_string()
                    + morphedEPW.getFile_suffix()
                    + "_"
                    + scenario_trigger
                    + "_"
                    + timeframe_trigger
                    + ".stat");

            // Saves errors and warnings
            saveErrorAndWarnings(printOutputs, errorsAndWarnings,
                    path
                    + this.model.getModel_type_string()
                    + morphedEPW.getFile_suffix()
                    + "_"
                    + scenario_trigger
                    + "_"
                    + timeframe_trigger
                    + "_"
                    + FutureWeatherGenerator.OUTPUT_FILE_ERRORS
                    + ".log" );

            System.out.println(printOutputs.toString());
        }
        
        this.dS.countDown();
    }
    
    /**
     * Saves to file the errors and warnings of the process.
     *
     * @param printOutputs String buffer of the prints outputs
     * @param errorsAndWarnings String buffer of the errors and warnings
     * @param path_output path to the err file
     */
    public static void saveErrorAndWarnings(StringBuffer printOutputs, StringBuffer errorsAndWarnings, String path_output) {
        printOutputs.append(String.format(Locale.ROOT, "%n\tSaving warnings and errors...%n\t")).append(path_output).append(String.format(Locale.ROOT, "%n"));
        try (OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(path_output), StandardCharsets.UTF_8)) {
            String errorsAndWarningsHeader = """
                                             #############################################################
                                             #################### ERRORS AND WARNINGS ####################
                                             #############################################################
                                             
                                             """;
            writer.write(errorsAndWarningsHeader + errorsAndWarnings.toString());
            writer.close();
        } catch (FileNotFoundException ex) {
            String err = " ** ERROR ** File not found: %s %n";
            errorsAndWarnings.append(err.formatted(Locale.ROOT, ex));
            System.err.print(err);
        } catch (IOException ex) {
            String err = " ** ERROR ** Cannot access the file: %s %n";
            errorsAndWarnings.append(err.formatted(Locale.ROOT, ex));
            System.err.print(err);
        }
    }
}
