/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.GCM;

/**
 * Sets the grid point object.
 *
 * @author eugenio
 */
public class GridPoint {

    private final Integer id;
    private final Integer x;
    private final Integer y;
    private final float longitude;
    private final float latitude;
    
    /**
     * Initiates a grid point.
     *
     * @param id row id
     * @param x x point id
     * @param y y point id
     * @param latitude latitude
     * @param longitude longitude
     */
    public GridPoint(Integer id, Integer x, Integer y, float longitude, float latitude) {
        this.id = id;
        this.x = x;
        this.y = y;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    /**
     * Returns the row ID.
     * @return row id
     */
    public Integer getId() {
        return id;
    }
    
    /**
     * Returns the point grid x id.
     *
     * @return point grid x id
     */
    public Integer getX() {
        return x;
    }

    /**
     * Returns the point grid y id.
     *
     * @return point grid y id
     */
    public Integer getY() {
        return y;
    }

    /**
     * Returns point longitude
     *
     * @return longitude
     */
    public float getLongitude() {
        return longitude;
    }
    
    /**
     * Returns point latitude.
     *
     * @return latitude
     */
    public float getLatitude() {
        return latitude;
    }

}
