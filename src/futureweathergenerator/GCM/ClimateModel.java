/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.GCM;

import futureweathergenerator.EPW.EPW;
import static futureweathergenerator.File.deleteDirectory;
import static futureweathergenerator.File.readFile;
import static futureweathergenerator.File.unzip;
import futureweathergenerator.FutureWeatherGenerator;
import futureweathergenerator.GCModel;
import java.io.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Generates the future EPW file based on a climate model.
 *
 * @author eugenio
 */
public class ClimateModel {

    private final GCModel model_type;
    private String model_type_string;
    private final String models_string;

    private final ArrayList<GridPoint> four_nearest_points = new ArrayList<>();

    private final EPW epw;

    private final String PATH_OUTPUT_FOLDER;
    private final String PATH_TO_OUTPUT_FOLDER_MODEL;
    private final String PATH_TO_MODEL_RESOURCES;

    private Timeframe timeframe_2050; // 2036-2065
    private Timeframe timeframe_2080; // 2066-2095

    private int NUMBER_OF_CPU_THREADS;

    /**
     * Initiates the climate model.
     *
     * @param model_type_string climate model string
     * @param models_string concatenated models string
     * @param epw EPW file
     * @param PATH_OUTPUT_FOLDER path to output folder
     */
    public ClimateModel(String model_type_string, String models_string, EPW epw, String PATH_OUTPUT_FOLDER) {
        // Climate model_type
        this.model_type = GCModel.valueOf(model_type_string);
        this.model_type_string = model_type_string.replace("_", "-");
        this.models_string = models_string.replace("_", "-").replace(",", ", ");
        this.PATH_OUTPUT_FOLDER = PATH_OUTPUT_FOLDER;
        this.PATH_TO_OUTPUT_FOLDER_MODEL = PATH_OUTPUT_FOLDER + model_type.toString().replace("_", "-");
        this.PATH_TO_MODEL_RESOURCES = FutureWeatherGenerator.PATH_TO_RESOURCES + FutureWeatherGenerator.CMIP + "/" + model_type.toString().replace("_", "-") + ".zip";

        // EPW
        this.epw = epw;

        // Unzip data
        try {
            // Unzip climate model
            unzip(FutureWeatherGenerator.class.getResourceAsStream(this.PATH_TO_MODEL_RESOURCES), new File(this.PATH_OUTPUT_FOLDER));
        } catch (IOException ex) {
            throw new UnsupportedOperationException();
        }
        
    }

    /**
     * Initiates the climate model.
     *
     * @param NUMBER_OF_CPU_THREADS number of threads
     * @param GRID_POINT_OPTION grid four nearest points weighted distance
     * option
     */
    public void readModelData(int NUMBER_OF_CPU_THREADS, int GRID_POINT_OPTION) {
        // Sets the number of cpu threads
        this.NUMBER_OF_CPU_THREADS = NUMBER_OF_CPU_THREADS;

        // 2036-2065 Mean monthly change fields -> 2050
        System.out.println(String.format(Locale.ROOT, "%nLoading " + this.model_type_string + " data for timeframe 2050"));
        this.timeframe_2050 = new Timeframe(this.model_type_string, NUMBER_OF_CPU_THREADS, GRID_POINT_OPTION, PATH_OUTPUT_FOLDER + model_type_string + "/", "2050", this.epw.getEpw_location().getN3_longitude(), this.epw.getEpw_location().getN2_latitude(), four_nearest_points);

        // 2066-2095 Mean monthly change fields -> 2080
        System.out.println(String.format(Locale.ROOT, "%nLoading " + this.model_type_string + " data for timeframe 2080"));
        this.timeframe_2080 = new Timeframe(this.model_type_string, NUMBER_OF_CPU_THREADS, GRID_POINT_OPTION, PATH_OUTPUT_FOLDER + model_type_string + "/", "2080", this.epw.getEpw_location().getN3_longitude(), this.epw.getEpw_location().getN2_latitude(), four_nearest_points);
    }

    public void readModelGrid() {
        System.out.println(String.format(Locale.ROOT, "%nLoading grid..."));
        // Create Grid
        if (four_nearest_points.isEmpty()) {

            // File names
            String filename_lon = PATH_TO_OUTPUT_FOLDER_MODEL + "/" + this.model_type_string + "-grid-lon.csv";
            String filename_lat = PATH_TO_OUTPUT_FOLDER_MODEL + "/" + this.model_type_string + "-grid-lat.csv";

            // Read and add to arrays
            float[] LONGITUDES = readFile(filename_lon);
            float[] LATITUDES = readFile(filename_lat);

            GridPoint[][] gridPoints = new GridPoint[LONGITUDES.length][LATITUDES.length];
            int id = 0;
            for (int i = 0; i < LONGITUDES.length; i++) {
                for (int j = 0; j < LATITUDES.length; j++) {
                    gridPoints[i][j] = new GridPoint(id++, i + 1, j + 1, Grid.correctLongitude(LONGITUDES[i]), LATITUDES[j]);
                }
            }
            Grid grid = new Grid(gridPoints);
            four_nearest_points.addAll(grid.selectNearestFourGridPoints(this.epw.getEpw_location().getN3_longitude(), this.epw.getEpw_location().getN2_latitude()));
            System.out.println();
        }
    }

    /**
     * Generates the future morphed EPW file based on a climate model.
     *
     * @param ENSEMBLE is an ensemble of models
     * @param EPW_VARIABLE_LIMITS apply the EPW variable limits
     * @param NUMBER_OF_HOURS_TO_SMOOTH number of hours to smooth the month
     * transition
     */
    public void generate(boolean ENSEMBLE, boolean EPW_VARIABLE_LIMITS, float NUMBER_OF_HOURS_TO_SMOOTH) {
        System.out.println(String.format(Locale.ROOT, "%nInitializing climate model..."));
        String[] timeframe_triggers = new String[]{"2050", "2080"};
        String[] scenario_triggers = new String[]{"ssp126", "ssp245", "ssp370", "ssp585"};
        ExecutorService pool = Executors.newFixedThreadPool(this.NUMBER_OF_CPU_THREADS);
        CountDownLatch dS = new CountDownLatch(timeframe_triggers.length * scenario_triggers.length);
        for (String timeframe_trigger : timeframe_triggers) {
            for (String scenario_trigger : scenario_triggers) {
                pool.execute(new TimeframeThread(ENSEMBLE, PATH_OUTPUT_FOLDER, EPW_VARIABLE_LIMITS, timeframe_trigger, scenario_trigger, this, NUMBER_OF_HOURS_TO_SMOOTH, dS));
            }
        }
        try {
            dS.await();
        } catch (InterruptedException ex) {
            throw new UnsupportedOperationException(ex);
        }
        pool.shutdown();
        
        // Clean folder
        deleteDirectory(new File(PATH_TO_OUTPUT_FOLDER_MODEL));
        deleteDirectory(new File(PATH_OUTPUT_FOLDER + "/" + "__MACOSX"));
    }
    
    /**
     * Returns the timeframe 2050.
     *
     * @return timeframe
     */
    public Timeframe getTimeframe_2050() {
        return timeframe_2050;
    }

    /**
     * Returns the timeframe 2080.
     *
     * @return timeframe
     */
    public Timeframe getTimeframe_2080() {
        return timeframe_2080;
    }

    /**
     * Returns climate model used.
     *
     * @return climate model
     */
    public GCModel getModel_type() {
        return model_type;
    }

    /**
     * Returns climate model used as a string.
     *
     * @return climate model type as a string
     */
    public String getModel_type_string() {
        return model_type_string;
    }

    /**
     * Returns a list with the four nearest points to the EPW location.
     *
     * @return list of four points
     */
    public ArrayList<GridPoint> getFour_nearest_points() {
        return four_nearest_points;
    }

    /**
     * Sets the string for the model type.
     *
     * @param model_type_string string for model type
     */
    public void setModel_type_string(String model_type_string) {
        this.model_type_string = model_type_string;
    }

    /**
     * Returns concatenated models string for ensemble
     *
     * @return concatenated string
     */
    public String getModels_string() {
        return models_string;
    }

    /**
     * Current EPW file.
     *
     * @return EPW file
     */
    public EPW getEpw() {
        return epw;
    }

}
