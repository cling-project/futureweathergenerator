/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.GCM;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Sets the world grid of points by latitude and longitude.
 *
 * @author eugenio
 */
public class Grid {

    private final GridPoint[][] grid;
    private final int number_of_x_points;
    private final int number_of_y_points;

    /**
     * Initiates the grid object.
     *
     * @param grid array of grid points
     */
    public Grid(GridPoint[][] grid) {
        this.grid = grid;
        this.number_of_x_points = grid.length;
        this.number_of_y_points = grid[0].length;
    }

    /**
     * Returns the grid as a 2-dimensional array.
     *
     * @return 2D array
     */
    public GridPoint[][] getGrid() {
        return grid;
    }

    /**
     * Returns the number of points in the x direction.
     *
     * @return number of points
     */
    public int getNumber_of_x_points() {
        return number_of_x_points;
    }

    /**
     * Returns the number of points in the y direction.
     *
     * @return number of points
     */
    public int getNumber_of_y_points() {
        return number_of_y_points;
    }

    /**
     * Selects the nearest four points to a given latitude and longitude.
     *
     * @param longitude longitude
     * @param latitude latitude
     * @return list of the four nearest points
     */
    public ArrayList<GridPoint> selectNearestFourGridPoints(float longitude, float latitude) {
        System.out.println(String.format(Locale.ROOT, "%nGrid size: " + this.number_of_x_points + " x " + this.number_of_y_points));
        System.out.println("Selecting the four nearest points to the weather file latitude and longitude...");
        System.out.println("Weather file longitude: " + longitude + ", latitude: " + latitude);
        ArrayList<GridPoint> selected_points = new ArrayList<>();

        float latminneg = Float.POSITIVE_INFINITY;
        Float latneg = null;
        float lonminneg = Float.POSITIVE_INFINITY;
        Float lonneg = null;

        float latminpos = Float.POSITIVE_INFINITY;
        Float latpos = null;
        float lonminpos = Float.POSITIVE_INFINITY;
        Float lonpos = null;

        for (int i = 0; i < this.grid.length; i++) {
            for (int j = 0; j < this.grid[i].length; j++) {
                GridPoint gp = this.grid[i][j];
                float glat = gp.getLatitude();
                float glon = gp.getLongitude();
                float latdist = latitude - glat;
                if (Math.abs(latdist) <= latminneg) {
                    if (latdist <= 0) {
                        latminneg = Math.abs(latdist);
                        latneg = glat;
                    }
                }
                if (Math.abs(latdist) <= latminpos) {
                    if (latdist > 0) {
                        latminpos = Math.abs(latdist);
                        latpos = glat;
                    }
                }

                float londist = longitude - glon;
                if (Math.abs(londist) <= lonminneg) {
                    if (londist <= 0) {
                        lonminneg = Math.abs(londist);
                        lonneg = glon;
                    }
                }
                if (Math.abs(londist) <= lonminpos) {
                    if (londist > 0) {
                        lonminpos = Math.abs(londist);
                        lonpos = glon;
                    }
                }
            }
        }
        
        if (lonneg == null || lonpos == null) {
            for (int i = 0; i < this.grid.length; i++) {
                for (int j = 0; j < this.grid[i].length; j++) {
                    GridPoint gp = this.grid[i][j];
                    float glon = gp.getLongitude();
                    
                    float londist = (longitude < 0 ? longitude + 360 : longitude) - (glon < 0 ? glon + 360 : glon);
                    if (Math.abs(londist) <= lonminneg) {
                        if (londist <= 0) {
                            lonminneg = Math.abs(londist);
                            lonneg = glon;
                        }
                    }
                    if (Math.abs(londist) <= lonminpos) {
                        if (londist > 0) {
                            lonminpos = Math.abs(londist);
                            lonpos = glon;
                        }
                    }
                }
            }
        }

        loop:for (int i = 0; i < this.grid.length; i++) {
            for (int j = 0; j < this.grid[i].length; j++) {
                GridPoint gp = this.grid[i][j];
                float tlat = gp.getLatitude();
                float tlon = gp.getLongitude();
                Integer x = gp.getX();
                Integer y = gp.getY();

                if(latneg != null) {
                    if (lonneg == tlon && latneg == tlat) {
                        selected_points.add(gp);
                        System.out.println("\tGrid nearest point: longitude " + tlon + ", latitude " + tlat + ", grid point " + x + ", " + y + ", id: " + gp.getId());
                    }
                    if (lonpos == tlon && latneg == tlat) {
                        selected_points.add(gp);
                        System.out.println("\tGrid nearest point: longitude " + tlon + ", latitude " + tlat + ", grid point " + x + ", " + y + ", id: " + gp.getId());
                    }
                }
                
                if(latpos != null) {
                    if (lonneg == tlon && latpos == tlat) {
                        selected_points.add(gp);
                        System.out.println("\tGrid nearest point: longitude " + tlon + ", latitude " + tlat + ", grid point " + x + ", " + y + ", id: " + gp.getId());
                    }
                    if (lonpos == tlon && latpos == tlat) {
                        selected_points.add(gp);
                        System.out.println("\tGrid nearest point: longitude " + tlon + ", latitude " + tlat + ", grid point " + x + ", " + y + ", id: " + gp.getId());
                    }
                }
                
                if(selected_points.size() == 4) {
                    break loop;
                }
            }
        }

        if(selected_points.size() < 4) {
            System.err.println("** ERROR ** Unable to find four nearest points for the given location.");
            System.exit(-1);
        }
        
        return selected_points;
    }

    /**
     * Determines the point nearest to the five points of the given latitude and
     * longitude.
     *
     * @param longitude longitude
     * @param latitude latitude
     * @param four_nearest_points four nearest points
     * @return grid point
     */
    public static int getNearestPoint(float longitude, float latitude, ArrayList<GridPoint> four_nearest_points) {
        float[] distance = new float[4];
        float minDistance = Float.MAX_VALUE;
        int k = 0;
        for (int i = 0; i < four_nearest_points.size(); i++) {
            distance[i] = (float) (Math.pow(Math.pow(four_nearest_points.get(i).getLatitude() - latitude, 2.0) + Math.pow(four_nearest_points.get(i).getLongitude() - longitude, 2.0), 0.5));
            if (minDistance > distance[i]) {
                minDistance = distance[i];
                k = i;
            }
        }
        return k;
    }

    /**
     * Converts 0° to 360° East longitude to -180° (W) to +180° (E) longitude.
     *
     * @param longitudePositiveEast360 0° to 360° East longitude
     * @return -180° (W) to +180° (E) longitude
     */
    public static float correctLongitude(float longitudePositiveEast360) {
        // Convert longitude as simply positive East 360°
        // to longitude negative East 180°
        float newLongitude = longitudePositiveEast360;
        newLongitude = newLongitude > 180.0f ? (newLongitude - 360.0f) : newLongitude;
        return newLongitude;
    }

    /**
     * Converts -180° (W) to +180° (E) to 0° to 360° East longitude.
     *
     * @param longitude -180° (W) to 180° (E) longitude
     * @return 0° to 360° East longitude
     */
    public static double convertLongitudeTo360East(double longitude) {
        return longitude < 0 ? longitude + 360 : longitude;
    }

    /**
     * Returns the weighted value of four grid points.
     *
     * @param longitude location longitude
     * @param latitude location latitude
     * @param four_nearest_points four grid points
     * @param a first point value
     * @param b second point value
     * @param c third point value
     * @param d fourth point value
     * @return weighted value
     */
    public static float getWeightedValue(float longitude, float latitude, ArrayList<GridPoint> four_nearest_points, float a, float b, float c, float d) {
        boolean extreme = 
                (longitude <= four_nearest_points.get(0).getLongitude() &&
                longitude <= four_nearest_points.get(1).getLongitude() && 
                longitude <= four_nearest_points.get(2).getLongitude() && 
                longitude <= four_nearest_points.get(3).getLongitude()) ||
                (longitude >= four_nearest_points.get(0).getLongitude() &&
                longitude >= four_nearest_points.get(1).getLongitude() && 
                longitude >= four_nearest_points.get(2).getLongitude() && 
                longitude >= four_nearest_points.get(3).getLongitude());
        
        float new_longitude = extreme ? longitude < 0 ? longitude + 360 : longitude : longitude;
        
        ArrayList<ExtendedGridPoint> points = new ArrayList<>();
        points.add(new ExtendedGridPoint(four_nearest_points.get(0).getId(), a, four_nearest_points.get(0).getX(), four_nearest_points.get(0).getY(), 
                extreme ? four_nearest_points.get(0).getLongitude() < 0 ? four_nearest_points.get(0).getLongitude() + 360 : four_nearest_points.get(0).getLongitude() : four_nearest_points.get(0).getLongitude(), 
                four_nearest_points.get(0).getLatitude()));
        points.add(new ExtendedGridPoint(four_nearest_points.get(1).getId(), b, four_nearest_points.get(1).getX(), four_nearest_points.get(1).getY(), 
                extreme ? four_nearest_points.get(1).getLongitude() < 0 ? four_nearest_points.get(1).getLongitude() + 360 : four_nearest_points.get(1).getLongitude() : four_nearest_points.get(1).getLongitude(), 
                four_nearest_points.get(1).getLatitude()));
        points.add(new ExtendedGridPoint(four_nearest_points.get(2).getId(), c, four_nearest_points.get(2).getX(), four_nearest_points.get(2).getY(), 
                extreme ? four_nearest_points.get(2).getLongitude() < 0 ? four_nearest_points.get(2).getLongitude() + 360 : four_nearest_points.get(2).getLongitude() : four_nearest_points.get(2).getLongitude(), 
                four_nearest_points.get(2).getLatitude()));
        points.add(new ExtendedGridPoint(four_nearest_points.get(3).getId(), d, four_nearest_points.get(3).getX(), four_nearest_points.get(3).getY(), 
                extreme ? four_nearest_points.get(3).getLongitude() < 0 ? four_nearest_points.get(3).getLongitude() + 360 : four_nearest_points.get(3).getLongitude() : four_nearest_points.get(3).getLongitude(), 
                four_nearest_points.get(3).getLatitude()));

        float minLon = Float.POSITIVE_INFINITY;
        float maxLon = Float.NEGATIVE_INFINITY;
        for (ExtendedGridPoint p : points) {
            if (p.getLongitude() <= minLon) {
                minLon = p.getLongitude();
            }
            if (p.getLongitude() >= maxLon) {
                maxLon = p.getLongitude();
            }
        }
        ArrayList<ExtendedGridPoint> minLonPoints = new ArrayList<>();
        ArrayList<ExtendedGridPoint> maxLonPoints = new ArrayList<>();
        for (ExtendedGridPoint p : points) {
            if (maxLon == p.getLongitude()) {
                maxLonPoints.add(p);
            }
            if (minLon == p.getLongitude()) {
                minLonPoints.add(p);
            }
        }
        float d1MinLon = Math.abs(minLonPoints.get(0).getLatitude() - latitude);
        float d2MinLon = Math.abs(minLonPoints.get(1).getLatitude() - latitude);
        float tMinLon = d1MinLon + d2MinLon;
        float w1MinLon = 1 - (d1MinLon / tMinLon);
        float w2MinLon = 1 - (d2MinLon / tMinLon);
        float v1MinLon = minLonPoints.get(0).value * w1MinLon;
        float v2MinLon = minLonPoints.get(1).value * w2MinLon;
        float vMinLon = v1MinLon + v2MinLon;

        float v1MaxLon = maxLonPoints.get(0).value * w1MinLon;
        float v2MaxLon = maxLonPoints.get(1).value * w2MinLon;
        float vMaxLon = v1MaxLon + v2MaxLon;

        float d1 = Math.abs(minLonPoints.get(0).getLongitude() - new_longitude);
        float d2 = Math.abs(maxLonPoints.get(0).getLongitude() - new_longitude);
        float t = d1 + d2;
        float w1 = 1 - (d1 / t);
        float w2 = 1 - (d2 / t);

        return w1 * vMinLon + w2 * vMaxLon;
    }

    /**
     * Extends grid point to include value.
     */
    public static class ExtendedGridPoint extends GridPoint {

        /**
         * Grid point value.
         */
        public float value;

        /**
         * Initiates an extended grid point.
         *
         * @param value value
         * @param x grid x coordinate
         * @param y grid y coordinate
         * @param longitude longitude
         * @param latitude latitude
         */
        public ExtendedGridPoint(int id, float value, Integer x, Integer y, float longitude, float latitude) {
            super(id, x, y, latitude, longitude);
            this.value = value;
        }
    }
}
