/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.functions;

import futureweathergenerator.EPW.EPW;
import futureweathergenerator.EPW.EPW_Comment;
import futureweathergenerator.GCM.InterpolatedVariablesScenario;

/**
 * Sets the comment 1 and comment 2 objects.
 *
 * @author eugenio
 */
public class Comments {

    /**
     * Sets the comment 1 and comment 2 objects.
     *
     * @param scenario scenario
     * @param morphedEPW morphed EPW object
     */
    public static void update(String model_type_string, InterpolatedVariablesScenario scenario, EPW morphedEPW) {
        morphedEPW.getEpw_comments().clear();
        String comment1 = "\"NOTICE: This EPW file is a morphed weather file generated using " + futureweathergenerator.FutureWeatherGenerator.APP_NAME + " (" + futureweathergenerator.FutureWeatherGenerator.APP_VERSION + "). The used general circulation model is the " + model_type_string + ". The scenario is " + scenario.getScenario().toUpperCase() + " " + scenario.getTimeframe() + ".\"";
        morphedEPW.getEpw_comments().add(new EPW_Comment(1, comment1));
        String comment2 = "\"DISCLAIMER: The software is provided as is, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the software.\"";
        morphedEPW.getEpw_comments().add(new EPW_Comment(2, comment2));
    }
}
