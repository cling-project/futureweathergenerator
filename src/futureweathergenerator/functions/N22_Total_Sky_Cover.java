/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.functions;

import futureweathergenerator.EPW.EPW;
import futureweathergenerator.GCM.InterpolatedVariablesScenario;
import futureweathergenerator.Months;

/**
 * Morphs N22 total sky cover.
 *
 * @author eugenio
 */
public class N22_Total_Sky_Cover {

    /**
     * Morphs N22 total sky cover.
     *
     * @param epw current EPW object
     * @param scenario GCM scenario
     * @param morphedEPW morphed EPW object
     * @param numberOfHoursToSmooth number of hours to smooth between months
     */
    public static void morph(EPW epw, InterpolatedVariablesScenario scenario, EPW morphedEPW, float numberOfHoursToSmooth) {
        for (int i = 0; i < Months.Abbreviation.values().length; i++) {
            int[] month_row_ids = Months.getMonthRowIds(Months.Abbreviation.values()[i]);
            int first_row_id = month_row_ids[1];
            int last_row_id = month_row_ids[2];
            for (int row_id = first_row_id; row_id < last_row_id; row_id++) {
                float mean_total_cloud_longwave_radiation_step = Utilities.getMonthlyTransitionVariableStep(i, row_id, first_row_id, last_row_id, scenario.getInterpolated_total_cloud_in_longwave_radiation(), numberOfHoursToSmooth);
                float mean_total_cloud_longwave_radiation_delta = Utilities.getMonthlyTransitionVariableHourDelta(row_id, first_row_id, last_row_id, mean_total_cloud_longwave_radiation_step, numberOfHoursToSmooth);
                float mean_total_cloud_longwave_radiation = scenario.getInterpolated_total_cloud_in_longwave_radiation()[i] + mean_total_cloud_longwave_radiation_delta;
                float weather_total_sky_cover = epw.getEpw_data_fields().get(row_id).getN22_total_sky_cover();
                float calculated_total_sky_cover = Math.round(((weather_total_sky_cover * 10.0) + mean_total_cloud_longwave_radiation) / 10.0);
                morphedEPW.getEpw_data_fields().get(row_id).setN22_total_sky_cover((float) calculated_total_sky_cover);
            }
        }
    }
}
