/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.functions;

import futureweathergenerator.EPW.EPW;
import futureweathergenerator.functions.solar.SolarFunctions;
import futureweathergenerator.Months;

/**
 * Calculates N10 extraterrestrial horizontal radiation from solar model.
 *
 * @author eugenio
 */
public class N10_Extraterrestrial_Horizontal_Radiation {

    /**
     * Calculates N10 extraterrestrial horizontal radiation from solar model.
     *
     * @param morphedEPW morphed EPW object
     */
    public static void calculate(EPW morphedEPW) {
        float latitude = morphedEPW.getEpw_location().getN2_latitude();
        float longitude = morphedEPW.getEpw_location().getN3_longitude();
        float time_zone = morphedEPW.getEpw_location().getN4_time_zone();

        for (int i = 0; i < Months.Abbreviation.values().length; i++) {
            calculate(morphedEPW, i, longitude, latitude, time_zone);
        }
    }

    public static void calculate(EPW morphedEPW, int i, float longitude, float latitude, float time_zone) {
        int[] month_row_ids = Months.getMonthRowIds(Months.Abbreviation.values()[i]);
        int number_of_days = month_row_ids[0] / 24;
        int first_row_id = month_row_ids[1];
        int row_id = first_row_id;
        for (int day = 0; day < number_of_days; day++) {
            row_id = calculate(morphedEPW, i, longitude, latitude, time_zone, row_id);
        }
    }

    public static int calculate(EPW morphedEPW, int month, float longitude, float latitude, float time_zone, int row_id) {
        float solarHourAdjustment;
        for (int hour = 0; hour < 24; hour++) {
            int julian_day = row_id / 24 + 1;
            switch (morphedEPW.getSolarHourAdjustmentOption()) {
                case None:
                    solarHourAdjustment = 1f;
                    break;
                default:
                case By_Month:
                    solarHourAdjustment = morphedEPW.solarHourAdjustment[month];
                    break;
                case By_Day:
                    solarHourAdjustment = morphedEPW.solarHourAdjustment[julian_day - 1];
                    break;
            }
            float day_angle = (float) SolarFunctions.getDayAngle(julian_day);
            float equation_of_time = (float) SolarFunctions.getEquationOfTime(day_angle);
            float correction_factor_for_solar_distance = (float) SolarFunctions.getCorrectionFactorForSolarDistance(day_angle);
            float calculated_extraterrestrial_direct_normal_radiation = (float) SolarFunctions.getExtraterrestrialDirectNormalRadiation(correction_factor_for_solar_distance);
            float local_standard_time = morphedEPW.getEpw_data_fields().get(row_id).getN4_hour();
            float local_standar_time_minutes_fraction = morphedEPW.getEpw_data_fields().get(row_id).getN5_minute() / 60f;
            float declination = (float) SolarFunctions.getDeclination(day_angle);
            float sin_Solar_Altitude_Angle_sum = 0.0f;
            for (int min = 1; min <= 60; min++) {
                float solar_time = (float) SolarFunctions.getSolarTime(local_standard_time - solarHourAdjustment + (min / 60f) + local_standar_time_minutes_fraction, longitude, time_zone, equation_of_time);
                float hour_angle = (float) SolarFunctions.getHourAngle(solar_time);
                float sin_Solar_Altitude_Angle_step = (float) SolarFunctions.getSinSolarAltitudeAngle(latitude, declination, hour_angle);
                if (sin_Solar_Altitude_Angle_step > 0) {
                    sin_Solar_Altitude_Angle_sum += sin_Solar_Altitude_Angle_step;
                }
            }
            float solar_altitude_angle = (float) SolarFunctions.getSolarAltitudeAngle(sin_Solar_Altitude_Angle_sum / 60f);
            float total_calculated_extraterrestrial_horizontal_radiation = (float) Math.sin(Math.toRadians(solar_altitude_angle)) * calculated_extraterrestrial_direct_normal_radiation;
            morphedEPW.getEpw_data_fields().get(row_id).setN10_extraterrestrial_horizontal_radiation(total_calculated_extraterrestrial_horizontal_radiation);
            row_id++;
        }
        return row_id;
    }

}
