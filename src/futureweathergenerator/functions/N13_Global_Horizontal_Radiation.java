/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.functions;

import futureweathergenerator.EPW.EPW;
import futureweathergenerator.GCM.InterpolatedVariablesScenario;
import futureweathergenerator.Months;

/**
 * Morphs N13 global horizontal radiation.
 *
 * @author eugenio
 */
public class N13_Global_Horizontal_Radiation {

    /**
     * Morphs N13 global horizontal radiation.
     *
     * @param epw current EPW object
     * @param scenario GCM scenario
     * @param morphedEPW morphed EPW object
     * @param numberOfHoursToSmooth number of hours to smooth between months
     */
    public static void morph(EPW epw, InterpolatedVariablesScenario scenario, EPW morphedEPW, float numberOfHoursToSmooth) {

        for (int i = 0; i < Months.Abbreviation.values().length; i++) {
            int[] month_row_ids = Months.getMonthRowIds(Months.Abbreviation.values()[i]);
            int rows_in_month = month_row_ids[0];
            int first_row_id = month_row_ids[1];
            int last_row_id = month_row_ids[2];
            float annual_average_global_horizontal_radiation = 0;
            for (int row_id = first_row_id; row_id < last_row_id; row_id++) {
                annual_average_global_horizontal_radiation += epw.getEpw_data_fields().get(row_id).getN13_global_horizontal_radiation();
            }
            annual_average_global_horizontal_radiation /= (float) rows_in_month;
            for (int row_id = first_row_id; row_id < last_row_id; row_id++) {
                float month_downward_surface_shortwave_flux_step = Utilities.getMonthlyTransitionVariableStep(i, row_id, first_row_id, last_row_id, scenario.getInterpolated_total_downward_surface_shortwave_flux(), numberOfHoursToSmooth);
                float month_downward_surface_shortwave_flux_delta = Utilities.getMonthlyTransitionVariableHourDelta(row_id, first_row_id, last_row_id, month_downward_surface_shortwave_flux_step, numberOfHoursToSmooth);
                float month_downward_surface_shortwave_flux = scenario.getInterpolated_total_downward_surface_shortwave_flux()[i] + month_downward_surface_shortwave_flux_delta;
                float scaling_factor_monthly_downward_surface_shortwave_flux = 1.0f + (month_downward_surface_shortwave_flux / annual_average_global_horizontal_radiation);
                float weather_global_horizontal_radiation = epw.getEpw_data_fields().get(row_id).getN13_global_horizontal_radiation();
                float calculated_global_horizontal_radiation = scaling_factor_monthly_downward_surface_shortwave_flux * weather_global_horizontal_radiation;
                morphedEPW.getEpw_data_fields().get(row_id).setN13_global_horizontal_radiation((float) calculated_global_horizontal_radiation);
            }
        }
    }
}
