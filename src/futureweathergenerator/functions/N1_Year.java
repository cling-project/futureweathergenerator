/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.functions;

import futureweathergenerator.EPW.EPW;
import futureweathergenerator.GCM.InterpolatedVariablesScenario;
import futureweathergenerator.Months;

/**
 * Sets N1 year to match scenario timeframe.
 * 
 * @author eugenio
 */
public class N1_Year {
    
    /**
     * Sets N1 year to match scenario timeframe.
     * 
     * @param scenario GCM scenario
     * @param morphedEPW morphed EPW object
     */
    public static void set(InterpolatedVariablesScenario scenario, EPW morphedEPW) {
        for(int i = 0; i < Months.Abbreviation.values().length; i++) {
            int[] month_row_ids = Months.getMonthRowIds(Months.Abbreviation.values()[i]);
            int first_row_id = month_row_ids[1];
            int last_row_id = month_row_ids[2];
            for(int row_id = first_row_id; row_id < last_row_id; row_id++) {
                int year = Integer.parseInt(scenario.getTimeframe().split("-")[0]);
                morphedEPW.getEpw_data_fields().get(row_id).setN1_year(year);
            }
        }
    }
}
