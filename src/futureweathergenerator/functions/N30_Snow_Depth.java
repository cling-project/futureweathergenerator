/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.functions;

import futureweathergenerator.EPW.EPW;
import futureweathergenerator.GCM.InterpolatedVariablesScenario;
import futureweathergenerator.Months;
import futureweathergenerator.UHI.UrbanHeatIslandDataPoint;
import java.util.Locale;

/**
 * Sets N30 snow depth.
 *
 * @author eugenio
 */
public class N30_Snow_Depth {
    
    /**
     * Morphs N30 snow depth.
     *
     * @param epw current EPW object
     * @param scenario GCM scenario
     * @param morphedEPW morphed EPW object
     * @param numberOfHoursToSmooth number of hours to smooth between months
     */
    public static void morph(EPW epw, InterpolatedVariablesScenario scenario, EPW morphedEPW, float numberOfHoursToSmooth) {
        for (int i = 0; i < Months.Abbreviation.values().length; i++) {
            int[] month_row_ids = Months.getMonthRowIds(Months.Abbreviation.values()[i]);
            int first_row_id = month_row_ids[1];
            int last_row_id = month_row_ids[2];
            for (int row_id = first_row_id; row_id < last_row_id; row_id++) {
                float scaling_factor_monthly_snow_depth_step = Utilities.getMonthlyTransitionVariableStep(i, row_id, first_row_id, last_row_id, scenario.getInterpolated_snow_depth(), numberOfHoursToSmooth);
                float scaling_factor_monthly_snow_depth_delta = Utilities.getMonthlyTransitionVariableHourDelta(row_id, first_row_id, last_row_id, scaling_factor_monthly_snow_depth_step, numberOfHoursToSmooth);
                float scaling_factor_monthly_snow_depth = scenario.getInterpolated_snow_depth()[i] + scaling_factor_monthly_snow_depth_delta;
                float weather_snow_depth = epw.getEpw_data_fields().get(row_id).getN30_snow_depth();
                float calculated_snow_depth = scaling_factor_monthly_snow_depth * weather_snow_depth;
                morphedEPW.getEpw_data_fields().get(row_id).setN30_snow_depth((float) calculated_snow_depth);
            }
        }
    }
    
    /**
     * Keeps the same N30 snow depth of the current EPW object.
     * 
     * @param epw current EPW object
     * @param morphedEPW morphed EPW object
     */
    public static void keepOriginal(EPW epw, EPW morphedEPW) {
        for(int i = 0; i < Months.Abbreviation.values().length; i++) {
            int[] month_row_ids = Months.getMonthRowIds(Months.Abbreviation.values()[i]);
            int first_row_id = month_row_ids[1];
            int last_row_id = month_row_ids[2];
            for(int row_id = first_row_id; row_id < last_row_id; row_id++) {
                float snow_depth = epw.getEpw_data_fields().get(row_id).getN30_snow_depth();
                morphedEPW.getEpw_data_fields().get(row_id).setN30_snow_depth(snow_depth);
            }
        }
    }

    /**
     * Sets N30 snow depth as missing.
     *
     * @param morphedEPW morphed EPW object
     */
    public static void setMissing(EPW morphedEPW) {
        for (int i = 0; i < Months.Abbreviation.values().length; i++) {
            int[] month_row_ids = Months.getMonthRowIds(Months.Abbreviation.values()[i]);
            int first_row_id = month_row_ids[1];
            int last_row_id = month_row_ids[2];
            for (int row_id = first_row_id; row_id < last_row_id; row_id++) {
                morphedEPW.getEpw_data_fields().get(row_id).setN30_snow_depth(Float.NaN);
            }
        }
    }

    public static void morph(UrbanHeatIslandDataPoint nearest_uhi_location, EPW uhiEPW, int BUFFER_AREA_TEMPERATURE_LEVEL, int URBAN_DENSITY, boolean doEcho) {
        // Calculated snow cover fraction cuts.
        nearest_uhi_location.selectBufferScfCluster(uhiEPW, BUFFER_AREA_TEMPERATURE_LEVEL);
        nearest_uhi_location.calculateScfDifferences(URBAN_DENSITY);
        float[] MONTH_DIFFS = nearest_uhi_location.getScf_DIFF_MONTHS();
        
        if (doEcho) {
            System.out.println(String.format(Locale.ROOT,
                    "Urban Heat Island - Snow Cover Fraction %n"
                            + "\tJan: %s %n"
                            + "\tFeb: %s %n"
                            + "\tMar: %s %n"
                            + "\tApr: %s %n"
                            + "\tMay: %s %n"
                            + "\tJun: %s %n"
                            + "\tJul: %s %n"
                            + "\tAug: %s %n"
                            + "\tSep: %s %n"
                            + "\tOct: %s %n"
                            + "\tNov: %s %n"
                            + "\tDec: %s ",
                    new Object[]{
                        MONTH_DIFFS[0],
                        MONTH_DIFFS[1],
                        MONTH_DIFFS[2],
                        MONTH_DIFFS[3],
                        MONTH_DIFFS[4],
                        MONTH_DIFFS[5],
                        MONTH_DIFFS[6],
                        MONTH_DIFFS[7],
                        MONTH_DIFFS[8],
                        MONTH_DIFFS[9],
                        MONTH_DIFFS[10],
                        MONTH_DIFFS[11]
                    }
            ));
        }
        
        // Cut snow depth if snow cover fraction is zero.
        for (int i = 0; i < Months.Abbreviation.values().length; i++) {
            int[] month_row_ids = Months.getMonthRowIds(Months.Abbreviation.values()[i]);
            int first_row_id = month_row_ids[1];
            int last_row_id = month_row_ids[2];
            for (int row_id = first_row_id; row_id < last_row_id; row_id++) {
                float weather_snow_depth = uhiEPW.getEpw_data_fields().get(row_id).getN30_snow_depth();
                float calculated_snow_depth = weather_snow_depth * MONTH_DIFFS[i];
                uhiEPW.getEpw_data_fields().get(row_id).setN30_snow_depth((float) calculated_snow_depth);
            }
        }
    }
}
