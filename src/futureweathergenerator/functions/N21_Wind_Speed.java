/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.functions;

import futureweathergenerator.EPW.EPW;
import futureweathergenerator.GCM.InterpolatedVariablesScenario;
import futureweathergenerator.Months;

/**
 * Morphs N21 wind speed.
 *
 * @author eugenio
 */
public class N21_Wind_Speed {

    /**
     * Morphs N21 wind speed.
     *
     * @param epw current EPW object
     * @param scenario GCM scenario
     * @param morphedEPW morphed EPW object
     * @param numberOfHoursToSmooth number of hours to smooth between months
     */
    public static void morph(EPW epw, InterpolatedVariablesScenario scenario, EPW morphedEPW, float numberOfHoursToSmooth) {
        for (int i = 0; i < Months.Abbreviation.values().length; i++) {
            int[] month_row_ids = Months.getMonthRowIds(Months.Abbreviation.values()[i]);
            int first_row_id = month_row_ids[1];
            int last_row_id = month_row_ids[2];
            for (int row_id = first_row_id; row_id < last_row_id; row_id++) {
                float scaling_factor_monthly_wind_speed_step = Utilities.getMonthlyTransitionVariableStep(i, row_id, first_row_id, last_row_id, scenario.getInterpolated_wind_speed(), numberOfHoursToSmooth);
                float scaling_factor_monthly_wind_speed_delta = Utilities.getMonthlyTransitionVariableHourDelta(row_id, first_row_id, last_row_id, scaling_factor_monthly_wind_speed_step, numberOfHoursToSmooth);
                float scaling_factor_monthly_wind_speed = scenario.getInterpolated_wind_speed()[i] + scaling_factor_monthly_wind_speed_delta;
                float weather_wind_speed = epw.getEpw_data_fields().get(row_id).getN21_wind_speed();
                float calculated_wind_speed = scaling_factor_monthly_wind_speed * weather_wind_speed;
                morphedEPW.getEpw_data_fields().get(row_id).setN21_wind_speed((float) calculated_wind_speed);
            }
        }
    }
}
