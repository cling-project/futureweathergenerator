/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.functions.psychrometric;

/**
 * Contains functions for calculating thermodynamic properties of gas-vapor
 * mixtures and standard atmosphere suitable for most engineering, physical and
 * meteorological applications. Most of the functions are an implementation of
 * the formulae found in the 2017 ASHRAE Handbook - Fundamentals.
 *
 * This code is inspired in the python version of the PsychroLib library. This
 * library can be found at https://github.com/psychrometrics/psychrolib/.
 *
 * @author eugenio
 */
public class Psychrometric2017 {

    // Zero degree Celsius (°C) expressed as Kelvin (K)
    private static final float ZERO_CELSIUS_AS_KELVIN = 273.15f;
    // Universal gas constant for dry air
    private static final float R_DA_SI = 287.042f;
    // Maximum number of iterations before exiting while loops.
    private static final int MAX_ITER_COUNT = 100;
    // Minimum acceptable humidity ratio used/returned by any functions.
    // Any value above 0 or below the MIN_HUM_RATIO will be reset to this value.
    private static final float MIN_HUM_RATIO = 1e-7f;
    // Freezing point of water in Celsius.
    private static final float FREEZING_POINT_WATER_SI = 0.0f;
    // Triple point of water in Celsius.
    private static final float TRIPLE_POINT_WATER_SI = 0.01f;
    // Temperature tolerance used in while loops.
    private static final float TOLERANCE = 0.001f;

    // Warning messages.
    private static final String WARNING_DEW_POINT_TEMPERATURE = "** WARNING ** Dew point temperature is above dry bulb temperature";
    private static final String WARNING_WET_BULB_TEMPERATURE = "** WARNING ** Wet bulb temperature is above dry bulb temperature";
    private static final String WARNING_RELATIVE_HUMIDITY = "** WARNING ** Relative humidity is outside range [0, 1.1]";
    private static final String WARNING_VAPOR_PRESSURE = "** WARNING ** Partial pressure of water vapor in moist air is below 0";
    private static final String WARNING_VAPOR_PRESSURE_EQUATIONS = "** WARNING ** Partial pressure of water vapor is outside range of validity of equations";
    private static final String WARNING_HUMIDITY_RATIO = "** WARNING ** Humidity ratio is below 0";
    private static final String WARNING_SPECIFIC_HUMIDITY = "** WARNING ** Specific humidity is outside range ]0, 1]";
    private static final String WARNING_DRY_BULB_TEMPERATURE = "** WARNING ** Dry bulb temperature is outside range of validity of equations";

    /**
     * Utility function to convert temperature to Kelvin (K) given temperature
     * in degree Celsius (°C).
     *
     * @param temperature temperature in degree Celsius (°C)
     * @return temperature in Kelvin (K)
     */
    public static float getDegreeCelsius_From_Kelvin(float temperature) {
        return temperature + ZERO_CELSIUS_AS_KELVIN;
    }

    /**
     * Returnss wet-bulb temperature given dry-bulb temperature, dew-point
     * temperature, and pressure.
     *
     * @param dry_bulb_temperature Dry-bulb temperature in °C
     * @param dew_point_temperature Station pressure in Pa
     * @param pressure Atmospheric pressure in Pa
     * @return Wet-bulb temperature in °C
     */
    public static float getWetBulbTemperature_From_DewPointTemperature(float dry_bulb_temperature, float dew_point_temperature, float pressure) {
        if (dew_point_temperature > dry_bulb_temperature) {
            System.err.println(WARNING_DEW_POINT_TEMPERATURE + ": " + dew_point_temperature);
        }
        float humidity_ratio = getHumidityRatio_From_DewPointTemperature(dew_point_temperature, pressure);
        float wet_bulb_temperature = getWetBulbTemperature_From_HumidityRatio(dry_bulb_temperature, humidity_ratio, pressure);
        return wet_bulb_temperature;
    }

    /**
     * Returns wet-bulb temperature given dry-bulb temperature, relative
     * humidity, and pressure.
     *
     * @param dry_bulb_temperature Dry-bulb temperature in °C
     * @param relative_humidity Relative humidity in range [0, 1]
     * @param pressure Atmospheric pressure in Pa
     * @return Wet-bulb temperature in °C
     */
    public static float getWetBulbTemperature_From_RelativeHumidity(float dry_bulb_temperature, float relative_humidity, float pressure) {
        if (relative_humidity < 0 || relative_humidity > 1.1) {
            System.err.println(WARNING_RELATIVE_HUMIDITY + ": " + relative_humidity);
        }
        float humidity_ratio = getHumidityRatio_From_RelativeHumidity(dry_bulb_temperature, relative_humidity, pressure);
        float wet_bulb_temperature = getWetBulbTemperature_From_HumidityRatio(dry_bulb_temperature, humidity_ratio, pressure);
        return wet_bulb_temperature;
    }

    /**
     * Returns relative humidity given dry-bulb temperature and dew-point
     * temperature.
     *
     * @param dry_bulb_temperature Dry-bulb temperature in °C
     * @param dew_point_temperature Station pressure in Pa
     * @return Relative humidity in range [0, 1]
     */
    public static float getRelativeHumidity_From_DewPointTemperature(float dry_bulb_temperature, float dew_point_temperature) {
        if (dew_point_temperature > dry_bulb_temperature) {
            System.err.println(WARNING_DEW_POINT_TEMPERATURE + ": " + dew_point_temperature);
        }
        float vapor_pressure = getSaturationVaporPressure(dew_point_temperature);
        float saturated_vapor_pressure = getSaturationVaporPressure(dry_bulb_temperature);
        float relative_humidity = vapor_pressure / saturated_vapor_pressure;
        return relative_humidity;
    }

    /**
     * Returns relative humidity given dry-bulb temperature, wet bulb
     * temperature and pressure.
     *
     * @param dry_bulb_temperature Dry-bulb temperature in °C
     * @param wet_bulb_temperature Wet-bulb temperature in °C
     * @param pressure Atmospheric pressure in Pa
     * @return Relative humidity in range [0, 1]
     */
    public static float getRelativeHumidity_From_WetBulbTemperature(float dry_bulb_temperature, float wet_bulb_temperature, float pressure) {
        if (wet_bulb_temperature > dry_bulb_temperature) {
            System.err.println(WARNING_WET_BULB_TEMPERATURE + ": " + wet_bulb_temperature);
        }
        float humidity_ratio = getHumidityRatio_From_WetBulbTemperature(dry_bulb_temperature, wet_bulb_temperature, pressure);
        float relative_humidity = getRelativeHumidity_From_HumidityRatio(dry_bulb_temperature, humidity_ratio, pressure);
        return relative_humidity;
    }

    /**
     * Returns dew-point temperature given dry-bulb temperature and relative
     * humidity.
     *
     * @param dry_bulb_temperature Dry-bulb temperature in °C
     * @param relative_humidity Relative humidity in range [0, 1]
     * @return Station pressure in Pa
     */
    public static float getDewPointTemperature_From_RelativeHumidity(float dry_bulb_temperature, float relative_humidity) {
        if (relative_humidity < 0 || relative_humidity > 1.1) {
            System.err.println(WARNING_RELATIVE_HUMIDITY + ": " + relative_humidity);
        }
        float vapor_pressure = getVaporPressure_From_RelativeHumidity(dry_bulb_temperature, relative_humidity);
        float dew_point_temperature = getDewPointTemperature_From_VaporPressure(dry_bulb_temperature, vapor_pressure);
        return dew_point_temperature;
    }

    /**
     * Returns dew-point temperature given dry-bulb temperature, wet-bulb
     * temperature, and pressure.
     *
     * @param dry_bulb_temperature Dry-bulb temperature in °C
     * @param wet_bulb_temperature Wet-bulb temperature in °C
     * @param pressure Atmospheric pressure in Pa
     * @return Station pressure in Pa
     */
    public static float getDewPointTemperature_From_WetBulbTemperature(float dry_bulb_temperature, float wet_bulb_temperature, float pressure) {
        if (wet_bulb_temperature > dry_bulb_temperature) {
            System.err.println(WARNING_WET_BULB_TEMPERATURE + ": " + wet_bulb_temperature);
        }
        float humidity_ratio = getHumidityRatio_From_WetBulbTemperature(dry_bulb_temperature, wet_bulb_temperature, pressure);
        float dew_point_temperature = getDewPointTemperature_From_HumidityRatio(dry_bulb_temperature, humidity_ratio, pressure);
        return dew_point_temperature;
    }

    /**
     * Returns partial pressure of water vapor as a function of relative
     * humidity and temperature.
     *
     * @param dry_bulb_temperature Dry-bulb temperature in °C
     * @param relative_humidity Relative humidity in range [0, 1]
     * @return Partial pressure of water vapor in moist air in Pa
     */
    public static float getVaporPressure_From_RelativeHumidity(float dry_bulb_temperature, float relative_humidity) {
        if (relative_humidity < 0 || relative_humidity > 1.1) {
            System.err.println(WARNING_RELATIVE_HUMIDITY + ": " + relative_humidity);
        }
        float vapor_pressure = relative_humidity * getSaturationVaporPressure(dry_bulb_temperature);
        return vapor_pressure;
    }

    /**
     * Returns relative humidity given dry-bulb temperature and vapor pressure.
     *
     * @param dry_bulb_temperature Dry-bulb temperature in °C
     * @param vapor_pressure Partial pressure of water vapor in moist air in Pa
     * @return Relative humidity in range [0, 1]
     */
    public static float getRelativeHumidity_From_VaporPressure(float dry_bulb_temperature, float vapor_pressure) {
        if (vapor_pressure < 0) {
            System.err.println(WARNING_VAPOR_PRESSURE + ": " + vapor_pressure);
        }
        float relative_humidity = vapor_pressure / getSaturationVaporPressure(dry_bulb_temperature);
        return relative_humidity;
    }

    /**
     * Helper function returning the derivative of the natural log of the
     * saturation vapor pressure as a function of dry-bulb temperature.
     *
     * @param dry_bulb_temperature Dry-bulb temperature in °C
     * @return Derivative of natural log of vapor pressure of saturated air in
     * Pa
     */
    public static float getDerivativeOfNaturalLog_Of_Saturation_Vapor_Pressure(float dry_bulb_temperature) {
        float dry_bulb_temperature_kelvin = getDegreeCelsius_From_Kelvin(dry_bulb_temperature);
        if (dry_bulb_temperature <= TRIPLE_POINT_WATER_SI) {
            return (float) (5.6745359E+03 / Math.pow(dry_bulb_temperature_kelvin, 2) - 9.677843E-03 + 2 * 6.2215701E-07 * dry_bulb_temperature_kelvin + 3 * 2.0747825E-09 * Math.pow(dry_bulb_temperature_kelvin, 2) - 4 * 9.484024E-13 * Math.pow(dry_bulb_temperature_kelvin, 3) + 4.1635019 / dry_bulb_temperature_kelvin);
        } else {
            return (float) (5.8002206E+03 / Math.pow(dry_bulb_temperature_kelvin, 2) - 4.8640239E-02 + 2 * 4.1764768E-05 * dry_bulb_temperature_kelvin - 3 * 1.4452093E-08 * Math.pow(dry_bulb_temperature_kelvin, 2) + 6.5459673 / dry_bulb_temperature_kelvin);
        }
    }

    /**
     * Returns dew-point temperature given dry-bulb temperature and vapor
     * pressure.
     *
     * The dew point temperature is solved by inverting the equation giving
     * water vapor pressure at saturation from temperature rather than using the
     * regressions provided by ASHRAE (eqn. 37 and 38) which are much less
     * accurate and have a narrower range of validity.
     *
     * The Newton-Raphson (NR) method is used on the logarithm of water vapour
     * pressure as a function of temperature, which is a very smooth function
     * Convergence is usually achieved in 3 to 5 iterations. TDryBulb is not
     * really needed here, just used for convenience.
     *
     * @param dry_bulb_temperature Dry-bulb temperature in °C
     * @param vapor_pressure Partial pressure of water vapor in moist air in Pa
     * @return Station pressure in Pa
     */
    public static float getDewPointTemperature_From_VaporPressure(float dry_bulb_temperature, float vapor_pressure) {
        float lower_bound = -100.0f;
        float higher_bound = 200.0f;
        if (vapor_pressure < getSaturationVaporPressure(lower_bound) || vapor_pressure > getSaturationVaporPressure(higher_bound)) {
            System.err.println(WARNING_VAPOR_PRESSURE_EQUATIONS + ": " + vapor_pressure);
        }
        float dew_point_temperature = dry_bulb_temperature; // First guess
        float lnVP = (float) Math.log(vapor_pressure);
        int index = 0;
        while (true) {
            // TDewPoint used in NR calculation
            float dew_point_temperature_iter = dew_point_temperature;
            float lnVP_iter = (float) Math.log(getSaturationVaporPressure(dew_point_temperature_iter));
            // Derivative of function, calculated analytically
            float d_lnVP = getDerivativeOfNaturalLog_Of_Saturation_Vapor_Pressure(dew_point_temperature_iter);

            // New estimate, bounded by the search domain defined above
            dew_point_temperature = dew_point_temperature_iter - (lnVP_iter - lnVP) / d_lnVP;
            dew_point_temperature = Math.max(dew_point_temperature, lower_bound);
            dew_point_temperature = Math.min(dew_point_temperature, higher_bound);

            if (Math.abs(dew_point_temperature - dew_point_temperature_iter) <= TOLERANCE) {
                break;
            }

            if (index > MAX_ITER_COUNT) {
                throw new UnsupportedOperationException(" ** WARNING ** Convergence not reached in getDewPointTemperature_From_VaporPressure");
            }

            index++;
        }
        dew_point_temperature = Math.min(dew_point_temperature, dry_bulb_temperature);
        return dew_point_temperature;
    }

    /**
     * Returns vapor pressure given dew point temperature.
     *
     * @param dew_point_temperature Station pressure in Pa
     * @return Partial pressure of water vapor in moist air in Pa
     */
    public static float getVaporPressure_From_DewPointTemperature(float dew_point_temperature) {
        return getSaturationVaporPressure(dew_point_temperature);
    }

    /**
     * Returns wet-bulb temperature given dry-bulb temperature, humidity ratio,
     * and pressure.
     *
     * @param dry_bulb_temperature Dry-bulb temperature in °C
     * @param humidity_ratio Humidity ratio in kg_H₂O kg_Air⁻¹
     * @param pressure Atmospheric pressure in Pa
     * @return Wet-bulb temperature in °C
     */
    public static float getWetBulbTemperature_From_HumidityRatio(float dry_bulb_temperature, float humidity_ratio, float pressure) {
        if (humidity_ratio < 0) {
            System.err.println(WARNING_HUMIDITY_RATIO + ": " + humidity_ratio);
        }
        float bounded_humidity_ratio = Math.max(humidity_ratio, MIN_HUM_RATIO);
        float dew_point_temperature = getDewPointTemperature_From_HumidityRatio(dry_bulb_temperature, bounded_humidity_ratio, pressure);
        float wet_bulb_temperature_sup = dry_bulb_temperature;
        float wet_bulb_temperature_inf = dew_point_temperature;
        float wet_bulb_temperature = (wet_bulb_temperature_inf + wet_bulb_temperature_sup) / 2.0f;
        int index = 0;
        while ((wet_bulb_temperature_sup - wet_bulb_temperature_inf) > TOLERANCE) {
            float wstar = getHumidityRatio_From_WetBulbTemperature(dry_bulb_temperature, wet_bulb_temperature, pressure);

            if (wstar > bounded_humidity_ratio) {
                wet_bulb_temperature_sup = wet_bulb_temperature;
            } else {
                wet_bulb_temperature_inf = wet_bulb_temperature;
            }

            wet_bulb_temperature = (wet_bulb_temperature_sup + wet_bulb_temperature_inf) / 2.0f;

            if (index >= MAX_ITER_COUNT) {
                throw new UnsupportedOperationException(" ** WARNING ** Convergence not reached in getWetBulbTemperature_From_VaporPressure");
            }

            index++;
        }
        return wet_bulb_temperature;
    }

    /**
     * Returns humidity ratio given dry-bulb temperature, wet-bulb temperature,
     * and pressure.
     *
     * @param dry_bulb_temperature Dry-bulb temperature in °C
     * @param wet_bulb_temperature Wet-bulb temperature in °C
     * @param pressure Atmospheric pressure in Pa
     * @return Humidity ratio in kg_H₂O kg_Air⁻¹
     */
    public static float getHumidityRatio_From_WetBulbTemperature(float dry_bulb_temperature, float wet_bulb_temperature, float pressure) {
        if (wet_bulb_temperature > dry_bulb_temperature) {
            System.err.println(WARNING_WET_BULB_TEMPERATURE + ": " + wet_bulb_temperature);
        }
        float wstar = getHumidityRatioOfSaturatedAir(wet_bulb_temperature, pressure);
        float humidity_ratio;
        if (wet_bulb_temperature >= FREEZING_POINT_WATER_SI) {
            humidity_ratio = (float) (((2501 - 2.326 * wet_bulb_temperature) * wstar - 1.006 * (dry_bulb_temperature - wet_bulb_temperature))
                    / (2501 + 1.86 * dry_bulb_temperature - 4.186 * wet_bulb_temperature));
        } else {
            humidity_ratio = (float) (((2830 - 0.24 * wet_bulb_temperature) * wstar - 1.006 * (dry_bulb_temperature - wet_bulb_temperature))
                    / (2830 + 1.86 * dry_bulb_temperature - 2.1 * wet_bulb_temperature));
        }
        return Math.max(humidity_ratio, MIN_HUM_RATIO);
    }

    /**
     * Returns humidity ratio given dry-bulb temperature, relative humidity, and
     * pressure.
     *
     * @param dry_bulb_temperature Dry-bulb temperature in °C
     * @param relative_humidity Relative humidity in range [0, 1]
     * @param pressure Atmospheric pressure in Pa
     * @return Humidity ratio in kg_H₂O kg_Air⁻¹
     */
    public static float getHumidityRatio_From_RelativeHumidity(float dry_bulb_temperature, float relative_humidity, float pressure) {
        if (relative_humidity < 0 || relative_humidity > 1.1) {
            System.err.println(WARNING_RELATIVE_HUMIDITY + ": " + relative_humidity);
        }
        float vapor_pressure = getVaporPressure_From_RelativeHumidity(dry_bulb_temperature, relative_humidity);
        float humidity_ratio = getHumidityRatio_From_VaporPressure(vapor_pressure, pressure);
        return humidity_ratio;
    }

    /**
     * Returns relative humidity given dry-bulb temperature, humidity ratio, and
     * pressure.
     *
     * @param dry_bulb_temperature Dry-bulb temperature in °C
     * @param humidity_ratio Humidity ratio in kg_H₂O kg_Air⁻¹
     * @param pressure Atmospheric pressure in Pa
     * @return Relative humidity in range [0, 1]
     */
    public static float getRelativeHumidity_From_HumidityRatio(float dry_bulb_temperature, float humidity_ratio, float pressure) {
        if (humidity_ratio < 0) {
            System.err.println(WARNING_HUMIDITY_RATIO + ": " + humidity_ratio);
        }
        float vapor_pressure = getVaporPressure_From_HumidityRatio(humidity_ratio, pressure);
        float relative_humidity = getRelativeHumidity_From_VaporPressure(dry_bulb_temperature, vapor_pressure);
        return relative_humidity;
    }

    /**
     * Returns humidity ratio given dew-point temperature and pressure.
     *
     * @param dew_point_temperature Station pressure in Pa
     * @param pressure Atmospheric pressure in Pa
     * @return Humidity ratio in kg_H₂O kg_Air⁻¹
     */
    public static float getHumidityRatio_From_DewPointTemperature(float dew_point_temperature, float pressure) {
        float vapor_pressure = getSaturationVaporPressure(dew_point_temperature);
        float humidity_ratio = getHumidityRatio_From_VaporPressure(vapor_pressure, pressure);
        return humidity_ratio;
    }

    /**
     * Returns dew-point temperature given dry-bulb temperature, humidity ratio,
     * and pressure.
     *
     * @param dry_bulb_temperature Dry-bulb temperature in °C
     * @param humidity_ratio Humidity ratio in kg_H₂O kg_Air⁻¹
     * @param pressure Atmospheric pressure in Pa
     * @return Station pressure in Pa
     */
    public static float getDewPointTemperature_From_HumidityRatio(float dry_bulb_temperature, float humidity_ratio, float pressure) {
        if (humidity_ratio < 0) {
            System.err.println(WARNING_HUMIDITY_RATIO + ": " + humidity_ratio);
        }
        float vapor_pressure = getVaporPressure_From_HumidityRatio(humidity_ratio, pressure);
        float dew_point_temperature = getDewPointTemperature_From_VaporPressure(dry_bulb_temperature, vapor_pressure);
        return dew_point_temperature;
    }

    /**
     * Returns humidity ratio given water vapor pressure and atmospheric
     * pressure.
     *
     * @param vapor_pressure Partial pressure of water vapor in moist air in Pa
     * @param pressure Atmospheric pressure in Pa
     * @return Humidity ratio in kg_H₂O kg_Air⁻¹
     */
    public static float getHumidityRatio_From_VaporPressure(float vapor_pressure, float pressure) {
        if (vapor_pressure < 0) {
            System.err.println(WARNING_VAPOR_PRESSURE + ": " + vapor_pressure);
        }
        float humidity_ratio = 0.621945f * vapor_pressure / (pressure - vapor_pressure);
        return Math.max(humidity_ratio, MIN_HUM_RATIO);
    }

    /**
     * Returns vapor pressure given humidity ratio and pressure.
     *
     * @param humidity_ratio Humidity ratio in kg_H₂O kg_Air⁻¹
     * @param pressure Atmospheric pressure in Pa
     * @return Partial pressure of water vapor in moist air in Pa
     */
    public static float getVaporPressure_From_HumidityRatio(float humidity_ratio, float pressure) {
        if (humidity_ratio < 0) {
            System.err.println(WARNING_HUMIDITY_RATIO + ": " + humidity_ratio);
        }
        float bounded_humidity_ratio = Math.max(humidity_ratio, MIN_HUM_RATIO);
        float vapor_pressure = pressure * bounded_humidity_ratio / (0.621945f + bounded_humidity_ratio);
        return vapor_pressure;
    }

    /**
     * Returns the specific humidity from humidity ratio (aka mixing ratio).
     *
     * @param humidity_ratio Humidity ratio in kg_H₂O kg_Dry_Air⁻¹
     * @return Specific humidity in kg_H₂O kg_Air⁻¹
     */
    public static float getSpecificHumidity_From_HumidityRatio(float humidity_ratio) {
        if (humidity_ratio < 0) {
            System.err.println(WARNING_HUMIDITY_RATIO + ": " + humidity_ratio);
        }
        float bounded_humidity_ratio = Math.max(humidity_ratio, MIN_HUM_RATIO);
        float specific_humidity = bounded_humidity_ratio / (1.0f + bounded_humidity_ratio);
        return specific_humidity;
    }

    /**
     * Returns the humidity ratio (aka mixing ratio) from specific humidity.
     *
     * @param specific_humidity Specific humidity in kg_H₂O kg_Air⁻¹
     * @return Humidity ratio in kg_H₂O kg_Dry_Air⁻¹
     */
    public static float getHumidityRatio_From_SpecificHumidity(float specific_humidity) {
        if (specific_humidity < 0.0 || specific_humidity >= 1.0) {
            System.err.println(WARNING_SPECIFIC_HUMIDITY + ": " + specific_humidity);
        }
        float humidity_ratio = specific_humidity / (1.0f - specific_humidity);
        return Math.max(humidity_ratio, MIN_HUM_RATIO);
    }

    /**
     * Returns dry-air enthalpy given dry-bulb temperature.
     *
     * @param dry_bulb_temperature Dry-bulb temperature in °C
     * @return Dry air enthalpy in J kg⁻¹
     */
    public static float getDryAirEnthalpy(float dry_bulb_temperature) {
        return 1006.0f * dry_bulb_temperature;
    }

    /**
     * Returns dry-air density given dry-bulb temperature and pressure.
     *
     * @param dry_bulb_temperature Dry-bulb temperature in °C
     * @param pressure Atmospheric pressure in Pa
     * @return Dry air density in kg m⁻³
     */
    public static float getDryAirDensity(float dry_bulb_temperature, float pressure) {
        return pressure / R_DA_SI / getDegreeCelsius_From_Kelvin(dry_bulb_temperature);
    }

    /**
     * Returns dry-air volume given dry-bulb temperature and pressure.
     *
     * @param dry_bulb_temperature Dry-bulb temperature in °C
     * @param pressure Atmospheric pressure in Pa
     * @return Dry air volume in m³ kg⁻¹
     */
    public static float getDryAirVolume(float dry_bulb_temperature, float pressure) {
        return R_DA_SI * getDegreeCelsius_From_Kelvin(dry_bulb_temperature) / pressure;
    }

    /**
     * Returns dry bulb temperature from enthalpy and humidity ratio.
     *
     * @param moist_air_enthalpy Moist air enthalpy in J kg⁻¹
     * @param humidity_ratio Humidity ratio in kg_H₂O kg_Air⁻¹
     * @return Dry-bulb temperature in °C
     */
    public static float getDryBulbTemperature_From_Enthalpy_And_HumidityRatio(float moist_air_enthalpy, float humidity_ratio) {
        if (humidity_ratio < 0) {
            System.err.println(WARNING_HUMIDITY_RATIO + ": " + humidity_ratio);
        }
        float bounded_humidity_ratio = Math.max(humidity_ratio, MIN_HUM_RATIO);
        return (moist_air_enthalpy / 1000.0f - 2501.0f * bounded_humidity_ratio) / (1.006f + 1.86f * bounded_humidity_ratio);
    }

    /**
     * Returns humidity ratio from enthalpy and dry-bulb temperature.
     *
     * @param moist_air_enthalpy Moist air enthalpy in J kg⁻¹
     * @param dry_bulb_temperature Dry-bulb temperature in °C
     * @return Humidity ratio in kg_H₂O kg_Air⁻¹
     */
    public static float getHumidityRatio_From_Enthalpy_And_DryBulbTemperature(float moist_air_enthalpy, float dry_bulb_temperature) {
        float humidity_ratio = (moist_air_enthalpy / 1000.0f - 1.006f * dry_bulb_temperature) / (2501.0f + 1.86f * dry_bulb_temperature);
        return Math.max(humidity_ratio, MIN_HUM_RATIO);
    }

    /**
     * Returns saturation vapor pressure given dry-bulb temperature.
     *
     * Important note: the ASHRAE formulae are defined above and below the
     * freezing point but have a discontinuity at the freezing point. This is a
     * small inaccuracy on ASHRAE's part: the formulae should be defined above
     * and below the triple point of water (not the freezing point) in which
     * case the discontinuity vanishes. It is essential to use the triple point
     * of water otherwise function getDewPointTemperature_From_VaporPressure,
     * which inverts the present function, does not converge properly around the
     * freezing point.
     *
     * @param dry_bulb_temperature Dry-bulb temperature in °C
     * @return Vapor pressure of saturated air in Pa
     */
    public static float getSaturationVaporPressure(float dry_bulb_temperature) {
        float lower_bound = -100.0f;
        float higher_bound = 200.0f;
        if (dry_bulb_temperature < lower_bound || dry_bulb_temperature > higher_bound) {
            System.err.println(WARNING_DRY_BULB_TEMPERATURE + ": " + dry_bulb_temperature);
        }
        float dry_bulb_temperature_kelvin = getDegreeCelsius_From_Kelvin(dry_bulb_temperature);
        float lnPws;
        if (dry_bulb_temperature <= TRIPLE_POINT_WATER_SI) {
            lnPws = (float) (-5.6745359E+03 / dry_bulb_temperature_kelvin + 6.3925247 - 9.677843E-03 * dry_bulb_temperature_kelvin + 6.2215701E-07 * dry_bulb_temperature_kelvin * dry_bulb_temperature_kelvin
                    + 2.0747825E-09 * Math.pow(dry_bulb_temperature_kelvin, 3) - 9.484024E-13 * Math.pow(dry_bulb_temperature_kelvin, 4) + 4.1635019 * Math.log(dry_bulb_temperature_kelvin));
        } else {
            lnPws = (float) (-5.8002206E+03 / dry_bulb_temperature_kelvin + 1.3914993 - 4.8640239E-02 * dry_bulb_temperature_kelvin + 4.1764768E-05 * dry_bulb_temperature_kelvin * dry_bulb_temperature_kelvin
                    - 1.4452093E-08 * Math.pow(dry_bulb_temperature_kelvin, 3) + 6.5459673 * Math.log(dry_bulb_temperature_kelvin));
        }
        return (float) Math.exp(lnPws);
    }

    /**
     * Returns humidity ratio of saturated air given dry-bulb temperature and
     * pressure.
     *
     * @param dry_bulb_temperature Dry-bulb temperature in °C
     * @param pressure Atmospheric pressure in Pa
     * @return Humidity ratio of saturated air in kg_H₂O kg_Air⁻¹
     */
    public static float getHumidityRatioOfSaturatedAir(float dry_bulb_temperature, float pressure) {
        float saturation_vapor_pressure = getSaturationVaporPressure(dry_bulb_temperature);
        float saturation_humidity_ratio = 0.621945f * saturation_vapor_pressure / (pressure - saturation_vapor_pressure);
        return Math.max(saturation_humidity_ratio, MIN_HUM_RATIO);
    }

    /**
     * Returns saturated air enthalpy given dry-bulb temperature and pressure.
     *
     * @param dry_bulb_temperature Dry-bulb temperature in °C
     * @param pressure Atmospheric pressure in Pa
     * @return Saturated air enthalpy in J kg⁻¹
     */
    public static float getSaturatedAirEnthalpy(float dry_bulb_temperature, float pressure) {
        float saturation_humidity_ratio = getHumidityRatioOfSaturatedAir(dry_bulb_temperature, pressure);
        float saturation_air_enthalpy = getMoistAirEnthalpy(dry_bulb_temperature, saturation_humidity_ratio);
        return saturation_air_enthalpy;
    }

    /**
     * Returns Vapor pressure deficit given dry-bulb temperature, humidity
     * ratio, and pressure.
     *
     * Oke (1987) eqn 2.13a
     *
     * @param dry_bulb_temperature Dry-bulb temperature in °C
     * @param humidity_ratio Humidity ratio in kg_H₂O kg_Air⁻¹
     * @param pressure Atmospheric pressure in Pa
     * @return Vapor pressure deficit in Pa
     */
    public static float getVaporPressureDeficit(float dry_bulb_temperature, float humidity_ratio, float pressure) {
        if (humidity_ratio < 0) {
            System.err.println(WARNING_HUMIDITY_RATIO + ": " + humidity_ratio);
        }
        float relative_humidity = getRelativeHumidity_From_HumidityRatio(dry_bulb_temperature, humidity_ratio, pressure);
        float vapor_pressure_deficit = getSaturationVaporPressure(dry_bulb_temperature) * (1.0f - relative_humidity);
        return vapor_pressure_deficit;
    }

    /**
     * Returns the degree of saturation (i.e humidity ratio of the air /
     * humidity ratio of the air at saturation at the same temperature and
     * pressure) given dry-bulb temperature, humidity ratio, and atmospheric
     * pressure.
     *
     * @param dry_bulb_temperature Dry-bulb temperature in °C
     * @param humidity_ratio Humidity ratio in kg_H₂O kg_Air⁻¹
     * @param pressure Atmospheric pressure in Pa
     * @return Degree of saturation in arbitrary unit
     */
    public static float getDegreeOfSaturation(float dry_bulb_temperature, float humidity_ratio, float pressure) {
        if (humidity_ratio < 0) {
            System.err.println(WARNING_HUMIDITY_RATIO + ": " + humidity_ratio);
        }
        float bounded_humidity_ratio = Math.max(humidity_ratio, MIN_HUM_RATIO);
        float saturation_humidity_ratio = getHumidityRatioOfSaturatedAir(dry_bulb_temperature, pressure);
        float degree_of_saturation = bounded_humidity_ratio / saturation_humidity_ratio;
        return degree_of_saturation;
    }

    /**
     * Returns moist air enthalpy given dry-bulb temperature and humidity ratio.
     *
     * @param dry_bulb_temperature Dry-bulb temperature in °C
     * @param humidity_ratio Humidity ratio in kg_H₂O kg_Air⁻¹
     * @return Moist air enthalpy in J kg⁻¹
     */
    public static float getMoistAirEnthalpy(float dry_bulb_temperature, float humidity_ratio) {
        if (humidity_ratio < 0) {
            System.err.println(WARNING_HUMIDITY_RATIO + ": " + humidity_ratio);
        }
        float bounded_humidity_ratio = Math.max(humidity_ratio, MIN_HUM_RATIO);
        return (1.006f * dry_bulb_temperature + bounded_humidity_ratio * (2501.0f + 1.86f * dry_bulb_temperature)) * 1000.0f;
    }

    /**
     * Returns moist air specific volume given dry-bulb temperature, humidity
     * ratio, and pressure.
     *
     * @param dry_bulb_temperature Dry-bulb temperature in °C
     * @param humidity_ratio Humidity ratio in kg_H₂O kg_Air⁻¹
     * @param pressure Atmospheric pressure in Pa
     * @return Specific volume of moist air in m³ kg⁻¹ of dry air
     */
    public static float getMoistAirVolume(float dry_bulb_temperature, float humidity_ratio, float pressure) {
        if (humidity_ratio < 0) {
            System.err.println(WARNING_HUMIDITY_RATIO + ": " + humidity_ratio);
        }
        float bounded_humidity_ratio = Math.max(humidity_ratio, MIN_HUM_RATIO);
        return R_DA_SI * getDegreeCelsius_From_Kelvin(dry_bulb_temperature) * (1.0f + 1.607858f * bounded_humidity_ratio) / pressure;
    }

    /**
     * Returns dry-bulb temperature given moist air specific volume, humidity
     * ratio, and pressure.
     *
     * @param moist_air_volume Specific volume of moist air in m³ kg⁻¹ of dry
     * air
     * @param humidity_ratio Humidity ratio in kg_H₂O kg_Air⁻¹
     * @param pressure Atmospheric pressure in Pa
     * @return Dry-bulb temperature in °C
     */
    public static float getDryBulbTemperature_From_MoistAirVolume_And_HumidityRatio(float moist_air_volume, float humidity_ratio, float pressure) {
        if (humidity_ratio < 0) {
            System.err.println(WARNING_HUMIDITY_RATIO + ": " + humidity_ratio);
        }
        float bounded_humidity_ratio = Math.max(humidity_ratio, MIN_HUM_RATIO);
        return getDegreeCelsius_From_Kelvin(moist_air_volume * pressure / (R_DA_SI * (1.0f + 1.607858f * bounded_humidity_ratio)));
    }

    /**
     * Returns moist air density given humidity ratio, dry bulb temperature, and
     * pressure.
     *
     * @param dry_bulb_temperature Dry-bulb temperature in °C
     * @param humidity_ratio Humidity ratio in kg_H₂O kg_Air⁻¹
     * @param pressure Atmospheric pressure in Pa
     * @return Moist air density in kg m⁻³
     */
    public static float getMoistAirDensity(float dry_bulb_temperature, float humidity_ratio, float pressure) {
        if (humidity_ratio < 0) {
            System.err.println(WARNING_HUMIDITY_RATIO + ": " + humidity_ratio);
        }
        float bounded_humidity_ratio = Math.max(humidity_ratio, MIN_HUM_RATIO);
        float moist_air_volume = getMoistAirVolume(dry_bulb_temperature, humidity_ratio, pressure);
        float moist_air_density = (1.0f + bounded_humidity_ratio) / moist_air_volume;
        return moist_air_density;
    }

    /**
     * Returns standard atmosphere barometric pressure, given the elevation
     * (altitude).
     *
     * @param altitude Altitude in m
     * @return Standard atmosphere barometric pressure in Pa
     */
    public static float getStandardAtmosphericPressure(float altitude) {
        return (float) (101325 * Math.pow(1 - 2.5577e-05 * altitude, 5.2559));
    }

    /**
     * Returns standard atmosphere temperature, given the elevation (altitude).
     *
     * @param altitude Altitude in m
     * @return Standard atmosphere dry-bulb temperature in °C
     */
    public static float getStandardAtmosphericTemperature(float altitude) {
        return 15.0f - 0.0065f * altitude;
    }

    /**
     * Returns sea level pressure given dry-bulb temperature, altitude above sea
     * level and pressure.
     *
     * Hess SL, Introduction to theoretical meteorology, Holt Rinehart and
     * Winston, NY 1959, ch. 6.5; Stull RB, Meteorology for scientists and
     * engineers, 2nd edition, Brooks/Cole 2000, ch. 1.
     *
     * @param station_pressure Observed station pressure in Pa
     * @param altitude Altitude in m
     * @param dry_bulb_temperature Dry-bulb temperature in °C
     * @return Sea level barometric pressure in Pa
     */
    public static float getSeaLevelPressure(float station_pressure, float altitude, float dry_bulb_temperature) {
        float tcolumn = dry_bulb_temperature * 0.0065f * altitude / 2.0f;
        float scale_height = 287.956f * getDegreeCelsius_From_Kelvin(tcolumn);
        return station_pressure * (float) Math.exp(altitude / scale_height);
    }

    /**
     * Returns station pressure from sea level pressure.
     *
     * @param sea_level_pressure Sea level barometric pressure in Pa
     * @param altitude Altitude in m
     * @param dry_bulb_temperature Dry-bulb temperature in °C
     * @return Station pressure in Pa
     */
    public static float getStationPressure(float sea_level_pressure, float altitude, float dry_bulb_temperature) {
        return sea_level_pressure / getSeaLevelPressure(1, altitude, dry_bulb_temperature);
    }

}
