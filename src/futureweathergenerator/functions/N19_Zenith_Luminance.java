/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.functions;

import futureweathergenerator.EPW.EPW;
import futureweathergenerator.functions.solar.PerezFunctions;
import futureweathergenerator.functions.solar.SolarFunctions;
import futureweathergenerator.Months;
import static futureweathergenerator.functions.solar.SolarHourAdjustment.By_Day;
import static futureweathergenerator.functions.solar.SolarHourAdjustment.By_Month;
import static futureweathergenerator.functions.solar.SolarHourAdjustment.None;

/**
 * Calculates N19 zenith luminance from solar model, extraterrestrial direct
 * normal radiation, future direct normal radiation, and future diffuse
 * horizontal radiation.
 *
 * @author eugenio
 */
public class N19_Zenith_Luminance {

    /**
     * Calculates N19 zenith luminance from solar model, extraterrestrial direct
     * normal radiation, future direct normal radiation, and future diffuse
     * horizontal radiation.
     *
     * @param morphedEPW morphed EPW object
     */
    public static void calculate(EPW morphedEPW) {
        // Requires pre-processing of:
        // N11 Extraterrestrial Direct Normal Radiation
        // N14 Direct Normal Radiation
        // N15 Diffuse Horizontal Radiation
        //
        // NOTICE:
        // CCWorldWeatherGen outputs different values for the 
        // illuminances and luminance (N16 to N19 fields).

        float longitude = morphedEPW.getEpw_location().getN3_longitude();
        float time_zone = morphedEPW.getEpw_location().getN4_time_zone();
        float latitude = morphedEPW.getEpw_location().getN2_latitude();

        for (int i = 0; i < Months.Abbreviation.values().length; i++) {
            calculate(morphedEPW, i, longitude, latitude, time_zone);
        }
    }

    public static void calculate(EPW morphedEPW, int month, float longitude, float latitude, float time_zone) {
        int[] month_row_ids = Months.getMonthRowIds(Months.Abbreviation.values()[month]);
        int first_row_id = month_row_ids[1];
        int last_row_id = month_row_ids[2];
        float solarHourAdjustment;
        for (int row_id = first_row_id; row_id < last_row_id; row_id++) {
            float future_diffuse_horizontal_radiation = morphedEPW.getEpw_data_fields().get(row_id).getN15_diffuse_horizontal_radiation();
            float future_extraterrestrial_direct_normal_radiation = morphedEPW.getEpw_data_fields().get(row_id).getN11_extraterrestrial_direct_normal_radiation();
            if (future_extraterrestrial_direct_normal_radiation == 0 || future_diffuse_horizontal_radiation == 0) {
                morphedEPW.getEpw_data_fields().get(row_id).setN19_zenith_luminance(0.0f);
                continue;
            }
            
            float future_direct_normal_radiation = morphedEPW.getEpw_data_fields().get(row_id).getN14_direct_normal_radiation();
            int julian_day = row_id / 24 + 1;
            switch (morphedEPW.getSolarHourAdjustmentOption()) {
                case None:
                    solarHourAdjustment = 1f;
                    break;
                default:
                case By_Month:
                    solarHourAdjustment = morphedEPW.solarHourAdjustment[month];
                    break;
                case By_Day:
                    solarHourAdjustment = morphedEPW.solarHourAdjustment[julian_day - 1];
                    break;
            }
            float day_angle = (float) SolarFunctions.getDayAngle(julian_day);
            float equation_of_time = (float) SolarFunctions.getEquationOfTime(day_angle);
            float declination = (float) SolarFunctions.getDeclination(day_angle);
            float local_standard_time = morphedEPW.getEpw_data_fields().get(row_id).getN4_hour();
            float local_standar_time_minutes_fraction = morphedEPW.getEpw_data_fields().get(row_id).getN5_minute() / 60f;
            float sin_Solar_Altitude_Angle_sum = 0.0f;
            float minutes = 0.0f;
            for (int min = 1; min <= 60; min++) {
                float solar_time = (float) SolarFunctions.getSolarTime(local_standard_time - solarHourAdjustment + (min / 60f) + local_standar_time_minutes_fraction, longitude, time_zone, equation_of_time);
                float hour_angle = (float) SolarFunctions.getHourAngle(solar_time);
                float sin_Solar_Altitude_Angle_step = (float) SolarFunctions.getSinSolarAltitudeAngle(latitude, declination, hour_angle);
                if (sin_Solar_Altitude_Angle_step > 0) {
                    sin_Solar_Altitude_Angle_sum += sin_Solar_Altitude_Angle_step;
                    minutes++;
                }
            }
            if (minutes == 0) {
                morphedEPW.getEpw_data_fields().get(row_id).setN19_zenith_luminance(0.0f);
                continue;
            }
            
            float sin_Solar_Altitude_Angle = sin_Solar_Altitude_Angle_sum / minutes;
            float solar_altitude_angle = (float) SolarFunctions.getSolarAltitudeAngle(sin_Solar_Altitude_Angle);
            solar_altitude_angle = solar_altitude_angle < 0.0f ? 0.0f : solar_altitude_angle;
            float solar_zenith_angle = (float) SolarFunctions.getSolarZenithAngleInRadians(solar_altitude_angle);
            float relative_optical_air_mass = (float) PerezFunctions.getRelativeOpticalAirMass(solar_altitude_angle);
            float sky_brightness = future_diffuse_horizontal_radiation * relative_optical_air_mass / future_extraterrestrial_direct_normal_radiation;
            float sky_clearness = (float) (((future_diffuse_horizontal_radiation + future_direct_normal_radiation) / future_diffuse_horizontal_radiation
                    + (1.041f * Math.pow(solar_zenith_angle, 3)))
                    / (1f + 1.041f * Math.pow(solar_zenith_angle, 3)));
            if (Float.isNaN(sky_clearness)) {
                morphedEPW.getEpw_data_fields().get(row_id).setN16_global_horizontal_illuminance(0.0f);
                continue;
            }
            
            int sky_clearness_category = PerezFunctions.getDiscreteSkyClearnessCategory(sky_clearness);
            float[] c = PerezFunctions.getZenithLuminancePrediction(sky_clearness_category);
            float calculated_zenith_luminance = (float) (future_diffuse_horizontal_radiation
                    * (c[0]
                    + (c[1] * Math.cos(solar_zenith_angle))
                    + (c[2] * Math.exp(-3f * solar_zenith_angle))
                    + (c[3] * sky_brightness)));
            morphedEPW.getEpw_data_fields().get(row_id).setN19_zenith_luminance(calculated_zenith_luminance);
            if (morphedEPW.getEpw_data_fields().get(row_id).getN19_zenith_luminance().isNaN()) {
                morphedEPW.getEpw_data_fields().get(row_id).setN19_zenith_luminance(0.0f);
            }
        }
    }
}
