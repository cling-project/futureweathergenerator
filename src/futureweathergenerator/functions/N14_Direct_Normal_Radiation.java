/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.functions;

import futureweathergenerator.EPW.EPW;
import futureweathergenerator.functions.solar.SolarFunctions;
import futureweathergenerator.Months;
import static futureweathergenerator.functions.solar.SolarHourAdjustment.By_Day;
import static futureweathergenerator.functions.solar.SolarHourAdjustment.By_Month;
import static futureweathergenerator.functions.solar.SolarHourAdjustment.None;

/**
 * Calculates N14 direct normal radiation using solar model, future global
 * horizontal radiation, and future diffuse horizontal radiation.
 *
 * @author eugenio
 */
public class N14_Direct_Normal_Radiation {

    /**
     * Calculates N14 direct normal radiation using solar model, future global
     * horizontal radiation, and future diffuse horizontal radiation.
     *
     * @param morphedEPW morphed EPW object
     */
    public static void calculate(EPW morphedEPW) {
        // Requires pre-processing of:
        // N13 Global Horizontal Radiation
        // N15 Diffuse Horizontal Radiation
        
        float longitude = morphedEPW.getEpw_location().getN3_longitude();
        float time_zone = morphedEPW.getEpw_location().getN4_time_zone();
        float latitude = morphedEPW.getEpw_location().getN2_latitude();
        
        for (int i = 0; i < Months.Abbreviation.values().length; i++) {
            calculate(morphedEPW, i, longitude, latitude, time_zone);
        }
    }
    
    public static void calculate(EPW morphedEPW, int i, float longitude, float latitude, float time_zone) {
        int[] month_row_ids = Months.getMonthRowIds(Months.Abbreviation.values()[i]);
        int number_of_days = month_row_ids[0] / 24;
        int first_row_id = month_row_ids[1];
        int row_id = first_row_id;
        for (int day = 0; day < number_of_days; day++) {
            row_id = calculate(morphedEPW, i, longitude, latitude, time_zone, row_id);
        }
    }
    
    public static int calculate(EPW morphedEPW, int month, float longitude, float latitude, float time_zone, int row_id) {
        float solarHourAdjustment;
        for (int hour = 0; hour < 24; hour++) {
            int julian_day = row_id / 24 + 1;
            switch (morphedEPW.getSolarHourAdjustmentOption()) {
                case None:
                    solarHourAdjustment = 1f;
                    break;
                default:
                case By_Month:
                    solarHourAdjustment = morphedEPW.solarHourAdjustment[month];
                    break;
                case By_Day:
                    solarHourAdjustment = morphedEPW.solarHourAdjustment[julian_day - 1];
                    break;
            }
            float day_angle = (float) SolarFunctions.getDayAngle(julian_day);
            float equation_of_time = (float) SolarFunctions.getEquationOfTime(day_angle);
            float declination = (float) SolarFunctions.getDeclination(day_angle);
            float local_standard_time = morphedEPW.getEpw_data_fields().get(row_id).getN4_hour();
            float local_standar_time_minutes_fraction = morphedEPW.getEpw_data_fields().get(row_id).getN5_minute() / 60f;
            float sin_Solar_Altitude_Angle_sum = 0.0f;
            float minutes = 0.0f;
            for (int min = 1; min <= 60; min++) {
                float solar_time = (float) SolarFunctions.getSolarTime(local_standard_time - solarHourAdjustment + (min / 60f) + local_standar_time_minutes_fraction, longitude, time_zone, equation_of_time);
                float hour_angle = (float) SolarFunctions.getHourAngle(solar_time);
                float sin_Solar_Altitude_Angle_step = (float) SolarFunctions.getSinSolarAltitudeAngle(latitude, declination, hour_angle);
                if (sin_Solar_Altitude_Angle_step > 0) {
                    sin_Solar_Altitude_Angle_sum += sin_Solar_Altitude_Angle_step;
                    minutes++;
                }
            }
            if (minutes == 0) {
                morphedEPW.getEpw_data_fields().get(row_id).setN14_direct_normal_radiation(0.0f);
                row_id++;
                continue;
            }
            
            float sin_Solar_Altitude_Angle = sin_Solar_Altitude_Angle_sum / minutes;
            float future_global_horizontal_radiation = morphedEPW.getEpw_data_fields().get(row_id).getN13_global_horizontal_radiation();
            float future_diffuse_horizontal_radiation = morphedEPW.getEpw_data_fields().get(row_id).getN15_diffuse_horizontal_radiation();
            float calculated_direct_normal_radiation = (future_global_horizontal_radiation - future_diffuse_horizontal_radiation) / sin_Solar_Altitude_Angle;
            // This is an abritrary correction to prevent abnormal DNI.
            if(calculated_direct_normal_radiation > future_global_horizontal_radiation * 3f) {
                calculated_direct_normal_radiation = future_global_horizontal_radiation * 3f;
            }
            morphedEPW.getEpw_data_fields().get(row_id).setN14_direct_normal_radiation(calculated_direct_normal_radiation);
            row_id++;
        }
        return row_id;
    }
}
