/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.functions;

import futureweathergenerator.EPW.EPW_Data_Fields;
import java.lang.reflect.Field;
import java.util.ArrayList;

/**
 * Utilities to help in calculations.
 *
 * @author eugenio
 */
public class Utilities {

    /**
     * Calculates daily min, avg, and max monthly average values.
     *
     * @param month_row_ids hour number of the month, starting hour, and ending
     * hour
     * @param epw_data_fields EPW data fields
     * @param column_name column name to use
     * @return array of daily min, avg, and max in a month
     */
    public static float[] getDailyMinAvgMax_MonthlyAverageValues(int[] month_row_ids, ArrayList<EPW_Data_Fields> epw_data_fields, String column_name) {
        int rows_in_month_days = month_row_ids[0];
        int first_row_id = month_row_ids[1];
        int last_row_id = month_row_ids[2];

        Float[][] month = new Float[rows_in_month_days / 24][24];
        int day_id = 0;
        int hour_id = 0;
        for (int row_id = first_row_id; row_id < last_row_id; row_id++) {
            Float value = (Float) getObject(row_id, epw_data_fields, column_name);
            month[day_id][hour_id] = value;
            hour_id++;
            if (hour_id == 24) {
                hour_id = 0;
                day_id++;
            }
        }

        Float[][] days = new Float[rows_in_month_days / 24][3];
        for (int i = 0; i < month.length; i++) {
            float max = Float.MIN_VALUE;
            float min = Float.MAX_VALUE;
            float avg = 0;
            for (Float value : month[i]) {
                if (value > max) {
                    max = value;
                }
                if (value < min) {
                    min = value;
                }
                avg += value;
            }
            avg /= 24;
            days[i][0] = min;
            days[i][1] = avg;
            days[i][2] = max;
        }

        float[] values = new float[3];
        for (Float[] day : days) {
            values[0] += day[0];
            values[1] += day[1];
            values[2] += day[2];
        }
        values[0] /= days.length;
        values[1] /= days.length;
        values[2] /= days.length;

        return values;
    }

    private static Object getObject(int row_id, ArrayList<EPW_Data_Fields> epw_data_fields, String column_name) {
        try {
            Field field = EPW_Data_Fields.class.getDeclaredField(column_name);
            field.setAccessible(true);
            try {
                return field.get(epw_data_fields.get(row_id));
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                throw new UnsupportedOperationException(ex);
            }
        } catch (NoSuchFieldException | SecurityException ex) {
            throw new UnsupportedOperationException(ex);
        }
    }

    /**
     * Calculates the transition step between two month for a given interpolated
     * variable according to the number of hours.
     *
     * @param month month
     * @param row_id hour in the year
     * @param first_row_id first hour in the year for the given month
     * @param last_row_id last hour in the year for the given month
     * @param interpolated_variable monthly interpolated variable
     * @param numberOfHoursToSmooth number of hours to smooth
     * @return step size
     */
    public static float getMonthlyTransitionVariableStep(int month, int row_id, int first_row_id, int last_row_id, float[] interpolated_variable, float numberOfHoursToSmooth) {
        float variable_step = 0.0f;
        if (month > 0 && (row_id - first_row_id < numberOfHoursToSmooth)) {
            variable_step = (interpolated_variable[month - 1] - interpolated_variable[month]) / 2.0f / numberOfHoursToSmooth;
        }
        if (month < 11 && (last_row_id - row_id < numberOfHoursToSmooth)) {
            variable_step = (interpolated_variable[month + 1] - interpolated_variable[month]) / 2.0f / numberOfHoursToSmooth;
        }
        return variable_step;
    }

    /**
     * Calculates the delta for a specific hour.
     *
     * @param row_id hour in the year
     * @param first_row_id first hour in the year of the given month
     * @param last_row_id last hour in the year of the given month
     * @param variable_step variable step
     * @param numberOfHoursToSmooth number of hours to smooth transition
     * @return delta for the give hour
     */
    public static float getMonthlyTransitionVariableHourDelta(int row_id, int first_row_id, int last_row_id, float variable_step, float numberOfHoursToSmooth) {
        float step_start = (row_id - first_row_id) > numberOfHoursToSmooth ? 0.0f : numberOfHoursToSmooth - (row_id - first_row_id);
        float step_end = (last_row_id - row_id) > numberOfHoursToSmooth ? 0.0f : numberOfHoursToSmooth - (last_row_id - row_id);
        float step = Math.max(step_start, step_end);
        return variable_step * step;
    }
}
