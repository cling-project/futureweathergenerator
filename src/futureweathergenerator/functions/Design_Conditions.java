/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.functions;

import futureweathergenerator.EPW.EPW;
import futureweathergenerator.EPW.EPW_Design_Condition;

/**
 * Resets design conditions object.
 *
 * @author eugenio
 */
public class Design_Conditions {

    /**
     * Resets design conditions object.
     *
     * @param morphedEPW EPW morphed data
     */
    public static void reset(EPW morphedEPW) {
        morphedEPW.getEpw_design_conditions().setN1_number_of_design_conditions(0);
        morphedEPW.getEpw_design_conditions().setAn_Design_Condition_Sources(new EPW_Design_Condition[0]);
    }
}
