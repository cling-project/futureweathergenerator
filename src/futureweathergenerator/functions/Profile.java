/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.functions;

import java.util.Stack;

/**
 * Profiles the running time of a code. Place Profile.tic() where to start and
 * Profile.toc() to stop counting.
 *
 * @author david
 */
public class Profile {

    private static final Stack<Long> stack = new Stack();

    /**
     * Starts counting time.
     */
    public static void tic() {
        System.err.println("--TIC Starting Profile");
        Profile.stack.push(System.currentTimeMillis());
    }

    /**
     * Stops counting time.
     */
    public static void toc() {
        System.err.println("--TOC Execution time (" + stack.size() + ") " + (System.currentTimeMillis() - stack.pop()) + " ms");
    }
}
