/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.functions;

import futureweathergenerator.EPW.EPW;
import futureweathergenerator.Months;

/**
 * Sets N26 present weather code.
 *
 * @author eugenio
 */
public class N27_Present_Weather_Code {

    /**
     * Sets N26 present weather code as missing.
     *
     * @param morphedEPW morphed EPW object
     */
    public static void setMissing(EPW morphedEPW) {
        for (int i = 0; i < Months.Abbreviation.values().length; i++) {
            int[] month_row_ids = Months.getMonthRowIds(Months.Abbreviation.values()[i]);
            int first_row_id = month_row_ids[1];
            int last_row_id = month_row_ids[2];
            for (int row_id = first_row_id; row_id < last_row_id; row_id++) {
                morphedEPW.getEpw_data_fields().get(row_id).setN27_present_weather_code("999999999");
            }
        }
    }
    
    /**
     * Keeps the same N27 present weather code of the current EPW object.
     * 
     * @param epw current EPW object
     * @param morphedEPW morphed EPW object
     */
    public static void keepOriginal(EPW epw, EPW morphedEPW) {
        for(int i = 0; i < Months.Abbreviation.values().length; i++) {
            int[] month_row_ids = Months.getMonthRowIds(Months.Abbreviation.values()[i]);
            int first_row_id = month_row_ids[1];
            int last_row_id = month_row_ids[2];
            for(int row_id = first_row_id; row_id < last_row_id; row_id++) {
                String present_weather_code = epw.getEpw_data_fields().get(row_id).getN27_present_weather_code();
                morphedEPW.getEpw_data_fields().get(row_id).setN27_present_weather_code(present_weather_code);
            }
        }
    }
}
