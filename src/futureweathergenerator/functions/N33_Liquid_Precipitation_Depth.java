/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.functions;

import futureweathergenerator.EPW.EPW;
import futureweathergenerator.GCM.InterpolatedVariablesScenario;
import futureweathergenerator.Months;

/**
 * Sets N33 liquid precipitation depth.
 *
 * @author eugenio
 */
public class N33_Liquid_Precipitation_Depth {

    /**
     * Morphs N33 liquid precipitation depth.
     *
     * @param epw current EPW object
     * @param scenario GCM scenario
     * @param morphedEPW morphed EPW object
     * @param numberOfHoursToSmooth number of hours to smooth between months
     */
    public static void morph(EPW epw, InterpolatedVariablesScenario scenario, EPW morphedEPW, float numberOfHoursToSmooth) {
        for (int i = 0; i < Months.Abbreviation.values().length; i++) {
            int[] month_row_ids = Months.getMonthRowIds(Months.Abbreviation.values()[i]);
            int first_row_id = month_row_ids[1];
            int last_row_id = month_row_ids[2];
            for (int row_id = first_row_id; row_id < last_row_id; row_id++) {
                float scaling_factor_monthly_precipitation_step = Utilities.getMonthlyTransitionVariableStep(i, row_id, first_row_id, last_row_id, scenario.getInterpolated_precipitation(), numberOfHoursToSmooth);
                float scaling_factor_monthly_precipitation_delta = Utilities.getMonthlyTransitionVariableHourDelta(row_id, first_row_id, last_row_id, scaling_factor_monthly_precipitation_step, numberOfHoursToSmooth);
                float scaling_factor_monthly_precipitation = scenario.getInterpolated_precipitation()[i] + scaling_factor_monthly_precipitation_delta;
                float weather_precipitation = epw.getEpw_data_fields().get(row_id).getN33_liquid_precipitation_depth();
                float calculated_precipitation = scaling_factor_monthly_precipitation * weather_precipitation;
                morphedEPW.getEpw_data_fields().get(row_id).setN33_liquid_precipitation_depth((float) calculated_precipitation);
            }
        }
    }

    /**
     * Sets N33 liquid precipitation depth as missing.
     *
     * @param morphedEPW morphed EPW object
     */
    public static void setMissing(EPW morphedEPW) {
        for (int i = 0; i < Months.Abbreviation.values().length; i++) {
            int[] month_row_ids = Months.getMonthRowIds(Months.Abbreviation.values()[i]);
            int first_row_id = month_row_ids[1];
            int last_row_id = month_row_ids[2];
            for (int row_id = first_row_id; row_id < last_row_id; row_id++) {
                morphedEPW.getEpw_data_fields().get(row_id).setN33_liquid_precipitation_depth(Float.NaN);
            }
        }
    }
}
