/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.functions;

import futureweathergenerator.EPW.EPW;
import futureweathergenerator.GCM.InterpolatedVariablesScenario;
import futureweathergenerator.Months;

/**
 * Morphs N9 atmospheric pressure.
 *
 * @author eugenio
 */
public class N9_Atmospheric_Pressure {

    /**
     * Morphs N9 atmospheric pressure.
     *
     * @param epw current EPW object
     * @param scenario GCM scenario
     * @param morphedEPW morphed EPW object
     * @param numberOfHoursToSmooth number of hours to smooth between months
     */
    public static void morph(EPW epw, InterpolatedVariablesScenario scenario, EPW morphedEPW, float numberOfHoursToSmooth) {
        for (int i = 0; i < Months.Abbreviation.values().length; i++) {
            int[] month_row_ids = Months.getMonthRowIds(Months.Abbreviation.values()[i]);
            int first_row_id = month_row_ids[1];
            int last_row_id = month_row_ids[2];
            for (int row_id = first_row_id; row_id < last_row_id; row_id++) {
                float month_mean_sea_level_pressure_step = Utilities.getMonthlyTransitionVariableStep(i, row_id, first_row_id, last_row_id, scenario.getInterpolated_mean_sea_level_pressure(), numberOfHoursToSmooth);
                float month_mean_sea_level_pressure_delta = Utilities.getMonthlyTransitionVariableHourDelta(row_id, first_row_id, last_row_id, month_mean_sea_level_pressure_step, numberOfHoursToSmooth);
                float month_mean_sea_level_pressure = scenario.getInterpolated_mean_sea_level_pressure()[i] + month_mean_sea_level_pressure_delta;
                float calculated_atmospheric_station_pressure = epw.getEpw_data_fields().get(row_id).getN9_atmospheric_station_pressure() + (month_mean_sea_level_pressure * 100.0f);
                morphedEPW.getEpw_data_fields().get(row_id).setN9_atmospheric_station_pressure((float) calculated_atmospheric_station_pressure);
            }
        }
    }
}
