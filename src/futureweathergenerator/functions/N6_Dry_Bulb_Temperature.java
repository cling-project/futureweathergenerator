/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.functions;

import futureweathergenerator.EPW.EPW;
import futureweathergenerator.GCM.InterpolatedVariablesScenario;
import futureweathergenerator.Months;
import futureweathergenerator.functions.solar.SunriseSunsetDayLength;
import futureweathergenerator.UHI.UrbanHeatIslandDataPoint;
import static futureweathergenerator.functions.solar.SunriseSunsetDayLength.calculateDaySunriseSunsetAndDayLength;
import static futureweathergenerator.functions.solar.SunriseSunsetDayLength.calculateIdealizedHourlyTemperatures;
import java.util.Locale;

/**
 * Morphs N6 dry bulb temperature.
 *
 * @author eugenio
 */
public class N6_Dry_Bulb_Temperature {

    /**
     * Morphs N6 dry bulb temperature.
     *
     * @param epw current EPW object
     * @param scenario GCM scenario
     * @param morphedEPW morphed EPW objects
     * @param numberOfHoursToSmooth number of hours to smooth between months
     */
    public static void morph(EPW epw, InterpolatedVariablesScenario scenario, EPW morphedEPW, float numberOfHoursToSmooth) {
        float[] weather_month_minimum = new float[12];
        float[] weather_month_mean = new float[12];
        float[] weather_month_maximum = new float[12];
        for (int i = 0; i < Months.Abbreviation.values().length; i++) {
            int[] month_row_ids = Months.getMonthRowIds(Months.Abbreviation.values()[i]);
            float[] temperatures = Utilities.getDailyMinAvgMax_MonthlyAverageValues(month_row_ids, epw.getEpw_data_fields(), "N6_dry_bulb_temperature");
            weather_month_minimum[i] = temperatures[0];
            weather_month_mean[i] = temperatures[1];
            weather_month_maximum[i] = temperatures[2];
        }
        for (int i = 0; i < Months.Abbreviation.values().length; i++) {
            int[] month_row_ids = Months.getMonthRowIds(Months.Abbreviation.values()[i]);
            int first_row_id = month_row_ids[1];
            int last_row_id = month_row_ids[2];
            for (int row_id = first_row_id; row_id < last_row_id; row_id++) {
                float weather_month_average_minimum_dry_bulb_temperature_step = Utilities.getMonthlyTransitionVariableStep(i, row_id, first_row_id, last_row_id, weather_month_minimum, numberOfHoursToSmooth);
                float weather_month_average_dry_bulb_temperature_step = Utilities.getMonthlyTransitionVariableStep(i, row_id, first_row_id, last_row_id, weather_month_mean, numberOfHoursToSmooth);
                float weather_month_average_maximum_dry_bulb_temperature_step = Utilities.getMonthlyTransitionVariableStep(i, row_id, first_row_id, last_row_id, weather_month_maximum, numberOfHoursToSmooth);
                float weather_month_average_minimum_dry_bulb_temperature_delta = Utilities.getMonthlyTransitionVariableHourDelta(row_id, first_row_id, last_row_id, weather_month_average_minimum_dry_bulb_temperature_step, numberOfHoursToSmooth);
                float weather_month_average_dry_bulb_temperature_delta = Utilities.getMonthlyTransitionVariableHourDelta(row_id, first_row_id, last_row_id, weather_month_average_dry_bulb_temperature_step, numberOfHoursToSmooth);
                float weather_month_average_maximum_dry_bulb_temperature_delta = Utilities.getMonthlyTransitionVariableHourDelta(row_id, first_row_id, last_row_id, weather_month_average_maximum_dry_bulb_temperature_step, numberOfHoursToSmooth);
                float weather_difference_dry_bulb_temperature = 
                        (weather_month_maximum[i] + weather_month_average_maximum_dry_bulb_temperature_delta) -
                        (weather_month_minimum[i] + weather_month_average_minimum_dry_bulb_temperature_delta);
                float month_maximum_daily_temperature_step = Utilities.getMonthlyTransitionVariableStep(i, row_id, first_row_id, last_row_id, scenario.getInterpolated_maximum_daily_temperature(), numberOfHoursToSmooth);
                float month_minimum_daily_temperature_step = Utilities.getMonthlyTransitionVariableStep(i, row_id, first_row_id, last_row_id, scenario.getInterpolated_minimum_daily_temperature(), numberOfHoursToSmooth);
                float month_mean_daily_temperature_step = Utilities.getMonthlyTransitionVariableStep(i, row_id, first_row_id, last_row_id, scenario.getInterpolated_mean_daily_temperature(), numberOfHoursToSmooth);
                float month_maximum_daily_temperature_delta = Utilities.getMonthlyTransitionVariableHourDelta(row_id, first_row_id, last_row_id, month_maximum_daily_temperature_step, numberOfHoursToSmooth);
                float month_minimum_daily_temperature_delta = Utilities.getMonthlyTransitionVariableHourDelta(row_id, first_row_id, last_row_id, month_minimum_daily_temperature_step, numberOfHoursToSmooth);
                float month_mean_daily_temperature_delta = Utilities.getMonthlyTransitionVariableHourDelta(row_id, first_row_id, last_row_id, month_mean_daily_temperature_step, numberOfHoursToSmooth);
                float month_maximum_daily_temperature = scenario.getInterpolated_maximum_daily_temperature()[i] + month_maximum_daily_temperature_delta;
                float month_minimum_daily_temperature = scenario.getInterpolated_minimum_daily_temperature()[i] + month_minimum_daily_temperature_delta;
                float month_mean_daily_temperature = scenario.getInterpolated_mean_daily_temperature()[i] + month_mean_daily_temperature_delta;
                float timeframe_difference_daily_temperature = month_maximum_daily_temperature - month_minimum_daily_temperature;
                float month_scaling_factor = timeframe_difference_daily_temperature / weather_difference_dry_bulb_temperature;
                float weather_dry_bulb_temperature = epw.getEpw_data_fields().get(row_id).getN6_dry_bulb_temperature();
                float calculated_dry_bulb_temperature = weather_dry_bulb_temperature + month_mean_daily_temperature
                        + month_scaling_factor * (weather_dry_bulb_temperature - (weather_month_mean[i] + weather_month_average_dry_bulb_temperature_delta));
                morphedEPW.getEpw_data_fields().get(row_id).setN6_dry_bulb_temperature((float) calculated_dry_bulb_temperature);
            }
        }
    }
    
    
    public static void morph(UrbanHeatIslandDataPoint nearest_uhi_location, EPW uhiEPW, int EPW_ORIGINAL_LCZ, int TARGET_UHI_LCZ, boolean doEcho) {
        // Calculated deltas from LCZs.
        nearest_uhi_location.selectBufferTaCluster(uhiEPW, EPW_ORIGINAL_LCZ);
        nearest_uhi_location.calculateTaDifferences(TARGET_UHI_LCZ);
        
        float DIFF_SEM1_TMAX = nearest_uhi_location.getTa_DIFF_SEM1_TMAX();
        float DIFF_SEM1_TMIN = nearest_uhi_location.getTa_DIFF_SEM1_TMIN();
        float DIFF_SEM2_TMAX = nearest_uhi_location.getTa_DIFF_SEM2_TMAX();
        float DIFF_SEM2_TMIN = nearest_uhi_location.getTa_DIFF_SEM2_TMIN();
        
        if (doEcho) {
            System.out.println(String.format(Locale.ROOT,
                    "Urban Heat Island - Air Temperature Difference %n"
                            + "\t1st Semestre: Daytime: %s °C, Nightime: %s °C.%n"
                            + "\t2st Semestre: Daytime: %s °C, Nightime: %s °C.",
                    new Object[]{
                        nearest_uhi_location.getTa_DIFF_SEM1_TMAX(),
                        nearest_uhi_location.getTa_DIFF_SEM1_TMIN(),
                        nearest_uhi_location.getTa_DIFF_SEM2_TMAX(),
                        nearest_uhi_location.getTa_DIFF_SEM2_TMIN()
                    }
            ));
        }

        // Determines the normalized annual temperature cycle for the given latitude (clear sky).
        SunriseSunsetDayLength[] sunriseSunsetDayLengths = new SunriseSunsetDayLength[365];
        float max_day_length = Float.NEGATIVE_INFINITY;
        float min_day_length = Float.POSITIVE_INFINITY;
        for (int i = 0; i < 365; i++) {
            sunriseSunsetDayLengths[i] = calculateDaySunriseSunsetAndDayLength(uhiEPW.getEpw_location().getN2_latitude(), i);
            if (max_day_length < sunriseSunsetDayLengths[i].getDaylength()) {
                max_day_length = sunriseSunsetDayLengths[i].getDaylength();
            }
            if (min_day_length > sunriseSunsetDayLengths[i].getDaylength()) {
                min_day_length = sunriseSunsetDayLengths[i].getDaylength();
            }
        }
        
        int theta = 37;
        float DIFF_TMAX;
        float TMAX_stretch_sin;
        if(DIFF_SEM1_TMAX == 0 && DIFF_SEM2_TMAX == 0) {
            DIFF_TMAX = 0;
            TMAX_stretch_sin = 0;
        } else {
            if(uhiEPW.getEpw_location().getN2_latitude() >= 0) {
                // North hemisphere.
                DIFF_TMAX = DIFF_SEM2_TMAX;
                TMAX_stretch_sin = (DIFF_SEM2_TMAX - DIFF_SEM1_TMAX) / DIFF_SEM2_TMAX;
                if(Float.isInfinite(TMAX_stretch_sin)) {
                    float TMAX_stretch_sin_temp = (DIFF_SEM1_TMAX - DIFF_SEM2_TMAX) / DIFF_SEM1_TMAX;
                    if(TMAX_stretch_sin < 0) {
                        TMAX_stretch_sin = -1 * TMAX_stretch_sin_temp;
                    } else {
                        TMAX_stretch_sin = TMAX_stretch_sin_temp;
                    }
                    DIFF_TMAX = DIFF_SEM1_TMAX;
                }
            } else {
                // South hemisphere.
                DIFF_TMAX = DIFF_SEM1_TMAX;
                TMAX_stretch_sin = (DIFF_SEM1_TMAX - DIFF_SEM2_TMAX) / DIFF_SEM1_TMAX;
                if(Float.isInfinite(TMAX_stretch_sin)) {
                    float TMAX_stretch_sin_temp = (DIFF_SEM2_TMAX - DIFF_SEM1_TMAX) / DIFF_SEM2_TMAX;
                    if(TMAX_stretch_sin < 0) {
                        TMAX_stretch_sin = TMAX_stretch_sin_temp;
                    } else {
                        TMAX_stretch_sin = -1 * TMAX_stretch_sin_temp;
                    }
                    DIFF_TMAX = DIFF_SEM2_TMAX;
                }
            }
        }
        
        float DIFF_TMIN;
        float TMIN_stretch_sin;
        if(DIFF_SEM1_TMIN == 0 && DIFF_SEM2_TMIN == 0) {
            DIFF_TMIN = 0;
            TMIN_stretch_sin = 0;
        } else {
            if(uhiEPW.getEpw_location().getN2_latitude() >= 0) {
                // North hemisphere.
                DIFF_TMIN = DIFF_SEM2_TMIN;
                TMIN_stretch_sin = (DIFF_SEM2_TMIN - DIFF_SEM1_TMIN) / DIFF_SEM2_TMIN;
                if(Float.isInfinite(TMIN_stretch_sin)) {
                    float TMIN_stretch_sin_temp = (DIFF_SEM1_TMIN - DIFF_SEM2_TMIN) / DIFF_SEM1_TMIN;
                    if(TMIN_stretch_sin < 0) {
                        TMIN_stretch_sin = -1 * TMIN_stretch_sin_temp;
                    } else {
                        TMIN_stretch_sin = TMIN_stretch_sin_temp;
                    }
                    DIFF_TMIN = DIFF_SEM1_TMIN;
                }
            } else {
                // South hemisphere.
                DIFF_TMIN = DIFF_SEM1_TMIN;
                TMIN_stretch_sin = (DIFF_SEM1_TMIN - DIFF_SEM2_TMIN) / DIFF_SEM1_TMIN;
                if(Float.isInfinite(TMIN_stretch_sin)) {
                    float TMIN_stretch_sin_temp = (DIFF_SEM2_TMIN - DIFF_SEM1_TMIN) / DIFF_SEM2_TMIN;
                    if(TMIN_stretch_sin < 0) {
                        TMIN_stretch_sin = TMIN_stretch_sin_temp;
                    } else {
                        TMIN_stretch_sin = -1 * TMIN_stretch_sin_temp;
                    }
                    DIFF_TMIN = DIFF_SEM2_TMIN;
                }
            }
        }
        
        float[] TMAX_annual_cycle_day_fractions = new float[365];
        float[] TMIN_annual_cycle_day_fractions = new float[365];
        for (int i = 0; i < 365; i++) {
            int j = i < 365 - theta ? i + theta : i - (365 - theta);
            
            // TMAX fractions
            float TMAX_annual_cycle_day_fraction = 
                    (sunriseSunsetDayLengths[i].getDaylength() - min_day_length) /
                    (max_day_length - min_day_length);
            TMAX_annual_cycle_day_fraction -= 1;
            TMAX_annual_cycle_day_fraction *= TMAX_stretch_sin;
            TMAX_annual_cycle_day_fraction += 1;
            TMAX_annual_cycle_day_fractions[j] = TMAX_annual_cycle_day_fraction;
            
            // TMIN fractions
            float TMIN_annual_cycle_day_fraction = 
                    (sunriseSunsetDayLengths[i].getDaylength() - min_day_length) /
                    (max_day_length - min_day_length);
            TMIN_annual_cycle_day_fraction -= 1;
            TMIN_annual_cycle_day_fraction *= TMIN_stretch_sin;
            TMIN_annual_cycle_day_fraction += 1;
            TMIN_annual_cycle_day_fractions[j] = TMIN_annual_cycle_day_fraction;
        }
        
        float TMAX_previous = Float.NEGATIVE_INFINITY;
        for (int hour = 364 * 24; hour < 364 * 24 + 24; hour++) {
            float weather_dry_bulb_temperature = uhiEPW.getEpw_data_fields().get(hour).getN6_dry_bulb_temperature();
            if (TMAX_previous < weather_dry_bulb_temperature) {
                TMAX_previous = weather_dry_bulb_temperature;
            }
        }
        for (int julian_day = 0; julian_day < 365; julian_day++) {
            // Determines the longest day EPW's minimum and maximum temperatures.
            float TMAX = Float.NEGATIVE_INFINITY;
            float TMIN = Float.POSITIVE_INFINITY;
            for (int hour = julian_day * 24; hour < julian_day * 24 + 24; hour++) {
                float weather_dry_bulb_temperature = uhiEPW.getEpw_data_fields().get(hour).getN6_dry_bulb_temperature();
                if (TMAX < weather_dry_bulb_temperature) {
                    TMAX = weather_dry_bulb_temperature;
                }
                if (TMIN > weather_dry_bulb_temperature) {
                    TMIN = weather_dry_bulb_temperature;
                }
            }

            float TMIN_next = Float.POSITIVE_INFINITY;
            int start_row_id = julian_day == 364 ? 0 : (julian_day + 1) * 24;
            for (int hour = start_row_id; hour < start_row_id + 24; hour++) {
                float weather_dry_bulb_temperature = uhiEPW.getEpw_data_fields().get(hour).getN6_dry_bulb_temperature();
                if (TMIN_next > weather_dry_bulb_temperature) {
                    TMIN_next = weather_dry_bulb_temperature;
                }
            }

            // Determines idealized hourly temperatures for rural area.
            SunriseSunsetDayLength ssdl_previous = sunriseSunsetDayLengths[julian_day == 0 ? 364 : julian_day - 1];
            SunriseSunsetDayLength ssdl = sunriseSunsetDayLengths[julian_day];
            SunriseSunsetDayLength ssdl_next = sunriseSunsetDayLengths[julian_day == 364 ? 0 : julian_day + 1];
            float[] idealized_hourly_rural_temperatures = calculateIdealizedHourlyTemperatures(
                    TMAX_previous,
                    TMAX,
                    TMIN,
                    TMIN_next,
                    ssdl_previous,
                    ssdl,
                    ssdl_next
            );

            // Determines idealized hourly temperatures for urban area.
            float TMAX_annual_cycle_day_fraction_previous = TMAX_annual_cycle_day_fractions[julian_day == 0 ? 364 : julian_day - 1];
            float TMAX_annual_cycle_day_fraction = TMAX_annual_cycle_day_fractions[julian_day];
            float TMIN_annual_cycle_day_fraction = TMIN_annual_cycle_day_fractions[julian_day];
            float TMIN_annual_cycle_day_fraction_next = TMIN_annual_cycle_day_fractions[julian_day == 364 ? 0 : julian_day + 1];
            float[] idealized_hourly_urban_temperatures = calculateIdealizedHourlyTemperatures(TMAX_previous + DIFF_TMAX * TMAX_annual_cycle_day_fraction_previous,
                    TMAX + DIFF_TMAX * TMAX_annual_cycle_day_fraction,
                    TMIN + DIFF_TMIN * TMIN_annual_cycle_day_fraction,
                    TMIN_next + DIFF_SEM2_TMIN * TMIN_annual_cycle_day_fraction_next,
                    ssdl_previous,
                    ssdl,
                    ssdl_next);

            // Calculate urban heat island effect hourly deltas.
            float[] idealized_hourly_uhi_deltas = calculateIdealizedHourlyDeltas(idealized_hourly_urban_temperatures, idealized_hourly_rural_temperatures);

            // Apply annual temperature cycle and uhi deltas to N6 Dry bulb temperature.
            for (int hour = 0; hour < 24; hour++) {
                float hour_uhi_delta = idealized_hourly_uhi_deltas[hour];
                
                // Sets value.
                int row_id = julian_day * 24 + hour;
                float uhi_dry_bulb_temperature = 
                        uhiEPW.getEpw_data_fields().get(row_id).getN6_dry_bulb_temperature() +
                        hour_uhi_delta;
                uhiEPW.getEpw_data_fields().get(row_id).setN6_dry_bulb_temperature(uhi_dry_bulb_temperature);
            }

            // Set variables for the next day.
            TMAX_previous = TMAX;
        }

        // Add comments.
        String comment = uhiEPW.getEpw_comments().get(0).getA1_comment().replace("\"", "");
        comment += " Dry-bulb tempearature adjusted to add Urban Heat Island effect for the location " +
                nearest_uhi_location.getName() + " (" + nearest_uhi_location.getIso3() + "). "
                + "1st Semestre as a Daytime temperature difference of " + DIFF_SEM1_TMAX + " °C and Nightime of " + DIFF_SEM1_TMIN + " °C. "
                + "2st Semestre as a Daytime temperature difference of " + DIFF_SEM2_TMAX + " °C and Nightime of " + DIFF_SEM1_TMIN + " °C.";
        comment = "\"" + comment + "\"";
        uhiEPW.getEpw_comments().get(0).setA1_comment(comment);
    }
    
    private static float[] calculateIdealizedHourlyDeltas(float[] idealized_hourly_urban_temperatures, float[] idealized_hourly_rural_temperatures) {
        float[] deltas = new float[24];
        for (int i = 0; i < 24; i++) {
            deltas[i] = idealized_hourly_urban_temperatures[i] - idealized_hourly_rural_temperatures[i];
        }
        return deltas;
    }
    
}
