/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.functions;

import futureweathergenerator.EPW.EPW;
import futureweathergenerator.GCM.InterpolatedVariablesScenario;
import futureweathergenerator.Months;
import futureweathergenerator.functions.psychrometric.Psychrometric2017;

/**
 * Morphs N8 relative humidity.
 *
 * @author eugenio
 */
public class N8_Relative_Humidity {

    /**
     * Morphs N8 relative humidity. It requires atmospheric station pressure.
     *
     * @param epw current EPW object
     * @param scenario GCM scenario
     * @param morphedEPW morphed EPW object
     * @param numberOfHoursToSmooth number of hours to smooth between months
     */
    public static void morph(EPW epw, InterpolatedVariablesScenario scenario, EPW morphedEPW, float numberOfHoursToSmooth) {
        // Requires pre-processing of:
        // N6 Dry bulb temperature
        // N9 Atmospheric station pressure
        for (int i = 0; i < Months.Abbreviation.values().length; i++) {
            int[] month_row_ids = Months.getMonthRowIds(Months.Abbreviation.values()[i]);
            int first_row_id = month_row_ids[1];
            int last_row_id = month_row_ids[2];
            for (int row_id = first_row_id; row_id < last_row_id; row_id++) {
                float current_dry_bulb_temperature = epw.getEpw_data_fields().get(row_id).getN6_dry_bulb_temperature();
                float current_relative_humidity = epw.getEpw_data_fields().get(row_id).getN8_relative_humidity();
                float current_atmospheric_station_pressure = epw.getEpw_data_fields().get(row_id).getN9_atmospheric_station_pressure();
                float current_humidity_ratio = Psychrometric2017.getHumidityRatio_From_RelativeHumidity(current_dry_bulb_temperature, current_relative_humidity / 100.0f, current_atmospheric_station_pressure);
                float current_specific_humidity = Psychrometric2017.getSpecificHumidity_From_HumidityRatio(current_humidity_ratio);
                float month_specific_humidity_step = Utilities.getMonthlyTransitionVariableStep(i, row_id, first_row_id, last_row_id, scenario.getInterpolated_specific_humidity(), numberOfHoursToSmooth);
                float month_specific_humidity_delta = Utilities.getMonthlyTransitionVariableHourDelta(row_id, first_row_id, last_row_id, month_specific_humidity_step, numberOfHoursToSmooth);
                float month_specific_humidity = scenario.getInterpolated_specific_humidity()[i] + month_specific_humidity_delta;
                float future_specific_humidity = current_specific_humidity + month_specific_humidity;
                float future_humidity_ratio = Psychrometric2017.getHumidityRatio_From_SpecificHumidity(future_specific_humidity);
                float future_atmospheric_station_pressure = morphedEPW.getEpw_data_fields().get(row_id).getN9_atmospheric_station_pressure();
                float future_dry_bulb_temperature = morphedEPW.getEpw_data_fields().get(row_id).getN6_dry_bulb_temperature();
                float calculated_relative_humidity = Psychrometric2017.getRelativeHumidity_From_HumidityRatio(future_dry_bulb_temperature, future_humidity_ratio, future_atmospheric_station_pressure);
                morphedEPW.getEpw_data_fields().get(row_id).setN8_relative_humidity(calculated_relative_humidity * 100.0f);
            }
        }
    }
    
    public static void calculate(EPW epw, EPW uhiEPW) {
        // N6 Dry bulb temperature
        // N9 Atmospheric station pressure
        for (int i = 0; i < Months.Abbreviation.values().length; i++) {
            int[] month_row_ids = Months.getMonthRowIds(Months.Abbreviation.values()[i]);
            int first_row_id = month_row_ids[1];
            int last_row_id = month_row_ids[2];
            for (int row_id = first_row_id; row_id < last_row_id; row_id++) {
                float current_dry_bulb_temperature = epw.getEpw_data_fields().get(row_id).getN6_dry_bulb_temperature();
                float current_relative_humidity = epw.getEpw_data_fields().get(row_id).getN8_relative_humidity();
                float current_atmospheric_station_pressure = epw.getEpw_data_fields().get(row_id).getN9_atmospheric_station_pressure();
                float current_humidity_ratio = Psychrometric2017.getHumidityRatio_From_RelativeHumidity(current_dry_bulb_temperature, current_relative_humidity / 100.0f, current_atmospheric_station_pressure);
                float current_specific_humidity = Psychrometric2017.getSpecificHumidity_From_HumidityRatio(current_humidity_ratio);
                float uhi_humidity_ratio = Psychrometric2017.getHumidityRatio_From_SpecificHumidity(current_specific_humidity);
                float uhi_atmospheric_station_pressure = uhiEPW.getEpw_data_fields().get(row_id).getN9_atmospheric_station_pressure();
                float uhi_dry_bulb_temperature = uhiEPW.getEpw_data_fields().get(row_id).getN6_dry_bulb_temperature();
                float calculated_relative_humidity = Psychrometric2017.getRelativeHumidity_From_HumidityRatio(uhi_dry_bulb_temperature, uhi_humidity_ratio, uhi_atmospheric_station_pressure);
                uhiEPW.getEpw_data_fields().get(row_id).setN8_relative_humidity(calculated_relative_humidity * 100.0f);
            }
        }
    }
}
