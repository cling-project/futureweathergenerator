/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.functions;

import futureweathergenerator.EPW.EPW;
import futureweathergenerator.EPW.EPW_Ground_Temperatures_Depth;
import futureweathergenerator.Months;

/**
 * Calculates new ground temperatures for 0.5 m, 2 m, 4 m depths.
 *
 * @author eugenio
 */
public class Ground_Temperatures {

    /**
     * Sets new ground temperatures for 0.5 m, 2 m, 4 m depths from future dry
     * bulb temperatures.
     *
     * @param morphedEPW morphed EPW object
     */
    public static void morph(EPW morphedEPW) {
        float[] depths = new float[]{0.5f, 2.0f, 4.0f}; // m
        morphedEPW.getEpw_ground_temperatures().setN1_number_of_ground_temperature_depths(depths.length);
        morphedEPW.getEpw_ground_temperatures().setNn_ground_temperatures_depth(new EPW_Ground_Temperatures_Depth[depths.length]);

        int month_with_minimum_dry_bulb_temperature = 0;
        float future_minimum_dry_bulb_temperature_future_day = Float.MAX_VALUE;
        float[] future_averages_monthly_dry_bulb_temperature = new float[12];
        for (int i = 0; i < Months.Abbreviation.values().length; i++) {
            int[] month_row_ids = Months.getMonthRowIds(Months.Abbreviation.values()[i]);
            int rows_in_month_days = month_row_ids[0];
            int first_row_id = month_row_ids[1];
            float future_average_monthly_dry_bulb_temperature = 0.0f;
            int countRows = first_row_id;
            for (int day = 0; day < rows_in_month_days / 24.0; day++) {
                for (int hour = 0; hour < 24; hour++) {
                    float future_dry_bulb_temperature = morphedEPW.getEpw_data_fields().get(countRows).getN6_dry_bulb_temperature();
                    future_average_monthly_dry_bulb_temperature += future_dry_bulb_temperature;
                    if (future_minimum_dry_bulb_temperature_future_day > future_dry_bulb_temperature) {
                        future_minimum_dry_bulb_temperature_future_day = future_dry_bulb_temperature;
                        month_with_minimum_dry_bulb_temperature = i;
                    }
                    countRows++;
                }
            }
            future_average_monthly_dry_bulb_temperature /= (float) rows_in_month_days;
            future_averages_monthly_dry_bulb_temperature[i] = future_average_monthly_dry_bulb_temperature;
        }
        float minimum_future_average_monthly_dry_bulb_temperature = Float.MAX_VALUE;
        float maximum_future_average_monthly_dry_bulb_temperature = Float.MIN_VALUE;
        float future_annual_mean_dry_bulb_temperature = 0.0f;
        for (int i = 0; i < future_averages_monthly_dry_bulb_temperature.length; i++) {
            if (minimum_future_average_monthly_dry_bulb_temperature > future_averages_monthly_dry_bulb_temperature[i]) {
                minimum_future_average_monthly_dry_bulb_temperature = future_averages_monthly_dry_bulb_temperature[i];
            }
            if (maximum_future_average_monthly_dry_bulb_temperature < future_averages_monthly_dry_bulb_temperature[i]) {
                maximum_future_average_monthly_dry_bulb_temperature = future_averages_monthly_dry_bulb_temperature[i];
            }
            future_annual_mean_dry_bulb_temperature += future_averages_monthly_dry_bulb_temperature[i];
        }
        future_annual_mean_dry_bulb_temperature /= future_averages_monthly_dry_bulb_temperature.length;
        float amplitude_warmest_coldest_future_average_monthly_dry_bulb_temperature = (maximum_future_average_monthly_dry_bulb_temperature - minimum_future_average_monthly_dry_bulb_temperature) / 2.0f;

        // Use the month of the day with the minimum temperature
        float[] median_day_of_the_month_containing_minimum_surface_temperature = new float[]{15, 46, 74, 95, 135, 166, 196, 227, 258, 288, 319, 349};
        float phase_lag = (median_day_of_the_month_containing_minimum_surface_temperature[month_with_minimum_dry_bulb_temperature] * 0.017214f) + 0.341787f;

        // Depths
        float thermal_diffusivity_of_the_ground_soil = (float) (Math.PI / (0.055741824 * 365.0));
        for (int d = 0; d < depths.length; d++) {
            float x = (float)(depths[d] * Math.pow(thermal_diffusivity_of_the_ground_soil, 0.5));
            float y = (float)((Math.pow(Math.exp(-x), 2.0) - 2.0 * Math.exp(-x) * Math.cos(x) + 1.0f)
                    / (2.0 * Math.pow(x, 2.0)));
            float z = (float)((1.0 - Math.exp(-x) * (Math.cos(x) + Math.sin(x)))
                    / (1.0 - Math.exp(-x) * (Math.cos(x) - Math.sin(x))));

            float[] future_mean_ground_temperature_for_given_day = new float[365];
            for (int i = 0; i < Months.Abbreviation.values().length; i++) {
                int[] month_row_ids = Months.getMonthRowIds(Months.Abbreviation.values()[i]);
                int first_row_id = month_row_ids[1];
                int last_row_id = month_row_ids[2];
                for (int row_id = first_row_id; row_id < last_row_id; row_id += 24) {
                    float day_number = (int) Math.floor(row_id / 24.0);
                    future_mean_ground_temperature_for_given_day[(int) day_number] += future_annual_mean_dry_bulb_temperature
                            - (amplitude_warmest_coldest_future_average_monthly_dry_bulb_temperature
                            * Math.cos(2.0 * (Math.PI / 365.0) * day_number - phase_lag - Math.atan(z)) * Math.pow(y, 0.5));
                }
            }

            float[] future_month_ground_temperatures = new float[12];
            for (int i = 0; i < Months.Abbreviation.values().length; i++) {
                int[] month_row_ids = Months.getMonthRowIds(Months.Abbreviation.values()[i]);
                int first_row_id = month_row_ids[1];
                int last_row_id = month_row_ids[2];

                float future_month_ground_temperature = 0;
                int number_of_days = 0;
                for (int row_id = first_row_id; row_id < last_row_id; row_id += 24) {
                    float day_number = (int) Math.floor(row_id / 24.0);
                    future_month_ground_temperature += future_mean_ground_temperature_for_given_day[(int) day_number];
                    number_of_days++;
                }
                future_month_ground_temperatures[i] = future_month_ground_temperature / (float) number_of_days;
            }

            morphedEPW.getEpw_ground_temperatures().getNn_ground_temperatures_depth()[d] = new EPW_Ground_Temperatures_Depth(
                    depths[d],
                    "",
                    "",
                    "",
                    future_month_ground_temperatures[0],
                    future_month_ground_temperatures[1],
                    future_month_ground_temperatures[2],
                    future_month_ground_temperatures[3],
                    future_month_ground_temperatures[4],
                    future_month_ground_temperatures[5],
                    future_month_ground_temperatures[6],
                    future_month_ground_temperatures[7],
                    future_month_ground_temperatures[8],
                    future_month_ground_temperatures[9],
                    future_month_ground_temperatures[10],
                    future_month_ground_temperatures[11]);
        }
    }
}
