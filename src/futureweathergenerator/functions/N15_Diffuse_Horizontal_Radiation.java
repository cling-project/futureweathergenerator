/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.functions;

import futureweathergenerator.EPW.EPW;
import futureweathergenerator.functions.solar.SolarFunctions;
import futureweathergenerator.Months;
import static futureweathergenerator.functions.solar.SolarHourAdjustment.By_Day;
import static futureweathergenerator.functions.solar.SolarHourAdjustment.By_Month;
import static futureweathergenerator.functions.solar.SolarHourAdjustment.None;

/**
 * Morphs N15 diffuse horizontal radiation.
 *
 * @author eugenio
 */
public class N15_Diffuse_Horizontal_Radiation {

    /**
     * Morphs N15 diffuse horizontal radiation.
     *
     * @param morphedEPW morphed EPW object
     */
    public static void calculate(EPW morphedEPW) {
        // Requires pre-processing of:
        // N10 Extraterrestrial Horizontal Radiation
        // N13 Global Horizontal Radiation

        float longitude = morphedEPW.getEpw_location().getN3_longitude();
        float time_zone = morphedEPW.getEpw_location().getN4_time_zone();
        float latitude = morphedEPW.getEpw_location().getN2_latitude();

        for (int i = 0; i < Months.Abbreviation.values().length; i++) {
            calculate(morphedEPW, i, longitude, latitude, time_zone);
        }
    }

    public static void calculate(EPW morphedEPW, int i, float longitude, float latitude, float time_zone) {
        int[] month_row_ids = Months.getMonthRowIds(Months.Abbreviation.values()[i]);
        int number_of_days = month_row_ids[0] / 24;
        int first_row_id = month_row_ids[1];
        int row_id = first_row_id;
        for (int day = 0; day < number_of_days; day++) {
            row_id = calculate(morphedEPW, i, longitude, latitude, time_zone, row_id);
        }
    }

    public static int calculate(EPW morphedEPW, int month, float longitude, float latitude, float time_zone, int row_id) {
        int original_row_id = row_id;
        float solarHourAdjustment;
        float fehr = 0f;
        float fghr = 0f;

        for (int hour = 0; hour < 24; hour++) {
            fehr += morphedEPW.getEpw_data_fields().get(row_id).getN10_extraterrestrial_horizontal_radiation();
            fghr += morphedEPW.getEpw_data_fields().get(row_id).getN13_global_horizontal_radiation();
            row_id++;
        }
        float clear_index_day = fghr / fehr;

        row_id = original_row_id;
        for (int hour = 0; hour < 24; hour++) {
            float future_extraterrestrial_horizontal_radiation = morphedEPW.getEpw_data_fields().get(row_id).getN10_extraterrestrial_horizontal_radiation();
            float future_global_horizontal_radiation = morphedEPW.getEpw_data_fields().get(row_id).getN13_global_horizontal_radiation();
            if (future_extraterrestrial_horizontal_radiation == 0 || future_global_horizontal_radiation == 0) {
                morphedEPW.getEpw_data_fields().get(row_id).setN15_diffuse_horizontal_radiation(0.0f);
                row_id++;
                continue;
            }
            
            int julian_day = row_id / 24 + 1;
            switch (morphedEPW.getSolarHourAdjustmentOption()) {
                case None:
                    solarHourAdjustment = 1f;
                    break;
                default:
                case By_Month:
                    solarHourAdjustment = morphedEPW.solarHourAdjustment[month];
                    break;
                case By_Day:
                    solarHourAdjustment = morphedEPW.solarHourAdjustment[julian_day - 1];
                    break;
            }
            float day_angle = (float) SolarFunctions.getDayAngle(julian_day);
            float equation_of_time = (float) SolarFunctions.getEquationOfTime(day_angle);
            float declination = (float) SolarFunctions.getDeclination(day_angle);
            float local_standard_time = morphedEPW.getEpw_data_fields().get(row_id).getN4_hour();
            float local_standar_time_minutes_fraction = morphedEPW.getEpw_data_fields().get(row_id).getN5_minute() / 60f;
            float sin_Solar_Altitude_Angle_sum = 0.0f;
            float minutes = 0.0f;
            for (int min = 1; min <= 60; min++) {
                float solar_time = (float) SolarFunctions.getSolarTime(local_standard_time - solarHourAdjustment + (min / 60f) + local_standar_time_minutes_fraction, longitude, time_zone, equation_of_time);
                float hour_angle = (float) SolarFunctions.getHourAngle(solar_time);
                float sin_Solar_Altitude_Angle_step = (float) SolarFunctions.getSinSolarAltitudeAngle(latitude, declination, hour_angle);
                if (sin_Solar_Altitude_Angle_step > 0) {
                    sin_Solar_Altitude_Angle_sum += sin_Solar_Altitude_Angle_step;
                    minutes++;
                }
            }

            if (minutes == 0) {
                morphedEPW.getEpw_data_fields().get(row_id).setN15_diffuse_horizontal_radiation(0.0f);
                row_id++;
                continue;
            }

            float sin_Solar_Altitude_Angle = sin_Solar_Altitude_Angle_sum / minutes;
            float solar_altitude_angle = (float) SolarFunctions.getSolarAltitudeAngle(sin_Solar_Altitude_Angle);

            float clear_index_hour = future_global_horizontal_radiation / future_extraterrestrial_horizontal_radiation;
            float solar_time = (float) SolarFunctions.getSolarTime(local_standard_time + local_standar_time_minutes_fraction, longitude, time_zone, equation_of_time);
            
            float solar_zenith_angle = (float) SolarFunctions.getSolarZenithAngleInDegrees(solar_altitude_angle);
            float clear_sky_model_global_horizontal_irradiation
                    = 926f * (float) Math.pow(Math.cos(Math.toRadians(solar_zenith_angle)), 1.29f)
                    + 131f * (float) Math.pow(Math.cos(Math.toRadians(solar_zenith_angle)), 0.60f);

            float calculated_diffuse_horizontal_radiation;
            switch (morphedEPW.getDiffuseIrradiationModel()) {
                
                default:
                case Ridley_Boland_Lauret_2010:
                    // Ridley, B., Boland, J. & Lauret, P. Modelling of diffuse solar fraction with multiple predictors. Renew. Energy 35, 478–483 (2010).
                    int hourPrevious = row_id - 1;
                    if (hourPrevious < 0) {
                        hourPrevious = 8759;
                    }
                    int hourNext = row_id + 1;
                    if (hourNext > 8759) {
                        hourNext = 0;
                    }
                    float future_global_horizontal_radiation_previous = morphedEPW.getEpw_data_fields().get(hourPrevious).getN13_global_horizontal_radiation();
                    float future_extraterrestrial_horizontal_radiation_previous = morphedEPW.getEpw_data_fields().get(hourPrevious).getN10_extraterrestrial_horizontal_radiation();
                    float clearIndexHour_previous = future_global_horizontal_radiation_previous / future_extraterrestrial_horizontal_radiation_previous;
                    float future_global_horizontal_radiation_next = morphedEPW.getEpw_data_fields().get(hourNext).getN13_global_horizontal_radiation();
                    float future_extraterrestrial_horizontal_radiation_next = morphedEPW.getEpw_data_fields().get(hourNext).getN10_extraterrestrial_horizontal_radiation();
                    float clearIndexHour_next = future_global_horizontal_radiation_next / future_extraterrestrial_horizontal_radiation_next;
                    float clear_persistence;
                    if (future_extraterrestrial_horizontal_radiation_previous == 0
                            && future_extraterrestrial_horizontal_radiation_next > 0) {
                        clear_persistence = clearIndexHour_next;
                    } else if (future_extraterrestrial_horizontal_radiation_previous > 0
                            && future_extraterrestrial_horizontal_radiation_next == 0) {
                        clear_persistence = clearIndexHour_previous;
                    } else if (future_extraterrestrial_horizontal_radiation_previous > 0
                            && future_extraterrestrial_horizontal_radiation_next > 0) {
                        clear_persistence = (clearIndexHour_previous + clearIndexHour_next) / 2.0f;
                    } else {
                        clear_persistence = 0.0f;
                    }
            
                    calculated_diffuse_horizontal_radiation = future_global_horizontal_radiation
                            * (1.0f
                            / (1.0f + (float) Math.exp(-5.38f
                                    + (6.63f * clear_index_hour)
                                    + (0.006f * solar_time)
                                    - (0.007f * solar_altitude_angle)
                                    + (1.75f * clear_index_day)
                                    + (1.31f * clear_persistence)
                            )));
                    break;
                    
                case Engerer_2015:
                    // Engerer, N. A. Minute resolution estimates of the diffuse fraction of global irradiance for southeastern Australia. Sol. Energy 116, 215–237 (2015).
                    // Paulescu, E. & Blaga, R. A simple and reliable empirical model with two predictors for estimating 1-minute diffuse fraction. Sol. Energy 180, 75–84 (2019).
                    // Biga, A. J. & Rosa, R. Contribution to the study of the solar radiation climate of Lisbon. Sol. Energy 23, 61–67 (1979).
                    float diffuse_fraction_from_cloud_enhancement = Math.max(0f, 1f - (clear_sky_model_global_horizontal_irradiation / future_global_horizontal_radiation));
                    float delta_clearness_index_of_clear_sky_global_horizontal_irradiance_and_clearness_index = (clear_sky_model_global_horizontal_irradiation / future_extraterrestrial_horizontal_radiation) - clear_index_hour;
                    calculated_diffuse_horizontal_radiation = future_global_horizontal_radiation * (0.042336f
                            + ((1f - 0.042336f) / (1f + (float) Math.exp(-3.7912f
                                    + (7.5479f * clear_index_hour)
                                    + (-0.010036f * solar_time)
                                    + (0.003148f * solar_zenith_angle)
                                    + (-5.3146f * delta_clearness_index_of_clear_sky_global_horizontal_irradiance_and_clearness_index)
                            )))
                            + 1.7073f * diffuse_fraction_from_cloud_enhancement);
                    break;

                case Paulescu_Blaga_2019:
                    // Paulescu, E. & Blaga, R. A simple and reliable empirical model with two predictors for estimating 1-minute diffuse fraction. Sol. Energy 180, 75–84 (2019).
                    float condtion_ikt4 = clear_index_hour >= 0.367f ? 1 : 0;
                    float condtion_ikt6 = clear_index_hour >= 0.734f ? 1 : 0;
                    float condtion_iktd8 = clear_index_day >= 0.462f ? 1 : 0;
                    calculated_diffuse_horizontal_radiation = future_global_horizontal_radiation * (1.0119f
                            + (-0.0316f * clear_index_hour)
                            + (-0.0294f * clear_index_day)
                            + (-1.6567f * (clear_index_hour - 0.367f)) * condtion_ikt4
                            + (1.8982f * (clear_index_hour - 0.734f)) * condtion_ikt6
                            + (-0.8548f * (clear_index_day - 0.462f)) * condtion_iktd8);
                    break;
            }

            morphedEPW.getEpw_data_fields().get(row_id).setN15_diffuse_horizontal_radiation(calculated_diffuse_horizontal_radiation);
            row_id++;
        }
        return row_id;
    }
}
