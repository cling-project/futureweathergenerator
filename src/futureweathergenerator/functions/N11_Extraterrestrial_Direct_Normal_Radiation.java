/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.functions;

import futureweathergenerator.EPW.EPW;
import futureweathergenerator.functions.solar.SolarFunctions;
import futureweathergenerator.Months;
import static futureweathergenerator.functions.solar.SolarHourAdjustment.By_Day;
import static futureweathergenerator.functions.solar.SolarHourAdjustment.By_Month;
import static futureweathergenerator.functions.solar.SolarHourAdjustment.None;

/**
 * Calculates N11 extraterrestrial direct normal radiation from solar model and
 * future extraterrestrial direct normal radiation.
 *
 * @author eugenio
 */
public class N11_Extraterrestrial_Direct_Normal_Radiation {

    /**
     * Calculates N11 extraterrestrial direct normal radiation from solar model
     * and future extraterrestrial direct normal radiation.
     *
     * @param morphedEPW morphed EPW object
     */
    public static void calculate(EPW morphedEPW) {
        float latitude = morphedEPW.getEpw_location().getN2_latitude();
        float longitude = morphedEPW.getEpw_location().getN3_longitude();
        float time_zone = morphedEPW.getEpw_location().getN4_time_zone();

        for (int i = 0; i < Months.Abbreviation.values().length; i++) {
            calculate(morphedEPW, i, longitude, latitude, time_zone);
        }
    }

    public static void calculate(EPW morphedEPW, int month, float longitude, float latitude, float time_zone) {
        int[] month_row_ids = Months.getMonthRowIds(Months.Abbreviation.values()[month]);
        int first_row_id = month_row_ids[1];
        int last_row_id = month_row_ids[2];
        float solarHourAdjustment;
        for (int row_id = first_row_id; row_id < last_row_id; row_id++) {
            int julian_day = row_id / 24 + 1;
            switch (morphedEPW.getSolarHourAdjustmentOption()) {
                case None:
                    solarHourAdjustment = 1f;
                    break;
                default:
                case By_Month:
                    solarHourAdjustment = morphedEPW.solarHourAdjustment[month];
                    break;
                case By_Day:
                    solarHourAdjustment = morphedEPW.solarHourAdjustment[julian_day - 1];
                    break;
            }
            float day_angle = (float) SolarFunctions.getDayAngle(julian_day);
            float equation_of_time = (float) SolarFunctions.getEquationOfTime(day_angle);
            float declination = (float) SolarFunctions.getDeclination(day_angle);
            float correction_factor_for_solar_distance = (float) SolarFunctions.getCorrectionFactorForSolarDistance(day_angle);
            float calculated_extraterrestrial_direct_normal_radiation = (float) SolarFunctions.getExtraterrestrialDirectNormalRadiation(correction_factor_for_solar_distance);
            float local_standard_time = morphedEPW.getEpw_data_fields().get(row_id).getN4_hour();
            float local_standar_time_minutes_fraction = morphedEPW.getEpw_data_fields().get(row_id).getN5_minute() / 60f;
            int minutes = 0;
            for (int min = 1; min <= 60; min++) {
                float solar_time = (float) SolarFunctions.getSolarTime(local_standard_time - solarHourAdjustment + (min / 60f) + local_standar_time_minutes_fraction, longitude, time_zone, equation_of_time);
                float hour_angle = (float) SolarFunctions.getHourAngle(solar_time);
                float sin_Solar_Altitude_Angle = (float) SolarFunctions.getSinSolarAltitudeAngle(latitude, declination, hour_angle);
                minutes = sin_Solar_Altitude_Angle > 0 ? minutes + 1 : minutes;
            }
            calculated_extraterrestrial_direct_normal_radiation = (minutes / 60.0f) * calculated_extraterrestrial_direct_normal_radiation;
            morphedEPW.getEpw_data_fields().get(row_id).setN11_extraterrestrial_direct_normal_radiation(calculated_extraterrestrial_direct_normal_radiation);
        }
    }
}
