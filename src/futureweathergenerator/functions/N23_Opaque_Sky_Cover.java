/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.functions;

import futureweathergenerator.EPW.EPW;
import futureweathergenerator.Months;

/**
 * Calculates N23 opaque sky cover from future total sky cover.
 *
 * @author eugenio
 */
public class N23_Opaque_Sky_Cover {

    /**
     * Calculates N23 opaque sky cover from future total sky cover.
     *
     * @param epw current EPW object
     * @param morphedEPW morphed EPW object
     */
    public static void calculate(EPW epw, EPW morphedEPW) {
        // Requires pre-processing of:
        // N22 Total Sky Cover
        for (int i = 0; i < Months.Abbreviation.values().length; i++) {
            int[] month_row_ids = Months.getMonthRowIds(Months.Abbreviation.values()[i]);
            int first_row_id = month_row_ids[1];
            int last_row_id = month_row_ids[2];
            for (int row_id = first_row_id; row_id < last_row_id; row_id++) {
                float future_total_sky_cover = morphedEPW.getEpw_data_fields().get(row_id).getN22_total_sky_cover();
                float weather_opaque_sky_cover = epw.getEpw_data_fields().get(row_id).getN23_opaque_sky_cover();
                float weather_total_sky_cover = epw.getEpw_data_fields().get(row_id).getN22_total_sky_cover();
                float calculated_opaque_sky_cover;
                if(weather_total_sky_cover == 0.0) {
                    calculated_opaque_sky_cover = Math.round(future_total_sky_cover / 2.0);
                } else {
                    calculated_opaque_sky_cover = Math.round((future_total_sky_cover * weather_opaque_sky_cover) / weather_total_sky_cover);
                }
                if(calculated_opaque_sky_cover > future_total_sky_cover) {
                    calculated_opaque_sky_cover = future_total_sky_cover;
                }
                morphedEPW.getEpw_data_fields().get(row_id).setN23_opaque_sky_cover((float) calculated_opaque_sky_cover);
            }
        }
    }
}
