/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.functions.solar;

/**
 * Presents a set of Perez Model functions. The source of these functions is
 * ﻿Perez R, Ineichen P, Seals R, Michalsky J, Stewart R. Modeling daylight
 * availability and irradiance components from direct and global irradiance. Sol
 * Energy 1990;44:271–89. https://doi.org/10.1016/0038-092X(90)90055-H. and
 * relative optical air mass from Kasten F, Young AT. Revised Optical Air Mass
 * Tables. Appl Opt 2000;28:4735–8.
 *
 * @author eugenio
 */
public class PerezFunctions {

    /**
     * Returns discrete sky clearness category [1 to 8] from sky clearness.
     *
     * @param sky_clearness sky clearness
     * @return discrete sky clearness category
     */
    public static int getDiscreteSkyClearnessCategory(float sky_clearness) {
        if (sky_clearness < 1.065) { // Perez paper actually states sky clearness above minimum 1.0.
            return 1;
        } else if (sky_clearness >= 1.065 && sky_clearness < 1.230) {
            return 2;
        } else if (sky_clearness >= 1.230 && sky_clearness < 1.500) {
            return 3;
        } else if (sky_clearness >= 1.500 && sky_clearness < 1.950) {
            return 4;
        } else if (sky_clearness >= 1.950 && sky_clearness < 2.800) {
            return 5;
        } else if (sky_clearness >= 2.800 && sky_clearness < 4.500) {
            return 6;
        } else if (sky_clearness >= 4.500 && sky_clearness < 6.200) {
            return 7;
        } else if (sky_clearness >= 6.200) {
            return 8;
        } else {
            throw new UnsupportedOperationException("Error: Sky Clearness out of bounds: " + sky_clearness);
        }
    }

    private static final float[][] global_luminous_efficacy = new float[][]{
        {96.63f, -0.47f, 11.5f, -9.16f},
        {107.54f, 0.79f, 1.79f, -1.19f},
        {98.73f, 0.7f, 4.4f, -6.95f},
        {92.72f, 0.56f, 8.36f, -8.31f},
        {86.73f, 0.98f, 7.1f, -10.94f},
        {88.34f, 1.39f, 6.06f, -7.6f},
        {78.63f, 1.47f, 4.93f, -11.37f},
        {99.65f, 1.86f, -4.46f, -3.15f}
    };

    /**
     * Returns the global luminous efficacy coefficients.
     *
     * @param sky_clearness_category discrete sky clearness category
     * @return global luminous efficacy coefficients
     */
    public static float[] getGlobalLuminousEfficacy(int sky_clearness_category) {
        return global_luminous_efficacy[sky_clearness_category - 1];
    }

    private static final float[][] diffuse_luminous_efficacy = new float[][]{
        {97.24f, -0.46f, 12f, -8.91f},
        {107.22f, 1.15f, 0.59f, -3.95f},
        {104.97f, 2.96f, -5.53f, -8.77f},
        {102.39f, 5.59f, -13.95f, -13.9f},
        {100.71f, 5.94f, -22.75f, -23.74f},
        {106.42f, 3.83f, -36.15f, -28.83f},
        {141.88f, 1.9f, -53.24f, -14.03f},
        {152.23f, 0.35f, -45.27f, -7.98f}
    };

    /**
     * Returns diffuse luminous efficacy coefficients.
     *
     * @param sky_clearness_category discrete sky clearness category
     * @return diffuse luminous efficacy coefficients
     */
    public static float[] getDiffuseLuminousEfficacy(int sky_clearness_category) {
        return diffuse_luminous_efficacy[sky_clearness_category - 1];
    }

    private static final float[][] direct_luminous_efficacy = new float[][]{
        {57.2f, -4.55f, -2.98f, 117.12f},
        {98.99f, -3.46f, -1.21f, 12.38f},
        {109.83f, -4.9f, -1.71f, -8.81f},
        {110.34f, -5.84f, -1.99f, -4.56f},
        {106.36f, -3.97f, -1.75f, -6.16f},
        {107.19f, -1.25f, -1.51f, -26.73f},
        {105.75f, 0.77f, -1.26f, -34.44f},
        {101.18f, 1.58f, -1.1f, -8.29f}
    };

    /**
     * Returns direct luminous efficacy coefficients.
     *
     * @param sky_clearness_category discrete sky clearness category
     * @return direct luminous efficacy coefficients
     */
    public static float[] getDirectLuminousEfficacy(int sky_clearness_category) {
        return direct_luminous_efficacy[sky_clearness_category - 1];
    }

    private static final float[][] zenithLuminancePrediction = new float[][]{
        {40.86f, 26.77f, -29.59f, -45.75f},
        {26.58f, 14.73f, 58.46f, -21.25f},
        {19.34f, 2.28f, 100f, 0.25f},
        {13.25f, -1.39f, 124.79f, 15.66f},
        {14.47f, -5.09f, 160.09f, 9.13f},
        {19.76f, -3.88f, 154.61f, -19.21f},
        {28.39f, -9.67f, 151.58f, -69.39f},
        {42.91f, -19.62f, 130.8f, -164.08f}
    };

    /**
     * Returns zenith luminance prediction coefficients.
     *
     * @param sky_clearness_category discrete sky clearness category
     * @return zenith luminance prediction coefficients
     */
    public static float[] getZenithLuminancePrediction(int sky_clearness_category) {
        return zenithLuminancePrediction[sky_clearness_category - 1];
    }

    /**
     * Returns relative optical air mass from solar altitude angle.
     *
     * @param solar_altitude_angle solar altitude angle
     * @return relative optical air mass
     */
    public static float getRelativeOpticalAirMass(float solar_altitude_angle) {
        String val = String.format("%.1f", solar_altitude_angle);
        switch (val.replace(",", ".")) {
            case "0.0" -> {
                return 38.0868f;
            }
            case "0.1" -> {
                return 36.5581f;
            }
            case "0.2" -> {
                return 35.1223f;
            }
            case "0.3" -> {
                return 33.7726f;
            }
            case "0.4" -> {
                return 32.5026f;
            }
            case "0.5" -> {
                return 31.3064f;
            }
            case "0.6" -> {
                return 30.1786f;
            }
            case "0.7" -> {
                return 29.1144f;
            }
            case "0.8" -> {
                return 28.1091f;
            }
            case "0.9" -> {
                return 27.1588f;
            }
            case "1.0" -> {
                return 26.2595f;
            }
            case "1.1" -> {
                return 25.4079f;
            }
            case "1.2" -> {
                return 24.6006f;
            }
            case "1.3" -> {
                return 23.8349f;
            }
            case "1.4" -> {
                return 23.1078f;
            }
            case "1.5" -> {
                return 22.417f;
            }
            case "1.6" -> {
                return 21.7601f;
            }
            case "1.7" -> {
                return 21.1349f;
            }
            case "1.8" -> {
                return 20.5395f;
            }
            case "1.9" -> {
                return 19.972f;
            }
            case "2.0" -> {
                return 19.4308f;
            }
            case "2.1" -> {
                return 18.9143f;
            }
            case "2.2" -> {
                return 18.4209f;
            }
            case "2.3" -> {
                return 17.9493f;
            }
            case "2.4" -> {
                return 17.4983f;
            }
            case "2.5" -> {
                return 17.0667f;
            }
            case "2.6" -> {
                return 16.6534f;
            }
            case "2.7" -> {
                return 16.2573f;
            }
            case "2.8" -> {
                return 15.8775f;
            }
            case "2.9" -> {
                return 15.5131f;
            }
            case "3.0" -> {
                return 15.1633f;
            }
            case "3.1" -> {
                return 14.8273f;
            }
            case "3.2" -> {
                return 14.5043f;
            }
            case "3.3" -> {
                return 14.1937f;
            }
            case "3.4" -> {
                return 13.8949f;
            }
            case "3.5" -> {
                return 13.6072f;
            }
            case "3.6" -> {
                return 13.3301f;
            }
            case "3.7" -> {
                return 13.063f;
            }
            case "3.8" -> {
                return 12.8056f;
            }
            case "3.9" -> {
                return 12.5572f;
            }
            case "4.0" -> {
                return 12.3174f;
            }
            case "4.1" -> {
                return 12.0859f;
            }
            case "4.2" -> {
                return 11.8623f;
            }
            case "4.3" -> {
                return 11.6462f;
            }
            case "4.4" -> {
                return 11.4372f;
            }
            case "4.5" -> {
                return 11.235f;
            }
            case "4.6" -> {
                return 11.0394f;
            }
            case "4.7" -> {
                return 10.85f;
            }
            case "4.8" -> {
                return 10.6665f;
            }
            case "4.9" -> {
                return 10.4887f;
            }
            case "5.0" -> {
                return 10.3164f;
            }
            case "5.1" -> {
                return 10.1493f;
            }
            case "5.2" -> {
                return 9.9872f;
            }
            case "5.3" -> {
                return 9.8299f;
            }
            case "5.4" -> {
                return 9.6772f;
            }
            case "5.5" -> {
                return 9.5289f;
            }
            case "5.6" -> {
                return 9.3848f;
            }
            case "5.7" -> {
                return 9.2448f;
            }
            case "5.8" -> {
                return 9.1087f;
            }
            case "5.9" -> {
                return 8.9763f;
            }
            case "6.0" -> {
                return 8.8475f;
            }
            case "6.1" -> {
                return 8.7222f;
            }
            case "6.2" -> {
                return 8.6002f;
            }
            case "6.3" -> {
                return 8.4815f;
            }
            case "6.4" -> {
                return 8.3658f;
            }
            case "6.5" -> {
                return 8.2531f;
            }
            case "6.6" -> {
                return 8.1433f;
            }
            case "6.7" -> {
                return 8.0363f;
            }
            case "6.8" -> {
                return 7.9319f;
            }
            case "6.9" -> {
                return 7.83f;
            }
            case "7.0" -> {
                return 7.7307f;
            }
            case "7.1" -> {
                return 7.6338f;
            }
            case "7.2" -> {
                return 7.5392f;
            }
            case "7.3" -> {
                return 7.4468f;
            }
            case "7.4" -> {
                return 7.3566f;
            }
            case "7.5" -> {
                return 7.2684f;
            }
            case "7.6" -> {
                return 7.1823f;
            }
            case "7.7" -> {
                return 7.0982f;
            }
            case "7.8" -> {
                return 7.0159f;
            }
            case "7.9" -> {
                return 6.9355f;
            }
            case "8.0" -> {
                return 6.8568f;
            }
            case "8.1" -> {
                return 6.7799f;
            }
            case "8.2" -> {
                return 6.7046f;
            }
            case "8.3" -> {
                return 6.631f;
            }
            case "8.4" -> {
                return 6.5589f;
            }
            case "8.5" -> {
                return 6.4883f;
            }
            case "8.6" -> {
                return 6.4192f;
            }
            case "8.7" -> {
                return 6.3515f;
            }
            case "8.8" -> {
                return 6.2852f;
            }
            case "8.9" -> {
                return 6.2202f;
            }
            case "9.0" -> {
                return 6.1565f;
            }
            case "9.1" -> {
                return 6.0942f;
            }
            case "9.2" -> {
                return 6.033f;
            }
            case "9.3" -> {
                return 5.973f;
            }
            case "9.4" -> {
                return 5.9142f;
            }
            case "9.5" -> {
                return 5.8565f;
            }
            case "9.6" -> {
                return 5.8f;
            }
            case "9.7" -> {
                return 5.7445f;
            }
            case "9.8" -> {
                return 5.69f;
            }
            case "9.9" -> {
                return 5.6365f;
            }
            case "10.0" -> {
                return 5.5841f;
            }
            case "10.1" -> {
                return 5.5325f;
            }
            case "10.2" -> {
                return 5.482f;
            }
            case "10.3" -> {
                return 5.4323f;
            }
            case "10.4" -> {
                return 5.3835f;
            }
            case "10.5" -> {
                return 5.3356f;
            }
            case "10.6" -> {
                return 5.2885f;
            }
            case "10.7" -> {
                return 5.2422f;
            }
            case "10.8" -> {
                return 5.1968f;
            }
            case "10.9" -> {
                return 5.1521f;
            }
            case "11.0" -> {
                return 5.1081f;
            }
            case "11.1" -> {
                return 5.0649f;
            }
            case "11.2" -> {
                return 5.0225f;
            }
            case "11.3" -> {
                return 4.9807f;
            }
            case "11.4" -> {
                return 4.9396f;
            }
            case "11.5" -> {
                return 4.8992f;
            }
            case "11.6" -> {
                return 4.8595f;
            }
            case "11.7" -> {
                return 4.8204f;
            }
            case "11.8" -> {
                return 4.7819f;
            }
            case "11.9" -> {
                return 4.744f;
            }
            case "12.0" -> {
                return 4.7067f;
            }
            case "12.1" -> {
                return 4.67f;
            }
            case "12.2" -> {
                return 4.6339f;
            }
            case "12.3" -> {
                return 4.5984f;
            }
            case "12.4" -> {
                return 4.5633f;
            }
            case "12.5" -> {
                return 4.5288f;
            }
            case "12.6" -> {
                return 4.4949f;
            }
            case "12.7" -> {
                return 4.4614f;
            }
            case "12.8" -> {
                return 4.4285f;
            }
            case "12.9" -> {
                return 4.396f;
            }
            case "13.0" -> {
                return 4.364f;
            }
            case "13.1" -> {
                return 4.3325f;
            }
            case "13.2" -> {
                return 4.3014f;
            }
            case "13.3" -> {
                return 4.2708f;
            }
            case "13.4" -> {
                return 4.2406f;
            }
            case "13.5" -> {
                return 4.2108f;
            }
            case "13.6" -> {
                return 4.1815f;
            }
            case "13.7" -> {
                return 4.1526f;
            }
            case "13.8" -> {
                return 4.1241f;
            }
            case "13.9" -> {
                return 4.0959f;
            }
            case "14.0" -> {
                return 4.0682f;
            }
            case "14.1" -> {
                return 4.0408f;
            }
            case "14.2" -> {
                return 4.0138f;
            }
            case "14.3" -> {
                return 3.9872f;
            }
            case "14.4" -> {
                return 3.961f;
            }
            case "14.5" -> {
                return 3.935f;
            }
            case "14.6" -> {
                return 3.9095f;
            }
            case "14.7" -> {
                return 3.8842f;
            }
            case "14.8" -> {
                return 3.8593f;
            }
            case "14.9" -> {
                return 3.8347f;
            }
            case "15.0" -> {
                return 3.8105f;
            }
            case "15.1" -> {
                return 3.7865f;
            }
            case "15.2" -> {
                return 3.7629f;
            }
            case "15.3" -> {
                return 3.7395f;
            }
            case "15.4" -> {
                return 3.7165f;
            }
            case "15.5" -> {
                return 3.6937f;
            }
            case "15.6" -> {
                return 3.6713f;
            }
            case "15.7" -> {
                return 3.6491f;
            }
            case "15.8" -> {
                return 3.6271f;
            }
            case "15.9" -> {
                return 3.6055f;
            }
            case "16.0" -> {
                return 3.5841f;
            }
            case "16.1" -> {
                return 3.563f;
            }
            case "16.2" -> {
                return 3.5421f;
            }
            case "16.3" -> {
                return 3.5215f;
            }
            case "16.4" -> {
                return 3.5011f;
            }
            case "16.5" -> {
                return 3.481f;
            }
            case "16.6" -> {
                return 3.4611f;
            }
            case "16.7" -> {
                return 3.4414f;
            }
            case "16.8" -> {
                return 3.422f;
            }
            case "16.9" -> {
                return 3.4028f;
            }
            case "17.0" -> {
                return 3.3838f;
            }
            case "17.1" -> {
                return 3.365f;
            }
            case "17.2" -> {
                return 3.3465f;
            }
            case "17.3" -> {
                return 3.3281f;
            }
            case "17.4" -> {
                return 3.31f;
            }
            case "17.5" -> {
                return 3.2921f;
            }
            case "17.6" -> {
                return 3.2743f;
            }
            case "17.7" -> {
                return 3.2568f;
            }
            case "17.8" -> {
                return 3.2395f;
            }
            case "17.9" -> {
                return 3.2223f;
            }
            case "18.0" -> {
                return 3.2054f;
            }
            case "18.1" -> {
                return 3.1886f;
            }
            case "18.2" -> {
                return 3.172f;
            }
            case "18.3" -> {
                return 3.1556f;
            }
            case "18.4" -> {
                return 3.1394f;
            }
            case "18.5" -> {
                return 3.1233f;
            }
            case "18.6" -> {
                return 3.1074f;
            }
            case "18.7" -> {
                return 3.0917f;
            }
            case "18.8" -> {
                return 3.0762f;
            }
            case "18.9" -> {
                return 3.0608f;
            }
            case "19.0" -> {
                return 3.0455f;
            }
            case "19.1" -> {
                return 3.0305f;
            }
            case "19.2" -> {
                return 3.0156f;
            }
            case "19.3" -> {
                return 3.0008f;
            }
            case "19.4" -> {
                return 2.9862f;
            }
            case "19.5" -> {
                return 2.9717f;
            }
            case "19.6" -> {
                return 2.9574f;
            }
            case "19.7" -> {
                return 2.9432f;
            }
            case "19.8" -> {
                return 2.9292f;
            }
            case "19.9" -> {
                return 2.9153f;
            }
            case "20.0" -> {
                return 2.9016f;
            }
            case "20.1" -> {
                return 2.88805f;
            }
            case "20.2" -> {
                return 2.8745f;
            }
            case "20.3" -> {
                return 2.8612f;
            }
            case "20.4" -> {
                return 2.8479f;
            }
            case "20.5" -> {
                return 2.8349f;
            }
            case "20.6" -> {
                return 2.8219f;
            }
            case "20.7" -> {
                return 2.80915f;
            }
            case "20.8" -> {
                return 2.7964f;
            }
            case "20.9" -> {
                return 2.78385f;
            }
            case "21.0" -> {
                return 2.7713f;
            }
            case "21.1" -> {
                return 2.759f;
            }
            case "21.2" -> {
                return 2.7467f;
            }
            case "21.3" -> {
                return 2.73465f;
            }
            case "21.4" -> {
                return 2.7226f;
            }
            case "21.5" -> {
                return 2.7108f;
            }
            case "21.6" -> {
                return 2.699f;
            }
            case "21.7" -> {
                return 2.68735f;
            }
            case "21.8" -> {
                return 2.6757f;
            }
            case "21.9" -> {
                return 2.6643f;
            }
            case "22.0" -> {
                return 2.6529f;
            }
            case "22.1" -> {
                return 2.6417f;
            }
            case "22.2" -> {
                return 2.6305f;
            }
            case "22.3" -> {
                return 2.61955f;
            }
            case "22.4" -> {
                return 2.6086f;
            }
            case "22.5" -> {
                return 2.5978f;
            }
            case "22.6" -> {
                return 2.587f;
            }
            case "22.7" -> {
                return 2.5764f;
            }
            case "22.8" -> {
                return 2.5658f;
            }
            case "22.9" -> {
                return 2.55535f;
            }
            case "23.0" -> {
                return 2.5449f;
            }
            case "23.1" -> {
                return 2.5347f;
            }
            case "23.2" -> {
                return 2.5245f;
            }
            case "23.3" -> {
                return 2.5144f;
            }
            case "23.4" -> {
                return 2.5043f;
            }
            case "23.5" -> {
                return 2.49445f;
            }
            case "23.6" -> {
                return 2.4846f;
            }
            case "23.7" -> {
                return 2.47485f;
            }
            case "23.8" -> {
                return 2.4651f;
            }
            case "23.9" -> {
                return 2.45555f;
            }
            case "24.0" -> {
                return 2.446f;
            }
            case "24.1" -> {
                return 2.4366f;
            }
            case "24.2" -> {
                return 2.4272f;
            }
            case "24.3" -> {
                return 2.418f;
            }
            case "24.4" -> {
                return 2.4088f;
            }
            case "24.5" -> {
                return 2.3997f;
            }
            case "24.6" -> {
                return 2.3906f;
            }
            case "24.7" -> {
                return 2.38165f;
            }
            case "24.8" -> {
                return 2.3727f;
            }
            case "24.9" -> {
                return 2.36395f;
            }
            case "25.0" -> {
                return 2.3552f;
            }
            case "25.1" -> {
                return 2.34655f;
            }
            case "25.2" -> {
                return 2.3379f;
            }
            case "25.3" -> {
                return 2.3294f;
            }
            case "25.4" -> {
                return 2.3209f;
            }
            case "25.5" -> {
                return 2.3125f;
            }
            case "25.6" -> {
                return 2.3041f;
            }
            case "25.7" -> {
                return 2.29585f;
            }
            case "25.8" -> {
                return 2.2876f;
            }
            case "25.9" -> {
                return 2.2795f;
            }
            case "26.0" -> {
                return 2.2714f;
            }
            case "26.1" -> {
                return 2.26345f;
            }
            case "26.2" -> {
                return 2.2555f;
            }
            case "26.3" -> {
                return 2.24765f;
            }
            case "26.4" -> {
                return 2.2398f;
            }
            case "26.5" -> {
                return 2.23205f;
            }
            case "26.6" -> {
                return 2.2243f;
            }
            case "26.7" -> {
                return 2.2167f;
            }
            case "26.8" -> {
                return 2.2091f;
            }
            case "26.9" -> {
                return 2.2016f;
            }
            case "27.0" -> {
                return 2.1941f;
            }
            case "27.1" -> {
                return 2.1867f;
            }
            case "27.2" -> {
                return 2.1793f;
            }
            case "27.3" -> {
                return 2.172f;
            }
            case "27.4" -> {
                return 2.1647f;
            }
            case "27.5" -> {
                return 2.15755f;
            }
            case "27.6" -> {
                return 2.1504f;
            }
            case "27.7" -> {
                return 2.14335f;
            }
            case "27.8" -> {
                return 2.1363f;
            }
            case "27.9" -> {
                return 2.12935f;
            }
            case "28.0" -> {
                return 2.1224f;
            }
            case "28.1" -> {
                return 2.11555f;
            }
            case "28.2" -> {
                return 2.1087f;
            }
            case "28.3" -> {
                return 2.10195f;
            }
            case "28.4" -> {
                return 2.0952f;
            }
            case "28.5" -> {
                return 2.08855f;
            }
            case "28.6" -> {
                return 2.0819f;
            }
            case "28.7" -> {
                return 2.07535f;
            }
            case "28.8" -> {
                return 2.0688f;
            }
            case "28.9" -> {
                return 2.0623f;
            }
            case "29.0" -> {
                return 2.0558f;
            }
            case "29.1" -> {
                return 2.04945f;
            }
            case "29.2" -> {
                return 2.0431f;
            }
            case "29.3" -> {
                return 2.0368f;
            }
            case "29.4" -> {
                return 2.0305f;
            }
            case "29.5" -> {
                return 2.0243f;
            }
            case "29.6" -> {
                return 2.0181f;
            }
            case "29.7" -> {
                return 2.012f;
            }
            case "29.8" -> {
                return 2.0059f;
            }
            case "29.9" -> {
                return 1.9999f;
            }
            case "30.0" -> {
                return 1.9939f;
            }
            case "30.1" -> {
                return 1.98802f;
            }
            case "30.2" -> {
                return 1.98214f;
            }
            case "30.3" -> {
                return 1.97626f;
            }
            case "30.4" -> {
                return 1.97038f;
            }
            case "30.5" -> {
                return 1.9645f;
            }
            case "30.6" -> {
                return 1.95882f;
            }
            case "30.7" -> {
                return 1.95314f;
            }
            case "30.8" -> {
                return 1.94746f;
            }
            case "30.9" -> {
                return 1.94178f;
            }
            case "31.0" -> {
                return 1.9361f;
            }
            case "31.1" -> {
                return 1.93062f;
            }
            case "31.2" -> {
                return 1.92514f;
            }
            case "31.3" -> {
                return 1.91966f;
            }
            case "31.4" -> {
                return 1.91418f;
            }
            case "31.5" -> {
                return 1.9087f;
            }
            case "31.6" -> {
                return 1.90338f;
            }
            case "31.7" -> {
                return 1.89806f;
            }
            case "31.8" -> {
                return 1.89274f;
            }
            case "31.9" -> {
                return 1.88742f;
            }
            case "32.0" -> {
                return 1.8821f;
            }
            case "32.1" -> {
                return 1.87698f;
            }
            case "32.2" -> {
                return 1.87186f;
            }
            case "32.3" -> {
                return 1.86674f;
            }
            case "32.4" -> {
                return 1.86162f;
            }
            case "32.5" -> {
                return 1.8565f;
            }
            case "32.6" -> {
                return 1.85152f;
            }
            case "32.7" -> {
                return 1.84654f;
            }
            case "32.8" -> {
                return 1.84156f;
            }
            case "32.9" -> {
                return 1.83658f;
            }
            case "33.0" -> {
                return 1.8316f;
            }
            case "33.1" -> {
                return 1.8268f;
            }
            case "33.2" -> {
                return 1.822f;
            }
            case "33.3" -> {
                return 1.8172f;
            }
            case "33.4" -> {
                return 1.8124f;
            }
            case "33.5" -> {
                return 1.8076f;
            }
            case "33.6" -> {
                return 1.80294f;
            }
            case "33.7" -> {
                return 1.79828f;
            }
            case "33.8" -> {
                return 1.79362f;
            }
            case "33.9" -> {
                return 1.78896f;
            }
            case "34.0" -> {
                return 1.7843f;
            }
            case "34.1" -> {
                return 1.77978f;
            }
            case "34.2" -> {
                return 1.77526f;
            }
            case "34.3" -> {
                return 1.77074f;
            }
            case "34.4" -> {
                return 1.76622f;
            }
            case "34.5" -> {
                return 1.7617f;
            }
            case "34.6" -> {
                return 1.75732f;
            }
            case "34.7" -> {
                return 1.75294f;
            }
            case "34.8" -> {
                return 1.74856f;
            }
            case "34.9" -> {
                return 1.74418f;
            }
            case "35.0" -> {
                return 1.7398f;
            }
            case "35.1" -> {
                return 1.73556f;
            }
            case "35.2" -> {
                return 1.73132f;
            }
            case "35.3" -> {
                return 1.72708f;
            }
            case "35.4" -> {
                return 1.72284f;
            }
            case "35.5" -> {
                return 1.7186f;
            }
            case "35.6" -> {
                return 1.71448f;
            }
            case "35.7" -> {
                return 1.71036f;
            }
            case "35.8" -> {
                return 1.70624f;
            }
            case "35.9" -> {
                return 1.70212f;
            }
            case "36.0" -> {
                return 1.698f;
            }
            case "36.1" -> {
                return 1.694f;
            }
            case "36.2" -> {
                return 1.69f;
            }
            case "36.3" -> {
                return 1.686f;
            }
            case "36.4" -> {
                return 1.682f;
            }
            case "36.5" -> {
                return 1.678f;
            }
            case "36.6" -> {
                return 1.67412f;
            }
            case "36.7" -> {
                return 1.67024f;
            }
            case "36.8" -> {
                return 1.66636f;
            }
            case "36.9" -> {
                return 1.66248f;
            }
            case "37.0" -> {
                return 1.6586f;
            }
            case "37.1" -> {
                return 1.65484f;
            }
            case "37.2" -> {
                return 1.65108f;
            }
            case "37.3" -> {
                return 1.64732f;
            }
            case "37.4" -> {
                return 1.64356f;
            }
            case "37.5" -> {
                return 1.6398f;
            }
            case "37.6" -> {
                return 1.63614f;
            }
            case "37.7" -> {
                return 1.63248f;
            }
            case "37.8" -> {
                return 1.62882f;
            }
            case "37.9" -> {
                return 1.62516f;
            }
            case "38.0" -> {
                return 1.6215f;
            }
            case "38.1" -> {
                return 1.61796f;
            }
            case "38.2" -> {
                return 1.61442f;
            }
            case "38.3" -> {
                return 1.61088f;
            }
            case "38.4" -> {
                return 1.60734f;
            }
            case "38.5" -> {
                return 1.6038f;
            }
            case "38.6" -> {
                return 1.60034f;
            }
            case "38.7" -> {
                return 1.59688f;
            }
            case "38.8" -> {
                return 1.59342f;
            }
            case "38.9" -> {
                return 1.58996f;
            }
            case "39.0" -> {
                return 1.5865f;
            }
            case "39.1" -> {
                return 1.58316f;
            }
            case "39.2" -> {
                return 1.57982f;
            }
            case "39.3" -> {
                return 1.57648f;
            }
            case "39.4" -> {
                return 1.57314f;
            }
            case "39.5" -> {
                return 1.5698f;
            }
            case "39.6" -> {
                return 1.56654f;
            }
            case "39.7" -> {
                return 1.56328f;
            }
            case "39.8" -> {
                return 1.56002f;
            }
            case "39.9" -> {
                return 1.55676f;
            }
            case "40.0" -> {
                return 1.5535f;
            }
            case "40.1" -> {
                return 1.55032f;
            }
            case "40.2" -> {
                return 1.54714f;
            }
            case "40.3" -> {
                return 1.54396f;
            }
            case "40.4" -> {
                return 1.54078f;
            }
            case "40.5" -> {
                return 1.5376f;
            }
            case "40.6" -> {
                return 1.53452f;
            }
            case "40.7" -> {
                return 1.53144f;
            }
            case "40.8" -> {
                return 1.52836f;
            }
            case "40.9" -> {
                return 1.52528f;
            }
            case "41.0" -> {
                return 1.5222f;
            }
            case "41.1" -> {
                return 1.5192f;
            }
            case "41.2" -> {
                return 1.5162f;
            }
            case "41.3" -> {
                return 1.5132f;
            }
            case "41.4" -> {
                return 1.5102f;
            }
            case "41.5" -> {
                return 1.5072f;
            }
            case "41.6" -> {
                return 1.50428f;
            }
            case "41.7" -> {
                return 1.50136f;
            }
            case "41.8" -> {
                return 1.49844f;
            }
            case "41.9" -> {
                return 1.49552f;
            }
            case "42.0" -> {
                return 1.4926f;
            }
            case "42.1" -> {
                return 1.48976f;
            }
            case "42.2" -> {
                return 1.48692f;
            }
            case "42.3" -> {
                return 1.48408f;
            }
            case "42.4" -> {
                return 1.48124f;
            }
            case "42.5" -> {
                return 1.4784f;
            }
            case "42.6" -> {
                return 1.47562f;
            }
            case "42.7" -> {
                return 1.47284f;
            }
            case "42.8" -> {
                return 1.47006f;
            }
            case "42.9" -> {
                return 1.46728f;
            }
            case "43.0" -> {
                return 1.4645f;
            }
            case "43.1" -> {
                return 1.46182f;
            }
            case "43.2" -> {
                return 1.45914f;
            }
            case "43.3" -> {
                return 1.45646f;
            }
            case "43.4" -> {
                return 1.45378f;
            }
            case "43.5" -> {
                return 1.4511f;
            }
            case "43.6" -> {
                return 1.44848f;
            }
            case "43.7" -> {
                return 1.44586f;
            }
            case "43.8" -> {
                return 1.44324f;
            }
            case "43.9" -> {
                return 1.44062f;
            }
            case "44.0" -> {
                return 1.438f;
            }
            case "44.1" -> {
                return 1.43544f;
            }
            case "44.2" -> {
                return 1.43288f;
            }
            case "44.3" -> {
                return 1.43032f;
            }
            case "44.4" -> {
                return 1.42776f;
            }
            case "44.5" -> {
                return 1.4252f;
            }
            case "44.6" -> {
                return 1.42272f;
            }
            case "44.7" -> {
                return 1.42024f;
            }
            case "44.8" -> {
                return 1.41776f;
            }
            case "44.9" -> {
                return 1.41528f;
            }
            case "45.0" -> {
                return 1.4128f;
            }
            case "45.1" -> {
                return 1.41036f;
            }
            case "45.2" -> {
                return 1.40792f;
            }
            case "45.3" -> {
                return 1.40548f;
            }
            case "45.4" -> {
                return 1.40304f;
            }
            case "45.5" -> {
                return 1.4006f;
            }
            case "45.6" -> {
                return 1.39824f;
            }
            case "45.7" -> {
                return 1.39588f;
            }
            case "45.8" -> {
                return 1.39352f;
            }
            case "45.9" -> {
                return 1.39116f;
            }
            case "46.0" -> {
                return 1.3888f;
            }
            case "46.1" -> {
                return 1.3865f;
            }
            case "46.2" -> {
                return 1.3842f;
            }
            case "46.3" -> {
                return 1.3819f;
            }
            case "46.4" -> {
                return 1.3796f;
            }
            case "46.5" -> {
                return 1.3773f;
            }
            case "46.6" -> {
                return 1.37506f;
            }
            case "46.7" -> {
                return 1.37282f;
            }
            case "46.8" -> {
                return 1.37058f;
            }
            case "46.9" -> {
                return 1.36834f;
            }
            case "47.0" -> {
                return 1.3661f;
            }
            case "47.1" -> {
                return 1.36392f;
            }
            case "47.2" -> {
                return 1.36174f;
            }
            case "47.3" -> {
                return 1.35956f;
            }
            case "47.4" -> {
                return 1.35738f;
            }
            case "47.5" -> {
                return 1.3552f;
            }
            case "47.6" -> {
                return 1.35306f;
            }
            case "47.7" -> {
                return 1.35092f;
            }
            case "47.8" -> {
                return 1.34878f;
            }
            case "47.9" -> {
                return 1.34664f;
            }
            case "48.0" -> {
                return 1.3445f;
            }
            case "48.1" -> {
                return 1.34242f;
            }
            case "48.2" -> {
                return 1.34034f;
            }
            case "48.3" -> {
                return 1.33826f;
            }
            case "48.4" -> {
                return 1.33618f;
            }
            case "48.5" -> {
                return 1.3341f;
            }
            case "48.6" -> {
                return 1.33208f;
            }
            case "48.7" -> {
                return 1.33006f;
            }
            case "48.8" -> {
                return 1.32804f;
            }
            case "48.9" -> {
                return 1.32602f;
            }
            case "49.0" -> {
                return 1.324f;
            }
            case "49.1" -> {
                return 1.32202f;
            }
            case "49.2" -> {
                return 1.32004f;
            }
            case "49.3" -> {
                return 1.31806f;
            }
            case "49.4" -> {
                return 1.31608f;
            }
            case "49.5" -> {
                return 1.3141f;
            }
            case "49.6" -> {
                return 1.31218f;
            }
            case "49.7" -> {
                return 1.31026f;
            }
            case "49.8" -> {
                return 1.30834f;
            }
            case "49.9" -> {
                return 1.30642f;
            }
            case "50.0" -> {
                return 1.3045f;
            }
            case "50.1" -> {
                return 1.30262f;
            }
            case "50.2" -> {
                return 1.30074f;
            }
            case "50.3" -> {
                return 1.29886f;
            }
            case "50.4" -> {
                return 1.29698f;
            }
            case "50.5" -> {
                return 1.2951f;
            }
            case "50.6" -> {
                return 1.29326f;
            }
            case "50.7" -> {
                return 1.29142f;
            }
            case "50.8" -> {
                return 1.28958f;
            }
            case "50.9" -> {
                return 1.28774f;
            }
            case "51.0" -> {
                return 1.2859f;
            }
            case "51.1" -> {
                return 1.2841f;
            }
            case "51.2" -> {
                return 1.2823f;
            }
            case "51.3" -> {
                return 1.2805f;
            }
            case "51.4" -> {
                return 1.2787f;
            }
            case "51.5" -> {
                return 1.2769f;
            }
            case "51.6" -> {
                return 1.27516f;
            }
            case "51.7" -> {
                return 1.27342f;
            }
            case "51.8" -> {
                return 1.27168f;
            }
            case "51.9" -> {
                return 1.26994f;
            }
            case "52.0" -> {
                return 1.2682f;
            }
            case "52.1" -> {
                return 1.2665f;
            }
            case "52.2" -> {
                return 1.2648f;
            }
            case "52.3" -> {
                return 1.2631f;
            }
            case "52.4" -> {
                return 1.2614f;
            }
            case "52.5" -> {
                return 1.2597f;
            }
            case "52.6" -> {
                return 1.25804f;
            }
            case "52.7" -> {
                return 1.25638f;
            }
            case "52.8" -> {
                return 1.25472f;
            }
            case "52.9" -> {
                return 1.25306f;
            }
            case "53.0" -> {
                return 1.2514f;
            }
            case "53.1" -> {
                return 1.24978f;
            }
            case "53.2" -> {
                return 1.24816f;
            }
            case "53.3" -> {
                return 1.24654f;
            }
            case "53.4" -> {
                return 1.24492f;
            }
            case "53.5" -> {
                return 1.2433f;
            }
            case "53.6" -> {
                return 1.24172f;
            }
            case "53.7" -> {
                return 1.24014f;
            }
            case "53.8" -> {
                return 1.23856f;
            }
            case "53.9" -> {
                return 1.23698f;
            }
            case "54.0" -> {
                return 1.2354f;
            }
            case "54.1" -> {
                return 1.23386f;
            }
            case "54.2" -> {
                return 1.23232f;
            }
            case "54.3" -> {
                return 1.23078f;
            }
            case "54.4" -> {
                return 1.22924f;
            }
            case "54.5" -> {
                return 1.2277f;
            }
            case "54.6" -> {
                return 1.2262f;
            }
            case "54.7" -> {
                return 1.2247f;
            }
            case "54.8" -> {
                return 1.2232f;
            }
            case "54.9" -> {
                return 1.2217f;
            }
            case "55.0" -> {
                return 1.2202f;
            }
            case "55.1" -> {
                return 1.21875f;
            }
            case "55.2" -> {
                return 1.2173f;
            }
            case "55.3" -> {
                return 1.21585f;
            }
            case "55.4" -> {
                return 1.2144f;
            }
            case "55.5" -> {
                return 1.21295f;
            }
            case "55.6" -> {
                return 1.2115f;
            }
            case "55.7" -> {
                return 1.21005f;
            }
            case "55.8" -> {
                return 1.2086f;
            }
            case "55.9" -> {
                return 1.20715f;
            }
            case "56.0" -> {
                return 1.2057f;
            }
            case "56.1" -> {
                return 1.20431f;
            }
            case "56.2" -> {
                return 1.20292f;
            }
            case "56.3" -> {
                return 1.20153f;
            }
            case "56.4" -> {
                return 1.20014f;
            }
            case "56.5" -> {
                return 1.19875f;
            }
            case "56.6" -> {
                return 1.19736f;
            }
            case "56.7" -> {
                return 1.19597f;
            }
            case "56.8" -> {
                return 1.19458f;
            }
            case "56.9" -> {
                return 1.19319f;
            }
            case "57.0" -> {
                return 1.1918f;
            }
            case "57.1" -> {
                return 1.19049f;
            }
            case "57.2" -> {
                return 1.18918f;
            }
            case "57.3" -> {
                return 1.18787f;
            }
            case "57.4" -> {
                return 1.18656f;
            }
            case "57.5" -> {
                return 1.18525f;
            }
            case "57.6" -> {
                return 1.18394f;
            }
            case "57.7" -> {
                return 1.18263f;
            }
            case "57.8" -> {
                return 1.18132f;
            }
            case "57.9" -> {
                return 1.18001f;
            }
            case "58.0" -> {
                return 1.1787f;
            }
            case "58.1" -> {
                return 1.17745f;
            }
            case "58.2" -> {
                return 1.1762f;
            }
            case "58.3" -> {
                return 1.17495f;
            }
            case "58.4" -> {
                return 1.1737f;
            }
            case "58.5" -> {
                return 1.17245f;
            }
            case "58.6" -> {
                return 1.1712f;
            }
            case "58.7" -> {
                return 1.16995f;
            }
            case "58.8" -> {
                return 1.1687f;
            }
            case "58.9" -> {
                return 1.16745f;
            }
            case "59.0" -> {
                return 1.1662f;
            }
            case "59.1" -> {
                return 1.16501f;
            }
            case "59.2" -> {
                return 1.16382f;
            }
            case "59.3" -> {
                return 1.16263f;
            }
            case "59.4" -> {
                return 1.16144f;
            }
            case "59.5" -> {
                return 1.16025f;
            }
            case "59.6" -> {
                return 1.15906f;
            }
            case "59.7" -> {
                return 1.15787f;
            }
            case "59.8" -> {
                return 1.15668f;
            }
            case "59.9" -> {
                return 1.15549f;
            }
            case "60.0" -> {
                return 1.1543f;
            }
            case "60.1" -> {
                return 1.15317f;
            }
            case "60.2" -> {
                return 1.15204f;
            }
            case "60.3" -> {
                return 1.15091f;
            }
            case "60.4" -> {
                return 1.14978f;
            }
            case "60.5" -> {
                return 1.14865f;
            }
            case "60.6" -> {
                return 1.14752f;
            }
            case "60.7" -> {
                return 1.14639f;
            }
            case "60.8" -> {
                return 1.14526f;
            }
            case "60.9" -> {
                return 1.14413f;
            }
            case "61.0" -> {
                return 1.143f;
            }
            case "61.1" -> {
                return 1.14192f;
            }
            case "61.2" -> {
                return 1.14084f;
            }
            case "61.3" -> {
                return 1.13976f;
            }
            case "61.4" -> {
                return 1.13868f;
            }
            case "61.5" -> {
                return 1.1376f;
            }
            case "61.6" -> {
                return 1.13652f;
            }
            case "61.7" -> {
                return 1.13544f;
            }
            case "61.8" -> {
                return 1.13436f;
            }
            case "61.9" -> {
                return 1.13328f;
            }
            case "62.0" -> {
                return 1.1322f;
            }
            case "62.1" -> {
                return 1.13118f;
            }
            case "62.2" -> {
                return 1.13016f;
            }
            case "62.3" -> {
                return 1.12914f;
            }
            case "62.4" -> {
                return 1.12812f;
            }
            case "62.5" -> {
                return 1.1271f;
            }
            case "62.6" -> {
                return 1.12608f;
            }
            case "62.7" -> {
                return 1.12506f;
            }
            case "62.8" -> {
                return 1.12404f;
            }
            case "62.9" -> {
                return 1.12302f;
            }
            case "63.0" -> {
                return 1.122f;
            }
            case "63.1" -> {
                return 1.12103f;
            }
            case "63.2" -> {
                return 1.12006f;
            }
            case "63.3" -> {
                return 1.11909f;
            }
            case "63.4" -> {
                return 1.11812f;
            }
            case "63.5" -> {
                return 1.11715f;
            }
            case "63.6" -> {
                return 1.11618f;
            }
            case "63.7" -> {
                return 1.11521f;
            }
            case "63.8" -> {
                return 1.11424f;
            }
            case "63.9" -> {
                return 1.11327f;
            }
            case "64.0" -> {
                return 1.1123f;
            }
            case "64.1" -> {
                return 1.11138f;
            }
            case "64.2" -> {
                return 1.11046f;
            }
            case "64.3" -> {
                return 1.10954f;
            }
            case "64.4" -> {
                return 1.10862f;
            }
            case "64.5" -> {
                return 1.1077f;
            }
            case "64.6" -> {
                return 1.10678f;
            }
            case "64.7" -> {
                return 1.10586f;
            }
            case "64.8" -> {
                return 1.10494f;
            }
            case "64.9" -> {
                return 1.10402f;
            }
            case "65.0" -> {
                return 1.1031f;
            }
            case "65.1" -> {
                return 1.10223f;
            }
            case "65.2" -> {
                return 1.10136f;
            }
            case "65.3" -> {
                return 1.10049f;
            }
            case "65.4" -> {
                return 1.09962f;
            }
            case "65.5" -> {
                return 1.09875f;
            }
            case "65.6" -> {
                return 1.09788f;
            }
            case "65.7" -> {
                return 1.09701f;
            }
            case "65.8" -> {
                return 1.09614f;
            }
            case "65.9" -> {
                return 1.09527f;
            }
            case "66.0" -> {
                return 1.0944f;
            }
            case "66.1" -> {
                return 1.09358f;
            }
            case "66.2" -> {
                return 1.09276f;
            }
            case "66.3" -> {
                return 1.09194f;
            }
            case "66.4" -> {
                return 1.09112f;
            }
            case "66.5" -> {
                return 1.0903f;
            }
            case "66.6" -> {
                return 1.08948f;
            }
            case "66.7" -> {
                return 1.08866f;
            }
            case "66.8" -> {
                return 1.08784f;
            }
            case "66.9" -> {
                return 1.08702f;
            }
            case "67.0" -> {
                return 1.0862f;
            }
            case "67.1" -> {
                return 1.08542f;
            }
            case "67.2" -> {
                return 1.08464f;
            }
            case "67.3" -> {
                return 1.08386f;
            }
            case "67.4" -> {
                return 1.08308f;
            }
            case "67.5" -> {
                return 1.0823f;
            }
            case "67.6" -> {
                return 1.08152f;
            }
            case "67.7" -> {
                return 1.08074f;
            }
            case "67.8" -> {
                return 1.07996f;
            }
            case "67.9" -> {
                return 1.07918f;
            }
            case "68.0" -> {
                return 1.0784f;
            }
            case "68.1" -> {
                return 1.07766f;
            }
            case "68.2" -> {
                return 1.07692f;
            }
            case "68.3" -> {
                return 1.07618f;
            }
            case "68.4" -> {
                return 1.07544f;
            }
            case "68.5" -> {
                return 1.0747f;
            }
            case "68.6" -> {
                return 1.07396f;
            }
            case "68.7" -> {
                return 1.07322f;
            }
            case "68.8" -> {
                return 1.07248f;
            }
            case "68.9" -> {
                return 1.07174f;
            }
            case "69.0" -> {
                return 1.071f;
            }
            case "69.1" -> {
                return 1.0703f;
            }
            case "69.2" -> {
                return 1.0696f;
            }
            case "69.3" -> {
                return 1.0689f;
            }
            case "69.4" -> {
                return 1.0682f;
            }
            case "69.5" -> {
                return 1.0675f;
            }
            case "69.6" -> {
                return 1.0668f;
            }
            case "69.7" -> {
                return 1.0661f;
            }
            case "69.8" -> {
                return 1.0654f;
            }
            case "69.9" -> {
                return 1.0647f;
            }
            case "70.0" -> {
                return 1.064f;
            }
            case "70.1" -> {
                return 1.06335f;
            }
            case "70.2" -> {
                return 1.0627f;
            }
            case "70.3" -> {
                return 1.06205f;
            }
            case "70.4" -> {
                return 1.0614f;
            }
            case "70.5" -> {
                return 1.06075f;
            }
            case "70.6" -> {
                return 1.0601f;
            }
            case "70.7" -> {
                return 1.05945f;
            }
            case "70.8" -> {
                return 1.0588f;
            }
            case "70.9" -> {
                return 1.05815f;
            }
            case "71.0" -> {
                return 1.0575f;
            }
            case "71.1" -> {
                return 1.05688f;
            }
            case "71.2" -> {
                return 1.05626f;
            }
            case "71.3" -> {
                return 1.05564f;
            }
            case "71.4" -> {
                return 1.05502f;
            }
            case "71.5" -> {
                return 1.0544f;
            }
            case "71.6" -> {
                return 1.05378f;
            }
            case "71.7" -> {
                return 1.05316f;
            }
            case "71.8" -> {
                return 1.05254f;
            }
            case "71.9" -> {
                return 1.05192f;
            }
            case "72.0" -> {
                return 1.0513f;
            }
            case "72.1" -> {
                return 1.05073f;
            }
            case "72.2" -> {
                return 1.05016f;
            }
            case "72.3" -> {
                return 1.04959f;
            }
            case "72.4" -> {
                return 1.04902f;
            }
            case "72.5" -> {
                return 1.04845f;
            }
            case "72.6" -> {
                return 1.04788f;
            }
            case "72.7" -> {
                return 1.04731f;
            }
            case "72.8" -> {
                return 1.04674f;
            }
            case "72.9" -> {
                return 1.04617f;
            }
            case "73.0" -> {
                return 1.0456f;
            }
            case "73.1" -> {
                return 1.04506f;
            }
            case "73.2" -> {
                return 1.04452f;
            }
            case "73.3" -> {
                return 1.04398f;
            }
            case "73.4" -> {
                return 1.04344f;
            }
            case "73.5" -> {
                return 1.0429f;
            }
            case "73.6" -> {
                return 1.04236f;
            }
            case "73.7" -> {
                return 1.04182f;
            }
            case "73.8" -> {
                return 1.04128f;
            }
            case "73.9" -> {
                return 1.04074f;
            }
            case "74.0" -> {
                return 1.0402f;
            }
            case "74.1" -> {
                return 1.0397f;
            }
            case "74.2" -> {
                return 1.0392f;
            }
            case "74.3" -> {
                return 1.0387f;
            }
            case "74.4" -> {
                return 1.0382f;
            }
            case "74.5" -> {
                return 1.0377f;
            }
            case "74.6" -> {
                return 1.0372f;
            }
            case "74.7" -> {
                return 1.0367f;
            }
            case "74.8" -> {
                return 1.0362f;
            }
            case "74.9" -> {
                return 1.0357f;
            }
            case "75.0" -> {
                return 1.0352f;
            }
            case "75.1" -> {
                return 1.03473f;
            }
            case "75.2" -> {
                return 1.03426f;
            }
            case "75.3" -> {
                return 1.03379f;
            }
            case "75.4" -> {
                return 1.03332f;
            }
            case "75.5" -> {
                return 1.03285f;
            }
            case "75.6" -> {
                return 1.03238f;
            }
            case "75.7" -> {
                return 1.03191f;
            }
            case "75.8" -> {
                return 1.03144f;
            }
            case "75.9" -> {
                return 1.03097f;
            }
            case "76.0" -> {
                return 1.0305f;
            }
            case "76.1" -> {
                return 1.03007f;
            }
            case "76.2" -> {
                return 1.02964f;
            }
            case "76.3" -> {
                return 1.02921f;
            }
            case "76.4" -> {
                return 1.02878f;
            }
            case "76.5" -> {
                return 1.02835f;
            }
            case "76.6" -> {
                return 1.02792f;
            }
            case "76.7" -> {
                return 1.02749f;
            }
            case "76.8" -> {
                return 1.02706f;
            }
            case "76.9" -> {
                return 1.02663f;
            }
            case "77.0" -> {
                return 1.0262f;
            }
            case "77.1" -> {
                return 1.02581f;
            }
            case "77.2" -> {
                return 1.02542f;
            }
            case "77.3" -> {
                return 1.02503f;
            }
            case "77.4" -> {
                return 1.02464f;
            }
            case "77.5" -> {
                return 1.02425f;
            }
            case "77.6" -> {
                return 1.02386f;
            }
            case "77.7" -> {
                return 1.02347f;
            }
            case "77.8" -> {
                return 1.02308f;
            }
            case "77.9" -> {
                return 1.02269f;
            }
            case "78.0" -> {
                return 1.0223f;
            }
            case "78.1" -> {
                return 1.02194f;
            }
            case "78.2" -> {
                return 1.02158f;
            }
            case "78.3" -> {
                return 1.02122f;
            }
            case "78.4" -> {
                return 1.02086f;
            }
            case "78.5" -> {
                return 1.0205f;
            }
            case "78.6" -> {
                return 1.02014f;
            }
            case "78.7" -> {
                return 1.01978f;
            }
            case "78.8" -> {
                return 1.01942f;
            }
            case "78.9" -> {
                return 1.01906f;
            }
            case "79.0" -> {
                return 1.0187f;
            }
            case "79.1" -> {
                return 1.01837f;
            }
            case "79.2" -> {
                return 1.01804f;
            }
            case "79.3" -> {
                return 1.01771f;
            }
            case "79.4" -> {
                return 1.01738f;
            }
            case "79.5" -> {
                return 1.01705f;
            }
            case "79.6" -> {
                return 1.01672f;
            }
            case "79.7" -> {
                return 1.01639f;
            }
            case "79.8" -> {
                return 1.01606f;
            }
            case "79.9" -> {
                return 1.01573f;
            }
            case "80.0" -> {
                return 1.0154f;
            }
            case "80.1" -> {
                return 1.0151f;
            }
            case "80.2" -> {
                return 1.0148f;
            }
            case "80.3" -> {
                return 1.0145f;
            }
            case "80.4" -> {
                return 1.0142f;
            }
            case "80.5" -> {
                return 1.0139f;
            }
            case "80.6" -> {
                return 1.0136f;
            }
            case "80.7" -> {
                return 1.0133f;
            }
            case "80.8" -> {
                return 1.013f;
            }
            case "80.9" -> {
                return 1.0127f;
            }
            case "81.0" -> {
                return 1.0124f;
            }
            case "81.1" -> {
                return 1.01214f;
            }
            case "81.2" -> {
                return 1.01188f;
            }
            case "81.3" -> {
                return 1.01162f;
            }
            case "81.4" -> {
                return 1.01136f;
            }
            case "81.5" -> {
                return 1.0111f;
            }
            case "81.6" -> {
                return 1.01084f;
            }
            case "81.7" -> {
                return 1.01058f;
            }
            case "81.8" -> {
                return 1.01032f;
            }
            case "81.9" -> {
                return 1.01006f;
            }
            case "82.0" -> {
                return 1.0098f;
            }
            case "82.1" -> {
                return 1.00957f;
            }
            case "82.2" -> {
                return 1.00934f;
            }
            case "82.3" -> {
                return 1.00911f;
            }
            case "82.4" -> {
                return 1.00888f;
            }
            case "82.5" -> {
                return 1.00865f;
            }
            case "82.6" -> {
                return 1.00842f;
            }
            case "82.7" -> {
                return 1.00819f;
            }
            case "82.8" -> {
                return 1.00796f;
            }
            case "82.9" -> {
                return 1.00773f;
            }
            case "83.0" -> {
                return 1.0075f;
            }
            case "83.1" -> {
                return 1.0073f;
            }
            case "83.2" -> {
                return 1.0071f;
            }
            case "83.3" -> {
                return 1.0069f;
            }
            case "83.4" -> {
                return 1.0067f;
            }
            case "83.5" -> {
                return 1.0065f;
            }
            case "83.6" -> {
                return 1.0063f;
            }
            case "83.7" -> {
                return 1.0061f;
            }
            case "83.8" -> {
                return 1.0059f;
            }
            case "83.9" -> {
                return 1.0057f;
            }
            case "84.0" -> {
                return 1.0055f;
            }
            case "84.1" -> {
                return 1.00533f;
            }
            case "84.2" -> {
                return 1.00516f;
            }
            case "84.3" -> {
                return 1.00499f;
            }
            case "84.4" -> {
                return 1.00482f;
            }
            case "84.5" -> {
                return 1.00465f;
            }
            case "84.6" -> {
                return 1.00448f;
            }
            case "84.7" -> {
                return 1.00431f;
            }
            case "84.8" -> {
                return 1.00414f;
            }
            case "84.9" -> {
                return 1.00397f;
            }
            case "85.0" -> {
                return 1.0038f;
            }
            case "85.1" -> {
                return 1.00366f;
            }
            case "85.2" -> {
                return 1.00352f;
            }
            case "85.3" -> {
                return 1.00338f;
            }
            case "85.4" -> {
                return 1.00324f;
            }
            case "85.5" -> {
                return 1.0031f;
            }
            case "85.6" -> {
                return 1.00296f;
            }
            case "85.7" -> {
                return 1.00282f;
            }
            case "85.8" -> {
                return 1.00268f;
            }
            case "85.9" -> {
                return 1.00254f;
            }
            case "86.0" -> {
                return 1.0024f;
            }
            case "86.1" -> {
                return 1.0023f;
            }
            case "86.2" -> {
                return 1.0022f;
            }
            case "86.3" -> {
                return 1.0021f;
            }
            case "86.4" -> {
                return 1.002f;
            }
            case "86.5" -> {
                return 1.0019f;
            }
            case "86.6" -> {
                return 1.0018f;
            }
            case "86.7" -> {
                return 1.0017f;
            }
            case "86.8" -> {
                return 1.0016f;
            }
            case "86.9" -> {
                return 1.0015f;
            }
            case "87.0" -> {
                return 1.0014f;
            }
            case "87.1" -> {
                return 1.00132f;
            }
            case "87.2" -> {
                return 1.00124f;
            }
            case "87.3" -> {
                return 1.00116f;
            }
            case "87.4" -> {
                return 1.00108f;
            }
            case "87.5" -> {
                return 1.001f;
            }
            case "87.6" -> {
                return 1.00092f;
            }
            case "87.7" -> {
                return 1.00084f;
            }
            case "87.8" -> {
                return 1.00076f;
            }
            case "87.9" -> {
                return 1.00068f;
            }
            case "88.0" -> {
                return 1.0006f;
            }
            case "88.1" -> {
                return 1.00056f;
            }
            case "88.2" -> {
                return 1.00052f;
            }
            case "88.3" -> {
                return 1.00048f;
            }
            case "88.4" -> {
                return 1.00044f;
            }
            case "88.5" -> {
                return 1.0004f;
            }
            case "88.6" -> {
                return 1.00036f;
            }
            case "88.7" -> {
                return 1.00032f;
            }
            case "88.8" -> {
                return 1.00028f;
            }
            case "88.9" -> {
                return 1.00024f;
            }
            case "89.0" -> {
                return 1.0002f;
            }
            case "89.1" -> {
                return 1.00018f;
            }
            case "89.2" -> {
                return 1.00016f;
            }
            case "89.3" -> {
                return 1.00014f;
            }
            case "89.4" -> {
                return 1.00012f;
            }
            case "89.5" -> {
                return 1.0001f;
            }
            case "89.6" -> {
                return 1.00008f;
            }
            case "89.7" -> {
                return 1.00006f;
            }
            case "89.8" -> {
                return 1.00004f;
            }
            case "89.9" -> {
                return 1.00002f;
            }
            case "90.0" -> {
                return 1f;
            }
            default ->
                throw new UnsupportedOperationException("Error: Unknown solar altitude angle: " + val);
        }
    }
}
