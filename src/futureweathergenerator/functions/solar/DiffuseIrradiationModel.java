/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.functions.solar;

/**
 *
 * @author eugenio
 */
public enum DiffuseIrradiationModel {
    
    // Ridley, B., Boland, J. & Lauret, P. Modelling of diffuse solar fraction with multiple predictors. Renew. Energy 35, 478–483 (2010).
    Ridley_Boland_Lauret_2010,
    
    // Engerer, N. A. Minute resolution estimates of the diffuse fraction of global irradiance for southeastern Australia. Sol. Energy 116, 215–237 (2015).
    Engerer_2015,
    
    // Paulescu, E. & Blaga, R. A simple and reliable empirical model with two predictors for estimating 1-minute diffuse fraction. Sol. Energy 180, 75–84 (2019).
    Paulescu_Blaga_2019;
}
