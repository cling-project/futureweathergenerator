/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.functions.solar;

/**
 * Presents a set of functions to calculate earth to sun position.
 *
 * @author eugenio
 */
public class SolarFunctions {

    /**
     * Computes day angle from the number of day of the year.
     *
     * @param day_number number of the day in the year
     * @return day angle
     */
    public static double getDayAngle(double day_number) {
        return day_number * (360.0 / 365.25);
    }

    /**
     * Returns the correction factor for solar distance.
     *
     * @param day_angle day angle
     * @return correction factor for solar distance
     */
    public static double getCorrectionFactorForSolarDistance(double day_angle) {
        return 1.0 + 0.03344 * Math.cos(Math.toRadians(day_angle - 2.8));
    }

    /**
     * Returns the equation of time.
     *
     * @param day_angle day angle
     * @return equation of time
     */
    public static double getEquationOfTime(double day_angle) {
        return -0.128 * Math.sin(Math.toRadians(day_angle - 2.8)) - 0.165 * Math.sin(Math.toRadians(2.0 * day_angle + 19.7));
    }

    /**
     * Returns the solar time.
     *
     * @param local_standard_time local standard time
     * @param longitude longitude
     * @param time_zone time zone
     * @param equation_of_time equation of time
     * @return solar time
     */
    public static double getSolarTime(double local_standard_time, double longitude, double time_zone, double equation_of_time) {
        return local_standard_time + (longitude - (time_zone * 15.0)) / 15.0 + equation_of_time;
    }

    /**
     * Returns hour angle.
     *
     * @param solar_time solar time
     * @return hour angle
     */
    public static double getHourAngle(double solar_time) {
        return 15.0 * (solar_time - 12.0);
    }

    /**
     * Returns solar declination.
     *
     * @param day_angle day angle
     * @return solar declination
     */
    public static double getDeclination(double day_angle) {
        return Math.asin(0.3978 * Math.sin(Math.toRadians(day_angle) - 1.4 + 0.0355 * Math.sin(Math.toRadians(day_angle) - 0.0489)));
    }

    /**
     * Returns the sin solar altitude angle.
     *
     * @param latitude latitude
     * @param declination declination
     * @param hour_angle hour angle
     * @return sin solar altitude angle
     */
    public static double getSinSolarAltitudeAngle(double latitude, double declination, double hour_angle) {
        return Math.sin(Math.toRadians(latitude)) * Math.sin(declination) + Math.cos(Math.toRadians(latitude)) * Math.cos(declination) * Math.cos(Math.toRadians(hour_angle));
    }

    /**
     * Returns solar altitude angle.
     *
     * @param sinSolarAltitudeAngle sin solar altitude angle
     * @return solar altitude angle
     */
    public static double getSolarAltitudeAngle(double sinSolarAltitudeAngle) {
        return Math.asin(sinSolarAltitudeAngle) * 180.0 / Math.PI;
    }
    
    /**
     * Returns solar zenith angle in radians.
     * @param solar_altitude_angle solar altitude angle
     * @return solar zenith angle
     */
    public static double getSolarZenithAngleInRadians(double solar_altitude_angle) {
        return Math.toRadians(getSolarZenithAngleInDegrees(solar_altitude_angle));
    }
    
    /**
     * Returns solar zenith angle in degrees.
     * @param solar_altitude_angle
     * @return 
     */
    public static double getSolarZenithAngleInDegrees(double solar_altitude_angle) {
        return 90.0 - solar_altitude_angle;
    }

    /**
     * Returns extraterrestrial direct normal radiation from the correction
     * factor for solar distance.
     *
     * @param correction_factor_for_solar_distance correction factor for solar
     * distance
     * @return extraterrestrial direct normal radiation
     */
    public static double getExtraterrestrialDirectNormalRadiation(double correction_factor_for_solar_distance) {
        return correction_factor_for_solar_distance * 1367.0; // Solar Constant of 1367 W/m2.
    }

    

}
