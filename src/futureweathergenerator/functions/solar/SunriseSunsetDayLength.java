/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.functions.solar;

/**
 *
 * @author eugenio
 */
public class SunriseSunsetDayLength {
    
    private float sunrise;
    private float sunset;
    private float daylength;

    public SunriseSunsetDayLength(float sunrise, float sunset, float daylength) {
        this.sunrise = sunrise;
        this.sunset = sunset;
        this.daylength = daylength;
    }

    public float getSunrise() {
        return sunrise;
    }

    public float getSunset() {
        return sunset;
    }

    public float getDaylength() {
        return daylength;
    }

    public void setSunrise(float sunrise) {
        this.sunrise = sunrise;
    }

    public void setSunset(float sunset) {
        this.sunset = sunset;
    }

    public void setDaylength(float daylength) {
        this.daylength = daylength;
    }
    
    public static float[] calculateIdealizedHourlyTemperatures(
            float t_max_previous,
            float t_max, float t_min,
            float t_min_next,
            SunriseSunsetDayLength ssdl_previous,
            SunriseSunsetDayLength ssdl,
            SunriseSunsetDayLength ssdl_next) {
        float[] idealized_hourly_temp_values = new float[24];
        float last_t_max = t_max_previous;
        for (int i = 0; i < 24; i++) {
            if (i < ssdl.getSunrise()) {
                // Morning.
                float sunriseAndSunset = (t_max_previous - t_min) / (float) Math.log(Math.max(1f, 24f - (ssdl_previous.getSunset() - ssdl.getSunrise())));
                sunriseAndSunset = Float.isInfinite(sunriseAndSunset) ? 1 : sunriseAndSunset;
                idealized_hourly_temp_values[i] = t_max_previous
                        - (sunriseAndSunset * (float) Math.log(i + 24f - ssdl.getSunset() + 1f));
            }
            if (i >= ssdl.getSunrise() && i <= ssdl.getSunset()) {
                // Daytime.
                idealized_hourly_temp_values[i] = t_min
                        + ((t_max - t_min)
                        * (float) Math.sin(Math.PI * (i - ssdl.getSunrise()) / (ssdl.getDaylength() + 4f)));
                last_t_max = idealized_hourly_temp_values[i];
            }
            if (i > ssdl.getSunset()) {
                // Evening.
                idealized_hourly_temp_values[i] = last_t_max
                        - ((last_t_max - t_min_next)
                        / (float) Math.log(24f - (ssdl.getSunset() - ssdl_next.getSunrise()) + 1f)
                        * (float) Math.log(i - ssdl.getSunset() + 1f));
            }
        }
        return idealized_hourly_temp_values;
    }

    public static SunriseSunsetDayLength calculateDaySunriseSunsetAndDayLength(float latitude, int julian_day) {
        float gamma = 2f * (float) Math.PI / 365f * julian_day;

        float delta = 180f / (float) Math.PI * (0.006918f - 0.399912f * (float) Math.cos(gamma) + 0.070257f
                * (float) Math.sin(gamma) - 0.006758f * (float) Math.cos(2f * gamma) + 0.000907f
                * (float) Math.sin(2f * (gamma)) - 0.002697f * (float) Math.cos(3f * (gamma)) + 0.00148f
                * (float) Math.sin(3f * (gamma)));

        float cosWo = ((float) Math.sin(-0.8333f / 360f * 2f * (float) Math.PI)
                - (float) Math.sin(latitude / 360f * 2f * (float) Math.PI)
                * (float) Math.sin(delta / 360f * 2f * (float) Math.PI))
                / ((float) Math.cos(latitude / 360f * 2f * (float) Math.PI)
                * (float) Math.cos(delta / 360f * 2f * (float) Math.PI));

        float sunrise;
        float sunset;
        if (cosWo >= -1 && cosWo <= 1) {
            sunrise = 12f - (float) Math.acos(cosWo) / (15f / 360f * 2f * (float) Math.PI);
            sunset = 12f + (float) Math.acos(cosWo) / (15f / 360f * 2f * (float) Math.PI);
        } else {
            sunrise = Float.NEGATIVE_INFINITY;
            sunset = Float.NEGATIVE_INFINITY;
        }

        float daylength = sunset - sunrise;
        daylength = cosWo > 1 ? 0 : daylength;
        daylength = cosWo < -1 ? 24 : daylength;

        sunrise = daylength == 24 ? Float.POSITIVE_INFINITY : sunrise;
        sunset = daylength == 24 ? Float.POSITIVE_INFINITY : sunset;

        sunrise = sunrise == Float.POSITIVE_INFINITY ? 0 : sunrise;
        sunrise = sunrise == Float.NEGATIVE_INFINITY ? 12 : sunrise;
        sunset = sunset == Float.POSITIVE_INFINITY ? 24 : sunset;
        sunset = sunset == Float.NEGATIVE_INFINITY ? 12 : sunset;

        return new SunriseSunsetDayLength(sunrise, sunset, daylength);
    }
    
}
