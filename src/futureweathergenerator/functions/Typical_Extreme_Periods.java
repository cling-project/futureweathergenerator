/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.functions;

import futureweathergenerator.EPW.EPW;
import futureweathergenerator.EPW.EPW_Typical_Extreme_Period;
import futureweathergenerator.Months;
import java.util.Locale;

/**
 * Resets typical/extreme periods object.
 *
 * @author eugenio
 */
public class Typical_Extreme_Periods {

    /**
     * Resets typical/extreme periods object.
     *
     * @param morphedEPW morphed EPW object
     */
    public static void reset(EPW morphedEPW) {
        morphedEPW.getEpw_typical_extreme_periods().setN1_number_of_typical_extreme_periods(0);
        morphedEPW.getEpw_typical_extreme_periods().setAn_typical_extreme_periods(new EPW_Typical_Extreme_Period[0]);
    }

    /**
     * Calculates the typical and extreme periods from the morphed EPW data.
     *
     * @param morphedEPW morphed EPW object
     */
    public static void calculate(EPW morphedEPW) {
        // Sets the number of typical/extreme periods
        morphedEPW.getEpw_typical_extreme_periods().setN1_number_of_typical_extreme_periods(6);
        EPW_Typical_Extreme_Period[] typical_extreme_periods = new EPW_Typical_Extreme_Period[6];

        // Summer
        // Extreme Summer Week (nearest maximum temperature for summer)
        String days[] = getWeekDays(morphedEPW, true, "Summer");
        typical_extreme_periods[0] = new EPW_Typical_Extreme_Period(
                "Summer - Week Nearest Max Temperature For Period",
                "Extreme",
                days[0],
                days[1]);

        // Typical Summer Week (nearest average temperature for summer)
        days = getWeekDays(morphedEPW, false, "Summer");
        typical_extreme_periods[1] = new EPW_Typical_Extreme_Period(
                "Summer - Week Nearest Average Temperature For Period",
                "Typical",
                days[0],
                days[1]);

        // Winter
        // Extreme Winter Week (nearest minimum temperature for winter)
        days = getWeekDays(morphedEPW, true, "Winter");
        typical_extreme_periods[2] = new EPW_Typical_Extreme_Period(
                "Winter - Week Nearest Min Temperature For Period",
                "Extreme",
                days[0],
                days[1]);

        // Typical Winter Week (nearest average temperature for winter)
        days = getWeekDays(morphedEPW, false, "Winter");
        typical_extreme_periods[3] = new EPW_Typical_Extreme_Period(
                "Winter - Week Nearest Average Temperature For Period",
                "Typical",
                days[0],
                days[1]);

        // Autumn
        // Typical Autumn Week (nearest average temperature for autumn)
        days = getWeekDays(morphedEPW, false, "Autumn");
        typical_extreme_periods[4] = new EPW_Typical_Extreme_Period(
                "Autumn - Week Nearest Average Temperature For Period",
                "Typical",
                days[0],
                days[1]);

        // Spring
        // Typical Spring Week (nearest average temperature for spring)
        days = getWeekDays(morphedEPW, false, "Spring");
        typical_extreme_periods[5] = new EPW_Typical_Extreme_Period(
                "Spring - Week Nearest Average Temperature For Period",
                "Typical",
                days[0],
                days[1]);

        morphedEPW.getEpw_typical_extreme_periods().setAn_typical_extreme_periods(typical_extreme_periods);
    }

    /**
     * Returns the typical or extreme week for given EPW data from a specified
     * season (Summer, Winter, Autumn, and Spring).
     *
     * @param morphedEPW morphed EPW
     * @param isExtreme true for extreme, false for typical
     * @param season season of the year (Summer, Winter, Autumn, and Spring)
     * @return array of strings with: date_start, date_end,
     * season_month_start, season_month_end, week_month_start,
     * week_day_start, week_month_end, week_day_end, value, deviation
     */
    public static String[] getWeekDays(EPW morphedEPW, boolean isExtreme, String season) {
        boolean isNorthHemisphere = morphedEPW.getEpw_location().getN2_latitude() > 0;
        int[] rows1, rows2;
        Months.Abbreviation season_month_start;
        Months.Abbreviation season_month_end;
        switch (season) {
            default:
            case "Summer":
                season_month_start = isNorthHemisphere ? Months.Abbreviation.Jul : Months.Abbreviation.Jan;
                season_month_end = isNorthHemisphere ? Months.Abbreviation.Sep : Months.Abbreviation.Mar;
                break;
            case "Winter":
                season_month_start = isNorthHemisphere ? Months.Abbreviation.Jan : Months.Abbreviation.Jul;
                season_month_end = isNorthHemisphere ? Months.Abbreviation.Mar : Months.Abbreviation.Sep;
                break;
            case "Autumn":
                season_month_start = isNorthHemisphere ? Months.Abbreviation.Oct : Months.Abbreviation.Apr;
                season_month_end = isNorthHemisphere ? Months.Abbreviation.Dec : Months.Abbreviation.Jun;
                break;
            case "Spring":
                season_month_start = isNorthHemisphere ? Months.Abbreviation.Apr : Months.Abbreviation.Oct;
                season_month_end = isNorthHemisphere ? Months.Abbreviation.Jun : Months.Abbreviation.Dec;
                break;
        }
        rows1 = Months.getMonthRowIds(season_month_start);
        rows2 = Months.getMonthRowIds(season_month_end);
        int[] rows = new int[]{
            rows1[1],
            rows2[2]
        };

        float season_dbt_avg = 0;
        float season_count = 0;
        for (int i = rows[0]; i < rows2[2]; i++) {
            season_dbt_avg += morphedEPW.getEpw_data_fields().get(i).getN6_dry_bulb_temperature();
            season_count++;
        }
        season_dbt_avg /= season_count;

        float dif_avg = Float.MAX_VALUE;
        float max = 0;
        float min = 0;
        float avg = 0;
        float dif_max = Float.MIN_VALUE;
        float dif_min = Float.MIN_VALUE;
        String day1_avg = "";
        String day2_avg = "";
        String day1_max = "";
        String day2_max = "";
        String day1_min = "";
        String day2_min = "";
        Months.Abbreviation week_month_start_max = season_month_start;
        int week_day_start_max = 0;
        Months.Abbreviation week_month_end_max = season_month_end;
        int week_day_end_max = 0;
        Months.Abbreviation week_month_start_min = season_month_start;
        int week_day_start_min = 0;
        Months.Abbreviation week_month_end_min = season_month_end;
        int week_day_end_min = 0;
        Months.Abbreviation week_month_start_avg = season_month_start;
        int week_day_start_avg = 0;
        Months.Abbreviation week_month_end_avg = season_month_end;
        int week_day_end_avg = 0;
        for (int i = rows[0]; i < rows2[2]; i = i + (7 * 24)) {
            float week_dbt_avg = 0;
            float week_count = 0;
            for (int j = i; j < Math.min(i + (7 * 24), rows2[2]); j++) {
                week_dbt_avg += morphedEPW.getEpw_data_fields().get(j).getN6_dry_bulb_temperature();
                week_count++;
            }
            if (week_count == 7 * 24) {
                week_dbt_avg /= week_count;
                if (dif_avg > Math.abs(week_dbt_avg - season_dbt_avg)) {
                    avg = week_dbt_avg;
                    dif_avg = Math.abs(week_dbt_avg - season_dbt_avg);
                    week_month_start_avg = Months.Abbreviation.values()[morphedEPW.getEpw_data_fields().get(i).getN2_month() - 1];
                    week_day_start_avg = morphedEPW.getEpw_data_fields().get(i).getN3_day();
                    week_month_end_avg = Months.Abbreviation.values()[morphedEPW.getEpw_data_fields().get(i + (int) week_count - 1).getN2_month() - 1];
                    week_day_end_avg = morphedEPW.getEpw_data_fields().get(i + (int) week_count - 1).getN3_day();
                    day1_avg = morphedEPW.getEpw_data_fields().get(i).getN2_month() + "/" + morphedEPW.getEpw_data_fields().get(i).getN3_day();
                    day2_avg = morphedEPW.getEpw_data_fields().get(i + (int) week_count - 1).getN2_month() + "/" + morphedEPW.getEpw_data_fields().get(i + (int) week_count - 1).getN3_day();
                }
                if (dif_max < week_dbt_avg - season_dbt_avg) {
                    max = week_dbt_avg;
                    dif_max = week_dbt_avg - season_dbt_avg;
                    week_month_start_max = Months.Abbreviation.values()[morphedEPW.getEpw_data_fields().get(i).getN2_month() - 1];
                    week_day_start_max = morphedEPW.getEpw_data_fields().get(i).getN3_day();
                    week_month_end_max = Months.Abbreviation.values()[morphedEPW.getEpw_data_fields().get(i + (int) week_count - 1).getN2_month() - 1];
                    week_day_end_max = morphedEPW.getEpw_data_fields().get(i + (int) week_count - 1).getN3_day();
                    day1_max = morphedEPW.getEpw_data_fields().get(i).getN2_month() + "/" + morphedEPW.getEpw_data_fields().get(i).getN3_day();
                    day2_max = morphedEPW.getEpw_data_fields().get(i + (int) week_count - 1).getN2_month() + "/" + morphedEPW.getEpw_data_fields().get(i + (int) week_count - 1).getN3_day();
                }
                if (dif_min < season_dbt_avg - week_dbt_avg) {
                    min = week_dbt_avg;
                    dif_min = season_dbt_avg - week_dbt_avg;
                    week_month_start_min = Months.Abbreviation.values()[morphedEPW.getEpw_data_fields().get(i).getN2_month() - 1];
                    week_day_start_min = morphedEPW.getEpw_data_fields().get(i).getN3_day();
                    week_month_end_min = Months.Abbreviation.values()[morphedEPW.getEpw_data_fields().get(i + (int) week_count - 1).getN2_month() - 1];
                    week_day_end_min = morphedEPW.getEpw_data_fields().get(i + (int) week_count - 1).getN3_day();
                    day1_min = morphedEPW.getEpw_data_fields().get(i).getN2_month() + "/" + morphedEPW.getEpw_data_fields().get(i).getN3_day();
                    day2_min = morphedEPW.getEpw_data_fields().get(i + (int) week_count - 1).getN2_month() + "/" + morphedEPW.getEpw_data_fields().get(i + (int) week_count - 1).getN3_day();
                }
            }
        }

        String date_start, date_end, week_month_start, week_day_start, week_month_end, week_day_end, value, deviation, season_value;

        if (isExtreme) {
            if (season.equals("Summer")) {
                date_start = day1_max;
                date_end = day2_max;
                week_month_start = week_month_start_max.name();
                week_day_start = String.format("%d", week_day_start_max);
                week_month_end = week_month_end_max.name();
                week_day_end = String.format("%d", week_day_end_max);
                value = String.format(Locale.ROOT, "%.3f", max);
                deviation = String.format(Locale.ROOT, "%.3f", dif_max);
                season_value = String.format(Locale.ROOT, "%.3f", season_dbt_avg);
            } else {
                date_start = day1_min;
                date_end = day2_min;
                week_month_start = week_month_start_min.name();
                week_day_start = String.format("%d", week_day_start_min);
                week_month_end = week_month_end_min.name();
                week_day_end = String.format("%d", week_day_end_min);
                value = String.format(Locale.ROOT, "%.3f", min);
                deviation = String.format(Locale.ROOT, "%.3f", dif_min);
                season_value = String.format(Locale.ROOT, "%.3f", season_dbt_avg);
            }
        } else {
            date_start = day1_avg;
            date_end = day2_avg;
            week_month_start = week_month_start_avg.name();
            week_day_start = String.format("%d", week_day_start_avg);
            week_month_end = week_month_end_avg.name();
            week_day_end = String.format("%d", week_day_end_avg);
            value = String.format(Locale.ROOT, "%.3f", avg);
            deviation = String.format(Locale.ROOT, "%.3f", dif_avg);
            season_value = String.format(Locale.ROOT, "%.3f", season_dbt_avg);
        }

        return new String[]{
            date_start,
            date_end,
            season_month_start.name(),
            season_month_end.name(),
            season_value,
            week_month_start,
            week_day_start,
            week_month_end,
            week_day_end,
            value,
            deviation
        };
    }
}
