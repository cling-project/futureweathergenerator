/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 *
 * @author eugenio
 */
public class File {
    
    public static void unzip(InputStream source, java.io.File target) throws IOException {
        final ZipInputStream zipStream = new ZipInputStream(source);
        ZipEntry nextEntry;
        while ((nextEntry = zipStream.getNextEntry()) != null) {
            final String name = nextEntry.getName();
            // only extract files
            if (!name.endsWith("/")) {
                final java.io.File nextFile = new java.io.File(target, name);

                // create directories
                final java.io.File parent = nextFile.getParentFile();
                if (parent != null) {
                    parent.mkdirs();
                }

                // write file
                try (OutputStream targetStream = new FileOutputStream(nextFile)) {
                    copy(zipStream, targetStream);
                }
            }
        }
    }

    public static void copy(final InputStream source, final OutputStream target) throws IOException {
        final int bufferSize = 4 * 1024;
        final byte[] buffer = new byte[bufferSize];

        int nextCount;
        while ((nextCount = source.read(buffer)) >= 0) {
            target.write(buffer, 0, nextCount);
        }
    }
    
    public static void deleteDirectory(java.io.File directoryToBeDeleted) {
        java.io.File[] allContents = directoryToBeDeleted.listFiles();
        if (allContents != null) {
            for (java.io.File file : allContents) {
                deleteDirectory(file);
            }
        }
        directoryToBeDeleted.delete();
    }
    
    public static float[] readFile(String filename) {
        java.io.File file = new java.io.File(filename);
        Scanner reader;
        try {
            reader = new Scanner(file);
        } catch (FileNotFoundException ex) {
            throw new UnsupportedOperationException(" ** ERROR ** Unknown grid file name: " + filename);
        }
        List<Float> listOfFloats = new ArrayList<>();
        while (reader.hasNextLine()) {
            String line = reader.nextLine();
            listOfFloats.add(Float.valueOf(line));
        }
        int i = 0;
        float[] floats = new float[listOfFloats.size()];
        for (float f : listOfFloats) {
            floats[i++] = f;
        }
        reader.close();
        return floats;
    }
    
    public static String verifyAndAddTrailingRightSlah(String path) {
        String lastChar = path.substring(path.length() -1);
        return switch (lastChar) {
            case "\\", "/" -> path;
            default -> path + "/";
        };
    } 
}
