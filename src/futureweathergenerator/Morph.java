/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator;

import futureweathergenerator.EPW.EPW;
import futureweathergenerator.GCM.ClimateModel;
import futureweathergenerator.GCM.Scenario;
import futureweathergenerator.UHI.UrbanHeatIsland;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author eugenio
 */
public class Morph {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String PATH_EPW_FILE_1 = args[0];
        String GCMs = args[1];
        String[] GCMs_split = GCMs.split(",");
        boolean ENSEMBLE = Integer.parseInt(args[2]) != 0;
        float NUMBER_OF_HOURS_TO_SMOOTH = Float.parseFloat(args[3]);
        String PATH_OUTPUT_FOLDER = args[4];

        int NUMBER_OF_CPU_THREADS = 1;
        if (Boolean.parseBoolean(args[5])) {
            NUMBER_OF_CPU_THREADS = Runtime.getRuntime().availableProcessors();
        }
        int GRID_POINT_OPTION = Integer.parseInt(args[6]);
        boolean EPW_VARIABLE_LIMITS = Boolean.parseBoolean(args[7]);
        int SOLAR_HOUR_ADJUSTMENT_OPTION = Integer.parseInt(args[8]);
        int DIFFUSE_IRRADIATION_MODEL_OPTION = Integer.parseInt(args[9]);
        boolean URBAN_HEAT_ISLAND_EFFECT = Boolean.parseBoolean(args[10].split(":")[0]);
        int EPW_ORIGINAL_LCZ = Integer.parseInt(args[10].split(":")[1]);
        int TARGET_UHI_LCZ = Integer.parseInt(args[10].split(":")[2]);

        // Loads EPW file to morph
        EPW epw = new EPW(new StringBuffer(), PATH_EPW_FILE_1, SOLAR_HOUR_ADJUSTMENT_OPTION, DIFFUSE_IRRADIATION_MODEL_OPTION, EPW_VARIABLE_LIMITS, true);
        
        if(epw.isEPW_READ() & URBAN_HEAT_ISLAND_EFFECT) {
            // Reads urban heat island EPW.
            EPW uhiEPW = new EPW(new StringBuffer(), PATH_EPW_FILE_1, SOLAR_HOUR_ADJUSTMENT_OPTION, DIFFUSE_IRRADIATION_MODEL_OPTION, EPW_VARIABLE_LIMITS, false);
            
            // Calculates the needed adjustment to solar hour in order to accurately morph the data.
            // EPW dummyEPW = new EPW(new StringBuffer(), PATH_EPW_FILE_1, SOLAR_HOUR_ADJUSTMENT_OPTION, DIFFUSE_IRRADIATION_MODEL_OPTION, EPW_VARIABLE_LIMITS, false);
            // uhiEPW.determineHourAdjustment(dummyEPW);
            
            // Calculates Urban Effects.
            UrbanHeatIsland.calculate(PATH_OUTPUT_FOLDER, epw, uhiEPW, EPW_ORIGINAL_LCZ, TARGET_UHI_LCZ, true);
            
            // Saves morphed weather to file
            String path = File.verifyAndAddTrailingRightSlah(PATH_OUTPUT_FOLDER)
                    + uhiEPW.getEpw_location().getA3_country().replace("/", "-").replace("\\", "-")
                    + "_"
                    + uhiEPW.getEpw_location().getA2_region().replace("/", "-").replace("\\", "-")
                    + "_"
                    + uhiEPW.getEpw_location().getA1_city().replace("/", "-").replace("\\", "-");
            uhiEPW.saveEPW(new StringBuffer(), new StringBuffer(),
                    path
                    + uhiEPW.getFile_suffix()
                    + "_Present-day"
                    + ".epw");
            epw = uhiEPW;
        }
        
        // Calculates the needed adjustment to solar hour in order to accurately morph the data.
        EPW dummyEPW = new EPW(new StringBuffer(), PATH_EPW_FILE_1, SOLAR_HOUR_ADJUSTMENT_OPTION, DIFFUSE_IRRADIATION_MODEL_OPTION, EPW_VARIABLE_LIMITS, false);
        epw.determineHourAdjustment(dummyEPW);

        // Extracts the model monthly changes and interpolates data.
        // If no Ensemble was chosen, each model will produce its morphed data.
        List<ClimateModel> models = new ArrayList<>();
        if (epw.isEPW_READ()) {
            if (GCMs_split.length > 0) {
                for (String mo : GCMs_split) {
                    switch (GCModel.valueOf(mo.replace("-", "_"))) {
                        case UKESM1_0_LL, BCC_CSM2_MR, CanESM5, CanESM5_1, CanESM5_CanOE,
                                CAS_ESM2_0, CMCC_ESM2, CNRM_CM6_1_HR, CNRM_ESM2_1, EC_Earth3,
                                EC_Earth3_Veg, FGOALS_g3, GISS_E2_1_G, GISS_E2_1_H, GISS_E2_2_G,
                                IPSL_CM6A_LR, MIROC_ES2H, MIROC_ES2L, MIROC6, MRI_ESM2_0 -> {
                            ClimateModel model = new ClimateModel(
                                    mo,
                                    GCMs,
                                    epw,
                                    PATH_OUTPUT_FOLDER);
                            model.readModelGrid();
                            model.readModelData(NUMBER_OF_CPU_THREADS, GRID_POINT_OPTION);
                            model.generate(ENSEMBLE, EPW_VARIABLE_LIMITS, NUMBER_OF_HOURS_TO_SMOOTH);
                            if (ENSEMBLE) {
                                models.add(model);
                            }
                        }
                        default ->
                            throw new UnsupportedOperationException("** ERROR ** Incorrect GCM defined: " + mo);
                    }
                }
            }
        }
        
        // Ensembles all models if this was chosen by the user.
        if (ENSEMBLE) {
            ClimateModel model_ensemble = createEnsemble(models);
            model_ensemble.generate(false, EPW_VARIABLE_LIMITS, NUMBER_OF_HOURS_TO_SMOOTH);
        }
    }

    /**
     * Creates an ensemble of provided climate models to generate future EPW
     * files.
     *
     * @param models list of climate models
     * transition
     * @return the model with the ensemble
     */
    public static ClimateModel createEnsemble(List<ClimateModel> models) {
        ClimateModel model_ensemble = models.get(0);
        model_ensemble.setModel_type_string("Ensemble");
        for (int i = 1; i < models.size(); i++) {
            sumTwoModelsVariables(model_ensemble, models.get(i));
        }
        divideByNumberOfModels(model_ensemble, models.size());
        return(model_ensemble);
    }

    private static void divideByNumberOfModels(ClimateModel model_ensemble, float number_of_models) {
        divideScenario(
                model_ensemble.getTimeframe_2050().getSsp126(),
                number_of_models);
        divideScenario(
                model_ensemble.getTimeframe_2080().getSsp126(),
                number_of_models);
        divideScenario(
                model_ensemble.getTimeframe_2050().getSsp245(),
                number_of_models);
        divideScenario(
                model_ensemble.getTimeframe_2080().getSsp245(),
                number_of_models);
        divideScenario(
                model_ensemble.getTimeframe_2050().getSsp370(),
                number_of_models);
        divideScenario(
                model_ensemble.getTimeframe_2080().getSsp370(),
                number_of_models);
        divideScenario(
                model_ensemble.getTimeframe_2050().getSsp585(),
                number_of_models);
        divideScenario(
                model_ensemble.getTimeframe_2080().getSsp585(),
                number_of_models);
    }

    private static void divideScenario(Scenario ensemble_scenario, float number_of_models) {
        ensemble_scenario.interpolated_daily_maximum_near_surface_air_temperature = divideArrays(ensemble_scenario.interpolated_daily_maximum_near_surface_air_temperature,
                number_of_models);

        ensemble_scenario.interpolated_near_surface_air_temperature = divideArrays(ensemble_scenario.interpolated_near_surface_air_temperature,
                number_of_models);

        ensemble_scenario.interpolated_air_pressure_at_mean_sea_level = divideArrays(ensemble_scenario.interpolated_air_pressure_at_mean_sea_level,
                number_of_models);

        ensemble_scenario.interpolated_daily_minimum_near_surface_air_temperature = divideArrays(ensemble_scenario.interpolated_daily_minimum_near_surface_air_temperature,
                number_of_models);

        ensemble_scenario.interpolated_precipitation = divideArrays(
                ensemble_scenario.interpolated_precipitation,
                number_of_models);

        ensemble_scenario.interpolated_snow_depth = divideArrays(
                ensemble_scenario.interpolated_snow_depth,
                number_of_models);

        ensemble_scenario.interpolated_near_surface_specific_humidity = divideArrays(ensemble_scenario.interpolated_near_surface_specific_humidity,
                number_of_models);

        ensemble_scenario.interpolated_total_cloud_cover_percentage = divideArrays(ensemble_scenario.interpolated_total_cloud_cover_percentage,
                number_of_models);

        ensemble_scenario.interpolated_surface_downwelling_shortwave_radiation = divideArrays(ensemble_scenario.interpolated_surface_downwelling_shortwave_radiation,
                number_of_models);

        ensemble_scenario.interpolated_near_surface_wind_speed = divideArrays(ensemble_scenario.interpolated_near_surface_wind_speed,
                number_of_models);
    }

    private static float[] divideArrays(float[] base_array, float number_of_models) {
        for (int i = 0; i < base_array.length; i++) {
            base_array[i] = base_array[i] / number_of_models;
        }
        return base_array;
    }

    private static void sumTwoModelsVariables(ClimateModel model_ensemble, ClimateModel model) {
        sumScenarios(
                model_ensemble.getTimeframe_2050().getSsp126(),
                model.getTimeframe_2050().getSsp126());
        sumScenarios(
                model_ensemble.getTimeframe_2080().getSsp126(),
                model.getTimeframe_2080().getSsp126());
        sumScenarios(
                model_ensemble.getTimeframe_2050().getSsp245(),
                model.getTimeframe_2050().getSsp245());
        sumScenarios(
                model_ensemble.getTimeframe_2080().getSsp245(),
                model.getTimeframe_2080().getSsp245());
        sumScenarios(
                model_ensemble.getTimeframe_2050().getSsp370(),
                model.getTimeframe_2050().getSsp370());
        sumScenarios(
                model_ensemble.getTimeframe_2080().getSsp370(),
                model.getTimeframe_2080().getSsp370());
        sumScenarios(
                model_ensemble.getTimeframe_2050().getSsp585(),
                model.getTimeframe_2050().getSsp585());
        sumScenarios(
                model_ensemble.getTimeframe_2080().getSsp585(),
                model.getTimeframe_2080().getSsp585());
    }

    private static void sumScenarios(Scenario ensemble_scenario, Scenario toAddScenario) {
        ensemble_scenario.interpolated_daily_maximum_near_surface_air_temperature = sumArrays(ensemble_scenario.interpolated_daily_maximum_near_surface_air_temperature,
                toAddScenario.interpolated_daily_maximum_near_surface_air_temperature);

        ensemble_scenario.interpolated_near_surface_air_temperature = sumArrays(ensemble_scenario.interpolated_near_surface_air_temperature,
                toAddScenario.interpolated_near_surface_air_temperature);

        ensemble_scenario.interpolated_air_pressure_at_mean_sea_level = sumArrays(ensemble_scenario.interpolated_air_pressure_at_mean_sea_level,
                toAddScenario.interpolated_air_pressure_at_mean_sea_level);

        ensemble_scenario.interpolated_daily_minimum_near_surface_air_temperature = sumArrays(ensemble_scenario.interpolated_daily_minimum_near_surface_air_temperature,
                toAddScenario.interpolated_daily_minimum_near_surface_air_temperature);

        ensemble_scenario.interpolated_precipitation = sumArrays(
                ensemble_scenario.interpolated_precipitation,
                toAddScenario.interpolated_precipitation);

        ensemble_scenario.interpolated_snow_depth = sumArrays(
                ensemble_scenario.interpolated_snow_depth,
                toAddScenario.interpolated_snow_depth);

        ensemble_scenario.interpolated_near_surface_specific_humidity = sumArrays(ensemble_scenario.interpolated_near_surface_specific_humidity,
                toAddScenario.interpolated_near_surface_specific_humidity);

        ensemble_scenario.interpolated_total_cloud_cover_percentage = sumArrays(ensemble_scenario.interpolated_total_cloud_cover_percentage,
                toAddScenario.interpolated_total_cloud_cover_percentage);

        ensemble_scenario.interpolated_surface_downwelling_shortwave_radiation = sumArrays(ensemble_scenario.interpolated_surface_downwelling_shortwave_radiation,
                toAddScenario.interpolated_surface_downwelling_shortwave_radiation);

        ensemble_scenario.interpolated_near_surface_wind_speed = sumArrays(ensemble_scenario.interpolated_near_surface_wind_speed,
                toAddScenario.interpolated_near_surface_wind_speed);
    }

    private static float[] sumArrays(float[] base_array, float[] toAddArray) {
        for (int i = 0; i < base_array.length; i++) {
            base_array[i] = base_array[i] + toAddArray[i];
        }
        return base_array;
    }

}
