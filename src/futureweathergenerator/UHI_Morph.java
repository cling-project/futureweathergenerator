/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator;

import futureweathergenerator.EPW.EPW;
import futureweathergenerator.UHI.UrbanHeatIsland;

/**
 *
 * @author eugenio
 */
public class UHI_Morph {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String PATH_EPW_FILE_1 = args[0];
        String PATH_OUTPUT_FOLDER = args[1];

        boolean EPW_VARIABLE_LIMITS = Boolean.parseBoolean(args[2]);
        int EPW_ORIGINAL_LCZ = Integer.parseInt(args[3].split(":")[0]);
        int TARGET_UHI_LCZ = Integer.parseInt(args[3].split(":")[1]);

        // Loads EPW file to morph
        EPW epw = new EPW(new StringBuffer(), PATH_EPW_FILE_1, 0, 0, EPW_VARIABLE_LIMITS, true);
        
        // Reads urban heat island EPW.
        EPW uhiEPW = new EPW(new StringBuffer(), PATH_EPW_FILE_1, 0, 0, EPW_VARIABLE_LIMITS, false);

        // Calculates Urban Effects.
        UrbanHeatIsland.calculate(PATH_OUTPUT_FOLDER, epw, uhiEPW, EPW_ORIGINAL_LCZ, TARGET_UHI_LCZ, true);

        // Saves morphed weather to file
        String path = File.verifyAndAddTrailingRightSlah(PATH_OUTPUT_FOLDER)
                + uhiEPW.getEpw_location().getA3_country().replace("/", "-").replace("\\", "-")
                + "_"
                + uhiEPW.getEpw_location().getA2_region().replace("/", "-").replace("\\", "-")
                + "_"
                + uhiEPW.getEpw_location().getA1_city().replace("/", "-").replace("\\", "-");
        uhiEPW.saveEPW(new StringBuffer(), new StringBuffer(),
                path
                + uhiEPW.getFile_suffix()
                + "_Present-day"
                + ".epw");
    }
}
