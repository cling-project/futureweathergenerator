/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator;

import java.util.Locale;
import java.util.Random;

/**
 * Echos the header and footer info when running the software.
 *
 * @author eugenio
 */
public class Info {

    private final static String HOR_LINE = String.format(Locale.ROOT, "--------------------------------------------------------------------------------%n");

    /**
     * Echos the header.
     */
    public static void echoHeader() {
        StringBuilder sb = new StringBuilder();
        sb.append(HOR_LINE);
        sb.append(String.format(Locale.ROOT, "### FUTURE WEATHER GENERATOR ###%n"));
        sb.append(HOR_LINE);
        sb.append(
                String.format(Locale.ROOT, "PURPOSE:%n"
                        + "This JAVA package is a future weather generator according to scenarios of the %n"
                        + "IPCC. It uses the 'morphing' shift and stretch methods to adjust EPW files%n"
                        + "to match projected GCM models simulations in different timeframes.%n%n"
                        + "The CMIP6 experiments were the basis for the 6th IPCC report.%n"
                        + "The original EPW file will be morphed to match the Shared Socioeconomic Pathways%n"
                        + "SSP1-2.6, SSP2-4.5, SSP3-7.0, and SSP5-8.5 for the 2050 and 2080 timeframes%n"
                        + "for typical, warmer and colder years.%n"
                        + "The EPW reference meteorological period ranges between 1985 and 2014.%n%n"
                        + "In addition, it can preprocess the original EPW to include Urban Heat Island%n"
                        + "effect from World averages of max and min air temperatures between 2004 and"
                        + "2018.%n%n"
                        + "%nMore information at https://adai.pt/future-weather-generator/.%n"));
        sb.append(
                String.format(Locale.ROOT, "%nAUTHORS:%n"
                        + "Eugénio Rodrigues, ADAI, University of Coimbra - erodrigues@adai.pt%n"
                        + "\tRole: Investigation, Programming, Verification, and Curation%n"
                        + "David Carvalho, CESAM, University of Aveiro - david.carvalho@ua.pt%n"
                        + "\tRole: Simulation of future scenarios%n"
                        + "Marco S. Fernandes, ADAI, University of Coimbra - marco.fernandes@adai.pt%n"
                        + "\tRole: Verification%n"));
        sb.append(
                String.format(Locale.ROOT, "%nINSTITUTIONS:%n"
                        + "Association for the Development of Industrial Aerodynamics - ADAI%n"
                        + "Centre for Environmental and Marine Studies - CESAM%n"
                        + "University of Coimbra – UC%n"
                        + "University of Aveiro – UA%n"));
        sb.append(
                String.format(Locale.ROOT, "%nCONTEXT:%n"
                        + "This package was developed under the CLING project and supported by the%n"
                        + "Foundation for Science and Technology (grant number PTDC/EME-REN/3460/2021).%n"
                        + "Further information on the project at http://www.adai.pt/cling-project/.%n"));
        sb.append(
                String.format(Locale.ROOT, "%nDISCLAIMER:%n"
                        + "The software is provided “as is”, without warranty of any kind, express or%n"
                        + "implied, including but not limited to the warranties of merchantability,%n"
                        + "fitness for a particular purpose and noninfringement. In no event shall%n"
                        + "the authors or copyright holders be liable for any claim, damages or%n"
                        + "other liability, whether in an action of contract, tort or otherwise,%n"
                        + "arising from, out of or in connection with the software or the use or%n"
                        + "other dealings in the software.%n"));
        sb.append(
                String.format(Locale.ROOT, "%nACKNOWLEDGMENTS:%n"
                        + "This work is framed under the ‘Energy for Sustainability Initiative’ of%n"
                        + "the University of Coimbra. The authors are grateful to all Ph.D. students%n"
                        + "that have tested the app.%n%n"
                        + "We acknowledge the World Climate Research Programme, which, through its%n"
                        + "Working Group on Coupled Modelling, coordinated and promoted CMIP6. We thank%n"
                        + "the climate modeling groups for producing and making available their model%n"
                        + "output, the Earth System Grid Federation (ESGF) for archiving the data and%n"
                        + "providing access, and the multiple funding agencies who support CMIP6 and ESGF.%n")
        );
        sb.append(String.format(Locale.ROOT, "%n2022-2024 Creative Commons CC BY-NC-SA%n"));
        sb.append(HOR_LINE);
        System.out.println(sb.toString());
    }

    /**
     * Echos the footer.
     */
    public static void echoFooter() {
        Random random = new Random();
        int quote_id = random.nextInt(5);
        StringBuilder sb = new StringBuilder();
        sb.append(HOR_LINE);
        switch (quote_id) {
            case 0 ->
                sb.append(String.format(Locale.ROOT, "\"Climate change is real. It is happening right now, it is the most urgent%n"
                        + "threat facing our entire species and we need to work collectively together%n"
                        + "and stop procrastinating.\" - Leonardo Di Caprio%n"));
            case 1 ->
                sb.append(String.format(Locale.ROOT, "\"What you do makes a difference, and you have to decide what kind of%n"
                        + "difference you want to make.\" - Jane Goodall%n"));
            case 2 ->
                sb.append(String.format(Locale.ROOT, "\"It’s not that the world hasn’t had more carbon dioxide, it’s not that%n"
                        + "the world hasn’t been warmer. The problem is the speed at which things%n"
                        + "are changing. We are inducing a sixth mass extinction event kind of by%n"
                        + "accident and we don’t want to be the ‘extinctee.'\" - Bill Nye%n"));
            case 3 ->
                sb.append(String.format(Locale.ROOT, "\"One can see from space how the human race has changed the Earth. Nearly%n"
                        + "all of the available land has been cleared of forest and is now used for%n"
                        + "agriculture or urban development. The polar icecaps are shrinking and the%n"
                        + "desert areas are increasing. At night, the Earth is no longer dark, but%n"
                        + "large areas are lit up. All of this is evidence that human exploitation%n"
                        + "of the planet is reaching a critical limit. But human demands and%n"
                        + "expectations are ever-increasing. We cannot continue to pollute the%n"
                        + "atmosphere, poison the ocean and exhaust the land. There isn’t any%n"
                        + "more available.\" - Stephen Hawking%n"));
            case 4 ->
                sb.append(String.format(Locale.ROOT, "\"We really need to kick the carbon habit and stop making our energy from%n"
                        + "burning things. Climate change is also really important. You can wreck%n"
                        + "one rainforest then move, drain one area of resources and move onto%n"
                        + "another, but climate change is global.\" - David Attenborough%n"));
        }
        sb.append(HOR_LINE);
        System.out.println(sb.toString());
    }
}
