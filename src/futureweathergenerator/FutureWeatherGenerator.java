/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator;

import futureweathergenerator.EPW.EPW;
import futureweathergenerator.UHI.UrbanHeatIsland;

/**
 * This software generates future weather by morphing current EPW files to match
 * simulated general circulation model scenarios. This software also allows to
 * compare two EPW files.
 *
 * @author eugenio
 */
public class FutureWeatherGenerator implements Runnable {

    /**
     * App name.
     */
    public static final String APP_NAME = "Future Weather Generator";

    /**
     * App version.
     */
    public static final String APP_VERSION = "v2.3.1";

    /**
     * Path to directory where GCMs data is stored.
     */
    public static final String PATH_TO_RESOURCES = "resources/";
    
    /**
     * Directory of CMIP models.
     */
    public static final String CMIP = "CMIP6";

    /**
     * Filename of the app logo 23 px x 23 px size.
     */
    public static final String PATH_TO_ICON_23_23 = PATH_TO_RESOURCES + "icon_23_23.png";
    /**
     * Filename of the app logo 50 px x 50 px size.
     */
    public static final String PATH_TO_ICON_50_50 = PATH_TO_RESOURCES + "icon_50_50.png";
    /**
     * Filename of the app logo 100 px x 100 px size.
     */
    public static final String PATH_TO_ICON_100_100 = PATH_TO_RESOURCES + "icon_100_100.png";
    /**
     * Filename of the app logo 200 px x 200 px size.
     */
    public static final String PATH_TO_ICON_200_200 = PATH_TO_RESOURCES + "icon_200_200.png";
    /**
     * Filename of the app logo 500 px x 500 px size.
     */
    public static final String PATH_TO_ICON_500_500 = PATH_TO_RESOURCES + "icon_500_500.png";
    /**
     * Filename of the app logo 500 px x 500 px size cropped to content.
     */
    public static final String PATH_TO_ICON_500_500_WIN = PATH_TO_RESOURCES + "icon_500_500_win.png";
    
    public static final String PATH_TO_UHI_DATASET = PATH_TO_RESOURCES + "UHI_LCZs/global_uhi.csv";
    public static final String PATH_TO_UHI_AIR_TEMPERATURE = PATH_TO_RESOURCES + "UHI_LCZs/UHI_AirTemperature_";
    public static final String PATH_TO_UHI_SNOW_COVER = PATH_TO_RESOURCES + "UHI_LCZs/UHI_SnowCover_";

    /**
     * Filename for climate model monthly variables.
     */
    public static final String OUTPUT_FILE_VARIABLES = "GridPointVariables";
    /**
     * Filename for climate model EPW and original EPW comparison.
     */
    public static final String OUTPUT_FILE_COMPARISON = "MorphedEPWsComparison";
    /**
     * Filename for the climate model errors and warnings file.
     */
    public static final String OUTPUT_FILE_ERRORS = "ErrorsAndWarnings";

    private static final String OUTPUT_FILE_COMPARISON_TWO_EPW_FILES = "EPWsComparison";

    private final int NUMBER_OF_CPU_THREADS;

    private final String ACTION;
    private final String PATH_EPW_FILE_1;
    private final String PATH_EPW_FILE_2;
    private final String GCMs;
    private final String ENSEMBLE;
    private final int NUMBER_OF_HOURS_TO_SMOOTH;
    private final String PATH_OUTPUT_FOLDER;
    private final Integer GRID_POINT_OPTION;
    private final Boolean EPW_VARIABLE_LIMITS;
    private final Integer SOLAR_HOUR_ADJUSTMENT_OPTION;
    private final Integer DIFFUSE_IRRADIATION_MODEL_OPTION;
    private final Boolean URBAN_HEAT_ISLAND_EFFECT;
    private final Integer EPW_ORIGINAL_LCZ;
    private final Integer TARGET_UHI_LCZ;

    /**
     * Actual future weather generator engine. The array of strings are the
     * running options.
     *
     * Generation [0] -g generate, [1] path to EPW file, [2] GCMs,
     * [3] number of hours to smooth monthly transitions, [4] path to output
     * folder, [5] run in multi-thread (true or false), [6] grid point option,
     * [7] applies EPW variable limits.
     *
     * Comparison [0] -c compare, [1] path to EPW #1 file, [2] path to EPW #2
     * file, [3] path to output folder.
     *
     * @param args array of strings
     */
    public FutureWeatherGenerator(String[] args) {
        this.ACTION = args[0];
        switch (args[0]) {
            case "-c" -> {
                this.PATH_EPW_FILE_1 = args[1];
                this.PATH_EPW_FILE_2 = args[2];
                this.GCMs = null;
                this.ENSEMBLE = null;
                this.NUMBER_OF_HOURS_TO_SMOOTH = 0;
                this.PATH_OUTPUT_FOLDER = File.verifyAndAddTrailingRightSlah(args[3]);
                this.NUMBER_OF_CPU_THREADS = Runtime.getRuntime().availableProcessors();
                this.GRID_POINT_OPTION = null;
                this.EPW_VARIABLE_LIMITS = null;
                this.SOLAR_HOUR_ADJUSTMENT_OPTION = 0;
                this.DIFFUSE_IRRADIATION_MODEL_OPTION = 0;
                this.URBAN_HEAT_ISLAND_EFFECT = false;
                this.EPW_ORIGINAL_LCZ = 0;
                this.TARGET_UHI_LCZ = 0;
            }
            case "-u" -> {
                this.PATH_EPW_FILE_1 = args[1];
                this.PATH_EPW_FILE_2 = null;
                this.GCMs = null;
                this.ENSEMBLE = null;
                this.NUMBER_OF_HOURS_TO_SMOOTH = 0;
                this.PATH_OUTPUT_FOLDER = File.verifyAndAddTrailingRightSlah(args[2]);
                this.NUMBER_OF_CPU_THREADS = Runtime.getRuntime().availableProcessors();
                this.GRID_POINT_OPTION = null;
                this.EPW_VARIABLE_LIMITS = false;
                this.SOLAR_HOUR_ADJUSTMENT_OPTION = 0;
                this.DIFFUSE_IRRADIATION_MODEL_OPTION = 0;
                this.URBAN_HEAT_ISLAND_EFFECT = true;
                this.EPW_ORIGINAL_LCZ = Integer.valueOf(args[3]);
                this.TARGET_UHI_LCZ = Integer.valueOf(args[4]);
            }
            case "-g" -> {
                this.PATH_EPW_FILE_1 = args[1];
                this.PATH_EPW_FILE_2 = null;
                this.GCMs = args[2];
                this.ENSEMBLE = args[3];
                this.NUMBER_OF_HOURS_TO_SMOOTH = Integer.parseInt(args[4]);
                this.PATH_OUTPUT_FOLDER = File.verifyAndAddTrailingRightSlah(args[5]);
                if (Boolean.parseBoolean(args[6])) {
                    this.NUMBER_OF_CPU_THREADS = Runtime.getRuntime().availableProcessors();
                } else {
                    this.NUMBER_OF_CPU_THREADS = 1;
                }
                this.GRID_POINT_OPTION = Integer.valueOf(args[7]);
                this.EPW_VARIABLE_LIMITS = Boolean.valueOf(args[8]);
                this.SOLAR_HOUR_ADJUSTMENT_OPTION = Integer.valueOf(args[9]);
                this.DIFFUSE_IRRADIATION_MODEL_OPTION = Integer.valueOf(args[10]);
                this.URBAN_HEAT_ISLAND_EFFECT = Boolean.valueOf(args[11].split(":")[0]);
                this.EPW_ORIGINAL_LCZ = Integer.valueOf(args[11].split(":")[1]);
                this.TARGET_UHI_LCZ = Integer.valueOf(args[11].split(":")[2]);
            }
            default ->
                throw new UnsupportedOperationException("** ERROR ** No action argument defined correctly (-c compare or -g generate.)");
        }
    }
    
    /**
     * Start the comparison or the generation process.
     */
    @Override
    public void run() {
        Info.echoHeader();
        if (this.ACTION.equals("-c")) {
            // Compares the morphing variables of two given EPW files
            EPW epw1 = new EPW(new StringBuffer(), this.PATH_EPW_FILE_1, 0, 0, false, true);
            EPW epw2 = new EPW(new StringBuffer(), this.PATH_EPW_FILE_2, 0, 0, false, true);

            if (epw1.isEPW_READ() && epw2.isEPW_READ()) {
                // Saves comparison table between original and morphed file
                StringBuffer printOutputs = new StringBuffer();
                StringBuffer errorsAndWarnings = new StringBuffer();
                epw1.saveEPWComparison(printOutputs, errorsAndWarnings,
                        this.PATH_OUTPUT_FOLDER
                        + epw1.getEpw_location().getA3_country()
                        + "_"
                        + epw1.getEpw_location().getA1_city()
                        + "_"
                        + OUTPUT_FILE_COMPARISON_TWO_EPW_FILES
                        + ".csv", epw2);
                System.out.println(printOutputs.toString());
            }
        }
        if (this.ACTION.equals("-u")) {
            // Loads EPW file to add urban heat island effect
            EPW uhiEPW = new EPW(new StringBuffer(), PATH_EPW_FILE_1, SOLAR_HOUR_ADJUSTMENT_OPTION, DIFFUSE_IRRADIATION_MODEL_OPTION, EPW_VARIABLE_LIMITS, false);
            
            if(uhiEPW.isEPW_READ()) {
                StringBuffer printOutputs = new StringBuffer();
                StringBuffer errorsAndWarnings = new StringBuffer();
            
                // Calculates the needed adjustment to solar hour in order to accurately morph the data.
                EPW dummyEPW = new EPW(new StringBuffer(), PATH_EPW_FILE_1, SOLAR_HOUR_ADJUSTMENT_OPTION, DIFFUSE_IRRADIATION_MODEL_OPTION, EPW_VARIABLE_LIMITS, false);
                uhiEPW.determineHourAdjustment(dummyEPW);
                
                // Adjust to include urban heat island effects.
                EPW epw = new EPW(new StringBuffer(), PATH_EPW_FILE_1, SOLAR_HOUR_ADJUSTMENT_OPTION, DIFFUSE_IRRADIATION_MODEL_OPTION, EPW_VARIABLE_LIMITS, true);
                UrbanHeatIsland.calculate(PATH_OUTPUT_FOLDER, epw, uhiEPW, EPW_ORIGINAL_LCZ, TARGET_UHI_LCZ, true);

                // Saves morphed weather to file
                String path = PATH_OUTPUT_FOLDER
                        + uhiEPW.getEpw_location().getA3_country().replace("/", "-").replace("\\", "-")
                        + "_"
                        + uhiEPW.getEpw_location().getA2_region().replace("/", "-").replace("\\", "-")
                        + "_"
                        + uhiEPW.getEpw_location().getA1_city().replace("/", "-").replace("\\", "-");
                uhiEPW.saveEPW(printOutputs, errorsAndWarnings,
                        path
                        + uhiEPW.getFile_suffix()
                        + "_Present-day"
                        + ".epw");
                System.out.println(printOutputs.toString());
            }
        }
        if (this.ACTION.equals("-g")) {
            // Generates a new future weather file from a given EPW file
            Morph.main(new String[]{
                PATH_EPW_FILE_1,
                GCMs,
                ENSEMBLE,
                String.valueOf(NUMBER_OF_HOURS_TO_SMOOTH),
                PATH_OUTPUT_FOLDER,
                String.valueOf(NUMBER_OF_CPU_THREADS > 1),
                GRID_POINT_OPTION.toString(),
                EPW_VARIABLE_LIMITS.toString(),
                SOLAR_HOUR_ADJUSTMENT_OPTION.toString(),
                DIFFUSE_IRRADIATION_MODEL_OPTION.toString(),
                URBAN_HEAT_ISLAND_EFFECT.toString() + ":" + this.EPW_ORIGINAL_LCZ + ":" + this.TARGET_UHI_LCZ}
            );
        }
        Info.echoFooter();
    }
    
}
