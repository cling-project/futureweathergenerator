/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.EPW;

import java.util.Arrays;

/**
 *
 * @author eugenio
 */
public class EPW_Design_Condition {

    private final String[] parameters;

    /**
     * Design condition object.
     *
     * @param args arguments
     */
    public EPW_Design_Condition(String[] args) {
        parameters = new String[args.length];
        Arrays.copyOfRange(args, 0, args.length);
    }

    /**
     * Returns the parameters of this design condition.
     * 
     * @return array
     */
    public String[] getParameters() {
        return parameters;
    }
    
    /**
     * Returns this design condition as a string.
     *
     * @return string
     */
    public String getString() {
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < parameters.length; i++) {
            if(i == parameters.length - 1) {
                sb.append(parameters[i]);
            } else {
                sb.append(parameters[i]).append(",");
            }
        }
        return sb.toString();
    }

}
