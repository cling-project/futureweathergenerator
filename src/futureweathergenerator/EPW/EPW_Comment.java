/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.EPW;

import java.util.Locale;

/**
 * EPW comment object.
 *
 * @author eugenio
 */
public class EPW_Comment {

    /**
     * EPW object string id.
     */
    public static final String ID = "COMMENTS";

    private int ID_value;
    private String A1_comment;

    /**
     * Comment object.
     *
     * @param id comment id
     * @param args comment text
     */
    public EPW_Comment(String id, String[] args) {
        this.ID_value = Integer.parseInt(id);
        StringBuilder sb = new StringBuilder();
        for (int i = 1; i < args.length; i++) {
            sb.append(args[i]);
        }
        this.A1_comment = sb.toString();
    }

    /**
     * Comment object.
     *
     * @param id comment id
     * @param comment comment text
     */
    public EPW_Comment(int id, String comment) {
        this.ID_value = id;
        this.A1_comment = comment;
    }

    /**
     * Checks the integrity of the object.
     *
     * @return true if passes the test
     */
    public boolean checkIntegrity() {
        if (this.ID_value == -1) {
            return false;
        }
        if (this.A1_comment == null) {
            return false;
        }
        return true;
    }

    /**
     * Returns this comment as a string.
     *
     * @return string
     */
    public String getString() {
        return String.format(Locale.ROOT, ID + " " + this.ID_value + "," + this.A1_comment + "%n");
    }

    /**
     * Returns this comment id.
     *
     * @return comment id
     */
    public int getID_value() {
        return ID_value;
    }

    /**
     * Returns this comment text.
     *
     * @return text
     */
    public String getA1_comment() {
        return A1_comment;
    }

    /**
     * Comment number.
     *
     * @param ID_value number
     */
    public void setID_value(int ID_value) {
        this.ID_value = ID_value;
    }

    /**
     * Comment text.
     *
     * @param A1_comment text
     */
    public void setA1_comment(String A1_comment) {
        this.A1_comment = A1_comment;
    }

}
