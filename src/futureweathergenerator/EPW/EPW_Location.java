/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.EPW;

import java.util.Locale;

/**
 * EPW location object.
 *
 * @author eugenio
 */
public class EPW_Location {

    /**
     * EPW object string id.
     */
    public static final String ID = "LOCATION";

    private String A1_city;
    private String A2_region;
    private String A3_country;
    private String A4_source;
    private String N1_World_Meteorological_Organization_station_number;
    private float N2_latitude;
    private float N3_longitude;
    private float N4_time_zone;
    private float N5_elevation;

    /**
     * Initiates the location object.
     *
     * @param A1_city city
     * @param A2_region region
     * @param A3_country country
     * @param A4_source source
     * @param N1_World_Meteorological_Organization_station_number WMO station
     * number
     * @param N2_latitude latitude [°]
     * @param N3_longitude longitude [°]
     * @param N4_time_zone time zone [+/- h]
     * @param N5_elevation elevation [m]
     */
    public EPW_Location(String A1_city, String A2_region, String A3_country, String A4_source, String N1_World_Meteorological_Organization_station_number, float N2_latitude, float N3_longitude, float N4_time_zone, float N5_elevation) {
        this.A1_city = A1_city;
        this.A2_region = A2_region;
        this.A3_country = A3_country;
        this.A4_source = A4_source;
        this.N1_World_Meteorological_Organization_station_number = N1_World_Meteorological_Organization_station_number;
        this.N2_latitude = N2_latitude;
        this.N3_longitude = N3_longitude;
        this.N4_time_zone = N4_time_zone;
        this.N5_elevation = N5_elevation;
    }

    /**
     * Initiates the location object from an array of strings.
     *
     * @param args array of strings
     */
    public EPW_Location(String[] args) {
        if (args.length == 10) {
            // args[0] is the object id
            this.A1_city = args[1];
            this.A2_region = args[2];
            this.A3_country = args[3];
            this.A4_source = args[4];
            this.N1_World_Meteorological_Organization_station_number = args[5];
            this.N2_latitude = Float.parseFloat(args[6]);
            this.N3_longitude = Float.parseFloat(args[7]);
            this.N4_time_zone = Float.parseFloat(args[8]);
            this.N5_elevation = Float.parseFloat(args[9]);
        } else {
            this.A1_city = null;
            this.A2_region = null;
            this.A3_country = null;
            this.A4_source = null;
            this.N1_World_Meteorological_Organization_station_number = null;
            this.N2_latitude = Float.NaN;
            this.N3_longitude = Float.NaN;
            this.N4_time_zone = Float.NaN;
            this.N5_elevation = Float.NaN;
        }
    }

    /**
     * Checks the integrity of the object.
     *
     * @return true if passes the test
     */
    public boolean checkIntegrity() {
        if (this.A1_city == null) {
            return false;
        }
        if (this.A2_region == null) {
            return false;
        }
        if (this.A3_country == null) {
            return false;
        }
        if (this.A4_source == null) {
            return false;
        }
        if (this.N1_World_Meteorological_Organization_station_number == null) {
            return false;
        }
        if (this.N2_latitude == Float.NaN) {
            return false;
        }
        if (this.N3_longitude == Float.NaN) {
            return false;
        }
        if (this.N4_time_zone == Float.NaN) {
            return false;
        }
        if (this.N5_elevation == Float.NaN) {
            return false;
        }
        return true;
    }

    /**
     * Returns this location objects as a string.
     *
     * @return string
     */
    public String getString() {
        Object[] args = new Object[]{
            this.A1_city,
            this.A2_region,
            this.A3_country,
            this.A4_source,
            this.N1_World_Meteorological_Organization_station_number,
            this.N2_latitude,
            this.N3_longitude,
            this.N4_time_zone,
            this.N5_elevation
        };
        return ID + String.format(Locale.ROOT, ",%s,%s,%s,%s,%s,%.5f,%.5f,%.1f,%.1f%n", args);
    }

    /**
     * Returns the city of this location.
     *
     * @return city name
     */
    public String getA1_city() {
        return A1_city;
    }

    /**
     * Returns the region of this location.
     *
     * @return region name
     */
    public String getA2_region() {
        return A2_region;
    }

    /**
     * Returns the country of this location.
     *
     * @return country name
     */
    public String getA3_country() {
        return A3_country;
    }

    /**
     * Returns the location source.
     *
     * @return source name
     */
    public String getA4_source() {
        return A4_source;
    }

    /**
     * Returns the WMO station number.
     *
     * @return station number
     */
    public String getN1_World_Meteorological_Organization_station_number() {
        return N1_World_Meteorological_Organization_station_number;
    }

    /**
     * Returns this location latitude [°].
     *
     * @return latitude
     */
    public float getN2_latitude() {
        return N2_latitude;
    }

    /**
     * Returns this location longitude [°].
     *
     * @return longitude
     */
    public float getN3_longitude() {
        return N3_longitude;
    }

    /**
     * Returns this location time zone [+/- h].
     *
     * @return time zone
     */
    public float getN4_time_zone() {
        return N4_time_zone;
    }

    /**
     * Returns this location elevation [m].
     *
     * @return elevation
     */
    public float getN5_elevation() {
        return N5_elevation;
    }

    /**
     * Sets the city name.
     *
     * @param A1_city city name
     */
    public void setA1_city(String A1_city) {
        this.A1_city = A1_city;
    }

    /**
     * Sets the region name.
     *
     * @param A2_region region name
     */
    public void setA2_region(String A2_region) {
        this.A2_region = A2_region;
    }

    /**
     * Sets the country name.
     *
     * @param A3_country country name
     */
    public void setA3_country(String A3_country) {
        this.A3_country = A3_country;
    }

    /**
     * Sets the source name.
     *
     * @param A4_source source name
     */
    public void setA4_source(String A4_source) {
        this.A4_source = A4_source;
    }

    /**
     * Sets the WMO station number.
     *
     * @param N1_World_Meteorological_Organization_station_number station number
     */
    public void setN1_World_Meteorological_Organization_station_number(String N1_World_Meteorological_Organization_station_number) {
        this.N1_World_Meteorological_Organization_station_number = N1_World_Meteorological_Organization_station_number;
    }

    /**
     * Field Latitude, units deg, minimum -90.0, maximum +90.0, default 0.0,
     * note + is North, - is South, degree minutes represented in decimal (i.e.
     * 30 minutes is .5), type real.
     *
     * @param N2_latitude Latitude
     */
    public void setN2_latitude(float N2_latitude) {
        this.N2_latitude = N2_latitude;
    }

    /**
     * Field Longitude, units deg, minimum -180.0, maximum +180.0, default 0.0,
     * note - is West, + is East, degree minutes represented in decimal (i.e. 30
     * minutes is .5), type real.
     *
     * @param N3_longitude Longitude
     */
    public void setN3_longitude(float N3_longitude) {
        this.N3_longitude = N3_longitude;
    }

    /**
     * Field TimeZone, units hr, minimum -12.0, maximum +12.0, default 0.0, note
     * Time relative to GMT, type real.
     *
     * @param N4_time_zone Time Zone
     */
    public void setN4_time_zone(float N4_time_zone) {
        this.N4_time_zone = N4_time_zone;
    }

    /**
     * Field Elevation, units m, minimum -1000.0, maximum +9999.9, default 0.0,
     * type real.
     *
     * @param N5_elevation Elevation
     */
    public void setN5_elevation(float N5_elevation) {
        this.N5_elevation = N5_elevation;
    }

}
