/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.EPW;

import java.util.Locale;

/**
 * Ground temperatures object.
 *
 * @author eugenio
 */
public class EPW_Ground_Temperatures {

    /**
     * EPW object string id.
     */
    public static final String ID = "GROUND TEMPERATURES";

    private int N1_number_of_ground_temperature_depths;
    private EPW_Ground_Temperatures_Depth[] Nn_ground_temperatures_depth;

    /**
     * Initiates the ground temperatures object.
     *
     * @param N1_number_of_ground_temperature_depths number of ground
     * temperatures in this object
     * @param Nn_Ground_Temperatures_Depth array of ground temperatures
     */
    public EPW_Ground_Temperatures(int N1_number_of_ground_temperature_depths, EPW_Ground_Temperatures_Depth[] Nn_Ground_Temperatures_Depth) {
        this.N1_number_of_ground_temperature_depths = N1_number_of_ground_temperature_depths;
        this.Nn_ground_temperatures_depth = Nn_Ground_Temperatures_Depth;
    }

    /**
     * Initiates the ground temperatures objects from an array of strings.
     *
     * @param args array of strings
     */
    public EPW_Ground_Temperatures(String[] args) {
        if(args.length > 2) {
            // args[0] is the object id
            this.N1_number_of_ground_temperature_depths = Integer.parseInt(args[1]);
            if(this.N1_number_of_ground_temperature_depths * 16 + 2 == args.length) {
                this.Nn_ground_temperatures_depth = new EPW_Ground_Temperatures_Depth[this.N1_number_of_ground_temperature_depths];
                for (int i = 2; i < args.length; i += 16) {
                    this.Nn_ground_temperatures_depth[(i - 2) / 16] = new EPW_Ground_Temperatures_Depth(
                            Float.parseFloat(args[i]),
                            args[i + 1],
                            args[i + 2],
                            args[i + 3],
                            Float.parseFloat(args[i + 4]),
                            Float.parseFloat(args[i + 5]),
                            Float.parseFloat(args[i + 6]),
                            Float.parseFloat(args[i + 7]),
                            Float.parseFloat(args[i + 8]),
                            Float.parseFloat(args[i + 9]),
                            Float.parseFloat(args[i + 10]),
                            Float.parseFloat(args[i + 11]),
                            Float.parseFloat(args[i + 12]),
                            Float.parseFloat(args[i + 13]),
                            Float.parseFloat(args[i + 14]),
                            Float.parseFloat(args[i + 15]));
                }
            } else {
                this.N1_number_of_ground_temperature_depths = 0;
                this.Nn_ground_temperatures_depth = new EPW_Ground_Temperatures_Depth[0];
            }
        } else if(args.length == 2) {
            this.N1_number_of_ground_temperature_depths = 0;
            this.Nn_ground_temperatures_depth = new EPW_Ground_Temperatures_Depth[0];
        } else {
            this.N1_number_of_ground_temperature_depths = 0;
            this.Nn_ground_temperatures_depth = new EPW_Ground_Temperatures_Depth[0];
        }
    }
    
    /**
     * Checks the integrity of the object.
     *
     * @return true if passes the test
     */
    public boolean checkIntegrity() {
        if (this.N1_number_of_ground_temperature_depths == -1) {
            return false;
        }
        return true;
    }

    /**
     * Returns this ground temperatures object as a string.
     *
     * @return string
     */
    public String getString() {
        String line = "" + N1_number_of_ground_temperature_depths;
        for (EPW_Ground_Temperatures_Depth s : this.Nn_ground_temperatures_depth) {
            String tps = s.getN1_depth() + ","
                    + s.getN2_soil_conductivity() + ","
                    + s.getN3_soil_density() + ","
                    + s.getN4_soil_specific_heat() + ","
                    + String.format(Locale.ROOT, "%.2f", s.getN5_January_average_ground_temperature()) + ","
                    + String.format(Locale.ROOT, "%.2f", s.getN6_February_average_ground_temperature()) + ","
                    + String.format(Locale.ROOT, "%.2f", s.getN7_March_average_ground_temperature()) + ","
                    + String.format(Locale.ROOT, "%.2f", s.getN8_April_average_ground_temperature()) + ","
                    + String.format(Locale.ROOT, "%.2f", s.getN9_May_average_ground_temperature()) + ","
                    + String.format(Locale.ROOT, "%.2f", s.getN10_June_average_ground_temperature()) + ","
                    + String.format(Locale.ROOT, "%.2f", s.getN11_July_average_ground_temperature()) + ","
                    + String.format(Locale.ROOT, "%.2f", s.getN12_August_average_ground_temperature()) + ","
                    + String.format(Locale.ROOT, "%.2f", s.getN13_September_average_ground_temperature()) + ","
                    + String.format(Locale.ROOT, "%.2f", s.getN14_October_average_ground_temperature()) + ","
                    + String.format(Locale.ROOT, "%.2f", s.getN15_November_average_ground_temperature()) + ","
                    + String.format(Locale.ROOT, "%.2f", s.getN16_December_average_ground_temperature());
            line += "," + tps;
        }
        return String.format(Locale.ROOT, ID + "," + line + "%n");
    }

    /**
     * Returns the number of ground temperatures in this object.
     *
     * @return number of ground temperatures
     */
    public int getN1_number_of_ground_temperature_depths() {
        return N1_number_of_ground_temperature_depths;
    }

    /**
     * Returns an array of ground temperatures.
     *
     * @return array of ground temperatures.
     */
    public EPW_Ground_Temperatures_Depth[] getNn_ground_temperatures_depth() {
        return Nn_ground_temperatures_depth;
    }

    /**
     * Number of Ground Temperature Depths.
     *
     * @param N1_number_of_ground_temperature_depths Number of Ground
     * Temperature Depths
     */
    public void setN1_number_of_ground_temperature_depths(int N1_number_of_ground_temperature_depths) {
        this.N1_number_of_ground_temperature_depths = N1_number_of_ground_temperature_depths;
    }

    /**
     * List of ground temperature depths.
     *
     * @param Nn_ground_temperatures_depth List of ground temperature depths
     */
    public void setNn_ground_temperatures_depth(EPW_Ground_Temperatures_Depth[] Nn_ground_temperatures_depth) {
        this.Nn_ground_temperatures_depth = Nn_ground_temperatures_depth;
    }

}
