/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.EPW;

/**
 * Typical extreme period object.
 * 
 * @author eugenio
 */
public class EPW_Typical_Extreme_Period {

    private String A1_Name;
    private String A2_Type;
    private String A3_start_day;
    private String A4_end_day;

    /**
     * Initiates a typical extreme period object.
     *
     * @param A1_Name name of the typical extreme period
     * @param A2_Type type of the typical extreme period
     * @param A3_start_day starting day
     * @param A4_end_day ending day
     */
    public EPW_Typical_Extreme_Period(String A1_Name, String A2_Type, String A3_start_day, String A4_end_day) {
        this.A1_Name = A1_Name;
        this.A2_Type = A2_Type;
        this.A3_start_day = A3_start_day.replace(" ", "");
        this.A4_end_day = A4_end_day.replace(" ", "");
    }

    /**
     * Returns the name.
     *
     * @return name
     */
    public String getA1_Name() {
        return A1_Name;
    }

    /**
     * Returns the type.
     *
     * @return type
     */
    public String getA2_Type() {
        return A2_Type;
    }

    /**
     * Returns starting day.
     *
     * @return starting day
     */
    public String getA3_start_day() {
        return A3_start_day;
    }

    /**
     * Returns ending day.
     *
     * @return ending day
     */
    public String getA4_end_day() {
        return A4_end_day;
    }

    /**
     * Typical/Extreme Period Name.
     *
     * @param A1_Name Typical/Extreme Period Name
     */
    public void setA1_Name(String A1_Name) {
        this.A1_Name = A1_Name;
    }

    /**
     * Typical/Extreme Period Type.
     *
     * @param A2_Type Typical/Extreme Period Type
     */
    public void setA2_Type(String A2_Type) {
        this.A2_Type = A2_Type;
    }

    /**
     * Typical/Extreme Period start day.
     *
     * @param A3_start_day Start day
     */
    public void setA3_start_day(String A3_start_day) {
        this.A3_start_day = A3_start_day;
    }

    /**
     * Typical/Extreme Period end day.
     *
     * @param A4_end_day End day
     */
    public void setA4_end_day(String A4_end_day) {
        this.A4_end_day = A4_end_day;
    }

}
