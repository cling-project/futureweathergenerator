/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.EPW;

import java.util.Locale;

/**
 * EPW holidays/daylight savings object.
 *
 * @author eugenio
 */
public class EPW_Holidays_Daylight_Savings {

    /**
     * EPW object string id.
     */
    public final static String ID = "HOLIDAYS/DAYLIGHT SAVINGS";
    public final static String ALTERNATIVE_ID = "HOLIDAYS/DAYLIGHT SAVING";

    private String A1_leap_year_observed;
    private String A2_daylight_saving_start_day;
    private String A3_daylight_saving_end_day;
    private int N1_number_of_holidays;
    private Holiday[] An_holidays;

    /**
     * Initiates the holidays/daylight savings object.
     *
     * @param A1_leap_year_observed leap year observed
     * @param A2_daylight_saving_start_day daylight saving starting day
     * @param A3_daylight_saving_end_day daylight saving ending day
     * @param N1_number_of_holidays number of holidays
     * @param holidays array of holidays
     */
    public EPW_Holidays_Daylight_Savings(String A1_leap_year_observed, String A2_daylight_saving_start_day, String A3_daylight_saving_end_day, int N1_number_of_holidays, Holiday[] holidays) {
        this.A1_leap_year_observed = A1_leap_year_observed;
        this.A2_daylight_saving_start_day = A2_daylight_saving_start_day;
        this.A3_daylight_saving_end_day = A3_daylight_saving_end_day;
        this.N1_number_of_holidays = N1_number_of_holidays;
        this.An_holidays = holidays;
    }

    /**
     * Initiates the holidays/daylight savings object from an array of strings.
     *
     * @param args array of strings
     */
    public EPW_Holidays_Daylight_Savings(String[] args) {
        if(args.length >= 5) {
            // args[0] is the object id
            this.A1_leap_year_observed = args[1];
            this.A2_daylight_saving_start_day = args[2];
            this.A3_daylight_saving_end_day = args[3];
            this.N1_number_of_holidays = Integer.parseInt(args[4]);
            if(this.N1_number_of_holidays * 2 + 5 == args.length) {
                this.An_holidays = new Holiday[this.N1_number_of_holidays];
                for (int i = 5; i < args.length; i += 2) {
                    this.An_holidays[(i - 5) / 2] = new Holiday(args[i], args[i + 1]);
                }
            } else {
                this.A1_leap_year_observed = null;
                this.A2_daylight_saving_start_day = null;
                this.A3_daylight_saving_end_day = null;
                this.N1_number_of_holidays = -1;
                this.An_holidays = new Holiday[0];
            }
        } else {
            this.A1_leap_year_observed = null;
            this.A2_daylight_saving_start_day = null;
            this.A3_daylight_saving_end_day = null;
            this.N1_number_of_holidays = -1;
            this.An_holidays = new Holiday[0];
        }
    }
    
    /**
     * Checks the integrity of the object.
     *
     * @return true if passes the test
     */
    public boolean checkIntegrity() {
        if (this.A1_leap_year_observed == null) {
            return false;
        }
        if (this.A2_daylight_saving_start_day == null) {
            return false;
        }
        if (this.A3_daylight_saving_end_day == null) {
            return false;
        }
        if (this.N1_number_of_holidays == -1) {
            return false;
        }
        return true;
    }

    /**
     * Returns this holidays/daylight savings object as a string.
     *
     * @return string
     */
    public String getString() {
        String line = "" + this.A1_leap_year_observed + "," + this.A2_daylight_saving_start_day + "," + this.A3_daylight_saving_end_day + "," + this.N1_number_of_holidays;
        for (Holiday s : this.An_holidays) {
            line += "," + String.format("%s", s.getA4_name()) + "," + String.format("%s", s.getA5_day());
        }
        return String.format(Locale.ROOT, ID + "," + line + "%n");
    }

    /**
     * Returns the leap year observed.
     *
     * @return leap year observed
     */
    public String getA1_leap_year_observed() {
        return A1_leap_year_observed;
    }

    /**
     * Returns the daylight saving starting day.
     *
     * @return starting day
     */
    public String getA2_daylight_saving_start_day() {
        return A2_daylight_saving_start_day;
    }

    /**
     * Returns the daylight saving ending day.
     *
     * @return ending day
     */
    public String getA3_daylight_saving_end_day() {
        return A3_daylight_saving_end_day;
    }

    /**
     * Returns the number of holidays.
     *
     * @return number of holidays
     */
    public int getN1_number_of_holidays() {
        return N1_number_of_holidays;
    }

    /**
     * Returns the array of holidays.
     *
     * @return array of holidays
     */
    public Holiday[] getAn_holidays() {
        return An_holidays;
    }

    /**
     * Leap year observed. Yes if Leap Year will be observed for this file. No
     * if Leap Year days (29 Feb) should be ignored in this file.
     *
     * @param A1_leap_year_observed Yer or No
     */
    public void setA1_leap_year_observed(String A1_leap_year_observed) {
        this.A1_leap_year_observed = A1_leap_year_observed;
    }

    /**
     * Daylight Saving Start Day.
     *
     * @param A2_daylight_saving_start_day start day
     */
    public void setA2_daylight_saving_start_day(String A2_daylight_saving_start_day) {
        this.A2_daylight_saving_start_day = A2_daylight_saving_start_day;
    }

    /**
     * Daylight Saving End Day.
     *
     * @param A3_daylight_saving_end_day end day
     */
    public void setA3_daylight_saving_end_day(String A3_daylight_saving_end_day) {
        this.A3_daylight_saving_end_day = A3_daylight_saving_end_day;
    }

    /**
     * Number of holidays.
     *
     * @param N1_number_of_holidays number of holidays
     */
    public void setN1_number_of_holidays(int N1_number_of_holidays) {
        this.N1_number_of_holidays = N1_number_of_holidays;
    }

    /**
     * Array of holidays.
     *
     * @param An_holidays array of holidays
     */
    public void setAn_holidays(Holiday[] An_holidays) {
        this.An_holidays = An_holidays;
    }

    /**
     * Holiday object.
     */
    public class Holiday {

        private String A4_name;
        private String A5_day;

        /**
         * Initiates the holiday object.
         *
         * @param name holiday name
         * @param day day of holiday
         */
        public Holiday(String name, String day) {
            this.A4_name = name;
            this.A5_day = day;
        }

        /**
         * Returns the name of the holiday
         *
         * @return name of the holiday
         */
        public String getA4_name() {
            return A4_name;
        }

        /**
         * Returns day of holiday
         *
         * @return day as a string
         */
        public String getA5_day() {
            return A5_day;
        }

        /**
         * Name of the holiday.
         *
         * @param A4_name name
         */
        public void setA4_name(String A4_name) {
            this.A4_name = A4_name;
        }

        /**
         * Day of the holiday.
         *
         * @param A5_day day
         */
        public void setA5_day(String A5_day) {
            this.A5_day = A5_day;
        }

    }
}
