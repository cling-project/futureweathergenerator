/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.EPW;

import java.util.Locale;

/**
 * EPW data fields object.
 *
 * @author eugenio
 */
public class EPW_Data_Fields {

    private int N1_year;
    private int N2_month;
    private int N3_day;
    private int N4_hour;
    private int N5_minute;

    private String A1_data_source_and_uncertainty_flag;

    private Float N6_dry_bulb_temperature;
    private Float N7_dew_point_temperature;
    private Float N8_relative_humidity;
    private Float N9_atmospheric_station_pressure;
    private Float N10_extraterrestrial_horizontal_radiation;
    private Float N11_extraterrestrial_direct_normal_radiation;
    private Float N12_horizontal_infrared_radiation_intensity;
    private Float N13_global_horizontal_radiation;
    private Float N14_direct_normal_radiation;
    private Float N15_diffuse_horizontal_radiation;
    private Float N16_global_horizontal_illuminance;
    private Float N17_direct_normal_illuminance;
    private Float N18_diffuse_horizontal_illuminance;
    private Float N19_zenith_luminance;
    private Float N20_wind_direction;
    private Float N21_wind_speed;
    private Float N22_total_sky_cover;
    private Float N23_opaque_sky_cover;
    private Float N24_visibility;
    private Float N25_ceiling_height;
    private Float N26_present_weather_observation;
    private String N27_present_weather_code;
    private Float N28_precipitable_water;
    private Float N29_aerosol_optical_depth;
    private Float N30_snow_depth;
    private Float N31_days_since_last_snowfall;
    private Float N32_albedo;
    private Float N33_liquid_precipitation_depth;
    private Float N34_liquid_precipitation_quantity;

    /**
     * Initiates the data field object.
     *
     * @param N1_year year
     * @param N2_month month
     * @param N3_day day
     * @param N4_hour hour
     * @param N5_minute minute
     * @param A1_dataSourceAndUncertaintyFlag data source and uncertainty flag
     * @param N6_dry_bulb_temperature dry bulb temperature
     * @param N7_dew_point_temperature dew point temperature
     * @param N8_relative_humidity relative humidity
     * @param N9_atmospheric_station_pressure atmospheric station pressure
     * @param N10_extraterrestrial_horizontal_radiation extraterrestrial
     * horizontal radiation
     * @param N11_extraterrestrial_direct_normal_radiation extraterrestrial
     * direct normal radiation
     * @param N12_horizontal_infrared_radiation_intensity horizontal infrared
     * radiation intensity
     * @param N13_global_horizontal_radiation global horizontal radiation
     * @param N14_direct_normal_radiation direct normal radiation
     * @param N15_diffuse_horizontal_radiation diffuse horizontal radiation
     * @param N16_global_horizontal_illuminance global horizontal illuminance
     * @param N17_direct_normal_illuminance direct normal illuminance
     * @param N18_diffuse_horizontal_illuminance diffuse horizontal illuminance
     * @param N19_zenith_luminance zenith luminance
     * @param N20_wind_direction wind direction
     * @param N21_wind_speed wind speed
     * @param N22_total_sky_cover total sky cover
     * @param N23_opaque_sky_cover opaque sky cover
     * @param N24_visibility visibility
     * @param N25_ceiling_height ceiling height
     * @param N26_present_weather_observation present weather observation
     * @param N27_present_weather_codes present weather codes
     * @param N28_precipitable_water precipitable water
     * @param N29_aerosol_optical_depth aerosol optical depth
     * @param N30_snow_depth snow depth
     * @param N31_days_since_last_snowfall days since last snowfall
     * @param N32_albedo albedo
     * @param N33_liquid_precipitation_depth liquid precipitation depth
     * @param N34_liquid_precipitation_quantity liquid precipitation quantity
     */
    public EPW_Data_Fields(int N1_year, int N2_month, int N3_day, int N4_hour, int N5_minute, String A1_dataSourceAndUncertaintyFlag, Float N6_dry_bulb_temperature, Float N7_dew_point_temperature, Float N8_relative_humidity, Float N9_atmospheric_station_pressure, Float N10_extraterrestrial_horizontal_radiation, Float N11_extraterrestrial_direct_normal_radiation, Float N12_horizontal_infrared_radiation_intensity, Float N13_global_horizontal_radiation, Float N14_direct_normal_radiation, Float N15_diffuse_horizontal_radiation, Float N16_global_horizontal_illuminance, Float N17_direct_normal_illuminance, Float N18_diffuse_horizontal_illuminance, Float N19_zenith_luminance, Float N20_wind_direction, Float N21_wind_speed, Float N22_total_sky_cover, Float N23_opaque_sky_cover, Float N24_visibility, Float N25_ceiling_height, Float N26_present_weather_observation, String N27_present_weather_codes, Float N28_precipitable_water, Float N29_aerosol_optical_depth, Float N30_snow_depth, Float N31_days_since_last_snowfall, Float N32_albedo, Float N33_liquid_precipitation_depth, Float N34_liquid_precipitation_quantity) {
        this.N1_year = N1_year;
        this.N2_month = N2_month;
        this.N3_day = N3_day;
        this.N4_hour = N4_hour;
        this.N5_minute = N5_minute;
        this.A1_data_source_and_uncertainty_flag = A1_dataSourceAndUncertaintyFlag;
        this.N6_dry_bulb_temperature = N6_dry_bulb_temperature;
        this.N7_dew_point_temperature = N7_dew_point_temperature;
        this.N8_relative_humidity = N8_relative_humidity;
        this.N9_atmospheric_station_pressure = N9_atmospheric_station_pressure;
        this.N10_extraterrestrial_horizontal_radiation = N10_extraterrestrial_horizontal_radiation;
        this.N11_extraterrestrial_direct_normal_radiation = N11_extraterrestrial_direct_normal_radiation;
        this.N12_horizontal_infrared_radiation_intensity = N12_horizontal_infrared_radiation_intensity;
        this.N13_global_horizontal_radiation = N13_global_horizontal_radiation;
        this.N14_direct_normal_radiation = N14_direct_normal_radiation;
        this.N15_diffuse_horizontal_radiation = N15_diffuse_horizontal_radiation;
        this.N16_global_horizontal_illuminance = N16_global_horizontal_illuminance;
        this.N17_direct_normal_illuminance = N17_direct_normal_illuminance;
        this.N18_diffuse_horizontal_illuminance = N18_diffuse_horizontal_illuminance;
        this.N19_zenith_luminance = N19_zenith_luminance;
        this.N20_wind_direction = N20_wind_direction;
        this.N21_wind_speed = N21_wind_speed;
        this.N22_total_sky_cover = N22_total_sky_cover;
        this.N23_opaque_sky_cover = N23_opaque_sky_cover;
        this.N24_visibility = N24_visibility;
        this.N25_ceiling_height = N25_ceiling_height;
        this.N26_present_weather_observation = N26_present_weather_observation;
        this.N27_present_weather_code = N27_present_weather_codes;
        this.N28_precipitable_water = N28_precipitable_water;
        this.N29_aerosol_optical_depth = N29_aerosol_optical_depth;
        this.N30_snow_depth = N30_snow_depth;
        this.N31_days_since_last_snowfall = N31_days_since_last_snowfall;
        this.N32_albedo = N32_albedo;
        this.N33_liquid_precipitation_depth = N33_liquid_precipitation_depth;
        this.N34_liquid_precipitation_quantity = N34_liquid_precipitation_quantity;
    }

    /**
     * Initiates the data fields object from an array of strings.
     *
     * @param args array of strings
     * @param EPW_VARIABLE_LIMITS apply the EPW variable limits
     * @param errorsAndWarnings String buffer for errors and warnings
     */
    public EPW_Data_Fields(String[] args, boolean EPW_VARIABLE_LIMITS, StringBuffer errorsAndWarnings) {
        if(args.length == 32 || args.length == 35) {
            this.N1_year = Integer.parseInt(args[0]);
            this.N2_month = Integer.parseInt(args[1]);
            this.N3_day = Integer.parseInt(args[2]);
            this.N4_hour = Integer.parseInt(args[3]);
            this.N5_minute = Integer.parseInt(args[4]);
            this.A1_data_source_and_uncertainty_flag = args[5];
            this.N6_dry_bulb_temperature = args[6].equals("99.9") ? Float.NaN : Float.valueOf(args[6]);
            this.N7_dew_point_temperature = args[7].equals("99.9") ? Float.NaN : Float.valueOf(args[7]);
            this.N8_relative_humidity = args[8].equals("999.") || args[8].equals("999") ? Float.NaN : Float.valueOf(args[8]);
            this.N9_atmospheric_station_pressure = args[9].equals("999999.") || args[9].equals("999999") ? Float.NaN : Float.valueOf(args[9]);
            this.N10_extraterrestrial_horizontal_radiation = args[10].equals("9999.") || args[10].equals("9999") ? Float.NaN : Float.valueOf(args[10]);
            this.N11_extraterrestrial_direct_normal_radiation = args[11].equals("9999.") || args[11].equals("9999") ? Float.NaN : Float.valueOf(args[11]);
            this.N12_horizontal_infrared_radiation_intensity = args[12].equals("9999.") || args[12].equals("9999") ? Float.NaN : Float.valueOf(args[12]);
            this.N13_global_horizontal_radiation = args[13].equals("9999.") || args[13].equals("9999") ? Float.NaN : Float.valueOf(args[13]);
            this.N14_direct_normal_radiation = args[14].equals("9999.") || args[14].equals("9999") ? Float.NaN : Float.valueOf(args[14]);
            this.N15_diffuse_horizontal_radiation = args[15].equals("9999.") || args[15].equals("9999") ? Float.NaN : Float.valueOf(args[15]);
            this.N16_global_horizontal_illuminance = args[16].equals("999999.") || args[16].equals("999999") ? Float.NaN : Float.valueOf(args[16]);
            this.N17_direct_normal_illuminance = args[17].equals("999999.") || args[17].equals("999999") ? Float.NaN : Float.valueOf(args[17]);
            this.N18_diffuse_horizontal_illuminance = args[18].equals("999999.") || args[18].equals("999999") ? Float.NaN : Float.valueOf(args[18]);
            this.N19_zenith_luminance = args[19].equals("99999.") || args[19].equals("99999") ? Float.NaN : Float.valueOf(args[19]);
            this.N20_wind_direction = args[20].equals("999.") || args[20].equals("999") ? Float.NaN : Float.valueOf(args[20]);
            this.N21_wind_speed = args[21].equals("999.") || args[21].equals("999") ? Float.NaN : Float.valueOf(args[21]);
            this.N22_total_sky_cover = args[22].equals("99") ? Float.NaN : Float.valueOf(args[22]);
            this.N23_opaque_sky_cover = args[23].equals("99") ? Float.NaN : Float.valueOf(args[23]);
            this.N24_visibility = args[24].equals("9999") ? Float.NaN : Float.valueOf(args[24]);
            this.N25_ceiling_height = args[25].equals("99999") ? Float.NaN : Float.valueOf(args[25]);
            this.N26_present_weather_observation = args[26].equals("9") ? Float.NaN : Float.valueOf(args[26]);
            this.N27_present_weather_code = args[27];
            this.N28_precipitable_water = args[28].equals("999") ? Float.NaN : Float.valueOf(args[28]);
            this.N29_aerosol_optical_depth = args[29].equals("999") ? Float.NaN : Float.valueOf(args[29]);
            this.N30_snow_depth = args[30].equals("999") ? Float.NaN : Float.valueOf(args[30]);
            this.N31_days_since_last_snowfall = args[31].equals("99") ? Float.NaN : Float.valueOf(args[31]);
            if (args.length > 33) {
                this.N32_albedo = args[32].equals("999") ? Float.NaN : Float.valueOf(args[32]);
                this.N33_liquid_precipitation_depth = args[33].equals("999") ? Float.NaN : Float.valueOf(args[33]);
                this.N34_liquid_precipitation_quantity = args[34].equals("99") ? Float.NaN : Float.valueOf(args[34]);
            } else {
                this.N32_albedo = Float.NaN;
                this.N33_liquid_precipitation_depth = Float.NaN;
                this.N34_liquid_precipitation_quantity = Float.NaN;
            }
            correctFieldLimitsAndMissingValues(EPW_VARIABLE_LIMITS, errorsAndWarnings);
        } else {
            this.N1_year = -1;
            this.N2_month = -1;
            this.N3_day = -1;
            this.N4_hour = -1;
            this.N5_minute = -1;
            this.A1_data_source_and_uncertainty_flag = null;
            this.N6_dry_bulb_temperature = Float.NaN;
            this.N7_dew_point_temperature = Float.NaN;
            this.N8_relative_humidity = Float.NaN;
            this.N9_atmospheric_station_pressure = Float.NaN;
            this.N10_extraterrestrial_horizontal_radiation = Float.NaN;
            this.N11_extraterrestrial_direct_normal_radiation = Float.NaN;
            this.N12_horizontal_infrared_radiation_intensity = Float.NaN;
            this.N13_global_horizontal_radiation = Float.NaN;
            this.N14_direct_normal_radiation = Float.NaN;
            this.N15_diffuse_horizontal_radiation = Float.NaN;
            this.N16_global_horizontal_illuminance = Float.NaN;
            this.N17_direct_normal_illuminance = Float.NaN;
            this.N18_diffuse_horizontal_illuminance = Float.NaN;
            this.N19_zenith_luminance = Float.NaN;
            this.N20_wind_direction = Float.NaN;
            this.N21_wind_speed = Float.NaN;
            this.N22_total_sky_cover = Float.NaN;
            this.N23_opaque_sky_cover = Float.NaN;
            this.N24_visibility = Float.NaN;
            this.N25_ceiling_height = Float.NaN;
            this.N26_present_weather_observation = Float.NaN;
            this.N27_present_weather_code = null;
            this.N28_precipitable_water = Float.NaN;
            this.N29_aerosol_optical_depth = Float.NaN;
            this.N30_snow_depth = Float.NaN;
            this.N31_days_since_last_snowfall = Float.NaN;
            this.N32_albedo = Float.NaN;
            this.N33_liquid_precipitation_depth = Float.NaN;
            this.N34_liquid_precipitation_quantity = Float.NaN;
        }
    }
    
    /**
     * Checks the integrity of the object.
     *
     * @return true if passes the test
     */
    public boolean checkIntegrity() {
        if (this.N1_year == -1) {
            return false;
        }
        if (this.N2_month == -1) {
            return false;
        }
        if (this.N3_day == -1) {
            return false;
        }
        if (this.N4_hour == -1) {
            return false;
        }
        if (this.N5_minute == -1) {
            return false;
        }
        return true;
    }

    private void correctFieldLimitsAndMissingValues(boolean EPW_VARIABLE_LIMITS, StringBuffer errorsAndWarnings) {
        String action = EPW_VARIABLE_LIMITS ? "corrected" : "not corrected";

        if (!this.N6_dry_bulb_temperature.isNaN()) {
            if (this.N6_dry_bulb_temperature < -70.0f) {
                String err = " ** WARNING ** EPW N6_dry_bulb_temperature < -70 °C [%s]: month %s, day %s, hour %s, value %s %n".formatted(new Object[]{
                    action, this.N2_month,
                    this.N3_day,
                    this.N4_hour,
                    this.N6_dry_bulb_temperature});
                errorsAndWarnings.append(err);
                if (EPW_VARIABLE_LIMITS) {
                    this.N6_dry_bulb_temperature = -70.0f;
                }
            }
            if (this.N6_dry_bulb_temperature > 70.0f) {
                String err = " ** WARNING ** EPW N6_dry_bulb_temperature > 70 °C [%s]: month %s, day %s, hour %s, value %s %n".formatted(new Object[]{
                    action, this.N2_month,
                    this.N3_day,
                    this.N4_hour,
                    this.N6_dry_bulb_temperature});
                errorsAndWarnings.append(err);
                if (EPW_VARIABLE_LIMITS) {
                    this.N6_dry_bulb_temperature = 70.0f;
                }
            }
        }
        if (!this.N7_dew_point_temperature.isNaN()) {
            if (this.N7_dew_point_temperature < -70.0f) {
                String err = " ** WANRING ** EPW N7_dew_point_temperature < -70 °C [%s]: month %s, day %s, hour %s, value %s %n".formatted(new Object[]{
                    action, this.N2_month,
                    this.N3_day,
                    this.N4_hour,
                    this.N7_dew_point_temperature});
                errorsAndWarnings.append(err);
                if (EPW_VARIABLE_LIMITS) {
                    this.N7_dew_point_temperature = -70.0f;
                }
            }
            if (this.N7_dew_point_temperature > 70.0f) {
                String err = " ** WARNING ** EPW N7_dew_point_temperature > 70 °C [%s]: month %s, day %s, hour %s, value %s %n".formatted(new Object[]{
                    action, this.N2_month,
                    this.N3_day,
                    this.N4_hour,
                    this.N7_dew_point_temperature});
                errorsAndWarnings.append(err);
                if (EPW_VARIABLE_LIMITS) {
                    this.N7_dew_point_temperature = 70.0f;
                }
            }
        }
        if (!this.N8_relative_humidity.isNaN()) {
            if (this.N8_relative_humidity < 0.0f) {
                String err = " ** WARNING ** EPW N8_relative_humidity < 0 %% [%s]: month %s, day %s, hour %s, value %s %n".formatted(new Object[]{
                    action, this.N2_month,
                    this.N3_day,
                    this.N4_hour,
                    this.N8_relative_humidity});
                errorsAndWarnings.append(err);
                if (EPW_VARIABLE_LIMITS) {
                    this.N8_relative_humidity = 0.0f;
                }
            }
            if (this.N8_relative_humidity > 110.0f) {
                String err = " ** WARNING ** EPW N8_relative_humidity > 110 %% [%s]: month %s, day %s, hour %s, value %s %n".formatted(new Object[]{
                    action, this.N2_month,
                    this.N3_day,
                    this.N4_hour,
                    this.N8_relative_humidity});
                errorsAndWarnings.append(err);
                if (EPW_VARIABLE_LIMITS) {
                    this.N8_relative_humidity = 110.0f;
                }
            }
        }
        if (!this.N9_atmospheric_station_pressure.isNaN()) {
            if (this.N9_atmospheric_station_pressure < 31000.0f) {
                String err = " ** WARNING ** EPW N9_atmospheric_station_pressure < 31000 Pa [%s]: month %s, day %s, hour %s, value %s %n".formatted(new Object[]{
                    action, this.N2_month,
                    this.N3_day,
                    this.N4_hour,
                    this.N9_atmospheric_station_pressure});
                errorsAndWarnings.append(err);
                if (EPW_VARIABLE_LIMITS) {
                    this.N9_atmospheric_station_pressure = 31000.0f;
                }
            }
            if (this.N9_atmospheric_station_pressure > 120000.0f) {
                String err = " ** WARNING ** EPW N9_atmospheric_station_pressure > 120000 Pa [%s]: month %s, day %s, hour %s, value %s %n".formatted(new Object[]{
                    action, this.N2_month,
                    this.N3_day,
                    this.N4_hour,
                    this.N9_atmospheric_station_pressure});
                errorsAndWarnings.append(err);
                if (EPW_VARIABLE_LIMITS) {
                    this.N9_atmospheric_station_pressure = 120000.0f;
                }
            }
        }
        if (!this.N10_extraterrestrial_horizontal_radiation.isNaN()) {
            if (this.N10_extraterrestrial_horizontal_radiation < 0.0f) {
                String err = " ** WARNING ** EPW N10_extraterrestrial_horizontal_radiation < 0 Wh/m2 [%s]: month %s, day %s, hour %s, value %s %n".formatted(new Object[]{
                    action, this.N2_month,
                    this.N3_day,
                    this.N4_hour,
                    this.N10_extraterrestrial_horizontal_radiation});
                errorsAndWarnings.append(err);
                if (EPW_VARIABLE_LIMITS) {
                    this.N10_extraterrestrial_horizontal_radiation = 0.0f;
                }
            }
            if (this.N10_extraterrestrial_horizontal_radiation > 9998.0f) {
                String err = " ** WARNING ** EPW N10_extraterrestrial_horizontal_radiation > 9998 Wh/m2 [%s]: month %s, day %s, hour %s, value %s %n".formatted(new Object[]{
                    action, this.N2_month,
                    this.N3_day,
                    this.N4_hour,
                    this.N10_extraterrestrial_horizontal_radiation});
                errorsAndWarnings.append(err);
                if (EPW_VARIABLE_LIMITS) {
                    this.N10_extraterrestrial_horizontal_radiation = 9998.0f;
                }
            }
        }
        if (!this.N11_extraterrestrial_direct_normal_radiation.isNaN()) {
            if (this.N11_extraterrestrial_direct_normal_radiation < 0.0f) {
                String err = " ** WARNING ** EPW N11_extraterrestrial_direct_normal_radiation < 0 Wh/m2 [%s]: month %s, day %s, hour %s, value %s %n".formatted(new Object[]{
                    action, this.N2_month,
                    this.N3_day,
                    this.N4_hour,
                    this.N11_extraterrestrial_direct_normal_radiation});
                errorsAndWarnings.append(err);
                if (EPW_VARIABLE_LIMITS) {
                    this.N11_extraterrestrial_direct_normal_radiation = 0.0f;
                }
            }
            if (this.N11_extraterrestrial_direct_normal_radiation > 9998.0f) {
                String err = " ** WARNING ** EPW N11_extraterrestrial_direct_normal_radiation > 9998 Wh/m2 [%s]: month %s, day %s, hour %s, value %s %n".formatted(new Object[]{
                    action, this.N2_month,
                    this.N3_day,
                    this.N4_hour,
                    this.N11_extraterrestrial_direct_normal_radiation});
                errorsAndWarnings.append(err);
                if (EPW_VARIABLE_LIMITS) {
                    this.N11_extraterrestrial_direct_normal_radiation = 9998.0f;
                }
            }
        }
        if (!this.N12_horizontal_infrared_radiation_intensity.isNaN()) {
            if (this.N12_horizontal_infrared_radiation_intensity < 0.0f) {
                String err = " ** WARNING ** EPW N12_horizontal_infrared_radiation_intensity < 0 Wh/m2 [%s]: month %s, day %s, hour %s, value %s %n".formatted(new Object[]{
                    action, this.N2_month,
                    this.N3_day,
                    this.N4_hour,
                    this.N12_horizontal_infrared_radiation_intensity});
                errorsAndWarnings.append(err);
                if (EPW_VARIABLE_LIMITS) {
                    this.N12_horizontal_infrared_radiation_intensity = 0.0f;
                }
            }
            if (this.N12_horizontal_infrared_radiation_intensity > 9998.0f) {
                String err = " ** WARNING ** EPW N12_horizontal_infrared_radiation_intensity > 9998 Wh/m2 [%s]: month %s, day %s, hour %s, value %s %n".formatted(new Object[]{
                    action, this.N2_month,
                    this.N3_day,
                    this.N4_hour,
                    this.N12_horizontal_infrared_radiation_intensity});
                errorsAndWarnings.append(err);
                if (EPW_VARIABLE_LIMITS) {
                    this.N12_horizontal_infrared_radiation_intensity = 9998.0f;
                }
            }
        }
        if (!this.N13_global_horizontal_radiation.isNaN()) {
            if (this.N13_global_horizontal_radiation < 0.0f) {
                String err = " ** WARNING ** EPW N13_global_horizontal_radiation < 0 Wh/m2 [%s]: month %s, day %s, hour %s, value %s %n".formatted(new Object[]{
                    action, this.N2_month,
                    this.N3_day,
                    this.N4_hour,
                    this.N13_global_horizontal_radiation});
                errorsAndWarnings.append(err);
                if (EPW_VARIABLE_LIMITS) {
                    this.N13_global_horizontal_radiation = 0.0f;
                }
            }
            if (this.N13_global_horizontal_radiation > 9998.0f) {
                String err = " ** WARNING ** EPW N13_global_horizontal_radiation > 9998 Wh/m2 [%s]: month %s, day %s, hour %s, value %s %n".formatted(new Object[]{
                    action, this.N2_month,
                    this.N3_day,
                    this.N4_hour,
                    this.N13_global_horizontal_radiation});
                errorsAndWarnings.append(err);
                if (EPW_VARIABLE_LIMITS) {
                    this.N13_global_horizontal_radiation = 9998.0f;
                }
            }
        }
        if (!this.N14_direct_normal_radiation.isNaN()) {
            if (this.N14_direct_normal_radiation < 0.0f) {
                String err = " ** WARNING ** EPW N14_direct_normal_radiation < 0 Wh/m2 [%s]: month %s, day %s, hour %s, value %s %n".formatted(new Object[]{
                    action, this.N2_month,
                    this.N3_day,
                    this.N4_hour,
                    this.N14_direct_normal_radiation});
                errorsAndWarnings.append(err);
                if (EPW_VARIABLE_LIMITS) {
                    this.N14_direct_normal_radiation = 0.0f;
                }
            }
            if (this.N14_direct_normal_radiation > 9998.0f) {
                String err = " ** WARNING ** EPW N14_direct_normal_radiation > 9998 Wh/m2 [%s]: month %s, day %s, hour %s, value %s %n".formatted(new Object[]{
                    action, this.N2_month,
                    this.N3_day,
                    this.N4_hour,
                    this.N14_direct_normal_radiation});
                errorsAndWarnings.append(err);
                if (EPW_VARIABLE_LIMITS) {
                    this.N14_direct_normal_radiation = 9998.0f;
                }
            }
        }
        if (!this.N15_diffuse_horizontal_radiation.isNaN()) {
            if (this.N15_diffuse_horizontal_radiation < 0.0f) {
                String err = " ** WARNING ** EPW N15_diffuse_horizontal_radiation < 0 Wh/m2 [%s]: month %s, day %s, hour %s, value %s %n".formatted(new Object[]{
                    action, this.N2_month,
                    this.N3_day,
                    this.N4_hour,
                    this.N15_diffuse_horizontal_radiation});
                errorsAndWarnings.append(err);
                if (EPW_VARIABLE_LIMITS) {
                    this.N15_diffuse_horizontal_radiation = 0.0f;
                }
            }
            if (this.N15_diffuse_horizontal_radiation > 9998.0f) {
                String err = " ** WARNING ** EPW N15_diffuse_horizontal_radiation > 9998 Wh/m2 [%s]: month %s, day %s, hour %s, value %s %n".formatted(new Object[]{
                    action, this.N2_month,
                    this.N3_day,
                    this.N4_hour,
                    this.N15_diffuse_horizontal_radiation});
                errorsAndWarnings.append(err);
                if (EPW_VARIABLE_LIMITS) {
                    this.N15_diffuse_horizontal_radiation = 9998.0f;
                }
            }
        }
        if (!this.N16_global_horizontal_illuminance.isNaN()) {
            if (this.N16_global_horizontal_illuminance < 0.0f) {
                String err = " ** WARNING ** EPW N16_global_horizontal_illuminance < 0 lux [%s]: month %s, day %s, hour %s, value %s %n".formatted(new Object[]{
                    action, this.N2_month,
                    this.N3_day,
                    this.N4_hour,
                    this.N16_global_horizontal_illuminance});
                errorsAndWarnings.append(err);
                if (EPW_VARIABLE_LIMITS) {
                    this.N16_global_horizontal_illuminance = 0.0f;
                }
            }
            if (this.N16_global_horizontal_illuminance >= 999900.0f) {
                String err = " ** WARNING ** EPW N16_global_horizontal_illuminance >= 999900 lux [%s]: month %s, day %s, hour %s, value %s %n".formatted(new Object[]{
                    action, this.N2_month,
                    this.N3_day,
                    this.N4_hour,
                    this.N16_global_horizontal_illuminance});
                errorsAndWarnings.append(err);
                if (EPW_VARIABLE_LIMITS) {
                    this.N16_global_horizontal_illuminance = 999889.0f;
                }
            }
        }
        if (!this.N17_direct_normal_illuminance.isNaN()) {
            if (this.N17_direct_normal_illuminance < 0.0f) {
                String err = " ** WARNING ** EPW N17_direct_normal_illuminance < 0 lux [%s]: month %s, day %s, hour %s, value %s %n".formatted(new Object[]{
                    action, this.N2_month,
                    this.N3_day,
                    this.N4_hour,
                    this.N17_direct_normal_illuminance});
                errorsAndWarnings.append(err);
                if (EPW_VARIABLE_LIMITS) {
                    this.N17_direct_normal_illuminance = 0.0f;
                }
            }
            if (this.N17_direct_normal_illuminance >= 999900.0f) {
                String err = " ** WARNING ** EPW N17_direct_normal_illuminance >= 999900 lux [%s]: month %s, day %s, hour %s, value %s %n".formatted(new Object[]{
                    action, this.N2_month,
                    this.N3_day,
                    this.N4_hour,
                    this.N17_direct_normal_illuminance});
                errorsAndWarnings.append(err);
                if (EPW_VARIABLE_LIMITS) {
                    this.N17_direct_normal_illuminance = 999889.0f;
                }
            }
        }
        if (!this.N18_diffuse_horizontal_illuminance.isNaN()) {
            if (this.N18_diffuse_horizontal_illuminance < 0.0f) {
                String err = " ** WARNING ** EPW N18_diffuse_horizontal_illuminance < 0 lux [%s]: month %s, day %s, hour %s, value %s %n".formatted(new Object[]{
                    action, this.N2_month,
                    this.N3_day,
                    this.N4_hour,
                    this.N18_diffuse_horizontal_illuminance});
                errorsAndWarnings.append(err);
                if (EPW_VARIABLE_LIMITS) {
                    this.N18_diffuse_horizontal_illuminance = 0.0f;
                }
            }
            if (this.N18_diffuse_horizontal_illuminance >= 999900.0f) {
                String err = " ** WARNING ** EPW N18_diffuse_horizontal_illuminance >= 999900 lux [%s]: month %s, day %s, hour %s, value %s %n".formatted(new Object[]{
                    action, this.N2_month,
                    this.N3_day,
                    this.N4_hour,
                    this.N18_diffuse_horizontal_illuminance});
                errorsAndWarnings.append(err);
                if (EPW_VARIABLE_LIMITS) {
                    this.N18_diffuse_horizontal_illuminance = 999889.0f;
                }
            }
        }
        if (!this.N19_zenith_luminance.isNaN()) {
            if (this.N19_zenith_luminance < 0.0f) {
                String err = " ** WARNING ** EPW N19_zenith_luminance < 0 Cd/m2 [%s]: month %s, day %s, hour %s, value %s %n".formatted(new Object[]{
                    action, this.N2_month,
                    this.N3_day,
                    this.N4_hour,
                    this.N19_zenith_luminance});
                errorsAndWarnings.append(err);
                if (EPW_VARIABLE_LIMITS) {
                    this.N19_zenith_luminance = 0.0f;
                }
            }
            if (this.N19_zenith_luminance >= 99999.0f) {
                String err = " ** WARNING ** EPW N19_zenith_luminance >= 99999 Cd/m2 [%s]: month %s, day %s, hour %s, value %s %n".formatted(new Object[]{
                    action, this.N2_month,
                    this.N3_day,
                    this.N4_hour,
                    this.N19_zenith_luminance});
                errorsAndWarnings.append(err);
                if (EPW_VARIABLE_LIMITS) {
                    this.N19_zenith_luminance = 99998.0f;
                }
            }
        }
        if (!this.N20_wind_direction.isNaN()) {
            if (this.N20_wind_direction < 0.0f) {
                String err = " ** WARNING ** EPW N20_wind_direction < 0° [%s]: month %s, day %s, hour %s, value %s %n".formatted(new Object[]{
                    action, this.N2_month,
                    this.N3_day,
                    this.N4_hour,
                    this.N20_wind_direction});
                errorsAndWarnings.append(err);
                if (EPW_VARIABLE_LIMITS) {
                    this.N20_wind_direction = (360.0f + this.N20_wind_direction) % 360.0f;
                }
            }
            if (this.N20_wind_direction > 360.0f) {
                String err = " ** WARNING ** EPW N20_wind_direction > 360° [%s]: month %s, day %s, hour %s, value %s %n".formatted(new Object[]{
                    action, this.N2_month,
                    this.N3_day,
                    this.N4_hour,
                    this.N20_wind_direction});
                errorsAndWarnings.append(err);
                if (EPW_VARIABLE_LIMITS) {
                    this.N20_wind_direction = this.N20_wind_direction % 360.0f;
                }
            }
        }

        if (!this.N21_wind_speed.isNaN()) {
            if (this.N21_wind_speed < 0.0f) {
                String err = " ** WARNING ** EPW N21_wind_speed < 0 m/s [%s]: month %s, day %s, hour %s, value %s %n".formatted(new Object[]{
                    action, this.N2_month,
                    this.N3_day,
                    this.N4_hour,
                    this.N21_wind_speed});
                errorsAndWarnings.append(err);
                if (EPW_VARIABLE_LIMITS) {
                    this.N21_wind_speed = 0.0f;
                }
            }
            if (this.N21_wind_speed > 40.0f) {
                String err = " ** WARNING ** EPW N21_wind_speed > 40 m/s [%s]: month %s, day %s, hour %s, value %s %n".formatted(new Object[]{
                    action, this.N2_month,
                    this.N3_day,
                    this.N4_hour,
                    this.N21_wind_speed});
                errorsAndWarnings.append(err);
                if (EPW_VARIABLE_LIMITS) {
                    this.N21_wind_speed = 40.0f;
                }
            }
        }
        if (!this.N22_total_sky_cover.isNaN()) {
            if (this.N22_total_sky_cover < 0.0f) {
                String err = " ** WARNING ** EPW N22_total_sky_cover < 0 deca [%s]: month %s, day %s, hour %s, value %s %n".formatted(new Object[]{
                    action, this.N2_month,
                    this.N3_day,
                    this.N4_hour,
                    this.N22_total_sky_cover});
                errorsAndWarnings.append(err);
                if (EPW_VARIABLE_LIMITS) {
                    this.N22_total_sky_cover = 0.0f;
                }
            }
            if (this.N22_total_sky_cover > 10.0f) {
                String err = " ** WARNING ** EPW N22_total_sky_cover > 10 deca [%s]: month %s, day %s, hour %s, value %s %n".formatted(new Object[]{
                    action, this.N2_month,
                    this.N3_day,
                    this.N4_hour,
                    this.N22_total_sky_cover});
                errorsAndWarnings.append(err);
                if (EPW_VARIABLE_LIMITS) {
                    this.N22_total_sky_cover = 10.0f;
                }
            }
        }
        if (!this.N23_opaque_sky_cover.isNaN()) {
            if (this.N23_opaque_sky_cover < 0.0f) {
                String err = " ** WARNING ** EPW N23_opaque_sky_cover < 0 deca [%s]: month %s, day %s, hour %s, value %s %n".formatted(new Object[]{
                    action, this.N2_month,
                    this.N3_day,
                    this.N4_hour,
                    this.N23_opaque_sky_cover});
                errorsAndWarnings.append(err);
                if (EPW_VARIABLE_LIMITS) {
                    this.N23_opaque_sky_cover = 0.0f;
                }
            }
            if (this.N23_opaque_sky_cover > 10.0f) {
                String err = " ** WARNING ** EPW N23_opaque_sky_cover > 10 deca [%s]: month %s, day %s, hour %s, value %s %n".formatted(new Object[]{
                    action, this.N2_month,
                    this.N3_day,
                    this.N4_hour,
                    this.N23_opaque_sky_cover});
                errorsAndWarnings.append(err);
                if (EPW_VARIABLE_LIMITS) {
                    this.N23_opaque_sky_cover = 10.0f;
                }
            }
        }
        if (!this.N24_visibility.isNaN()) {
            if (this.N24_visibility < 0.0f) {
                String err = " ** WARNING ** EPW N24_visibility < 0 km [%s]: month %s, day %s, hour %s, value %s %n".formatted(new Object[]{
                    action, this.N2_month,
                    this.N3_day,
                    this.N4_hour,
                    this.N24_visibility});
                errorsAndWarnings.append(err);
                if (EPW_VARIABLE_LIMITS) {
                    this.N24_visibility = 0.0f;
                }
            }
            if (this.N24_visibility > 9998.999f) {
                String err = " ** WARNING ** EPW N24_visibility > 9998.999 km [%s]: month %s, day %s, hour %s, value %s %n".formatted(new Object[]{
                    action, this.N2_month,
                    this.N3_day,
                    this.N4_hour,
                    this.N24_visibility});
                errorsAndWarnings.append(err);
                if (EPW_VARIABLE_LIMITS) {
                    this.N24_visibility = 9998.999f;
                }
            }
        }
        if (!this.N25_ceiling_height.isNaN()) {
            if (this.N25_ceiling_height < 0.0f) {
                String err = " ** WARNING ** EPW N25_ceiling_height < 0 m [%s]: month %s, day %s, hour %s, value %s %n".formatted(new Object[]{
                    action, this.N2_month,
                    this.N3_day,
                    this.N4_hour,
                    this.N25_ceiling_height});
                errorsAndWarnings.append(err);
                if (EPW_VARIABLE_LIMITS) {
                    this.N25_ceiling_height = 0.0f;
                }
            }
            if (this.N25_ceiling_height > 99998.999f) {
                String err = " ** WARNING ** EPW N25_ceiling_height > 9998.999 m [%s]: month %s, day %s, hour %s, value %s %n".formatted(new Object[]{
                    action, this.N2_month,
                    this.N3_day,
                    this.N4_hour,
                    this.N25_ceiling_height});
                errorsAndWarnings.append(err);
                if (EPW_VARIABLE_LIMITS) {
                    this.N25_ceiling_height = 99998.999f;
                }
            }
        }
        if (!this.N26_present_weather_observation.isNaN()) {
            if (this.N26_present_weather_observation != 0.0f && this.N26_present_weather_observation != 9.0f) {
                String err = " ** WARNING ** EPW N26_present_weather_observation != 0 and != 9 [%s]: month %s, day %s, hour %s, value %s %n".formatted(new Object[]{
                    action, this.N2_month,
                    this.N3_day,
                    this.N4_hour,
                    this.N26_present_weather_observation});
                errorsAndWarnings.append(err);
                if (EPW_VARIABLE_LIMITS) {
                    this.N26_present_weather_observation = 9.0f;
                }
            }
        }
        if (this.N27_present_weather_code.length() < 9) {
            String err = " ** WARNING ** EPW N27_present_weather_code text lenth < 9: month %s, day %s, hour %s, value %s %n".formatted(new Object[]{
                action, this.N2_month,
                this.N3_day,
                this.N4_hour,
                this.N27_present_weather_code});
            errorsAndWarnings.append(err);
            if (EPW_VARIABLE_LIMITS) {
                int dif = 9 - this.N27_present_weather_code.length();
                for(int i = 0; i < dif; i++) {
                    this.N27_present_weather_code = "0" + this.N27_present_weather_code;
                }
            }
        }
        if (!this.N28_precipitable_water.isNaN()) {
            if (this.N28_precipitable_water < 0.0f) {
                String err = " ** WARNING ** EPW N28_precipitable_water < 0 mm [%s]: month %s, day %s, hour %s, value %s %n".formatted(new Object[]{
                    action, this.N2_month,
                    this.N3_day,
                    this.N4_hour,
                    this.N28_precipitable_water});
                errorsAndWarnings.append(err);
                if (EPW_VARIABLE_LIMITS) {
                    this.N28_precipitable_water = 0.0f;
                }
            }
            if (this.N28_precipitable_water > 998.999) {
                String err = " ** WARNING ** EPW N28_precipitable_water > 998.999 mm [%s]: month %s, day %s, hour %s, value %s %n".formatted(new Object[]{
                    action, this.N2_month,
                    this.N3_day,
                    this.N4_hour,
                    this.N28_precipitable_water});
                errorsAndWarnings.append(err);
                if (EPW_VARIABLE_LIMITS) {
                    this.N28_precipitable_water = 998.999f;
                }
            }
        }
        if (!this.N29_aerosol_optical_depth.isNaN()) {
            if (this.N29_aerosol_optical_depth < 0.0f) {
                String err = " ** WARNING ** EPW N29_aerosol_optical_depth < 0 [%s]: month %s, day %s, hour %s, value %s %n".formatted(new Object[]{
                    action, this.N2_month,
                    this.N3_day,
                    this.N4_hour,
                    this.N29_aerosol_optical_depth});
                errorsAndWarnings.append(err);
                if (EPW_VARIABLE_LIMITS) {
                    this.N29_aerosol_optical_depth = 0.0f;
                }
            }
            if (this.N29_aerosol_optical_depth > 0.998f) {
                String err = " ** WARNING ** EPW N29_aerosol_optical_depth > 0.998 [%s]: month %s, day %s, hour %s, value %s %n".formatted(new Object[]{
                    action, this.N2_month,
                    this.N3_day,
                    this.N4_hour,
                    this.N29_aerosol_optical_depth});
                errorsAndWarnings.append(err);
                if (EPW_VARIABLE_LIMITS) {
                    this.N29_aerosol_optical_depth = 0.998f;
                }
            }
        }
        if (!this.N30_snow_depth.isNaN()) {
            if (this.N30_snow_depth < 0.0f) {
                String err = " ** WARNING ** EPW N30_snow_depth < 0 cm [%s]: month %s, day %s, hour %s, value %s %n".formatted(new Object[]{
                    action, this.N2_month,
                    this.N3_day,
                    this.N4_hour,
                    this.N30_snow_depth});
                errorsAndWarnings.append(err);
                if (EPW_VARIABLE_LIMITS) {
                    this.N30_snow_depth = 0.0f;
                }
            }
            if (this.N30_snow_depth > 998.999f) {
                String err = " ** WARNING ** EPW N30_snow_depth > 998.999 cm [%s]: month %s, day %s, hour %s, value %s %n".formatted(new Object[]{
                    action, this.N2_month,
                    this.N3_day,
                    this.N4_hour,
                    this.N30_snow_depth});
                errorsAndWarnings.append(err);
                if (EPW_VARIABLE_LIMITS) {
                    this.N30_snow_depth = 998.999f;
                }
            }
        }
        if (!this.N31_days_since_last_snowfall.isNaN()) {
            if (this.N31_days_since_last_snowfall < 0.0f) {
                String err = " ** WARNING ** EPW N31_days_since_last_snowfall < 0 days [%s]: month %s, day %s, hour %s, value %s %n".formatted(new Object[]{
                    action, this.N2_month,
                    this.N3_day,
                    this.N4_hour,
                    this.N31_days_since_last_snowfall});
                errorsAndWarnings.append(err);
                if (EPW_VARIABLE_LIMITS) {
                    this.N31_days_since_last_snowfall = 0.0f;
                }
            }
            if (this.N31_days_since_last_snowfall > 98.0f) {
                String err = " ** WARNING ** EPW N31_days_since_last_snowfall > 98 days [%s]: month %s, day %s, hour %s, value %s %n".formatted(new Object[]{
                    action, this.N2_month,
                    this.N3_day,
                    this.N4_hour,
                    this.N31_days_since_last_snowfall});
                errorsAndWarnings.append(err);
                if (EPW_VARIABLE_LIMITS) {
                    this.N31_days_since_last_snowfall = 98.0f;
                }
            }
        }
        if (!this.N32_albedo.isNaN()) {
            if (this.N32_albedo < 0.0f) {
                String err = " ** WARNING ** EPW N32_albedo < 0 [%s]: month %s, day %s, hour %s, value %s %n".formatted(new Object[]{
                    action, this.N2_month,
                    this.N3_day,
                    this.N4_hour,
                    this.N32_albedo});
                errorsAndWarnings.append(err);
                if (EPW_VARIABLE_LIMITS) {
                    this.N32_albedo = 0.0f;
                }
            }
            if (this.N32_albedo > 1.0f) {
                String err = " ** WARNING ** EPW N32_albedo > 1 [%s]: month %s, day %s, hour %s, value %s %n".formatted(new Object[]{
                    action, this.N2_month,
                    this.N3_day,
                    this.N4_hour,
                    this.N32_albedo});
                errorsAndWarnings.append(err);
                if (EPW_VARIABLE_LIMITS) {
                    this.N32_albedo = 1.0f;
                }
            }
        }
        if (!this.N33_liquid_precipitation_depth.isNaN()) {
            if (this.N33_liquid_precipitation_depth < 0.0f) {
                String err = " ** WARNING ** EPW N33_liquid_precipitation_depth < 0 mm [%s]: month %s, day %s, hour %s, value %s %n".formatted(new Object[]{
                    action, this.N2_month,
                    this.N3_day,
                    this.N4_hour,
                    this.N33_liquid_precipitation_depth});
                errorsAndWarnings.append(err);
                if (EPW_VARIABLE_LIMITS) {
                    this.N33_liquid_precipitation_depth = 0.0f;
                }
            }
            if (this.N33_liquid_precipitation_depth > 998.999f) {
                String err = " ** WARNING ** EPW N33_liquid_precipitation_depth > 998.999 mm [%s]: month %s, day %s, hour %s, value %s %n".formatted(new Object[]{
                    action, this.N2_month,
                    this.N3_day,
                    this.N4_hour,
                    this.N33_liquid_precipitation_depth});
                errorsAndWarnings.append(err);
                if (EPW_VARIABLE_LIMITS) {
                    this.N33_liquid_precipitation_depth = 998.999f;
                }
            }
        }
        if (!this.N34_liquid_precipitation_quantity.isNaN()) {
            if (this.N34_liquid_precipitation_quantity < 0.0f) {
                String err = " ** WARNING ** EPW N34_liquid_precipitation_quantity < 0 h [%s]: month %s, day %s, hour %s, value %s %n".formatted(new Object[]{
                    action, this.N2_month,
                    this.N3_day,
                    this.N4_hour,
                    this.N34_liquid_precipitation_quantity});
                errorsAndWarnings.append(err);
                if (EPW_VARIABLE_LIMITS) {
                    this.N34_liquid_precipitation_quantity = 0.0f;
                }
            }
            if (this.N34_liquid_precipitation_quantity > 98.999f) {
                String err = " ** WARNING ** EPW N34_liquid_precipitation_quantity > 98.999 h [%s]: month %s, day %s, hour %s, value %s %n".formatted(new Object[]{
                    action, this.N2_month,
                    this.N3_day,
                    this.N4_hour,
                    this.N34_liquid_precipitation_quantity});
                errorsAndWarnings.append(err);
                if (EPW_VARIABLE_LIMITS) {
                    this.N34_liquid_precipitation_quantity = 98.999f;
                }
            }
        }
    }

    /**
     * Returns this data fields object as a string.
     *
     * @param EPW_VARIABLE_LIMITS apply the EPW variable limits
     * @param errorsAndWarnings String buffer for errors and warnings
     * @return string
     */
    public String getString(boolean EPW_VARIABLE_LIMITS, StringBuffer errorsAndWarnings) {
        correctFieldLimitsAndMissingValues(EPW_VARIABLE_LIMITS, errorsAndWarnings);
        Object[] args = new Object[]{
            this.N1_year,
            this.N2_month,
            this.N3_day,
            this.N4_hour,
            this.N5_minute,
            this.A1_data_source_and_uncertainty_flag,
            this.N6_dry_bulb_temperature.isNaN() ? "99.9" : String.format(Locale.ROOT, "%.1f", this.N6_dry_bulb_temperature),
            this.N7_dew_point_temperature.isNaN() ? "99.9" : String.format(Locale.ROOT, "%.1f", this.N7_dew_point_temperature),
            this.N8_relative_humidity.isNaN() ? "999." : String.format(Locale.ROOT, "%.0f", this.N8_relative_humidity),
            this.N9_atmospheric_station_pressure.isNaN() ? "999999." : String.format(Locale.ROOT, "%.0f", this.N9_atmospheric_station_pressure),
            this.N10_extraterrestrial_horizontal_radiation.isNaN() ? "9999." : String.format(Locale.ROOT, "%.0f", this.N10_extraterrestrial_horizontal_radiation),
            this.N11_extraterrestrial_direct_normal_radiation.isNaN() ? "9999." : String.format(Locale.ROOT, "%.0f", this.N11_extraterrestrial_direct_normal_radiation),
            this.N12_horizontal_infrared_radiation_intensity.isNaN() ? "9999." : String.format(Locale.ROOT, "%.0f", this.N12_horizontal_infrared_radiation_intensity),
            this.N13_global_horizontal_radiation.isNaN() ? "9999." : String.format(Locale.ROOT, "%.0f", this.N13_global_horizontal_radiation),
            this.N14_direct_normal_radiation.isNaN() ? "9999." : String.format(Locale.ROOT, "%.0f", this.N14_direct_normal_radiation),
            this.N15_diffuse_horizontal_radiation.isNaN() ? "9999." : String.format(Locale.ROOT, "%.0f", this.N15_diffuse_horizontal_radiation),
            this.N16_global_horizontal_illuminance.isNaN() ? "999999." : String.format(Locale.ROOT, "%.0f", this.N16_global_horizontal_illuminance),
            this.N17_direct_normal_illuminance.isNaN() ? "999999." : String.format(Locale.ROOT, "%.0f", this.N17_direct_normal_illuminance),
            this.N18_diffuse_horizontal_illuminance.isNaN() ? "999999." : String.format(Locale.ROOT, "%.0f", this.N18_diffuse_horizontal_illuminance),
            this.N19_zenith_luminance.isNaN() ? "9999." : String.format(Locale.ROOT, "%.0f", this.N19_zenith_luminance),
            this.N20_wind_direction.isNaN() ? "999." : String.format(Locale.ROOT, "%.0f", this.N20_wind_direction),
            this.N21_wind_speed.isNaN() ? "999." : String.format(Locale.ROOT, "%.1f", this.N21_wind_speed),
            this.N22_total_sky_cover.isNaN() ? "99" : String.format(Locale.ROOT, "%.0f", this.N22_total_sky_cover),
            this.N23_opaque_sky_cover.isNaN() ? "99" : String.format(Locale.ROOT, "%.0f", this.N23_opaque_sky_cover),
            this.N24_visibility.isNaN() ? "9999" : String.format(Locale.ROOT, "%.0f", this.N24_visibility),
            this.N25_ceiling_height.isNaN() ? "99999" : String.format(Locale.ROOT, "%.0f", this.N25_ceiling_height),
            this.N26_present_weather_observation.isNaN() ? "9" : String.format(Locale.ROOT, "%.0f", this.N26_present_weather_observation),
            String.format(Locale.ROOT, "%s", this.N27_present_weather_code),
            this.N28_precipitable_water.isNaN() ? "999" : String.format(Locale.ROOT, "%.0f", this.N28_precipitable_water),
            this.N29_aerosol_optical_depth.isNaN() ? "999" : String.format(Locale.ROOT, "%.3f", this.N29_aerosol_optical_depth),
            this.N30_snow_depth.isNaN() ? "999" : String.format(Locale.ROOT, "%.2f", this.N30_snow_depth),
            this.N31_days_since_last_snowfall.isNaN() ? "99" : String.format(Locale.ROOT, "%.0f", this.N31_days_since_last_snowfall),
            this.N32_albedo.isNaN() ? "999" : String.format(Locale.ROOT, "%.3f", this.N32_albedo),
            this.N33_liquid_precipitation_depth.isNaN() ? "999" : String.format(Locale.ROOT, "%.2f", this.N33_liquid_precipitation_depth),
            this.N34_liquid_precipitation_quantity.isNaN() ? "99" : String.format(Locale.ROOT, "%.2f", this.N34_liquid_precipitation_quantity)
        };
        return String.format(Locale.ROOT, "%4d,%d,%d,%d,%d,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s%n", args);
    }

    /**
     * Returns the year.
     *
     * @return year
     */
    public int getN1_year() {
        return N1_year;
    }

    /**
     * Returns the month.
     *
     * @return month
     */
    public int getN2_month() {
        return N2_month;
    }

    /**
     * Returns the day.
     *
     * @return day
     */
    public int getN3_day() {
        return N3_day;
    }

    /**
     * Returns the hour.
     *
     * @return hour
     */
    public int getN4_hour() {
        return N4_hour;
    }

    /**
     * Returns the minute.
     *
     * @return minute
     */
    public int getN5_minute() {
        return N5_minute;
    }

    /**
     * Returns the data source and uncertainty flag string.
     *
     * @return data source flags
     */
    public String getA1_data_source_and_uncertainty_flag() {
        return A1_data_source_and_uncertainty_flag;
    }

    /**
     * Returns the dry bulb temperature [°C].
     *
     * @return dry bulb temperature
     */
    public Float getN6_dry_bulb_temperature() {
        return N6_dry_bulb_temperature;
    }

    /**
     * Returns the dew point temperature [°C].
     *
     * @return dew point temperature
     */
    public Float getN7_dew_point_temperature() {
        return N7_dew_point_temperature;
    }

    /**
     * Returns the relative humidity [%].
     *
     * @return relative humidity
     */
    public Float getN8_relative_humidity() {
        return N8_relative_humidity;
    }

    /**
     * Returns atmospheric station pressure [Pa].
     *
     * @return atmospheric pressure
     */
    public Float getN9_atmospheric_station_pressure() {
        return N9_atmospheric_station_pressure;
    }

    /**
     * Returns extraterrestrial horizontal radiation [Wh/m2].
     *
     * @return extraterrestrial horizontal radiation
     */
    public Float getN10_extraterrestrial_horizontal_radiation() {
        return N10_extraterrestrial_horizontal_radiation;
    }

    /**
     * Returns extraterrestrial direct normal radiation [Wh/m2].
     *
     * @return extraterrestrial direct normal radiation
     */
    public Float getN11_extraterrestrial_direct_normal_radiation() {
        return N11_extraterrestrial_direct_normal_radiation;
    }

    /**
     * Returns horizontal infrared radiation intensity [Wh/m2].
     *
     * @return horizontal infrared radiation intensity
     */
    public Float getN12_horizontal_infrared_radiation_intensity() {
        return N12_horizontal_infrared_radiation_intensity;
    }

    /**
     * Returns global horizontal radiation [Wh/m2].
     *
     * @return global horizontal radiation
     */
    public Float getN13_global_horizontal_radiation() {
        return N13_global_horizontal_radiation;
    }

    /**
     * Returns direct normal radiation [Wh/m2].
     *
     * @return direct normal radiation
     */
    public Float getN14_direct_normal_radiation() {
        return N14_direct_normal_radiation;
    }

    /**
     * Returns diffuse horizontal radiation [Wh/m2].
     *
     * @return diffuse horizontal radiation
     */
    public Float getN15_diffuse_horizontal_radiation() {
        return N15_diffuse_horizontal_radiation;
    }

    /**
     * Returns global horizontal illuminance [lux].
     *
     * @return global horizontal illuminance
     */
    public Float getN16_global_horizontal_illuminance() {
        return N16_global_horizontal_illuminance;
    }

    /**
     * Returns direct normal illuminance [lux].
     *
     * @return direct normal illuminance
     */
    public Float getN17_direct_normal_illuminance() {
        return N17_direct_normal_illuminance;
    }

    /**
     * Returns diffuse horizontal illuminance [lux].
     *
     * @return diffuse horizontal illuminance
     */
    public Float getN18_diffuse_horizontal_illuminance() {
        return N18_diffuse_horizontal_illuminance;
    }

    /**
     * Returns zenith luminance [Cd/m2].
     *
     * @return zenith luminance
     */
    public Float getN19_zenith_luminance() {
        return N19_zenith_luminance;
    }

    /**
     * Returns wind direction [°].
     *
     * @return wind direction
     */
    public Float getN20_wind_direction() {
        return N20_wind_direction;
    }

    /**
     * Returns wind speed [m/s].
     *
     * @return wind speed
     */
    public Float getN21_wind_speed() {
        return N21_wind_speed;
    }

    /**
     * Returns total sky cover [deca].
     *
     * @return total sky cover
     */
    public Float getN22_total_sky_cover() {
        return N22_total_sky_cover;
    }

    /**
     * Returns opaque sky cover [deca].
     *
     * @return opaque sky cover
     */
    public Float getN23_opaque_sky_cover() {
        return N23_opaque_sky_cover;
    }

    /**
     * Returns visibility [km].
     *
     * @return visibility
     */
    public Float getN24_visibility() {
        return N24_visibility;
    }

    /**
     * Return ceiling height [m].
     *
     * @return ceiling height
     */
    public Float getN25_ceiling_height() {
        return N25_ceiling_height;
    }

    /**
     * Returns present weather observation.
     *
     * @return present weather observation
     */
    public Float getN26_present_weather_observation() {
        return N26_present_weather_observation;
    }

    /**
     * Returns present weather code.
     *
     * @return present weather code
     */
    public String getN27_present_weather_code() {
        return N27_present_weather_code;
    }

    /**
     * Returns precipitable water [mm].
     *
     * @return precipitable water
     */
    public Float getN28_precipitable_water() {
        return N28_precipitable_water;
    }

    /**
     * Returns aerosol optical depth [thousandths].
     *
     * @return aerosol optical depth
     */
    public Float getN29_aerosol_optical_depth() {
        return N29_aerosol_optical_depth;
    }

    /**
     * Returns snow depth [cm].
     *
     * @return snow depth
     */
    public Float getN30_snow_depth() {
        return N30_snow_depth;
    }

    /**
     * Returns days since last snowfall.
     *
     * @return number of days
     */
    public Float getN31_days_since_last_snowfall() {
        return N31_days_since_last_snowfall;
    }

    /**
     * Returns albedo [-].
     *
     * @return albedo
     */
    public Float getN32_albedo() {
        return N32_albedo;
    }

    /**
     * Returns liquid precipitation depth [mm].
     *
     * @return liquid precipitation depth
     */
    public Float getN33_liquid_precipitation_depth() {
        return N33_liquid_precipitation_depth;
    }

    /**
     * Returns liquid precipitation quantity [h].
     *
     * @return liquid precipitation quantity
     */
    public Float getN34_liquid_precipitation_quantity() {
        return N34_liquid_precipitation_quantity;
    }

    /**
     * Sets the year.
     *
     * @param N1_year year
     */
    public void setN1_year(int N1_year) {
        this.N1_year = N1_year;
    }

    /**
     * Sets the month.
     *
     * @param N2_month month number
     */
    public void setN2_month(int N2_month) {
        this.N2_month = N2_month;
    }

    /**
     * Sets the day.
     *
     * @param N3_day day
     */
    public void setN3_day(int N3_day) {
        this.N3_day = N3_day;
    }

    /**
     * Sets the hour.
     *
     * @param N4_hour hour
     */
    public void setN4_hour(int N4_hour) {
        this.N4_hour = N4_hour;
    }

    /**
     * Sets the minute.
     *
     * @param N5_minute minute
     */
    public void setN5_minute(int N5_minute) {
        this.N5_minute = N5_minute;
    }

    /**
     * Sets data source and uncertainty flags.
     *
     * @param A1_data_source_and_uncertainty_flag text
     */
    public void setA1_data_source_and_uncertainty_flag(String A1_data_source_and_uncertainty_flag) {
        this.A1_data_source_and_uncertainty_flag = A1_data_source_and_uncertainty_flag;
    }

    /**
     * Sets dry bulb temperature [°C], minimum -70, maximum 70, missing "99.9".
     *
     * @param N6_dry_bulb_temperature temperature
     */
    public void setN6_dry_bulb_temperature(Float N6_dry_bulb_temperature) {
        this.N6_dry_bulb_temperature = N6_dry_bulb_temperature;
    }

    /**
     * Sets dew point temperature [°C], minimum -70, maximum 70, missing "99.9".
     *
     * @param N7_dew_point_temperature temperature
     */
    public void setN7_dew_point_temperature(Float N7_dew_point_temperature) {
        this.N7_dew_point_temperature = N7_dew_point_temperature;
    }

    /**
     * Sets relative humidity [%], minimum 0, maximum 110, missing "999.".
     *
     * @param N8_relative_humidity relative humidity
     */
    public void setN8_relative_humidity(Float N8_relative_humidity) {
        this.N8_relative_humidity = N8_relative_humidity;
    }

    /**
     * Sets atmospheric station pressure [Pa], minimum 31000, maximum 120000,
     * missing "999999.".
     *
     * @param N9_atmospheric_station_pressure pressure
     */
    public void setN9_atmospheric_station_pressure(Float N9_atmospheric_station_pressure) {
        this.N9_atmospheric_station_pressure = N9_atmospheric_station_pressure;
    }

    /**
     * Sets extraterrestrial horizontal radiation [Wh/m2], minimum 0, missing
     * "9999.".
     *
     * @param N10_extraterrestrial_horizontal_radiation radiation
     */
    public void setN10_extraterrestrial_horizontal_radiation(Float N10_extraterrestrial_horizontal_radiation) {
        this.N10_extraterrestrial_horizontal_radiation = N10_extraterrestrial_horizontal_radiation;
    }

    /**
     * Sets extraterrestrial direct normal radiation [Wh/m2], minimum 0, missing
     * "9999.".
     *
     * @param N11_extraterrestrial_direct_normal_radiation radiation
     */
    public void setN11_extraterrestrial_direct_normal_radiation(Float N11_extraterrestrial_direct_normal_radiation) {
        this.N11_extraterrestrial_direct_normal_radiation = N11_extraterrestrial_direct_normal_radiation;
    }

    /**
     * Sets horizontal infrared radiation intensity [Wh/m2], minimum 0, missing
     * "9999.".
     *
     * @param N12_horizontal_infrared_radiation_intensity radiation
     */
    public void setN12_horizontal_infrared_radiation_intensity(Float N12_horizontal_infrared_radiation_intensity) {
        this.N12_horizontal_infrared_radiation_intensity = N12_horizontal_infrared_radiation_intensity;
    }

    /**
     * Sets global horizontal radiation [Wh/m2], minimum 0, missing "9999.".
     *
     * @param N13_global_horizontal_radiation radiation
     */
    public void setN13_global_horizontal_radiation(Float N13_global_horizontal_radiation) {
        this.N13_global_horizontal_radiation = N13_global_horizontal_radiation;
    }

    /**
     * Sets direct normal radiation [Wh/m2], minimum 0, missing "9999.".
     *
     * @param N14_direct_normal_radiation radiation
     */
    public void setN14_direct_normal_radiation(Float N14_direct_normal_radiation) {
        this.N14_direct_normal_radiation = N14_direct_normal_radiation;
    }

    /**
     * Sets diffuse horizontal radiation [Wh/m2], minimum 0, missing "9999.".
     *
     * @param N15_diffuse_horizontal_radiation radiation
     */
    public void setN15_diffuse_horizontal_radiation(Float N15_diffuse_horizontal_radiation) {
        this.N15_diffuse_horizontal_radiation = N15_diffuse_horizontal_radiation;
    }

    /**
     * Sets global horizontal illuminance [lux], minimum 0, missing "999999." or
     * above 999900.
     *
     * @param N16_global_horizontal_illuminance illuminance
     */
    public void setN16_global_horizontal_illuminance(Float N16_global_horizontal_illuminance) {
        this.N16_global_horizontal_illuminance = N16_global_horizontal_illuminance;
    }

    /**
     * Sets direct normal illuminance [lux], minimum 0, missing "999999." or
     * above 999900.
     *
     * @param N17_direct_normal_illuminance illuminance
     */
    public void setN17_direct_normal_illuminance(Float N17_direct_normal_illuminance) {
        this.N17_direct_normal_illuminance = N17_direct_normal_illuminance;
    }

    /**
     * Sets diffuse horizontal illuminance [lux], minimum 0, missing "999999."
     * or above 999900.
     *
     * @param N18_diffuse_horizontal_illuminance illuminance
     */
    public void setN18_diffuse_horizontal_illuminance(Float N18_diffuse_horizontal_illuminance) {
        this.N18_diffuse_horizontal_illuminance = N18_diffuse_horizontal_illuminance;
    }

    /**
     * Sets zenith luminance [Cd/m2], minimum 0, missing "9999." or above.
     *
     * @param N19_zenith_luminance luminance
     */
    public void setN19_zenith_luminance(Float N19_zenith_luminance) {
        this.N19_zenith_luminance = N19_zenith_luminance;
    }

    /**
     * Sets wind direction [°], minimum 0, maximum 360, missing "999.".
     *
     * @param N20_wind_direction direction
     */
    public void setN20_wind_direction(Float N20_wind_direction) {
        this.N20_wind_direction = N20_wind_direction;
    }

    /**
     * Sets wind speed [m/s], minimum 0, maximum 40, missing "999.".
     *
     * @param N21_wind_speed speed
     */
    public void setN21_wind_speed(Float N21_wind_speed) {
        this.N21_wind_speed = N21_wind_speed;
    }

    /**
     * Sets total sky cover [deca], minimum 0, maximum 10, missing "99".
     *
     * @param N22_total_sky_cover sky cover
     */
    public void setN22_total_sky_cover(Float N22_total_sky_cover) {
        this.N22_total_sky_cover = N22_total_sky_cover;
    }

    /**
     * Sets opaque sky cover [deca], minimum 0, maximum 10, missing "99".
     *
     * @param N23_opaque_sky_cover sky cover
     */
    public void setN23_opaque_sky_cover(Float N23_opaque_sky_cover) {
        this.N23_opaque_sky_cover = N23_opaque_sky_cover;
    }

    /**
     * Sets visibility distance [km], missing "9999". Despite not having minimum
     * value, 0 is a logical value.
     *
     * @param N24_visibility distance
     */
    public void setN24_visibility(Float N24_visibility) {
        this.N24_visibility = N24_visibility;
    }

    /**
     * Sets ceiling height [m], missing "99999". Despite not having minimum
     * value, 0 is a logical value.
     *
     * @param N25_ceiling_height height
     */
    public void setN25_ceiling_height(Float N25_ceiling_height) {
        this.N25_ceiling_height = N25_ceiling_height;
    }

    /**
     * Sets present weather observation.
     *
     * @param N26_present_weather_observation text
     */
    public void setN26_present_weather_observation(Float N26_present_weather_observation) {
        this.N26_present_weather_observation = N26_present_weather_observation;
    }

    /**
     * Sets present weather code.
     *
     * @param N27_present_weather_code text
     */
    public void setN27_present_weather_code(String N27_present_weather_code) {
        this.N27_present_weather_code = N27_present_weather_code;
    }

    /**
     * Sets precipitable water [mm], missing "999". Despite not having minimum
     * value, 0 is a logical value.
     *
     * @param N28_precipitable_water precipitable water
     */
    public void setN28_precipitable_water(Float N28_precipitable_water) {
        this.N28_precipitable_water = N28_precipitable_water;
    }

    /**
     * Sets aerosol optical depth [thousandths], missing ".999".
     *
     * @param N29_aerosol_optical_depth thousandths
     */
    public void setN29_aerosol_optical_depth(Float N29_aerosol_optical_depth) {
        this.N29_aerosol_optical_depth = N29_aerosol_optical_depth;
    }

    /**
     * Sets snow depth [cm], missing "999". Despite not having minimum value, 0
     * is a logical value.
     *
     * @param N30_snow_depth depth
     */
    public void setN30_snow_depth(Float N30_snow_depth) {
        this.N30_snow_depth = N30_snow_depth;
    }

    /**
     * Sets the number of days since last snowfall, missing "99". Despite not
     * having minimum value, 0 is a logical value.
     *
     * @param N31_days_since_last_snowfall number of days
     */
    public void setN31_days_since_last_snowfall(Float N31_days_since_last_snowfall) {
        this.N31_days_since_last_snowfall = N31_days_since_last_snowfall;
    }

    /**
     * Sets albedo [-] (ratio of reflected solar irradiance to global horizontal
     * irradiance), missing "999".
     *
     * @param N32_albedo ratio
     */
    public void setN32_albedo(Float N32_albedo) {
        this.N32_albedo = N32_albedo;
    }

    /**
     * Sets liquid precipitation depth [mm], missing "999". Despite not having
     * minimum value, 0 is a logical value.
     *
     * @param N33_liquid_precipitation_depth depth
     */
    public void setN33_liquid_precipitation_depth(Float N33_liquid_precipitation_depth) {
        this.N33_liquid_precipitation_depth = N33_liquid_precipitation_depth;
    }

    /**
     * Sets liquid precipitation quantity [hr], missing "99". Despite not having
     * minimum value, 0 is a logical value.
     *
     * @param N34_liquid_precipitation_quantity hours
     */
    public void setN34_liquid_precipitation_quantity(Float N34_liquid_precipitation_quantity) {
        this.N34_liquid_precipitation_quantity = N34_liquid_precipitation_quantity;
    }

}
