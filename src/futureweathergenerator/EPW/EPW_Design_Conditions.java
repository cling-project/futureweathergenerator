/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.EPW;

import java.util.Arrays;
import java.util.Locale;

/**
 * Design conditions object.
 *
 * @author eugenio
 */
public class EPW_Design_Conditions {

    /**
     * EPW object string id.
     */
    public static final String ID = "DESIGN CONDITIONS";

    private int N1_number_of_design_conditions;
    private EPW_Design_Condition[] An_Design_Condition_Sources;

    /**
     * Initiates the design conditions object.
     *
     * @param N1_number_of_design_conditions number of design conditions
     * @param An_Design_Condition_Sources array of design conditions
     */
    public EPW_Design_Conditions(int N1_number_of_design_conditions, EPW_Design_Condition[] An_Design_Condition_Sources) {
        this.N1_number_of_design_conditions = N1_number_of_design_conditions;
        this.An_Design_Condition_Sources = An_Design_Condition_Sources;
    }

    /**
     * Initiates the design conditions object from an array of strings.
     *
     * @param args array of strings
     */
    public EPW_Design_Conditions(String[] args) {
        if(args.length >= 2) {
            // args[0] is the object id
            this.N1_number_of_design_conditions = Integer.parseInt(args[1]);
            this.An_Design_Condition_Sources = new EPW_Design_Condition[this.N1_number_of_design_conditions];
            for (int i = 0; i < this.N1_number_of_design_conditions; i++) {
                int argsLength = args.length - 2;
                argsLength = argsLength / this.N1_number_of_design_conditions;
                String[] designConditionArgs = Arrays.copyOfRange(args, 2 + (i * argsLength - 1), argsLength);
                this.An_Design_Condition_Sources[i] = new EPW_Design_Condition(designConditionArgs);
            }
        } else {
            this.N1_number_of_design_conditions = -1;
            this.An_Design_Condition_Sources = new EPW_Design_Condition[0];
        }
    }
    
    /**
     * Checks the integrity of the object.
     *
     * @return true if passes the test
     */
    public boolean checkIntegrity() {
        if (this.N1_number_of_design_conditions == -1) {
            return false;
        }
        return true;
    }

    /**
     * Returns this design conditions object as a string.
     *
     * @return string
     */
    public String getString() {
        String line = "" + this.N1_number_of_design_conditions;
        for (EPW_Design_Condition dc : this.An_Design_Condition_Sources) {
            line += "," + dc.getString();
        }
        return String.format(Locale.ROOT, ID + "," + line + "%n");
    }

    /**
     * Returns the number of design conditions.
     *
     * @return number of design conditions
     */
    public int getN1_number_of_design_conditions() {
        return N1_number_of_design_conditions;
    }

    /**
     * Returns the array of design conditions.
     *
     * @return array of design conditions
     */
    public EPW_Design_Condition[] getAn_Design_Condition_Sources() {
        return An_Design_Condition_Sources;
    }

    /**
     * Number of Design Conditions.
     *
     * @param N1_number_of_design_conditions Number of design conditions.
     */
    public void setN1_number_of_design_conditions(int N1_number_of_design_conditions) {
        this.N1_number_of_design_conditions = N1_number_of_design_conditions;
    }

    /**
     * List of design conditions.
     *
     * @param An_Design_Condition_Sources List of design conditions.
     */
    public void setAn_Design_Condition_Sources(EPW_Design_Condition[] An_Design_Condition_Sources) {
        this.An_Design_Condition_Sources = An_Design_Condition_Sources;
    }

}
