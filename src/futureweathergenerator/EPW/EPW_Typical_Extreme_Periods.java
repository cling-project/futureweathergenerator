/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.EPW;

import java.util.Locale;

/**
 *
 * @author eugenio
 */
public class EPW_Typical_Extreme_Periods {

    /**
     * EPW object string id.
     */
    public static final String ID = "TYPICAL/EXTREME PERIODS";

    private int N1_number_of_typical_extreme_periods;
    private EPW_Typical_Extreme_Period[] An_typical_extreme_periods;

    /**
     * Initiates the typical extreme periods object.
     *
     * @param number_of_typical_extreme_periods number of extreme periods
     * @param typical_extreme_periods array of extreme periods
     */
    public EPW_Typical_Extreme_Periods(int number_of_typical_extreme_periods, EPW_Typical_Extreme_Period[] typical_extreme_periods) {
        this.N1_number_of_typical_extreme_periods = number_of_typical_extreme_periods;
        this.An_typical_extreme_periods = typical_extreme_periods;
    }

    /**
     * Initiates the typical extreme periods from an array of strings.
     *
     * @param args array of strings
     */
    public EPW_Typical_Extreme_Periods(String[] args) {
        if(args.length >= 2) {
            this.N1_number_of_typical_extreme_periods = Integer.parseInt(args[1]);
            if(this.N1_number_of_typical_extreme_periods * 4 + 2 == args.length) {
                this.An_typical_extreme_periods = new EPW_Typical_Extreme_Period[this.N1_number_of_typical_extreme_periods];
                for (int i = 2; i < args.length; i += 4) {
                    this.An_typical_extreme_periods[(i - 2) / 4] = new EPW_Typical_Extreme_Period(args[i], args[i + 1], args[i + 2], args[i + 3]);
                }
            } else {
                this.N1_number_of_typical_extreme_periods = -1;
                this.An_typical_extreme_periods = new EPW_Typical_Extreme_Period[0];
            }
        } else {
            this.N1_number_of_typical_extreme_periods = -1;
            this.An_typical_extreme_periods = new EPW_Typical_Extreme_Period[0];
        }
    }
    
    /**
     * Checks the integrity of the object.
     *
     * @return true if passes the test
     */
    public boolean checkIntegrity() {
        if (this.N1_number_of_typical_extreme_periods == -1) {
            return false;
        }
        return true;
    }

    /**
     * Returns this typical extreme periods as a string.
     *
     * @return string
     */
    public String getString() {
        String line = "" + this.N1_number_of_typical_extreme_periods;
        for (EPW_Typical_Extreme_Period s : this.An_typical_extreme_periods) {
            line += "," + String.format("%s", s.getA1_Name()) + "," + String.format("%s", s.getA2_Type()) + "," + String.format("%s", s.getA3_start_day()) + "," + String.format("%s", s.getA4_end_day());
        }
        return String.format(Locale.ROOT, ID + "," + line + "%n");
    }

    /**
     * Returns the number of typical extreme periods in this object.
     *
     * @return number of typical extreme periods
     */
    public int getN1_number_of_typical_extreme_periods() {
        return N1_number_of_typical_extreme_periods;
    }

    /**
     * Returns an array of the typical extreme periods.
     *
     * @return array of typical extreme periods
     */
    public EPW_Typical_Extreme_Period[] getAn_typical_extreme_periods() {
        return An_typical_extreme_periods;
    }

    /**
     * Number of Typical/Extreme Periods.
     *
     * @param N1_number_of_typical_extreme_periods Number of Typical/Extreme
     * Periods
     */
    public void setN1_number_of_typical_extreme_periods(int N1_number_of_typical_extreme_periods) {
        this.N1_number_of_typical_extreme_periods = N1_number_of_typical_extreme_periods;
    }

    /**
     * List of Typical/Extreme Periods.
     *
     * @param An_typical_extreme_periods List of Typical/Extreme Periods
     */
    public void setAn_typical_extreme_periods(EPW_Typical_Extreme_Period[] An_typical_extreme_periods) {
        this.An_typical_extreme_periods = An_typical_extreme_periods;
    }
    
}
