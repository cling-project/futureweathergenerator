/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.EPW;

import futureweathergenerator.FutureWeatherGenerator;
import futureweathergenerator.Months;
import futureweathergenerator.UHI.LCZ;
import futureweathergenerator.functions.N10_Extraterrestrial_Horizontal_Radiation;
import futureweathergenerator.functions.N14_Direct_Normal_Radiation;
import futureweathergenerator.functions.N15_Diffuse_Horizontal_Radiation;
import futureweathergenerator.functions.Typical_Extreme_Periods;
import futureweathergenerator.functions.solar.DiffuseIrradiationModel;
import futureweathergenerator.functions.solar.SolarHourAdjustment;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

/**
 * This class allows to load, edit, and save EPW files.
 *
 * @author eugenio
 */
public class EPW {

    private final String path_EPW;
    private final boolean EPW_READ;
    private final boolean EPW_VARIABLE_LIMITS;
    private EPW_Location epw_location;
    private EPW_Design_Conditions epw_design_conditions;
    private EPW_Typical_Extreme_Periods epw_typical_extreme_periods;
    private EPW_Ground_Temperatures epw_ground_temperatures;
    private EPW_Holidays_Daylight_Savings epw_holidays_daylight_savings;
    private ArrayList<EPW_Comment> epw_comments = new ArrayList<>();
    private EPW_Data_Periods epw_data_periods;
    private ArrayList<EPW_Data_Fields> epw_data_fields = new ArrayList<>();
    
    private DiffuseIrradiationModel diffuseIrradiationModel;

    private SolarHourAdjustment solarHourAdjustmentOption;
    private StringBuffer adjustmentWanings = new StringBuffer();
    public float[] solarHourAdjustment;
    public int[] numberOfHoursOfAdjustmentOfGlobalHorizontalRadiation;
    private Integer epw_original_lcz;
    private Integer target_uhi_lcz;
    private String file_suffix;

    /**
     * Initiates the EPW class with a path to a EPW file.
     *
     * @param errorsAndWarnings String buffer for errors and warnings
     * @param path_EPW path to EPW file
     * @param solarHourAdjustmentOption solar hour adjustment option.
     * @param diffuseIrradiationModelOption diffuse solar irradiation model option.
     * @param EPW_VARIABLE_LIMITS apply EPW variable limits
     * @param doEcho prints if it loads EPW file
     */
    public EPW(StringBuffer errorsAndWarnings, String path_EPW, int solarHourAdjustmentOption, int diffuseIrradiationModelOption, boolean EPW_VARIABLE_LIMITS, boolean doEcho) {
        this.file_suffix = "";
        this.epw_original_lcz = null;
        this.target_uhi_lcz = null;
        this.path_EPW = path_EPW;
        this.EPW_VARIABLE_LIMITS = EPW_VARIABLE_LIMITS;
        
        this.solarHourAdjustmentOption = SolarHourAdjustment.values()[solarHourAdjustmentOption];
        switch(this.solarHourAdjustmentOption) {
            case None -> {
                this.solarHourAdjustment = new float[0];
                this.numberOfHoursOfAdjustmentOfGlobalHorizontalRadiation = new int[0];
            }
            case By_Month -> {
                this.solarHourAdjustment = new float[12];
                this.numberOfHoursOfAdjustmentOfGlobalHorizontalRadiation = new int[12];
            }
            case By_Day -> {
                this.solarHourAdjustment = new float[365];
                this.numberOfHoursOfAdjustmentOfGlobalHorizontalRadiation = new int[365];
            }
        }
        
        this.diffuseIrradiationModel = DiffuseIrradiationModel.values()[diffuseIrradiationModelOption];
        
        if (doEcho) {
            System.out.println(String.format(Locale.ROOT, "Diffuse Irradiatin model option: %s", this.diffuseIrradiationModel.name()));
            System.out.println(String.format(Locale.ROOT, "Solar hour adjustment option: %s%n", this.solarHourAdjustmentOption.name()));
            System.out.println(String.format(Locale.ROOT, "Loading EPW File... %s%n", path_EPW));
        }
        
        try {
            File myObj = new File(path_EPW);
            boolean epwReadDone;
            try (Scanner myReader = new Scanner(myObj, "UTF-8")) {
                epwReadDone = readEncodedEPW(myReader, errorsAndWarnings);
                if (doEcho && epwReadDone) {
                    System.out.println(String.format(Locale.ROOT, "EPW File encoding UTF-8.%n"));
                }
            }
            if (!epwReadDone) {
                try (Scanner myReader = new Scanner(myObj, "UTF-16")) {
                    epwReadDone = readEncodedEPW(myReader, errorsAndWarnings);
                    if (doEcho && epwReadDone) {
                        System.out.println(String.format(Locale.ROOT, "EPW File encoding UTF-16.%n"));
                    }
                }
            }
            if (!epwReadDone) {
                try (Scanner myReader = new Scanner(myObj, "UTF-16BE")) {
                    epwReadDone = readEncodedEPW(myReader, errorsAndWarnings);
                    if (doEcho && epwReadDone) {
                        System.out.println(String.format(Locale.ROOT, "EPW File encoding UTF-16BE.%n"));
                    }
                }
            }
            if (!epwReadDone) {
                try (Scanner myReader = new Scanner(myObj, "UTF-16LE")) {
                    epwReadDone = readEncodedEPW(myReader, errorsAndWarnings);
                    if (doEcho && epwReadDone) {
                        System.out.println(String.format(Locale.ROOT, "EPW File encoding UTF-16LE.%n"));
                    }
                }
            }
            if (!epwReadDone) {
                try (Scanner myReader = new Scanner(myObj, "UTF-32")) {
                    epwReadDone = readEncodedEPW(myReader, errorsAndWarnings);
                    if (doEcho && epwReadDone) {
                        System.out.println(String.format(Locale.ROOT, "EPW File encoding UTF-32.%n"));
                    }
                }
            }
            if (!epwReadDone) {
                try (Scanner myReader = new Scanner(myObj, "ISO-8859-1")) {
                    epwReadDone = readEncodedEPW(myReader, errorsAndWarnings);
                    if (doEcho && epwReadDone) {
                        System.out.println(String.format(Locale.ROOT, "EPW File encoding ISO-8859-1.%n"));
                    }
                }
            }
            if (!epwReadDone) {
                try (Scanner myReader = new Scanner(myObj, "ISO-8859-2")) {
                    epwReadDone = readEncodedEPW(myReader, errorsAndWarnings);
                    if (doEcho && epwReadDone) {
                        System.out.println(String.format(Locale.ROOT, "EPW File encoding ISO-8859-2.%n"));
                    }
                }
            }
            if (!epwReadDone) {
                try (Scanner myReader = new Scanner(myObj, "ISO-8859-4")) {
                    epwReadDone = readEncodedEPW(myReader, errorsAndWarnings);
                    if (doEcho && epwReadDone) {
                        System.out.println(String.format(Locale.ROOT, "EPW File encoding ISO-8859-4.%n"));
                    }
                }
            }
            if (!epwReadDone) {
                try (Scanner myReader = new Scanner(myObj, "ISO-8859-5")) {
                    epwReadDone = readEncodedEPW(myReader, errorsAndWarnings);
                    if (doEcho && epwReadDone) {
                        System.out.println(String.format(Locale.ROOT, "EPW File encoding ISO-8859-5.%n"));
                    }
                }
            }
            if (!epwReadDone) {
                try (Scanner myReader = new Scanner(myObj, "ISO-8859-7")) {
                    epwReadDone = readEncodedEPW(myReader, errorsAndWarnings);
                    if (doEcho && epwReadDone) {
                        System.out.println(String.format(Locale.ROOT, "EPW File encoding ISO-8859-7.%n"));
                    }
                }
            }
            if (!epwReadDone) {
                try (Scanner myReader = new Scanner(myObj, "ISO-8859-9")) {
                    epwReadDone = readEncodedEPW(myReader, errorsAndWarnings);
                    if (doEcho && epwReadDone) {
                        System.out.println(String.format(Locale.ROOT, "EPW File encoding ISO-8859-9.%n"));
                    }
                }
            }
            if (!epwReadDone) {
                try (Scanner myReader = new Scanner(myObj, "ISO-8859-13")) {
                    epwReadDone = readEncodedEPW(myReader, errorsAndWarnings);
                    if (doEcho && epwReadDone) {
                        System.out.println(String.format(Locale.ROOT, "EPW File encoding ISO-8859-13.%n"));
                    }
                }
            }
            if (!epwReadDone) {
                try (Scanner myReader = new Scanner(myObj, "ISO-8859-15")) {
                    epwReadDone = readEncodedEPW(myReader, errorsAndWarnings);
                    if (doEcho && epwReadDone) {
                        System.out.println(String.format(Locale.ROOT, "EPW File encoding ISO-8859-15.%n"));
                    }
                }
            }
            if (!epwReadDone) {
                try (Scanner myReader = new Scanner(myObj, "US-ASCII")) {
                    epwReadDone = readEncodedEPW(myReader, errorsAndWarnings);
                    if (doEcho && epwReadDone) {
                        System.out.println(String.format(Locale.ROOT, "EPW File encoding US-ASCII.%n"));
                    }
                }
            }
            if (!epwReadDone) {
                try (Scanner myReader = new Scanner(myObj, "windows-1250")) {
                    epwReadDone = readEncodedEPW(myReader, errorsAndWarnings);
                    if (doEcho && epwReadDone) {
                        System.out.println(String.format(Locale.ROOT, "EPW File encoding windows-1250.%n"));
                    }
                }
            }
            if (!epwReadDone) {
                try (Scanner myReader = new Scanner(myObj, "windows-1251")) {
                    epwReadDone = readEncodedEPW(myReader, errorsAndWarnings);
                    if (doEcho && epwReadDone) {
                        System.out.println(String.format(Locale.ROOT, "EPW File encoding windows-1251.%n"));
                    }
                }
            }
            if (!epwReadDone) {
                try (Scanner myReader = new Scanner(myObj, "windows-1252")) {
                    epwReadDone = readEncodedEPW(myReader, errorsAndWarnings);
                    if (doEcho && epwReadDone) {
                        System.out.println(String.format(Locale.ROOT, "EPW File encoding windows-1252.%n"));
                    }
                }
            }
            if (!epwReadDone) {
                try (Scanner myReader = new Scanner(myObj, "windows-1253")) {
                    epwReadDone = readEncodedEPW(myReader, errorsAndWarnings);
                    if (doEcho && epwReadDone) {
                        System.out.println(String.format(Locale.ROOT, "EPW File encoding windows-1253.%n"));
                    }
                }
            }
            if (!epwReadDone) {
                try (Scanner myReader = new Scanner(myObj, "windows-1254")) {
                    epwReadDone = readEncodedEPW(myReader, errorsAndWarnings);
                    if (doEcho && epwReadDone) {
                        System.out.println(String.format(Locale.ROOT, "EPW File encoding windows-1254.%n"));
                    }
                }
            }
            if (!epwReadDone) {
                try (Scanner myReader = new Scanner(myObj, "windows-1257")) {
                    epwReadDone = readEncodedEPW(myReader, errorsAndWarnings);
                    if (doEcho && epwReadDone) {
                        System.out.println(String.format(Locale.ROOT, "EPW File encoding windows-1257.%n"));
                    }
                }
            }
            if (!epwReadDone) {
                try (Scanner myReader = new Scanner(myObj, "GB18030")) {
                    epwReadDone = readEncodedEPW(myReader, errorsAndWarnings);
                    if (doEcho && epwReadDone) {
                        System.out.println(String.format(Locale.ROOT, "EPW File encoding GB18030.%n"));
                    }
                }
            }
            if (!epwReadDone) {
                try (Scanner myReader = new Scanner(myObj, "Shift_JIS")) {
                    epwReadDone = readEncodedEPW(myReader, errorsAndWarnings);
                    if (doEcho && epwReadDone) {
                        System.out.println(String.format(Locale.ROOT, "EPW File encoding Shift_JIS.%n"));
                    }
                }
            }
            if (!epwReadDone) {
                try (Scanner myReader = new Scanner(myObj, "KOI8-R")) {
                    epwReadDone = readEncodedEPW(myReader, errorsAndWarnings);
                    if (doEcho && epwReadDone) {
                        System.out.println(String.format(Locale.ROOT, "EPW File encoding KOI8-R.%n"));
                    }
                }
            }
            if (!epwReadDone) {
                try (Scanner myReader = new Scanner(myObj, "KOI8-U")) {
                    epwReadDone = readEncodedEPW(myReader, errorsAndWarnings);
                    if (doEcho && epwReadDone) {
                        System.out.println(String.format(Locale.ROOT, "EPW File encoding KOI8-U.%n"));
                    }
                }
            }
            if (!epwReadDone) {
                System.out.println(String.format(Locale.ROOT,
                        "%n *** ERROR *** The provide EPW file is encoded in unknown character setting%n or does not have an EPW structure. %n"));
            }
            EPW_READ = epwReadDone;
        } catch (FileNotFoundException ex) {
            throw new UnsupportedOperationException(ex);
        }
    }

    private boolean readEncodedEPW(Scanner myReader, StringBuffer errorsAndWarnings) {
        boolean hasReadLines = false;
        while (myReader.hasNextLine()) {
            hasReadLines = true;
            String line = myReader.nextLine();
            if (line.equals("")) {
                continue;
            }
            String[] fields = line.split(",");
            switch (fields[0]) {
                case EPW_Location.ID -> {
                    epw_location = new EPW_Location(fields);
                    if (!epw_location.checkIntegrity()) {
                        System.err.println(epw_location.getString());
                        return false;
                    }
                }
                case EPW_Typical_Extreme_Periods.ID -> {
                    epw_typical_extreme_periods = new EPW_Typical_Extreme_Periods(fields);
                    if (!epw_typical_extreme_periods.checkIntegrity()) {
                        System.err.println(epw_typical_extreme_periods.getString());
                        return false;
                    }
                }
                case EPW_Design_Conditions.ID -> {
                    epw_design_conditions = new EPW_Design_Conditions(fields);
                    if (!epw_design_conditions.checkIntegrity()) {
                        System.err.println(epw_design_conditions.getString());
                        return false;
                    }
                }
                case EPW_Ground_Temperatures.ID -> {
                    epw_ground_temperatures = new EPW_Ground_Temperatures(fields);
                    if (!epw_ground_temperatures.checkIntegrity()) {
                        System.err.println(epw_ground_temperatures.getString());
                        return false;
                    }
                }
                case EPW_Holidays_Daylight_Savings.ID -> {
                    epw_holidays_daylight_savings = new EPW_Holidays_Daylight_Savings(fields);
                    if (!epw_holidays_daylight_savings.checkIntegrity()) {
                        System.err.println(epw_holidays_daylight_savings.getString());
                        return false;
                    }
                }
                case EPW_Holidays_Daylight_Savings.ALTERNATIVE_ID -> {
                    epw_holidays_daylight_savings = new EPW_Holidays_Daylight_Savings(fields);
                    if (!epw_holidays_daylight_savings.checkIntegrity()) {
                        System.err.println(epw_holidays_daylight_savings.getString());
                        return false;
                    }
                }
                case EPW_Data_Periods.ID -> {
                    epw_data_periods = new EPW_Data_Periods(fields);
                    if (!epw_data_periods.checkIntegrity()) {
                        System.err.println(epw_data_periods.getString());
                        return false;
                    }
                }
                default -> {
                    if (fields[0].split(" ")[0].equals(EPW_Comment.ID)) {
                        epw_comments.add(new EPW_Comment(fields[0].split(" ")[1], fields));
                    } else {
                        epw_data_fields.add(new EPW_Data_Fields(fields, EPW_VARIABLE_LIMITS, errorsAndWarnings));
                    }
                }
            }
        }
        for (EPW_Comment ec : epw_comments) {
            if (!ec.checkIntegrity()) {
                System.err.println(ec.getString());
                epw_comments.clear();
                return false;
            }
        }
        for (EPW_Data_Fields df : epw_data_fields) {
            if (!df.checkIntegrity()) {
                epw_data_fields.clear();
                return false;
            }
        }
        if(epw_data_fields.size() != 8760) {
            System.err.println("** ERROR ** EPW data fields are not 8760 points in size: " + epw_data_fields.size());
            epw_data_fields.clear();
            return false;
        }
        return hasReadLines;
    }
    
    
    /**
     * Sets information about UHI settings to retrieve file suffix.
     * 
     * @param EPW_ORIGINAL_LCZ buffer area temperature level
     * @param TARGET_UHI_LCZ urban density level
     */
    public void setUHIInfo(Integer EPW_ORIGINAL_LCZ, Integer TARGET_UHI_LCZ) {
        this.epw_original_lcz = EPW_ORIGINAL_LCZ;
        this.target_uhi_lcz = TARGET_UHI_LCZ;
        if(this.epw_original_lcz != null && this.target_uhi_lcz != null) {
            this.file_suffix = 
                    "_UHI" +
                    "_Target_" + LCZ.fromId(TARGET_UHI_LCZ).toString().replace("_", "-") + 
                    "_Original_" + LCZ.fromId(EPW_ORIGINAL_LCZ).toString().replace("_", "-");
            
            
        } else {
            this.file_suffix = "";
        }
    }

    /**
     * Gets the EPW original LCZ.
     * @return EPW original LCZ.
     */
    public Integer getEpw_original_lcz() {
        return epw_original_lcz;
    }

    /**
     * Gets the target UHI LCZ.
     * @return target UHI LCZ.
     */
    public Integer getTarget_uhi_lcz() {
        return target_uhi_lcz;
    }
    
    /**
     * Returns file suffix string.
     * @return file suffix.
     */
    public String getFile_suffix() {
        return file_suffix;
    }
    
    /**
     * Returns the EPW content in a string.
     *
     * @param errorsAndWarnings String buffer of errors and warnings
     * @return string
     */
    public String getString(StringBuffer errorsAndWarnings) {
        StringBuilder sb = new StringBuilder();
        sb.append(epw_location.getString());
        sb.append(epw_design_conditions.getString());
        sb.append(epw_typical_extreme_periods.getString());
        sb.append(epw_ground_temperatures.getString());
        sb.append(epw_holidays_daylight_savings.getString());
        for (int i = 0; i < epw_comments.size(); i++) {
            sb.append(epw_comments.get(i).getString());
        }
        sb.append(epw_data_periods.getString());
        for (int i = 0; i < epw_data_fields.size(); i++) {
            sb.append(epw_data_fields.get(i).getString(EPW_VARIABLE_LIMITS, errorsAndWarnings));
        }
        return sb.toString().replace(",-0,", ",0,").replace(",-0.0,", ",0.0,");
    }

    /**
     * Returns the path to the EPW file.
     *
     * @return path to EPW file
     */
    public String getPath_EPW() {
        return path_EPW;
    }

    /**
     * Returns the EPW location object.
     *
     * @return EPW location object
     */
    public EPW_Location getEpw_location() {
        return epw_location;
    }

    /**
     * Returns the EPW design conditions object.
     *
     * @return EPW design conditions object.
     */
    public EPW_Design_Conditions getEpw_design_conditions() {
        return epw_design_conditions;
    }

    /**
     * Returns the EPW typical/extreme periods object.
     *
     * @return EPW typical/extreme periods object
     */
    public EPW_Typical_Extreme_Periods getEpw_typical_extreme_periods() {
        return epw_typical_extreme_periods;
    }

    /**
     * Returns the EPW ground temperatures object.
     *
     * @return EPW ground temperatures
     */
    public EPW_Ground_Temperatures getEpw_ground_temperatures() {
        return epw_ground_temperatures;
    }

    /**
     * Returns the EPW holidays and daylight savings object.
     *
     * @return EPW holidays and daylight savings object
     */
    public EPW_Holidays_Daylight_Savings getEpw_holidays_daylight_savings() {
        return epw_holidays_daylight_savings;
    }

    /**
     * Return the EPW comments object.
     *
     * @return EPW comments object
     */
    public ArrayList<EPW_Comment> getEpw_comments() {
        return epw_comments;
    }

    /**
     * Returns the EPW data periods object.
     *
     * @return EPW data periods object
     */
    public EPW_Data_Periods getEpw_data_periods() {
        return epw_data_periods;
    }

    /**
     * Returns the EPW data fields objects list. Each element in the list is an
     * EPW data fields object.
     *
     * @return list of EPW data fields.
     */
    public ArrayList<EPW_Data_Fields> getEpw_data_fields() {
        return epw_data_fields;
    }

    /**
     * Sets the EPW location object.
     *
     * @param epw_location EPW location object
     */
    public void setEpw_location(EPW_Location epw_location) {
        this.epw_location = epw_location;
    }

    /**
     * Sets the EPW design conditions object.
     *
     * @param epw_design_conditions EPW design conditions
     */
    public void setEpw_design_conditions(EPW_Design_Conditions epw_design_conditions) {
        this.epw_design_conditions = epw_design_conditions;
    }

    /**
     * Sets the EPW typical/extreme periods object.
     *
     * @param epw_typical_extreme_periods EPW typical/extreme periods object
     */
    public void setEpw_typical_extreme_periods(EPW_Typical_Extreme_Periods epw_typical_extreme_periods) {
        this.epw_typical_extreme_periods = epw_typical_extreme_periods;
    }

    /**
     * Sets the EPW ground temperatures object.
     *
     * @param epw_ground_temperatures EPW ground temperatures
     */
    public void setEpw_ground_temperatures(EPW_Ground_Temperatures epw_ground_temperatures) {
        this.epw_ground_temperatures = epw_ground_temperatures;
    }

    /**
     * Sets the EPW holidays and daylight savings object.
     *
     * @param epw_holidays_daylight_savings EPW holidays and daylight savings
     */
    public void setEpw_holidays_daylight_savings(EPW_Holidays_Daylight_Savings epw_holidays_daylight_savings) {
        this.epw_holidays_daylight_savings = epw_holidays_daylight_savings;
    }

    /**
     * Sets the EPW comments object.
     *
     * @param epw_comments EPW comments
     */
    public void setEpw_comments(ArrayList<EPW_Comment> epw_comments) {
        this.epw_comments = epw_comments;
    }

    /**
     * Sets EPW data periods object.
     *
     * @param epw_data_periods EPW data periods
     */
    public void setEpw_data_periods(EPW_Data_Periods epw_data_periods) {
        this.epw_data_periods = epw_data_periods;
    }

    /**
     * Sets the list of EPW data fields. Each element of the list is an EPW data
     * fields object.
     *
     * @param epw_data_fields list of EPW data fields
     */
    public void setEpw_data_fields(ArrayList<EPW_Data_Fields> epw_data_fields) {
        this.epw_data_fields = epw_data_fields;
    }

    /**
     * Checks if EPW file was read correctly.
     *
     * @return true if epw was read correctly
     */
    public boolean isEPW_READ() {
        return EPW_READ;
    }

    /**
     * Stores the solar hour month adjustments used during solar radiation
     * calculations.
     *
     * @return monthly solar hour adjustments warnings.
     */
    public StringBuffer getAdjustmentWanings() {
        return adjustmentWanings;
    }

    /**
     * Saves a comparison between this EPW and another given EPW file.The
     * comparison is the monthly daily average of the morphed variables.
     *
     * @param printOutputs String buffer
     * @param errorsAndWarnings String buffer for errors and warnings
     * @param path_output path to the output folder
     * @param epw given EPW
     */
    public void saveEPWComparison(StringBuffer printOutputs, StringBuffer errorsAndWarnings, String path_output, EPW epw) {
        printOutputs.append(String.format(Locale.ROOT, "%n\tSaving comparison between the two EPW files...%n\t")).append(path_output).append(String.format(Locale.ROOT, "%n"));
        StringBuilder comparison = new StringBuilder();
        comparison.append(String.format(Locale.ROOT, "# Monthly average difference between variables of two EPW files (last EPW variables minus the first EPW variables).%n"));
        comparison.append(String.format(Locale.ROOT, "Variable,Unit,Description,Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec%n"));
        for (int i = 0; i < Math.min(
                this.getEpw_ground_temperatures().getNn_ground_temperatures_depth().length,
                epw.epw_ground_temperatures.getNn_ground_temperatures_depth().length
        ); i++) {
            Object[] args = new Object[]{
                this.getEpw_ground_temperatures().getNn_ground_temperatures_depth()[i].getN1_depth(),
                this.getEpw_ground_temperatures().getNn_ground_temperatures_depth()[i].getN5_January_average_ground_temperature() - epw.epw_ground_temperatures.getNn_ground_temperatures_depth()[i].getN5_January_average_ground_temperature(),
                this.getEpw_ground_temperatures().getNn_ground_temperatures_depth()[i].getN6_February_average_ground_temperature() - epw.epw_ground_temperatures.getNn_ground_temperatures_depth()[i].getN6_February_average_ground_temperature(),
                this.getEpw_ground_temperatures().getNn_ground_temperatures_depth()[i].getN7_March_average_ground_temperature() - epw.epw_ground_temperatures.getNn_ground_temperatures_depth()[i].getN7_March_average_ground_temperature(),
                this.getEpw_ground_temperatures().getNn_ground_temperatures_depth()[i].getN8_April_average_ground_temperature() - epw.epw_ground_temperatures.getNn_ground_temperatures_depth()[i].getN8_April_average_ground_temperature(),
                this.getEpw_ground_temperatures().getNn_ground_temperatures_depth()[i].getN9_May_average_ground_temperature() - epw.epw_ground_temperatures.getNn_ground_temperatures_depth()[i].getN9_May_average_ground_temperature(),
                this.getEpw_ground_temperatures().getNn_ground_temperatures_depth()[i].getN10_June_average_ground_temperature() - epw.epw_ground_temperatures.getNn_ground_temperatures_depth()[i].getN10_June_average_ground_temperature(),
                this.getEpw_ground_temperatures().getNn_ground_temperatures_depth()[i].getN11_July_average_ground_temperature() - epw.epw_ground_temperatures.getNn_ground_temperatures_depth()[i].getN11_July_average_ground_temperature(),
                this.getEpw_ground_temperatures().getNn_ground_temperatures_depth()[i].getN12_August_average_ground_temperature() - epw.epw_ground_temperatures.getNn_ground_temperatures_depth()[i].getN12_August_average_ground_temperature(),
                this.getEpw_ground_temperatures().getNn_ground_temperatures_depth()[i].getN13_September_average_ground_temperature() - epw.epw_ground_temperatures.getNn_ground_temperatures_depth()[i].getN13_September_average_ground_temperature(),
                this.getEpw_ground_temperatures().getNn_ground_temperatures_depth()[i].getN14_October_average_ground_temperature() - epw.epw_ground_temperatures.getNn_ground_temperatures_depth()[i].getN14_October_average_ground_temperature(),
                this.getEpw_ground_temperatures().getNn_ground_temperatures_depth()[i].getN15_November_average_ground_temperature() - epw.epw_ground_temperatures.getNn_ground_temperatures_depth()[i].getN15_November_average_ground_temperature(),
                this.getEpw_ground_temperatures().getNn_ground_temperatures_depth()[i].getN16_December_average_ground_temperature() - epw.epw_ground_temperatures.getNn_ground_temperatures_depth()[i].getN16_December_average_ground_temperature()
            };
            comparison.append(String.format(Locale.ROOT, "Ground Temperatures,∆°C,depth %.1f m,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f%n", args));
        }

        double[] thisEPWaverage_N6 = new double[12], otherEPWaverage_N6 = new double[12];
        double[] thisEPWaverage_N7 = new double[12], otherEPWaverage_N7 = new double[12];
        double[] thisEPWaverage_N8 = new double[12], otherEPWaverage_N8 = new double[12];
        double[] thisEPWaverage_N9 = new double[12], otherEPWaverage_N9 = new double[12];
        double[] thisEPWaverage_N10 = new double[12], otherEPWaverage_N10 = new double[12];
        double[] thisEPWaverage_N11 = new double[12], otherEPWaverage_N11 = new double[12];
        double[] thisEPWaverage_N12 = new double[12], otherEPWaverage_N12 = new double[12];
        double[] thisEPWaverage_N13 = new double[12], otherEPWaverage_N13 = new double[12];
        double[] thisEPWaverage_N14 = new double[12], otherEPWaverage_N14 = new double[12];
        double[] thisEPWaverage_N15 = new double[12], otherEPWaverage_N15 = new double[12];
        double[] thisEPWaverage_N16 = new double[12], otherEPWaverage_N16 = new double[12];
        double[] thisEPWaverage_N17 = new double[12], otherEPWaverage_N17 = new double[12];
        double[] thisEPWaverage_N18 = new double[12], otherEPWaverage_N18 = new double[12];
        double[] thisEPWaverage_N19 = new double[12], otherEPWaverage_N19 = new double[12];
        double[] thisEPWaverage_N21 = new double[12], otherEPWaverage_N21 = new double[12];
        double[] thisEPWaverage_N22 = new double[12], otherEPWaverage_N22 = new double[12];
        double[] thisEPWaverage_N23 = new double[12], otherEPWaverage_N23 = new double[12];
        double[] thisEPWaverage_N30 = new double[12], otherEPWaverage_N30 = new double[12];
        double[] thisEPWaverage_N33 = new double[12], otherEPWaverage_N33 = new double[12];

        for (int i = 0; i < Months.Abbreviation.values().length; i++) {
            int[] month_row_ids = Months.getMonthRowIds(Months.Abbreviation.values()[i]);
            float number_of_rows_in_month = month_row_ids[0];
            int first_row_id = month_row_ids[1];
            int last_row_id = month_row_ids[2];
            for (int row_id = first_row_id; row_id < last_row_id; row_id++) {
                thisEPWaverage_N6[i] += this.epw_data_fields.get(row_id).getN6_dry_bulb_temperature();
                otherEPWaverage_N6[i] += epw.getEpw_data_fields().get(row_id).getN6_dry_bulb_temperature();
                thisEPWaverage_N7[i] += this.epw_data_fields.get(row_id).getN7_dew_point_temperature();
                otherEPWaverage_N7[i] += epw.getEpw_data_fields().get(row_id).getN7_dew_point_temperature();
                thisEPWaverage_N8[i] += this.epw_data_fields.get(row_id).getN8_relative_humidity();
                otherEPWaverage_N8[i] += epw.getEpw_data_fields().get(row_id).getN8_relative_humidity();
                thisEPWaverage_N9[i] += this.epw_data_fields.get(row_id).getN9_atmospheric_station_pressure();
                otherEPWaverage_N9[i] += epw.getEpw_data_fields().get(row_id).getN9_atmospheric_station_pressure();
                thisEPWaverage_N10[i] += this.epw_data_fields.get(row_id).getN10_extraterrestrial_horizontal_radiation();
                otherEPWaverage_N10[i] += epw.getEpw_data_fields().get(row_id).getN10_extraterrestrial_horizontal_radiation();
                thisEPWaverage_N11[i] += this.epw_data_fields.get(row_id).getN11_extraterrestrial_direct_normal_radiation();
                otherEPWaverage_N11[i] += epw.getEpw_data_fields().get(row_id).getN11_extraterrestrial_direct_normal_radiation();
                thisEPWaverage_N12[i] += this.epw_data_fields.get(row_id).getN12_horizontal_infrared_radiation_intensity();
                otherEPWaverage_N12[i] += epw.getEpw_data_fields().get(row_id).getN12_horizontal_infrared_radiation_intensity();
                thisEPWaverage_N13[i] += this.epw_data_fields.get(row_id).getN13_global_horizontal_radiation();
                otherEPWaverage_N13[i] += epw.getEpw_data_fields().get(row_id).getN13_global_horizontal_radiation();
                thisEPWaverage_N14[i] += this.epw_data_fields.get(row_id).getN14_direct_normal_radiation();
                otherEPWaverage_N14[i] += epw.getEpw_data_fields().get(row_id).getN14_direct_normal_radiation();
                thisEPWaverage_N15[i] += this.epw_data_fields.get(row_id).getN15_diffuse_horizontal_radiation();
                otherEPWaverage_N15[i] += epw.getEpw_data_fields().get(row_id).getN15_diffuse_horizontal_radiation();

                thisEPWaverage_N16[i] += this.epw_data_fields.get(row_id).getN16_global_horizontal_illuminance();
                otherEPWaverage_N16[i] += epw.getEpw_data_fields().get(row_id).getN16_global_horizontal_illuminance();
                thisEPWaverage_N17[i] += this.epw_data_fields.get(row_id).getN17_direct_normal_illuminance();
                otherEPWaverage_N17[i] += epw.getEpw_data_fields().get(row_id).getN17_direct_normal_illuminance();
                thisEPWaverage_N18[i] += this.epw_data_fields.get(row_id).getN18_diffuse_horizontal_illuminance();
                otherEPWaverage_N18[i] += epw.getEpw_data_fields().get(row_id).getN18_diffuse_horizontal_illuminance();
                thisEPWaverage_N19[i] += this.epw_data_fields.get(row_id).getN19_zenith_luminance();
                otherEPWaverage_N19[i] += epw.getEpw_data_fields().get(row_id).getN19_zenith_luminance();

                thisEPWaverage_N21[i] += this.epw_data_fields.get(row_id).getN21_wind_speed();
                otherEPWaverage_N21[i] += epw.getEpw_data_fields().get(row_id).getN21_wind_speed();
                thisEPWaverage_N22[i] += this.epw_data_fields.get(row_id).getN22_total_sky_cover();
                otherEPWaverage_N22[i] += epw.getEpw_data_fields().get(row_id).getN22_total_sky_cover();
                thisEPWaverage_N23[i] += this.epw_data_fields.get(row_id).getN23_opaque_sky_cover();
                otherEPWaverage_N23[i] += epw.getEpw_data_fields().get(row_id).getN23_opaque_sky_cover();

                thisEPWaverage_N30[i] += this.epw_data_fields.get(row_id).getN30_snow_depth();
                otherEPWaverage_N30[i] += epw.getEpw_data_fields().get(row_id).getN30_snow_depth();

                thisEPWaverage_N33[i] += this.epw_data_fields.get(row_id).getN33_liquid_precipitation_depth();
                otherEPWaverage_N33[i] += epw.getEpw_data_fields().get(row_id).getN33_liquid_precipitation_depth();
            }

            thisEPWaverage_N6[i] /= number_of_rows_in_month;
            otherEPWaverage_N6[i] /= number_of_rows_in_month;
            thisEPWaverage_N7[i] /= number_of_rows_in_month;
            otherEPWaverage_N7[i] /= number_of_rows_in_month;
            thisEPWaverage_N8[i] /= number_of_rows_in_month;
            otherEPWaverage_N8[i] /= number_of_rows_in_month;
            thisEPWaverage_N9[i] /= number_of_rows_in_month;
            otherEPWaverage_N9[i] /= number_of_rows_in_month;
            thisEPWaverage_N10[i] /= number_of_rows_in_month;
            otherEPWaverage_N10[i] /= number_of_rows_in_month;
            thisEPWaverage_N11[i] /= number_of_rows_in_month;
            otherEPWaverage_N11[i] /= number_of_rows_in_month;
            thisEPWaverage_N12[i] /= number_of_rows_in_month;
            otherEPWaverage_N12[i] /= number_of_rows_in_month;
            thisEPWaverage_N13[i] /= number_of_rows_in_month;
            otherEPWaverage_N13[i] /= number_of_rows_in_month;
            thisEPWaverage_N14[i] /= number_of_rows_in_month;
            otherEPWaverage_N14[i] /= number_of_rows_in_month;
            thisEPWaverage_N15[i] /= number_of_rows_in_month;
            otherEPWaverage_N15[i] /= number_of_rows_in_month;
            thisEPWaverage_N16[i] /= number_of_rows_in_month;
            otherEPWaverage_N16[i] /= number_of_rows_in_month;
            thisEPWaverage_N17[i] /= number_of_rows_in_month;
            otherEPWaverage_N17[i] /= number_of_rows_in_month;
            thisEPWaverage_N18[i] /= number_of_rows_in_month;
            otherEPWaverage_N18[i] /= number_of_rows_in_month;
            thisEPWaverage_N19[i] /= number_of_rows_in_month;
            otherEPWaverage_N19[i] /= number_of_rows_in_month;
            thisEPWaverage_N21[i] /= number_of_rows_in_month;
            otherEPWaverage_N21[i] /= number_of_rows_in_month;
            thisEPWaverage_N22[i] /= number_of_rows_in_month;
            otherEPWaverage_N22[i] /= number_of_rows_in_month;
            thisEPWaverage_N23[i] /= number_of_rows_in_month;
            otherEPWaverage_N23[i] /= number_of_rows_in_month;
            thisEPWaverage_N30[i] /= number_of_rows_in_month;
            otherEPWaverage_N30[i] /= number_of_rows_in_month;
            thisEPWaverage_N33[i] /= number_of_rows_in_month;
            otherEPWaverage_N33[i] /= number_of_rows_in_month;
        }

        addComparisonLine(comparison, "N6 Dry Bulb Temperature", "∆°C", "-", thisEPWaverage_N6, otherEPWaverage_N6);
        addComparisonLine(comparison, "N7 Dew Point Temperature", "∆°C", "-", thisEPWaverage_N7, otherEPWaverage_N7);
        addComparisonLine(comparison, "N8 Relative Humidity", "∆%", "-", thisEPWaverage_N8, otherEPWaverage_N8);
        addComparisonLine(comparison, "N9 Atmospheric Station Pressure", "∆Pa", "-", thisEPWaverage_N9, otherEPWaverage_N9);
        addComparisonLine(comparison, "N10 Extraterrestrial Horizontal Radiation", "∆Wh/m²", "-", thisEPWaverage_N10, otherEPWaverage_N10);
        addComparisonLine(comparison, "N11 Extraterrestrial Direct Normal Radiation", "∆Wh/m²", "-", thisEPWaverage_N11, otherEPWaverage_N11);
        addComparisonLine(comparison, "N12 Horizontal Infrared Radiation Intensity", "∆Wh/m²", "-", thisEPWaverage_N12, otherEPWaverage_N12);
        addComparisonLine(comparison, "N13 Global Horizontal Radiation", "∆Wh/m²", "-", thisEPWaverage_N13, otherEPWaverage_N13);
        addComparisonLine(comparison, "N14 Direct Normal Radiation", "∆Wh/m²", "-", thisEPWaverage_N14, otherEPWaverage_N14);
        addComparisonLine(comparison, "N15 Diffuse Horizontal Radiation", "∆Wh/m²", "-", thisEPWaverage_N15, otherEPWaverage_N15);
        addComparisonLine(comparison, "N16 Global Horizontal Illuminance", "∆lux", "-", thisEPWaverage_N16, otherEPWaverage_N16);
        addComparisonLine(comparison, "N17 Direct Normal Illuminance", "∆lux", "-", thisEPWaverage_N17, otherEPWaverage_N17);
        addComparisonLine(comparison, "N18 Diffuse Horizontal Illuminance", "∆lux", "-", thisEPWaverage_N18, otherEPWaverage_N18);
        addComparisonLine(comparison, "N19 Zenith Luminance", "∆Cd/m²", "-", thisEPWaverage_N19, otherEPWaverage_N19);
        addComparisonLine(comparison, "N21 Wind Speed", "∆m/s", "-", thisEPWaverage_N21, otherEPWaverage_N21);
        addComparisonLine(comparison, "N22 Total Sky Cover", "∆deca", "-", thisEPWaverage_N22, otherEPWaverage_N22);
        addComparisonLine(comparison, "N23 Opaque Sky Cover", "∆deca", "-", thisEPWaverage_N23, otherEPWaverage_N23);
        addComparisonLine(comparison, "N30 Snow Depth", "∆cm", "-", thisEPWaverage_N30, otherEPWaverage_N30);
        addComparisonLine(comparison, "N33 Liquid Precipitation Depth", "∆mm", "-", thisEPWaverage_N33, otherEPWaverage_N33);

        try (OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(path_output), StandardCharsets.UTF_8)) {
            writer.write(comparison.toString());
            writer.close();
        } catch (FileNotFoundException ex) {
            String err = " ** ERROR ** File not found: %s %n";
            errorsAndWarnings.append(err.formatted(Locale.ROOT, ex));
            System.err.print(err);
        } catch (IOException ex) {
            String err = " ** ERROR ** Cannot write file: %s %n";
            errorsAndWarnings.append(err.formatted(Locale.ROOT, ex));
            System.err.print(err);
        }
    }

    /**
     * Saves the stat file for this EPW object.
     *
     * @param printOutputs Collects the print outputs
     * @param errorsAndWarnings Collects the errors and warnings
     * @param path_output Path to STAT file
     */
    public void saveSTATFile(StringBuffer printOutputs, StringBuffer errorsAndWarnings, String path_output) {
        printOutputs.append(String.format(Locale.ROOT, "%n\tSaving STAT file...%n\t")).append(path_output).append(String.format(Locale.ROOT, "%n"));
        StringBuilder stats = new StringBuilder();

        // Header
        String app_creator = " - %s %s %n".formatted(new Object[]{FutureWeatherGenerator.APP_NAME, FutureWeatherGenerator.APP_VERSION});
        stats.append(app_creator);
        // File
        stats.append(" Statistics for %s %n".formatted(new Object[]{this.path_EPW}));
        // Location
        DecimalFormat fmt = new DecimalFormat("+#,##0.0;-#");
        stats.append("""
                      Location -- %s %s %s  
                           {%s %d°  %d'} {%s   %d° %d'} {GMT %s Hours}
                     """.formatted(new Object[]{
            this.epw_location.getA1_city(),
            this.epw_location.getA2_region(),
            this.epw_location.getA3_country(),
            this.epw_location.getN2_latitude() > 0 ? "N" : "S",
            (int) this.epw_location.getN2_latitude(),
            (int) ((this.epw_location.getN2_latitude() % 1) * 60f),
            this.epw_location.getN3_longitude() > 0 ? "E" : "W",
            (int) this.epw_location.getN3_longitude(),
            (int) ((this.epw_location.getN3_longitude() % 1) * 60f),
            fmt.format(this.epw_location.getN4_time_zone())
        }));
        // Elevation
        stats.append(" Elevation --   %d m above sea level %n".formatted(new Object[]{(int) this.epw_location.getN5_elevation()}));
        // Standard Pressure at Elevation
        stats.append(" Standard Pressure at Elevation --  %d Pa %n".formatted(new Object[]{(int) (101325f - (this.epw_location.getN5_elevation() / 100f * 1190f))}));
        // Data Source
        stats.append(" Data Source -- %s %n%n".formatted(new Object[]{"Morphed Weather Data"}));

        String tab03 = """
                        - %s [%s]
                        	           	  Jan	  Feb	  Mar	  Apr	  May	  Jun	  Jul	  Aug	  Sep	  Oct	  Nov	  Dec	
                        	Maximum 	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	
                        	 Day:Hour	%s:%s	%s:%s	%s:%s	%s:%s	%s:%s	%s:%s	%s:%s	%s:%s	%s:%s	%s:%s	%s:%s	%s:%s	
                       
                        	Minimum 	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	
                        	 Day:Hour	%s:%s	%s:%s	%s:%s	%s:%s	%s:%s	%s:%s	%s:%s	%s:%s	%s:%s	%s:%s	%s:%s	%s:%s	
                       
                        	Daily Avg	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s 	
                       
                          - Maximum %s of %s %s on %s %s
                          - Minimum %s of %s %s on %s %s
                       
                       """;
        //  - Monthly Statistics for Dry Bulb temperatures °C
        addSTATMonthlyVariableTable(stats, true, tab03, "Monthly Statistics for Dry Bulb temperatures", "°C", "Dry Bulb Temperature", "°C", "N6_dry_bulb_temperature");

        String tab06 = """
                        - %s [%s]
                        	  #Days    	  Jan	  Feb	  Mar	  Apr	  May	  Jun	  Jul	  Aug	  Sep	  Oct	  Nov	  Dec	
                        	Max >= 32	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	Max <=  0	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	Min <=  0	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	Min <=-18	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                       
                       """;
        //  - Monthly Statistics for Extreme Dry Bulb temperatures °C
        addSTATMonthlyVariableTable4(stats, tab06, "Monthly Statistics for Extreme Dry Bulb temperatures", "°C");

        //  - Monthly Statistics for Dew Point temperatures °C
        addSTATMonthlyVariableTable(stats, true, tab03, "Monthly Statistics for Dew Point temperatures", "°C", "Dew Point temperatures", "°C", "N7_dew_point_temperature");
        // Variables
        String tab01 = """
                        - %s [%s]
                        	           	  Jan	  Feb	  Mar	  Apr	  May	  Jun	  Jul	  Aug	  Sep	  Oct	  Nov	  Dec
                        	 0:01- 1:00	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	 1:01- 2:00	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	 2:01- 3:00	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	 3:01- 4:00	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	 4:01- 5:00	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	 5:01- 6:00	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	 6:01- 7:00	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	 7:01- 8:00	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	 8:01- 9:00	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	 9:01-10:00	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	10:01-11:00	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	11:01-12:00	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	12:01-13:00	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	13:01-14:00	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	14:01-15:00	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	15:01-16:00	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	16:01-17:00	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	17:01-18:00	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	18:01-19:00	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	19:01-20:00	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	20:01-21:00	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	21:01-22:00	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	22:01-23:00	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	23:01-24:00	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	 Max Hour  	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	 Min Hour  	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                       
                       """;

        //  - Average Hourly Statistics for Dry Bulb temperatures °C
        addSTATVariableTable(stats, true, tab01, "Average Hourly Statistics for Dry Bulb temperatures", "°C", "N6_dry_bulb_temperature");
        //  - Average Hourly Statistics for Dew Point temperatures °C
        addSTATVariableTable(stats, true, tab01, "Average Hourly Statistics for Dew Point temperatures", "°C", "N7_dew_point_temperature");

        String tab04 = """
                        - %s [%s]
                        	           	  Jan	  Feb	  Mar	  Apr	  May	  Jun	  Jul	  Aug	  Sep	  Oct	  Nov	  Dec	
                        	Maximum 	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	
                        	 Day:Hour	%s:%s	%s:%s	%s:%s	%s:%s	%s:%s	%s:%s	%s:%s	%s:%s	%s:%s	%s:%s	%s:%s	%s:%s	
                       
                        	Minimum 	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	
                        	 Day:Hour	%s:%s	%s:%s	%s:%s	%s:%s	%s:%s	%s:%s	%s:%s	%s:%s	%s:%s	%s:%s	%s:%s	%s:%s	
                       
                        	Daily Avg	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s 	
                       
                       """;
        //  - Monthly Statistics for Relative Humidity %
        addSTATMonthlyRelativeHumidityTable(stats, tab04, "Monthly Statistics for Relative Humidity", "%");
        //  - Average Hourly Relative Humidity %
        addSTATVariableTable(stats, false, tab01, "Average Hourly Relative Humidity", "%", "N8_relative_humidity");

        //  - Monthly Indicators for Precipitation/Moisture (kPa)
        //  - Monthly Statistics for Wind Chill/Heat Index temperatures °C **
        //  - Monthly Statistics for Wind Speed m/s
        addSTATMonthlyVariableTable(stats, true, tab03, "Monthly Statistics for Wind Speed", "m/s", "Wind Speed", "m/s", "N21_wind_speed");
        //  - Average Hourly Statistics for Wind Speed m/s
        addSTATVariableTable(stats, true, tab01, "Average Hourly Statistics for Wind Speed", "m/s", "N21_wind_speed");
        //  - Average Hourly Statistics for Wind Direction ° {N=0 or 360,E=90,S=180,W=270}
        addSTATVariableTable(stats, false, tab01, "Average Hourly Statistics for Wind Direction {N=0° or 360°, E=90°, S=180°, W=270°}", "°", "N20_wind_direction");

        //  - Monthly Wind Direction {Interval 11.25 deg from displayed deg} [%]
        String tab11 = """
                        - %s [%s]
                        	           	  Jan	  Feb	  Mar	  Apr	  May	  Jun	  Jul	  Aug	  Sep	  Oct	  Nov	  Dec	
                        	N      0.0°	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	ENE   22.5°	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	NE    45.0°	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	ENE   67.5°	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	E     90.0°	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	ESE  112.5°	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	SE   135.0°	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	SSE  157.5°	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	S    180.0°	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	SSW  202.5°	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	SW   225.0°	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	WSW  247.5°	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	W    270.0°	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	WNW  292.5°	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	NW   315.0°	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	NNW  337.5°	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                       
                       """;
        addSTATMonthlyVariableTable8(stats, tab11, "Monthly Wind Direction {Interval 11.25 deg from displayed deg}", "%");

        //  - Wind Frequency (m/s) by Direction {Interval 11.25 deg from displayed deg} [%]
        String tab12 = """
                        - %s [%s]
                        	           	 <=.5	  <=2	  <=4	  <=6	  <=8	 <=10	 <=12	 <=14	 <=16	 <=18	  >18	Total	
                        	N      0.0°	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	ENE   22.5°	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	NE    45.0°	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	ENE   67.5°	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	E     90.0°	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	ESE  112.5°	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	SE   135.0°	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	SSE  157.5°	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	S    180.0°	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	SSW  202.5°	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	SW   225.0°	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	WSW  247.5°	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	W    270.0°	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	WNW  292.5°	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	NW   315.0°	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	NNW  337.5°	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                       
                        	Total     	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                       
                       """;
        addSTATWindFrequencyTable(stats, tab12, "Wind Frequency [m/s] by Direction {Interval 11.25 deg from displayed deg}", "%");

        String tab07 = """
                        - %s [%s]
                        	           	  Jan	  Feb	  Mar	  Apr	  May	  Jun	  Jul	  Aug	  Sep	  Oct	  Nov	  Dec	
                        	  Total    	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	Max Hourly 	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                       
                       """;
        //  - Monthly Statistics for Liquid Precipitation mm
        addSTATMonthlyVariableTable5(stats, tab07, "Monthly Statistics for Liquid Precipitation", "mm");

        String tab08 = """
                        - %s
                        	           	  Jan	  Feb	  Mar	  Apr	  May	  Jun	  Jul	  Aug	  Sep	  Oct	  Nov	  Dec	
                        	  Average  	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                       
                       """;
        //  - Monthly Statistics for Albedo
        addSTATMonthlyVariableTable6(stats, tab08, "Monthly Statistics for Albedo");

        String tab05 = """
                        - %s [%s]
                        	           	  Jan	  Feb	  Mar	  Apr	  May	  Jun	  Jul	  Aug	  Sep	  Oct	  Nov	  Dec	
                        	Direct Avg 	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                       
                        	Direct Max 	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	     Day   	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                       
                        	Diffuse Avg	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                       
                        	Global Avg 	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                       
                          - Maximum %s of %s %s on %s %s
                       
                       """;
        //  - Monthly Statistics for Solar Radiation (Direct Normal, Diffuse, Global Horizontal) Wh/m²
        // CHECK: This differs from WeatherConverter
        addSTATMonthlyVariableTable3(stats, tab05, "Monthly Statistics for Solar Radiation (Direct Normal, Diffuse, Global Horizontal)", "Wh/m²", "Direct Normal Radiation", "Wh/m²");

        //  - Average Hourly Statistics for Direct Normal Solar Radiation Wh/m²
        addSTATVariableTable(stats, false, tab01, "Average Hourly Statistics for Direct Normal Solar Radiation", "Wh/m²", "N14_direct_normal_radiation");
        //  - Average Hourly Statistics for Diffuse Horizontal Solar Radiation Wh/m²
        addSTATVariableTable(stats, false, tab01, "Average Hourly Statistics for Diffuse Horizontal Solar Radiation", "Wh/m²", "N15_diffuse_horizontal_radiation");
        //  - Average Hourly Statistics for Global Horizontal Solar Radiation Wh/m²
        addSTATVariableTable(stats, false, tab01, "Average Hourly Statistics for Global Horizontal Solar Radiation", "Wh/m²", "N13_global_horizontal_radiation");
        //  - Average Hourly Statistics for Total Sky Cover %
        addSTATVariableTable(stats, false, tab01, "Average Hourly Statistics for Total Sky Cover", "%", "N22_total_sky_cover");
        //  - Average Hourly Statistics for Opaque Sky Cover %
        addSTATVariableTable(stats, false, tab01, "Average Hourly Statistics for Opaque Sky Cover", "%", "N23_opaque_sky_cover");

        //  - Monthly Calculated "undisturbed" Ground Temperatures** °C
        // CHECK: This differs from WeatherConverter diffusivity
        String tab02 = """
                        - %s [%s]
                        	           	  Jan	  Feb	  Mar	  Apr	  May	  Jun	  Jul	  Aug	  Sep	  Oct	  Nov	  Dec
                        	    0.5 m  	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	    2.0 m  	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	    4.0 m  	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                       
                          - **These ground temperatures should NOT BE USED in the GroundTemperatures object to compute building floor losses.
                          -   The temperatures for 0.5 m depth can be used for GroundTemperatures:Surface.
                          -   The temperatures for 4.0 m depth can be used for GroundTemperatures:Deep.
                          -   Calculations use a thermal ground diffusivity of 0.055741824 {m²/day}
                       
                       """;
        addSTATGroundTemperatureTable(stats, tab02, "Monthly Calculated \"undisturbed\" Ground Temperatures**", "°C");

        String tab09 = """
                        - Heating/Cooling Degree Days/Hours calculated from this weather file.
                        - Monthly Weather File Heating/Cooling Degree Days/Hours
                        	           	  Jan	  Feb	  Mar	  Apr	  May	  Jun	  Jul	  Aug	  Sep	  Oct	  Nov	  Dec
                        	HDD base10C	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	HDD base18C	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                       
                        	CDD base10C	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	CDD base18C	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                       
                        	CDH base20C	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	CDH base23C	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                        	HDH base27C	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s	%s
                       
                           - %s annual (wthr file) cooling degree-days (10 °C baseline)
                           - %s annual (wthr file) heating degree-days (10 °C baseline)
                       
                           - %s annual (wthr file) cooling degree-days (18 °C baseline)
                           - %s annual (wthr file) heating degree-days (18 °C baseline)
                       
                       """;
        //  - Heating/Cooling Degree Days/Hours calculated from this weather file.
        //  - Monthly Weather File Heating/Cooling Degree Days/Hours
        addSTATMonthlyVariableTable7(stats, tab09);

        String tab10 = """
                        - Typical/Extreme Period Determination
                       
                        - Summer is %s:%s, Average Temp= %s °C
                            Extreme Summer Week (nearest maximum temperature for summer)
                            Extreme Hot Week Period selected: %s %s:%s %s, Average Temp= %s °C, Deviation= |%s| °C
                            Typical Summer Week (nearest average temperature for summer)
                            Typical Week Period selected: %s %s:%s %s, Average Temp= %s °C, Deviation= |%s| °C
                       
                        - Winter is %s:%s, Average Temp= %s °C
                            Extreme Winter Week (nearest minimum temperature for winter)
                            Extreme Cold Week Period selected: %s %s:%s %s, Average Temp= %s °C, Deviation= |%s| °C
                            Typical Winter Week (nearest average temperature for winter)
                            Typical Week Period selected: %s %s:%s %s, Average Temp= %s °C, Deviation= |%s| °C
                       
                        - Autumn is %s:%s, Average Temp= %s °C
                            Typical Autumn Week (nearest average temperature for autumn)
                            Typical Week Period selected: %s %s:%s %s, Average Temp= %s °C, Deviation= |%s| °C
                       
                        - Spring is %s:%s, Average Temp= %s °C
                            Typical Spring Week (nearest average temperature for spring)
                            Typical Week Period selected: %s %s:%s %s, Average Temp= %s °C, Deviation= |%s| °C
                       """;
        //  - Typical/Extreme Period Determination
        addSTATMonthlyVariableTable9(stats, tab10);

        try (OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(path_output), StandardCharsets.UTF_8)) {
            writer.write(stats.toString());
            writer.close();
        } catch (FileNotFoundException ex) {
            String err = " ** ERROR ** File not found: %s %n";
            errorsAndWarnings.append(err.formatted(Locale.ROOT, ex));
            System.err.print(err);
        } catch (IOException ex) {
            String err = " ** ERROR ** Cannot write file: %s %n";
            errorsAndWarnings.append(err.formatted(Locale.ROOT, ex));
            System.err.print(err);
        }

    }

    private void addSTATGroundTemperatureTable(StringBuilder stats, String table, String tableName, String tableUnits) {
        Object[] args = new Object[12 * 3 + 2];
        args[0] = tableName;
        args[1] = tableUnits;

        for (int d = 0; d < this.epw_ground_temperatures.getNn_ground_temperatures_depth().length; d++) {
            int id = (d * 12) + 2;
            args[id + 0] = String.format(Locale.ROOT, "%5.1f", (Object) this.epw_ground_temperatures.getNn_ground_temperatures_depth()[d].getN5_January_average_ground_temperature());
            args[id + 1] = String.format(Locale.ROOT, "%5.1f", (Object) this.epw_ground_temperatures.getNn_ground_temperatures_depth()[d].getN6_February_average_ground_temperature());
            args[id + 2] = String.format(Locale.ROOT, "%5.1f", (Object) this.epw_ground_temperatures.getNn_ground_temperatures_depth()[d].getN7_March_average_ground_temperature());
            args[id + 3] = String.format(Locale.ROOT, "%5.1f", (Object) this.epw_ground_temperatures.getNn_ground_temperatures_depth()[d].getN8_April_average_ground_temperature());
            args[id + 4] = String.format(Locale.ROOT, "%5.1f", (Object) this.epw_ground_temperatures.getNn_ground_temperatures_depth()[d].getN9_May_average_ground_temperature());
            args[id + 5] = String.format(Locale.ROOT, "%5.1f", (Object) this.epw_ground_temperatures.getNn_ground_temperatures_depth()[d].getN10_June_average_ground_temperature());
            args[id + 6] = String.format(Locale.ROOT, "%5.1f", (Object) this.epw_ground_temperatures.getNn_ground_temperatures_depth()[d].getN11_July_average_ground_temperature());
            args[id + 7] = String.format(Locale.ROOT, "%5.1f", (Object) this.epw_ground_temperatures.getNn_ground_temperatures_depth()[d].getN12_August_average_ground_temperature());
            args[id + 8] = String.format(Locale.ROOT, "%5.1f", (Object) this.epw_ground_temperatures.getNn_ground_temperatures_depth()[d].getN13_September_average_ground_temperature());
            args[id + 9] = String.format(Locale.ROOT, "%5.1f", (Object) this.epw_ground_temperatures.getNn_ground_temperatures_depth()[d].getN14_October_average_ground_temperature());
            args[id + 10] = String.format(Locale.ROOT, "%5.1f", (Object) this.epw_ground_temperatures.getNn_ground_temperatures_depth()[d].getN15_November_average_ground_temperature());
            args[id + 11] = String.format(Locale.ROOT, "%5.1f", (Object) this.epw_ground_temperatures.getNn_ground_temperatures_depth()[d].getN16_December_average_ground_temperature());
        }

        stats.append(String.format(table, args));
    }

    private void addSTATMonthlyVariableTable(StringBuilder stats, boolean isFloat, String table, String tableName, String tableUnits, String maxAndMinVariableName, String maxAndMinVariableUnits, String variableName) {
        float[] max = new float[12];
        Arrays.fill(max, Float.NEGATIVE_INFINITY);
        int[] max_day = new int[12];
        int[] max_hour = new int[12];
        float[] min = new float[12];
        Arrays.fill(min, Float.POSITIVE_INFINITY);
        int[] min_day = new int[12];
        int[] min_hour = new int[12];
        float[] avg = new float[12];
        float[] count = new float[12];
        float maximum = Float.NEGATIVE_INFINITY;
        int maximum_month = 0;
        int maximum_day = 0;
        float minimum = Float.POSITIVE_INFINITY;
        int minimum_month = 0;
        int minimum_day = 0;

        // Hourly monthly average of the variable
        for (int i = 0; i < this.epw_data_fields.size(); i++) {
            int m = this.epw_data_fields.get(i).getN2_month() - 1;
            count[m]++;
            switch (variableName) {
                case "N6_dry_bulb_temperature":
                    if (max[m] < this.epw_data_fields.get(i).getN6_dry_bulb_temperature()) {
                        max[m] = this.epw_data_fields.get(i).getN6_dry_bulb_temperature();
                        max_day[m] = this.epw_data_fields.get(i).getN3_day();
                        max_hour[m] = this.epw_data_fields.get(i).getN4_hour();
                    }
                    if (min[m] > this.epw_data_fields.get(i).getN6_dry_bulb_temperature()) {
                        min[m] = this.epw_data_fields.get(i).getN6_dry_bulb_temperature();
                        min_day[m] = this.epw_data_fields.get(i).getN3_day();
                        min_hour[m] = this.epw_data_fields.get(i).getN4_hour();
                    }
                    if (maximum < this.epw_data_fields.get(i).getN6_dry_bulb_temperature()) {
                        maximum = this.epw_data_fields.get(i).getN6_dry_bulb_temperature();
                        maximum_month = this.epw_data_fields.get(i).getN2_month();
                        maximum_day = this.epw_data_fields.get(i).getN3_day();
                    }
                    if (minimum > this.epw_data_fields.get(i).getN6_dry_bulb_temperature()) {
                        minimum = this.epw_data_fields.get(i).getN6_dry_bulb_temperature();
                        minimum_month = this.epw_data_fields.get(i).getN2_month();
                        minimum_day = this.epw_data_fields.get(i).getN3_day();
                    }
                    avg[m] += this.epw_data_fields.get(i).getN6_dry_bulb_temperature();
                    break;
                case "N7_dew_point_temperature":
                    if (max[m] < this.epw_data_fields.get(i).getN7_dew_point_temperature()) {
                        max[m] = this.epw_data_fields.get(i).getN7_dew_point_temperature();
                        max_day[m] = this.epw_data_fields.get(i).getN3_day();
                        max_hour[m] = this.epw_data_fields.get(i).getN4_hour();
                    }
                    if (min[m] > this.epw_data_fields.get(i).getN7_dew_point_temperature()) {
                        min[m] = this.epw_data_fields.get(i).getN7_dew_point_temperature();
                        min_day[m] = this.epw_data_fields.get(i).getN3_day();
                        min_hour[m] = this.epw_data_fields.get(i).getN4_hour();
                    }
                    if (maximum < this.epw_data_fields.get(i).getN7_dew_point_temperature()) {
                        maximum = this.epw_data_fields.get(i).getN7_dew_point_temperature();
                        maximum_month = this.epw_data_fields.get(i).getN2_month();
                        maximum_day = this.epw_data_fields.get(i).getN3_day();
                    }
                    if (minimum > this.epw_data_fields.get(i).getN7_dew_point_temperature()) {
                        minimum = this.epw_data_fields.get(i).getN7_dew_point_temperature();
                        minimum_month = this.epw_data_fields.get(i).getN2_month();
                        minimum_day = this.epw_data_fields.get(i).getN3_day();
                    }
                    avg[m] += this.epw_data_fields.get(i).getN7_dew_point_temperature();
                    break;
                case "N21_wind_speed":
                    if (max[m] < this.epw_data_fields.get(i).getN21_wind_speed()) {
                        max[m] = this.epw_data_fields.get(i).getN21_wind_speed();
                        max_day[m] = this.epw_data_fields.get(i).getN3_day();
                        max_hour[m] = this.epw_data_fields.get(i).getN4_hour();
                    }
                    if (min[m] > this.epw_data_fields.get(i).getN21_wind_speed()) {
                        min[m] = this.epw_data_fields.get(i).getN21_wind_speed();
                        min_day[m] = this.epw_data_fields.get(i).getN3_day();
                        min_hour[m] = this.epw_data_fields.get(i).getN4_hour();
                    }
                    if (maximum < this.epw_data_fields.get(i).getN21_wind_speed()) {
                        maximum = this.epw_data_fields.get(i).getN21_wind_speed();
                        maximum_month = this.epw_data_fields.get(i).getN2_month();
                        maximum_day = this.epw_data_fields.get(i).getN3_day();
                    }
                    if (minimum > this.epw_data_fields.get(i).getN21_wind_speed()) {
                        minimum = this.epw_data_fields.get(i).getN21_wind_speed();
                        minimum_month = this.epw_data_fields.get(i).getN2_month();
                        minimum_day = this.epw_data_fields.get(i).getN3_day();
                    }
                    avg[m] += this.epw_data_fields.get(i).getN21_wind_speed();
                    break;
                default:
                    throw new UnsupportedOperationException("** ERROR ** Unknown variable.");
            }
        }
        for (int i = 0; i < avg.length; i++) {
            avg[i] /= count[i];
        }

        Object[] args = new Object[2 + (7 * 12) + 10];
        args[0] = tableName;
        args[1] = tableUnits;
        for (int i = 0; i < max.length; i++) {
            if (isFloat) {
                args[i + 2] = String.format(Locale.ROOT, "%5.1f", (Object) max[i]);
            } else {
                args[i + 2] = String.format("%5d", (Object) ((int) Math.round(max[i])));
            }
        }
        for (int i = 0; i < max_day.length; i++) {
            args[14 + i * 2] = String.format("%2d", (Object) max_day[i]);
            args[15 + i * 2] = String.format("%02d", (Object) max_hour[i]);
        }

        for (int i = 0; i < min.length; i++) {
            if (isFloat) {
                args[38 + i] = String.format(Locale.ROOT, "%5.1f", (Object) min[i]);
            } else {
                args[39 + i] = String.format("%5d", (Object) ((int) Math.round(min[i])));
            }
        }
        for (int i = 0; i < min_day.length; i++) {
            args[50 + i * 2] = String.format("%2d", (Object) min_day[i]);
            args[51 + i * 2] = String.format("%02d", (Object) min_hour[i]);
        }
        for (int i = 0; i < avg.length; i++) {
            args[74 + i] = String.format(Locale.ROOT, "%5.1f", (Object) avg[i]);
        }
        args[86] = maxAndMinVariableName;
        args[87] = String.format(Locale.ROOT, "%5.1f", (Object) maximum);
        args[88] = maxAndMinVariableUnits;
        args[89] = Months.Abbreviation.values()[maximum_month - 1];
        args[90] = String.format("%2d", maximum_day);
        args[91] = maxAndMinVariableName;
        args[92] = String.format(Locale.ROOT, "%5.1f", (Object) minimum);
        args[93] = maxAndMinVariableUnits;
        args[94] = Months.Abbreviation.values()[minimum_month - 1];
        args[95] = String.format("%2d", minimum_day);
        stats.append(String.format(table, args));
    }

    private void addSTATMonthlyRelativeHumidityTable(StringBuilder stats, String table, String tableName, String tableUnits) {
        float[] max = new float[12];
        Arrays.fill(max, Float.NEGATIVE_INFINITY);
        int[] max_day = new int[12];
        int[] max_hour = new int[12];
        float[] min = new float[12];
        Arrays.fill(min, Float.POSITIVE_INFINITY);
        int[] min_day = new int[12];
        int[] min_hour = new int[12];
        float[] avg = new float[12];
        float[] count = new float[12];

        // Hourly monthly average of the variable
        for (int i = 0; i < this.epw_data_fields.size(); i++) {
            int m = this.epw_data_fields.get(i).getN2_month() - 1;
            count[m]++;
            if (max[m] < this.epw_data_fields.get(i).getN8_relative_humidity()) {
                max[m] = this.epw_data_fields.get(i).getN8_relative_humidity();
                max_day[m] = this.epw_data_fields.get(i).getN3_day();
                max_hour[m] = this.epw_data_fields.get(i).getN4_hour();
            }
            if (min[m] > this.epw_data_fields.get(i).getN8_relative_humidity()) {
                min[m] = this.epw_data_fields.get(i).getN8_relative_humidity();
                min_day[m] = this.epw_data_fields.get(i).getN3_day();
                min_hour[m] = this.epw_data_fields.get(i).getN4_hour();
            }
            avg[m] += this.epw_data_fields.get(i).getN8_relative_humidity();
        }
        for (int i = 0; i < avg.length; i++) {
            avg[i] /= count[i];
        }

        Object[] args = new Object[2 + (7 * 12)];
        args[0] = tableName;
        args[1] = tableUnits;
        for (int i = 0; i < max.length; i++) {
            args[i + 2] = String.format("%5d", (Object) ((int) Math.round(max[i])));
        }
        for (int i = 0; i < max_day.length; i++) {
            args[14 + i * 2] = String.format("%2d", (Object) max_day[i]);
            args[15 + i * 2] = String.format("%02d", (Object) max_hour[i]);
        }

        for (int i = 0; i < min.length; i++) {
            args[38 + i] = String.format("%5d", (Object) ((int) Math.round(min[i])));
        }
        for (int i = 0; i < min_day.length; i++) {
            args[50 + i * 2] = String.format("%2d", (Object) min_day[i]);
            args[51 + i * 2] = String.format("%02d", (Object) min_hour[i]);
        }
        for (int i = 0; i < avg.length; i++) {
            args[74 + i] = String.format(Locale.ROOT, "%5.1f", (Object) avg[i]);
        }
        stats.append(String.format(table, args));
    }

    private void addSTATMonthlyVariableTable3(StringBuilder stats, String table, String tableName, String tableUnits, String maxAndMinVariableName, String maxAndMinVariableUnits) {
        float[] direct_max = new float[12];
        Arrays.fill(direct_max, Float.NEGATIVE_INFINITY);
        int[] direct_max_day = new int[12];

        float[] direct_avg = new float[12];
        float[] diffuse_avg = new float[12];
        float[] global_avg = new float[12];
        float[] count = new float[12];

        float direct_maximum = Float.NEGATIVE_INFINITY;
        int direct_maximum_month = 0;
        int direct_maximum_day = 0;

        // Hourly monthly average of the variable
        for (int i = 0; i < this.epw_data_fields.size(); i++) {
            int m = this.epw_data_fields.get(i).getN2_month() - 1;
            count[m]++;

            direct_avg[m] += this.epw_data_fields.get(i).getN14_direct_normal_radiation();
            diffuse_avg[m] += this.epw_data_fields.get(i).getN15_diffuse_horizontal_radiation();
            global_avg[m] += this.epw_data_fields.get(i).getN13_global_horizontal_radiation();

            if (direct_max[m] < this.epw_data_fields.get(i).getN14_direct_normal_radiation()) {
                direct_max[m] = this.epw_data_fields.get(i).getN14_direct_normal_radiation();
                direct_max_day[m] = this.epw_data_fields.get(i).getN3_day();
            }

            if (direct_maximum < this.epw_data_fields.get(i).getN14_direct_normal_radiation()) {
                direct_maximum = this.epw_data_fields.get(i).getN14_direct_normal_radiation();
                direct_maximum_month = this.epw_data_fields.get(i).getN2_month();
                direct_maximum_day = this.epw_data_fields.get(i).getN3_day();
            }
        }
        for (int i = 0; i < direct_avg.length; i++) {
            direct_avg[i] /= count[i];
            diffuse_avg[i] /= count[i];
            global_avg[i] /= count[i];
        }

        Object[] args = new Object[2 + (5 * 12) + 5];
        args[0] = tableName;
        args[1] = tableUnits;
        for (int i = 0; i < direct_avg.length; i++) {
            args[i + 2] = String.format("%5d", (Object) ((int) Math.round(direct_avg[i])));
        }
        for (int i = 0; i < direct_max.length; i++) {
            args[i + 14] = String.format("%5d", (Object) ((int) Math.round(direct_max[i])));
        }
        for (int i = 0; i < direct_max_day.length; i++) {
            args[i + 26] = String.format("%5d", (Object) direct_max_day[i]);
        }
        for (int i = 0; i < diffuse_avg.length; i++) {
            args[i + 38] = String.format("%5d", (Object) ((int) Math.round(diffuse_avg[i])));
        }
        for (int i = 0; i < global_avg.length; i++) {
            args[i + 50] = String.format("%5d", (Object) ((int) Math.round(global_avg[i])));
        }

        args[62] = maxAndMinVariableName;
        args[63] = String.format("%5d", (Object) ((int) direct_maximum));
        args[64] = maxAndMinVariableUnits;
        args[65] = Months.Abbreviation.values()[direct_maximum_month - 1];
        args[66] = String.format("%2d", (Object) direct_maximum_day);
        stats.append(String.format(table, args));
    }

    private void addSTATMonthlyVariableTable4(StringBuilder stats, String table, String tableName, String tableUnits) {
        int[] count_max_32 = new int[12];
        int[] count_max_0 = new int[12];
        int[] count_min_0 = new int[12];
        int[] count_min_18 = new int[12];
        // Hourly monthly average of the variable
        int count = 0;
        for (int m = 0; m < 12; m++) {
            int[] month_row_ids = Months.getMonthRowIds(Months.Abbreviation.values()[m]);
            for (int d = 0; d < month_row_ids[0] / 24; d++) {
                boolean max_32_go = false;
                int max_0_go_count = 0;
                boolean min_0_go = false;
                boolean min_18_go = false;
                for (int h = 0; h < 24; h++) {
                    if (this.epw_data_fields.get(count).getN6_dry_bulb_temperature() >= 32) {
                        max_32_go = true;
                    }
                    if (this.epw_data_fields.get(count).getN6_dry_bulb_temperature() <= 0) {
                        max_0_go_count++;
                    }
                    if (this.epw_data_fields.get(count).getN6_dry_bulb_temperature() <= 0) {
                        min_0_go = true;
                    }
                    if (this.epw_data_fields.get(count).getN6_dry_bulb_temperature() <= -18) {
                        min_18_go = true;
                    }
                    count++;
                }
                if (max_32_go) {
                    count_max_32[m]++;
                }
                if (max_0_go_count == 24) {
                    count_max_0[m]++;
                }
                if (min_0_go) {
                    count_min_0[m]++;
                }
                if (min_18_go) {
                    count_min_18[m]++;
                }
            }
        }

        Object[] args = new Object[12 * 4 + 2];
        args[0] = tableName;
        args[1] = tableUnits;
        for (int i = 0; i < count_max_32.length; i++) {
            String s = String.format("%d", (Object) count_max_32[i]);
            if (s.equals("0")) {
                s = "";
            }
            args[i + 2] = String.format("%5s", s);
        }
        for (int i = 0; i < count_max_0.length; i++) {
            String s = String.format("%d", (Object) count_max_0[i]);
            if (s.equals("0")) {
                s = "";
            }
            args[i + 14] = String.format("%5s", s);
        }
        for (int i = 0; i < count_min_0.length; i++) {
            String s = String.format("%d", (Object) count_min_0[i]);
            if (s.equals("0")) {
                s = "";
            }
            args[i + 26] = String.format("%5s", s);
        }
        for (int i = 0; i < count_min_18.length; i++) {
            String s = String.format("%d", (Object) count_min_18[i]);
            if (s.equals("0")) {
                s = "";
            }
            args[i + 38] = String.format("%5s", s);
        }
        stats.append(String.format(table, args));
    }

    private void addSTATMonthlyVariableTable5(StringBuilder stats, String table, String tableName, String tableUnits) {
        float[] total = new float[12];
        float[] max = new float[12];
        Arrays.fill(max, Integer.MIN_VALUE);

        for (int i = 0; i < this.epw_data_fields.size(); i++) {
            int m = this.epw_data_fields.get(i).getN2_month() - 1;
            total[m] += this.epw_data_fields.get(i).getN33_liquid_precipitation_depth();
            if (max[m] < this.epw_data_fields.get(i).getN33_liquid_precipitation_depth()) {
                max[m] = this.epw_data_fields.get(i).getN33_liquid_precipitation_depth();
            }
        }

        Object[] args = new Object[2 + (12 * 2)];
        args[0] = tableName;
        args[1] = tableUnits;
        for (int i = 0; i < total.length; i++) {
            args[i + 2] = String.format("%5d", (Object) ((int) Math.round(total[i])));
        }
        for (int i = 0; i < max.length; i++) {
            args[i + 14] = String.format("%5d", (Object) ((int) Math.round(max[i])));
        }
        stats.append(String.format(table, args));
    }

    private void addSTATMonthlyVariableTable6(StringBuilder stats, String table, String tableName) {
        float[] average = new float[12];
        float[] count = new float[12];

        for (int i = 0; i < this.epw_data_fields.size(); i++) {
            int m = this.epw_data_fields.get(i).getN2_month() - 1;
            average[m] += this.epw_data_fields.get(i).getN32_albedo();
            count[m]++;
        }
        for (int i = 0; i < average.length; i++) {
            average[i] /= count[i];
        }

        Object[] args = new Object[1 + 12];
        args[0] = tableName;
        for (int i = 0; i < average.length; i++) {
            args[i + 1] = String.format(Locale.ROOT, "%5.3f", (Object) average[i]);
        }
        stats.append(String.format(table, args));
    }

    private void addSTATMonthlyVariableTable7(StringBuilder stats, String table) {
        float[] hdd10C = new float[12];
        float[] hdd18C = new float[12];
        float[] cdd10C = new float[12];
        float[] cdd18C = new float[12];
        float[] cdh20C = new float[12];
        float[] cdh23C = new float[12];
        float[] cdh27C = new float[12];

        float acdd10C = 0;
        float ahdd10C = 0;
        float acdd18C = 0;
        float ahdd18C = 0;

        for (int i = 0; i < this.epw_data_fields.size(); i++) {
            int m = this.epw_data_fields.get(i).getN2_month() - 1;
            if (this.epw_data_fields.get(i).getN6_dry_bulb_temperature() < 10) {
                hdd10C[m] += 10 - this.epw_data_fields.get(i).getN6_dry_bulb_temperature();
                ahdd10C += 10 - this.epw_data_fields.get(i).getN6_dry_bulb_temperature();
            }
            if (this.epw_data_fields.get(i).getN6_dry_bulb_temperature() < 18) {
                hdd18C[m] += 18 - this.epw_data_fields.get(i).getN6_dry_bulb_temperature();
                ahdd18C += 18 - this.epw_data_fields.get(i).getN6_dry_bulb_temperature();
            }

            if (this.epw_data_fields.get(i).getN6_dry_bulb_temperature() > 10) {
                cdd10C[m] += this.epw_data_fields.get(i).getN6_dry_bulb_temperature() - 10;
                acdd10C += this.epw_data_fields.get(i).getN6_dry_bulb_temperature() - 10;
            }
            if (this.epw_data_fields.get(i).getN6_dry_bulb_temperature() > 18) {
                cdd18C[m] += this.epw_data_fields.get(i).getN6_dry_bulb_temperature() - 18;
                acdd18C += this.epw_data_fields.get(i).getN6_dry_bulb_temperature() - 18;
            }

            if (this.epw_data_fields.get(i).getN6_dry_bulb_temperature() > 20) {
                cdh20C[m] += this.epw_data_fields.get(i).getN6_dry_bulb_temperature() - 20;
            }
            if (this.epw_data_fields.get(i).getN6_dry_bulb_temperature() > 23) {
                cdh23C[m] += this.epw_data_fields.get(i).getN6_dry_bulb_temperature() - 23;
            }
            if (this.epw_data_fields.get(i).getN6_dry_bulb_temperature() > 27) {
                cdh27C[m] += this.epw_data_fields.get(i).getN6_dry_bulb_temperature() - 27;
            }
        }

        Object[] args = new Object[12 * 7 + 4];
        for (int i = 0; i < hdd10C.length; i++) {
            args[i] = String.format("%5d", (Object) (int) Math.round(hdd10C[i] / 24f));
        }
        for (int i = 0; i < hdd18C.length; i++) {
            args[12 + i] = String.format("%5d", (Object) (int) Math.round(hdd18C[i] / 24f));
        }
        for (int i = 0; i < cdd10C.length; i++) {
            args[24 + i] = String.format("%5d", (Object) (int) Math.round(cdd10C[i] / 24f));
        }
        for (int i = 0; i < cdd18C.length; i++) {
            args[36 + i] = String.format("%5d", (Object) (int) Math.round(cdd18C[i] / 24f));
        }
        for (int i = 0; i < cdh20C.length; i++) {
            args[48 + i] = String.format("%5d", (Object) (int) Math.round(cdh20C[i]));
        }
        for (int i = 0; i < cdh23C.length; i++) {
            args[60 + i] = String.format("%5d", (Object) (int) Math.round(cdh23C[i]));
        }
        for (int i = 0; i < cdh27C.length; i++) {
            args[72 + i] = String.format("%5d", (Object) (int) Math.round(cdh27C[i]));
        }

        args[84] = String.format("%5d", (Object) (int) Math.round(acdd10C / 24f));
        args[85] = String.format("%5d", (Object) (int) Math.round(ahdd10C / 24f));
        args[86] = String.format("%5d", (Object) (int) Math.round(acdd18C / 24f));
        args[87] = String.format("%5d", (Object) (int) Math.round(ahdd18C / 24f));

        stats.append(String.format(table, args));
    }

    private void addSTATMonthlyVariableTable9(StringBuilder stats, String table) {
        Object[] args = new Object[48];
        // - Summer
        String[] summer_extreme = Typical_Extreme_Periods.getWeekDays(this, true, "Summer");
        args[0] = summer_extreme[2];
        args[1] = summer_extreme[3];
        args[2] = summer_extreme[4];
        args[3] = summer_extreme[5];
        args[4] = summer_extreme[6];
        args[5] = summer_extreme[7];
        args[6] = summer_extreme[8];
        args[7] = summer_extreme[9];
        args[8] = summer_extreme[10];
        String[] summer_typical = Typical_Extreme_Periods.getWeekDays(this, false, "Summer");
        args[9] = summer_typical[4];
        args[10] = summer_typical[5];
        args[11] = summer_typical[7];
        args[12] = summer_typical[8];
        args[13] = summer_typical[9];
        args[14] = summer_typical[10];

        // - Winter
        String[] winter_extreme = Typical_Extreme_Periods.getWeekDays(this, true, "Winter");
        args[15] = winter_extreme[2];
        args[16] = winter_extreme[3];
        args[17] = winter_extreme[4];
        args[18] = winter_extreme[5];
        args[19] = winter_extreme[6];
        args[20] = winter_extreme[7];
        args[21] = winter_extreme[8];
        args[22] = winter_extreme[9];
        args[23] = winter_extreme[10];
        String[] winter_typical = Typical_Extreme_Periods.getWeekDays(this, false, "Winter");
        args[24] = winter_typical[4];
        args[25] = winter_typical[5];
        args[26] = winter_typical[7];
        args[27] = winter_typical[8];
        args[28] = winter_typical[9];
        args[29] = winter_typical[10];

        // - Autumn
        String[] autumn_typical = Typical_Extreme_Periods.getWeekDays(this, false, "Autumn");
        args[30] = autumn_typical[2];
        args[31] = autumn_typical[3];
        args[32] = autumn_typical[4];
        args[33] = autumn_typical[5];
        args[34] = autumn_typical[6];
        args[35] = autumn_typical[7];
        args[36] = autumn_typical[8];
        args[37] = autumn_typical[9];
        args[38] = autumn_typical[10];

        // - Spring
        String[] spring_typical = Typical_Extreme_Periods.getWeekDays(this, false, "Spring");
        args[39] = spring_typical[2];
        args[40] = spring_typical[3];
        args[41] = spring_typical[4];
        args[42] = spring_typical[5];
        args[43] = spring_typical[6];
        args[44] = spring_typical[7];
        args[45] = spring_typical[8];
        args[46] = spring_typical[9];
        args[47] = spring_typical[10];

        stats.append(String.format(table, args));
    }

    private void addSTATMonthlyVariableTable8(StringBuilder stats, String table, String tableName, String tableUnits) {
        float[] n = new float[12];
        float[] ene1 = new float[12];
        float[] ne = new float[12];
        float[] ene2 = new float[12];
        float[] e = new float[12];
        float[] ese = new float[12];
        float[] se = new float[12];
        float[] sse = new float[12];
        float[] s = new float[12];
        float[] ssw = new float[12];
        float[] sw = new float[12];
        float[] wsw = new float[12];
        float[] w = new float[12];
        float[] wnw = new float[12];
        float[] nw = new float[12];
        float[] nnw = new float[12];

        for (int i = 0; i < this.epw_data_fields.size(); i++) {
            int m = this.epw_data_fields.get(i).getN2_month() - 1;
            float wd = this.epw_data_fields.get(i).getN20_wind_direction();

            n[m] = wd >= 348.75 || wd < 11.25 ? n[m] + 1 : n[m];
            ene1[m] = wd >= 11.25 && wd < 33.75 ? ene1[m] + 1 : ene1[m];
            ne[m] = wd >= 33.75 && wd < 56.25 ? ne[m] + 1 : ne[m];
            ene2[m] = wd >= 56.25 && wd < 78.75 ? ene2[m] + 1 : ene2[m];
            e[m] = wd >= 78.75 && wd < 101.25 ? e[m] + 1 : e[m];
            ese[m] = wd >= 101.25 && wd < 123.75 ? ese[m] + 1 : ese[m];
            se[m] = wd >= 123.75 && wd < 146.25 ? se[m] + 1 : se[m];
            sse[m] = wd >= 146.25 && wd < 168.75 ? sse[m] + 1 : sse[m];
            s[m] = wd >= 168.75 && wd < 191.25 ? s[m] + 1 : s[m];
            ssw[m] = wd >= 191.25 && wd < 213.75 ? ssw[m] + 1 : ssw[m];
            sw[m] = wd >= 213.75 && wd < 236.25 ? sw[m] + 1 : sw[m];
            wsw[m] = wd >= 236.25 && wd < 258.75 ? wsw[m] + 1 : wsw[m];
            w[m] = wd >= 258.75 && wd < 281.25 ? w[m] + 1 : w[m];
            wnw[m] = wd >= 281.25 && wd < 303.75 ? wnw[m] + 1 : wnw[m];
            nw[m] = wd >= 303.75 && wd < 326.25 ? nw[m] + 1 : nw[m];
            nnw[m] = wd >= 326.25 && wd < 348.75 ? nnw[m] + 1 : nnw[m];
        }

        Object[] args = new Object[2 + (12 * 16)];
        args[0] = tableName;
        args[1] = tableUnits;
        for (int m = 0; m < 12; m++) {
            int[] rows = Months.getMonthRowIds(Months.Abbreviation.values()[m]);
            n[m] /= (float) rows[0];
            ene1[m] /= (float) rows[0];
            ne[m] /= (float) rows[0];
            ene2[m] /= (float) rows[0];
            e[m] /= (float) rows[0];
            ese[m] /= (float) rows[0];
            se[m] /= (float) rows[0];
            sse[m] /= (float) rows[0];
            s[m] /= (float) rows[0];
            ssw[m] /= (float) rows[0];
            sw[m] /= (float) rows[0];
            wsw[m] /= (float) rows[0];
            w[m] /= (float) rows[0];
            wnw[m] /= (float) rows[0];
            nw[m] /= (float) rows[0];
            nnw[m] /= (float) rows[0];

            args[2 + m] = String.format("%5d", (int) Math.round(n[m] * 100f));
            args[14 + m] = String.format("%5d", (int) Math.round(ene1[m] * 100f));
            args[26 + m] = String.format("%5d", (int) Math.round(ne[m] * 100f));
            args[38 + m] = String.format("%5d", (int) Math.round(ene2[m] * 100f));
            args[50 + m] = String.format("%5d", (int) Math.round(e[m] * 100f));
            args[62 + m] = String.format("%5d", (int) Math.round(ese[m] * 100f));
            args[74 + m] = String.format("%5d", (int) Math.round(se[m] * 100f));
            args[86 + m] = String.format("%5d", (int) Math.round(sse[m] * 100f));
            args[98 + m] = String.format("%5d", (int) Math.round(s[m] * 100f));
            args[110 + m] = String.format("%5d", (int) Math.round(ssw[m] * 100f));
            args[122 + m] = String.format("%5d", (int) Math.round(sw[m] * 100f));
            args[134 + m] = String.format("%5d", (int) Math.round(wsw[m] * 100f));
            args[146 + m] = String.format("%5d", (int) Math.round(w[m] * 100f));
            args[158 + m] = String.format("%5d", (int) Math.round(wnw[m] * 100f));
            args[170 + m] = String.format("%5d", (int) Math.round(nw[m] * 100f));
            args[182 + m] = String.format("%5d", (int) Math.round(nnw[m] * 100f));
        }
        stats.append(String.format(table, args));
    }

    private void addSTATWindFrequencyTable(StringBuilder stats, String table, String tableName, String tableUnits) {
        float[] n = new float[12];
        float[] ene1 = new float[12];
        float[] ne = new float[12];
        float[] ene2 = new float[12];
        float[] e = new float[12];
        float[] ese = new float[12];
        float[] se = new float[12];
        float[] sse = new float[12];
        float[] s = new float[12];
        float[] ssw = new float[12];
        float[] sw = new float[12];
        float[] wsw = new float[12];
        float[] w = new float[12];
        float[] wnw = new float[12];
        float[] nw = new float[12];
        float[] nnw = new float[12];
        float[] t = new float[12];

        for (int i = 0; i < this.epw_data_fields.size(); i++) {
            float wd = this.epw_data_fields.get(i).getN20_wind_direction();
            float ws = this.epw_data_fields.get(i).getN21_wind_speed();
            int m;

            if (ws <= 0.5) {
                m = 0;
            } else if (ws <= 2) {
                m = 1;
            } else if (ws <= 4) {
                m = 2;
            } else if (ws <= 6) {
                m = 3;
            } else if (ws <= 8) {
                m = 4;
            } else if (ws <= 10) {
                m = 5;
            } else if (ws <= 12) {
                m = 6;
            } else if (ws <= 14) {
                m = 7;
            } else if (ws <= 16) {
                m = 8;
            } else if (ws <= 18) {
                m = 9;
            } else {
                m = 10;
            }
            n[m] = wd >= 348.75 || wd < 11.25 ? n[m] + 1 : n[m];
            ene1[m] = wd >= 11.25 && wd < 33.75 ? ene1[m] + 1 : ene1[m];
            ne[m] = wd >= 33.75 && wd < 56.25 ? ne[m] + 1 : ne[m];
            ene2[m] = wd >= 56.25 && wd < 78.75 ? ene2[m] + 1 : ene2[m];
            e[m] = wd >= 78.75 && wd < 101.25 ? e[m] + 1 : e[m];
            ese[m] = wd >= 101.25 && wd < 123.75 ? ese[m] + 1 : ese[m];
            se[m] = wd >= 123.75 && wd < 146.25 ? se[m] + 1 : se[m];
            sse[m] = wd >= 146.25 && wd < 168.75 ? sse[m] + 1 : sse[m];
            s[m] = wd >= 168.75 && wd < 191.25 ? s[m] + 1 : s[m];
            ssw[m] = wd >= 191.25 && wd < 213.75 ? ssw[m] + 1 : ssw[m];
            sw[m] = wd >= 213.75 && wd < 236.25 ? sw[m] + 1 : sw[m];
            wsw[m] = wd >= 236.25 && wd < 258.75 ? wsw[m] + 1 : wsw[m];
            w[m] = wd >= 258.75 && wd < 281.25 ? w[m] + 1 : w[m];
            wnw[m] = wd >= 281.25 && wd < 303.75 ? wnw[m] + 1 : wnw[m];
            nw[m] = wd >= 303.75 && wd < 326.25 ? nw[m] + 1 : nw[m];
            nnw[m] = wd >= 326.25 && wd < 348.75 ? nnw[m] + 1 : nnw[m];
            t[m]++;

            m = 11;
            n[m] = wd >= 348.75 || wd < 11.25 ? n[m] + 1 : n[m];
            ene1[m] = wd >= 11.25 && wd < 33.75 ? ene1[m] + 1 : ene1[m];
            ne[m] = wd >= 33.75 && wd < 56.25 ? ne[m] + 1 : ne[m];
            ene2[m] = wd >= 56.25 && wd < 78.75 ? ene2[m] + 1 : ene2[m];
            e[m] = wd >= 78.75 && wd < 101.25 ? e[m] + 1 : e[m];
            ese[m] = wd >= 101.25 && wd < 123.75 ? ese[m] + 1 : ese[m];
            se[m] = wd >= 123.75 && wd < 146.25 ? se[m] + 1 : se[m];
            sse[m] = wd >= 146.25 && wd < 168.75 ? sse[m] + 1 : sse[m];
            s[m] = wd >= 168.75 && wd < 191.25 ? s[m] + 1 : s[m];
            ssw[m] = wd >= 191.25 && wd < 213.75 ? ssw[m] + 1 : ssw[m];
            sw[m] = wd >= 213.75 && wd < 236.25 ? sw[m] + 1 : sw[m];
            wsw[m] = wd >= 236.25 && wd < 258.75 ? wsw[m] + 1 : wsw[m];
            w[m] = wd >= 258.75 && wd < 281.25 ? w[m] + 1 : w[m];
            wnw[m] = wd >= 281.25 && wd < 303.75 ? wnw[m] + 1 : wnw[m];
            nw[m] = wd >= 303.75 && wd < 326.25 ? nw[m] + 1 : nw[m];
            nnw[m] = wd >= 326.25 && wd < 348.75 ? nnw[m] + 1 : nnw[m];
            t[m]++;

        }

        Object[] args = new Object[2 + (12 * 17)];
        args[0] = tableName;
        args[1] = tableUnits;
        for (int m = 0; m < 12; m++) {
            n[m] /= 8760f;
            ene1[m] /= 8760f;
            ne[m] /= 8760f;
            ene2[m] /= 8760f;
            e[m] /= 8760f;
            ese[m] /= 8760f;
            se[m] /= 8760f;
            sse[m] /= 8760f;
            s[m] /= 8760f;
            ssw[m] /= 8760f;
            sw[m] /= 8760f;
            wsw[m] /= 8760f;
            w[m] /= 8760f;
            wnw[m] /= 8760f;
            nw[m] /= 8760f;
            nnw[m] /= 8760f;
            t[m] /= 8760f;

            args[2 + m] = String.format(Locale.ROOT, "%5.1f", n[m] * 100f);
            args[14 + m] = String.format(Locale.ROOT, "%5.1f", ene1[m] * 100f);
            args[26 + m] = String.format(Locale.ROOT, "%5.1f", ne[m] * 100f);
            args[38 + m] = String.format(Locale.ROOT, "%5.1f", ene2[m] * 100f);
            args[50 + m] = String.format(Locale.ROOT, "%5.1f", e[m] * 100f);
            args[62 + m] = String.format(Locale.ROOT, "%5.1f", ese[m] * 100f);
            args[74 + m] = String.format(Locale.ROOT, "%5.1f", se[m] * 100f);
            args[86 + m] = String.format(Locale.ROOT, "%5.1f", sse[m] * 100f);
            args[98 + m] = String.format(Locale.ROOT, "%5.1f", s[m] * 100f);
            args[110 + m] = String.format(Locale.ROOT, "%5.1f", ssw[m] * 100f);
            args[122 + m] = String.format(Locale.ROOT, "%5.1f", sw[m] * 100f);
            args[134 + m] = String.format(Locale.ROOT, "%5.1f", wsw[m] * 100f);
            args[146 + m] = String.format(Locale.ROOT, "%5.1f", w[m] * 100f);
            args[158 + m] = String.format(Locale.ROOT, "%5.1f", wnw[m] * 100f);
            args[170 + m] = String.format(Locale.ROOT, "%5.1f", nw[m] * 100f);
            args[182 + m] = String.format(Locale.ROOT, "%5.1f", nnw[m] * 100f);
            args[194 + m] = String.format(Locale.ROOT, "%5.1f", t[m] * 100f);
        }

        stats.append(String.format(table, args));
    }

    private void addSTATVariableTable(StringBuilder stats, boolean isFloat, String table, String tableName, String tableUnits, String variableName) {
        float[][] val = new float[12][24];
        float[][] count = new float[12][24];

        // Hourly monthly average of the variable
        for (int i = 0; i < this.epw_data_fields.size(); i++) {
            int m = this.epw_data_fields.get(i).getN2_month() - 1;
            int h = this.epw_data_fields.get(i).getN4_hour() - 1;
            count[m][h]++;
            switch (variableName) {
                case "N6_dry_bulb_temperature":
                    val[m][h] += this.epw_data_fields.get(i).getN6_dry_bulb_temperature();
                    break;
                case "N7_dew_point_temperature":
                    val[m][h] += this.epw_data_fields.get(i).getN7_dew_point_temperature();
                    break;
                case "N8_relative_humidity":
                    val[m][h] += this.epw_data_fields.get(i).getN8_relative_humidity();
                    break;
                case "N21_wind_speed":
                    val[m][h] += this.epw_data_fields.get(i).getN21_wind_speed();
                    break;
                case "N20_wind_direction":
                    val[m][h] += this.epw_data_fields.get(i).getN20_wind_direction();
                    break;
                case "N14_direct_normal_radiation":
                    val[m][h] += this.epw_data_fields.get(i).getN14_direct_normal_radiation();
                    break;
                case "N15_diffuse_horizontal_radiation":
                    val[m][h] += this.epw_data_fields.get(i).getN15_diffuse_horizontal_radiation();
                    break;
                case "N13_global_horizontal_radiation":
                    val[m][h] += this.epw_data_fields.get(i).getN13_global_horizontal_radiation();
                    break;
                case "N22_total_sky_cover":
                    val[m][h] += this.epw_data_fields.get(i).getN22_total_sky_cover() * 10f;
                    break;
                case "N23_opaque_sky_cover":
                    val[m][h] += this.epw_data_fields.get(i).getN23_opaque_sky_cover() * 10f;
                    break;
                default:
                    throw new UnsupportedOperationException("** ERROR ** Unknown variable.");
            }
        }
        float[] max = new float[12];
        Arrays.fill(max, Float.NEGATIVE_INFINITY);
        float[] min = new float[12];
        Arrays.fill(min, Float.POSITIVE_INFINITY);
        int[] max_id = new int[12];
        int[] min_id = new int[12];
        for (int m = 0; m < val.length; m++) {
            for (int h = 0; h < val[m].length; h++) {
                val[m][h] /= count[m][h];
                if (val[m][h] > max[m]) {
                    max[m] = val[m][h];
                    max_id[m] = h;
                }
                if (val[m][h] < min[m]) {
                    min[m] = val[m][h];
                    min_id[m] = h;
                }
            }
        }

        Object[] args = new Object[2 + (12 * 24) + (12 * 2)];
        args[0] = tableName;
        args[1] = tableUnits;
        for (int h = 0; h < val[0].length; h++) {
            for (int m = 0; m < val.length; m++) {
                if (isFloat) {
                    args[((h * 12) + m) + 2] = String.format(Locale.ROOT, "%5.1f", (Object) val[m][h]);
                } else {
                    args[((h * 12) + m) + 2] = String.format("%5d", (Object) ((int) Math.round(val[m][h])));
                }
            }
        }
        for (int i = args.length - 24; i < args.length - 12; i++) {
            args[i] = String.format("%5d", (Object) (max_id[i - (args.length - 24)] + 1));
        }
        for (int i = args.length - 12; i < args.length; i++) {
            args[i] = String.format("%5d", (Object) (min_id[i - (args.length - 12)] + 1));
        }

        stats.append(String.format(table, args));
    }

    private void addComparisonLine(StringBuilder comparison, String name, String unit, String description, double[] thisEPWaverageVariable, double[] otherEPWAverageVariable) {
        Object[] args = new Object[]{
            name,
            unit,
            description,
            thisEPWaverageVariable[0] - otherEPWAverageVariable[0],
            thisEPWaverageVariable[1] - otherEPWAverageVariable[1],
            thisEPWaverageVariable[2] - otherEPWAverageVariable[2],
            thisEPWaverageVariable[3] - otherEPWAverageVariable[3],
            thisEPWaverageVariable[4] - otherEPWAverageVariable[4],
            thisEPWaverageVariable[5] - otherEPWAverageVariable[5],
            thisEPWaverageVariable[6] - otherEPWAverageVariable[6],
            thisEPWaverageVariable[7] - otherEPWAverageVariable[7],
            thisEPWaverageVariable[8] - otherEPWAverageVariable[8],
            thisEPWaverageVariable[9] - otherEPWAverageVariable[9],
            thisEPWaverageVariable[10] - otherEPWAverageVariable[10],
            thisEPWaverageVariable[11] - otherEPWAverageVariable[11]
        };
        comparison.append(String.format(Locale.ROOT, "%s,%s,%s,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f%n", args));
    }

    /**
     * Saves the EPW object as a EPW file (.epw) to the given path.
     *
     * @param printOutputs String buffer
     * @param errorsAndWarnings String buffer for errors and warnings
     * @param path_output path to the file
     */
    public void saveEPW(StringBuffer printOutputs, StringBuffer errorsAndWarnings, String path_output) {
        printOutputs.append(String.format(Locale.ROOT, "%n\tSaving morphed EPW file...%n\t")).append(path_output).append(String.format(Locale.ROOT, "%n"));
        try (OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(path_output), StandardCharsets.UTF_8)) {
            writer.write(this.getString(errorsAndWarnings));
            writer.close();
        } catch (FileNotFoundException ex) {
            String err = " ** ERROR ** File not found: %s %n";
            errorsAndWarnings.append(err.formatted(Locale.ROOT, ex));
            System.err.print(err);
        } catch (IOException ex) {
            String err = " ** ERROR ** Cannot access the file: %s %n";
            errorsAndWarnings.append(err.formatted(Locale.ROOT, ex));
            System.err.print(err);
        }
    }

    public void determineHourAdjustment(EPW dummyEPW) {
        if(SolarHourAdjustment.None != this.solarHourAdjustmentOption) {
            float latitude = this.getEpw_location().getN2_latitude();
            float longitude = this.getEpw_location().getN3_longitude();
            float time_zone = this.getEpw_location().getN4_time_zone();
            float step = (1f / 60f); // minute by minute.
            
            
            for (int i = 0; i < this.solarHourAdjustment.length; i++) {
                int row_id = i * 24;
                float[] referenceHourlyAverages;
                switch(this.solarHourAdjustmentOption) {
                    default:
                    case By_Month:
                        referenceHourlyAverages = getN14DirectNormalRadiationMonth(this, i);
                        break;
                    case By_Day:
                        referenceHourlyAverages = getN14DirectNormalRadiationDay(this, row_id);
                        break;
                }
                
                float determinedSolarHourAdjustment = 3f; // 0 means one less hour, 2 means one more hour, 1 no change.
                dummyEPW.solarHourAdjustment[i] = determinedSolarHourAdjustment;
                float minDeviation = Float.MAX_VALUE;
                
                if (countNumberOfHoursWithDirectNormalRadiation(referenceHourlyAverages) > 0) {
                    for (int s = 1; s <= 240; s++) {
                        dummyEPW.solarHourAdjustment[i] -= step;
                        float[] targetHourlyAverages;
                        switch(this.solarHourAdjustmentOption) {
                            default:
                            case By_Month:
                                N10_Extraterrestrial_Horizontal_Radiation.calculate(dummyEPW, i, longitude, latitude, time_zone);
                                N15_Diffuse_Horizontal_Radiation.calculate(dummyEPW, i, longitude, latitude, time_zone);
                                N14_Direct_Normal_Radiation.calculate(dummyEPW, i, longitude, latitude, time_zone);
                                targetHourlyAverages = getN14DirectNormalRadiationMonth(dummyEPW, i);
                                break;
                            case By_Day:
                                N10_Extraterrestrial_Horizontal_Radiation.calculate(dummyEPW, i, longitude, latitude, time_zone, row_id);
                                N15_Diffuse_Horizontal_Radiation.calculate(dummyEPW, i, longitude, latitude, time_zone, row_id);
                                N14_Direct_Normal_Radiation.calculate(dummyEPW, i, longitude, latitude, time_zone, row_id);
                                targetHourlyAverages = getN14DirectNormalRadiationDay(dummyEPW, row_id);
                                break;
                        }
                        float deviation = getMSE(referenceHourlyAverages, targetHourlyAverages);
                        if (deviation < minDeviation) {
                            minDeviation = deviation;
                            determinedSolarHourAdjustment = dummyEPW.solarHourAdjustment[i];
                        }
                    }
                } else {
                    determinedSolarHourAdjustment = 1f;
                }
                this.solarHourAdjustment[i] = determinedSolarHourAdjustment;
            }

            if(this.solarHourAdjustmentOption == SolarHourAdjustment.By_Day) {
                // Adjust Global Horizontal Radiation to match solar hour.
                // Only for daily adjustment.
                fixN13GlobalHorizontalRadiation(this);
            }
            
        
            // Print solar adjustments.
            for (int i = 0; i < this.solarHourAdjustment.length; i++) {
                String msg;
                String dayOrMonth;
                switch(this.solarHourAdjustmentOption) {
                    default:
                    case By_Month:
                        msg = " ** WARNING ** EPW Solar hour adjustment for the month of %s: %d [min] and N13 (GHR) shift: %d [h].%n";
                        dayOrMonth = Months.Abbreviation.values()[i].name();
                        break;
                    case By_Day:
                        msg = " ** WARNING ** EPW Solar hour adjustment for the Julian day %s: %d [min] and N13 (GHR) shift: %d [h].%n";
                        dayOrMonth = (i + 1) + "";
                        break;
                }
                String warning = String.format(Locale.ROOT, 
                        msg,
                        dayOrMonth,
                        Math.round((this.solarHourAdjustment[i] - 1f) * 60f),
                        this.numberOfHoursOfAdjustmentOfGlobalHorizontalRadiation[i]);
                System.out.print(warning);
                this.adjustmentWanings.append(warning);
            }
        }
    }

    private static void fixN13GlobalHorizontalRadiation(EPW epw) {
        for (int julian_day = 0; julian_day < 365; julian_day++) {
            // Forward.
            fixForward(epw, julian_day, 2.5f, 1f);
            fixForward(epw, julian_day, 2f, 1f);
            fixForward(epw, julian_day, 1.5f + 0.08f, 1f);

            // Backward.
            fixBackward(epw, julian_day, -0.5f, 1f);
            fixBackward(epw, julian_day, 0f, 1f);
            fixBackward(epw, julian_day, 0.5f - 0.08f, 1f);
        }
    }

    private static void fixForward(EPW epw, int julian_day, float adjustmentThreshold, float deviation) {
        if (epw.solarHourAdjustment[julian_day] >= adjustmentThreshold) {
            int row_id = julian_day * 24;
            float[] values = new float[24];
            for (int hour = 0; hour < 24; hour++) {
                values[hour] = epw.getEpw_data_fields().get(row_id).getN13_global_horizontal_radiation();
                row_id++;
            }
            row_id = julian_day * 24;
            for (int hour = 0; hour < 24; hour++) {
                if (hour == 23) {
                    epw.getEpw_data_fields().get(row_id).setN13_global_horizontal_radiation(values[0]);
                } else {
                    epw.getEpw_data_fields().get(row_id).setN13_global_horizontal_radiation(values[hour + 1]);
                }
                row_id++;
            }
            epw.numberOfHoursOfAdjustmentOfGlobalHorizontalRadiation[julian_day] += 1;
            epw.solarHourAdjustment[julian_day] -= deviation;
        }
    }
    
    private static void fixBackward(EPW epw, int julian_day, float adjustmentThreshold, float deviation) {
        if (epw.solarHourAdjustment[julian_day] <= adjustmentThreshold) {
            int row_id = julian_day * 24;
            float[] values = new float[24];
            for (int hour = 0; hour < 24; hour++) {
                values[hour] = epw.getEpw_data_fields().get(row_id).getN13_global_horizontal_radiation();
                row_id++;
            }
            row_id = julian_day * 24;
            for (int hour = 0; hour < 24; hour++) {
                if (hour == 0) {
                    epw.getEpw_data_fields().get(row_id).setN13_global_horizontal_radiation(values[23]);
                } else {
                    epw.getEpw_data_fields().get(row_id).setN13_global_horizontal_radiation(values[hour - 1]);
                }
                row_id++;
            }
            epw.numberOfHoursOfAdjustmentOfGlobalHorizontalRadiation[julian_day] -= 1;
            epw.solarHourAdjustment[julian_day] += deviation;
        }
    }

    private static int countNumberOfHoursWithDirectNormalRadiation(float[] reference) {
        int count = 0;
        for (int hour = 0; hour < 24; hour++) {
            if (reference[hour] > 0) {
                count++;
            }
        }
        return count;
    }

    private static float[] getN14DirectNormalRadiationDay(EPW epw, int row_id) {
        float[] hourlyValues = new float[24];
        for (int hour = 0; hour < 24; hour++) {
            hourlyValues[hour] = epw.getEpw_data_fields().get(row_id + hour).getN14_direct_normal_radiation();
        }
        return hourlyValues;
    }
    
    private static float[] getN14DirectNormalRadiationMonth(EPW epw, int month) {
        float[] hourlyAverages = new float[24];
        int[] month_row_ids = Months.getMonthRowIds(Months.Abbreviation.values()[month]);
        float number_of_days = month_row_ids[0] / 24;
        int first_row_id = month_row_ids[1];
        int last_row_id = month_row_ids[2];
        for (int row_id = first_row_id; row_id < last_row_id; row_id++) {
            hourlyAverages[row_id % 24] += epw.getEpw_data_fields().get(row_id).getN14_direct_normal_radiation();
        }
        for (int i = 0; i < 24; i++) {
            hourlyAverages[i] /= number_of_days;
        }
        return hourlyAverages;
    }
    
    private static float getMSE(float[] reference, float[] target) {
        float deviation = 0f;
        for (int hour = 0; hour < 24; hour++) {
            deviation += Math.pow(target[hour] - reference[hour], 2);
        }
        return deviation / 24f;
    }

    public SolarHourAdjustment getSolarHourAdjustmentOption() {
        return solarHourAdjustmentOption;
    }

    public DiffuseIrradiationModel getDiffuseIrradiationModel() {
        return diffuseIrradiationModel;
    }
    
}
