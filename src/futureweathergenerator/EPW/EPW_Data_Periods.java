/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.EPW;

import java.util.Locale;

/**
 * EPW data periods object.
 *
 * @author eugenio
 */
public class EPW_Data_Periods {

    /**
     * EPW object id string.
     */
    public static final String ID = "DATA PERIODS";

    private int N1_number_of_data_periods;
    private Data_Period[] An_data_periods;

    /**
     * Initiates the EPW data periods object.
     *
     * @param N1_number_of_data_periods number of data periods
     * @param An_data_periods array o data periods
     */
    public EPW_Data_Periods(int N1_number_of_data_periods, Data_Period[] An_data_periods) {
        this.N1_number_of_data_periods = N1_number_of_data_periods;
        this.An_data_periods = An_data_periods;
    }

    /**
     * Initiates the EPW data periods object from array of strings.
     *
     * @param args array of strings
     */
    public EPW_Data_Periods(String[] args) {
        if(args.length > 2) {
            // args[0] is the object id
            this.N1_number_of_data_periods = Integer.parseInt(args[1]);
            if(this.N1_number_of_data_periods * 5 + 2 == args.length) {
                this.An_data_periods = new Data_Period[N1_number_of_data_periods];
                for (int i = 2; i < args.length; i += 5) {
                    this.An_data_periods[(i - 2) / 5] = new Data_Period(
                            Integer.parseInt(args[i]),
                            args[i + 1],
                            args[i + 2],
                            args[i + 3],
                            args[i + 4]);
                }
            } else {
                this.N1_number_of_data_periods = -1;
                this.An_data_periods = new Data_Period[0];
            }
        } else {
            this.N1_number_of_data_periods = -1;
            this.An_data_periods = new Data_Period[0];
        }
    }
    
    /**
     * Checks the integrity of the object.
     *
     * @return true if passes the test
     */
    public boolean checkIntegrity() {
        if (this.N1_number_of_data_periods == -1) {
            return false;
        }
        return true;
    }
    

    /**
     * Returns this data periods object as a string.
     *
     * @return string
     */
    public String getString() {
        String line = "" + this.N1_number_of_data_periods;
        for (Data_Period s : this.An_data_periods) {
            line += "," + String.format("%d", s.getN2_number_of_records_per_hour()) + ","
                    + String.format("%s", s.getA1_description()) + ","
                    + String.format("%s", s.getA2_start_day_of_week()) + ","
                    + String.format("%s", s.getA3_start_day()) + ","
                    + String.format("%s", s.getA4_end_day());
        }
        return String.format(Locale.ROOT, ID + "," + line + "%n");
    }

    /**
     * Returns the number of data periods.
     *
     * @return number of data periods
     */
    public int getN1_number_of_data_periods() {
        return N1_number_of_data_periods;
    }

    /**
     * Returns array of data periods.
     *
     * @return array of data periods
     */
    public Data_Period[] getAn_data_periods() {
        return An_data_periods;
    }

    /**
     * Number of Data Periods.
     *
     * @param N1_number_of_data_periods number
     */
    public void setN1_number_of_data_periods(int N1_number_of_data_periods) {
        this.N1_number_of_data_periods = N1_number_of_data_periods;
    }

    /**
     * List of data periods.
     *
     * @param An_data_periods list
     */
    public void setAn_data_periods(Data_Period[] An_data_periods) {
        this.An_data_periods = An_data_periods;
    }

    /**
     * Data Period object.
     */
    public class Data_Period {

        private int N2_number_of_records_per_hour;
        private String A1_description;
        private String A2_start_day_of_week;
        private String A3_start_day;
        private String A4_end_day;

        /**
         * Initiates the data period object.
         *
         * @param N2_number_of_records_per_hour number of records per hour
         * @param A1_description description
         * @param A2_start_day_of_week start day of the week
         * @param A3_start_day starting day
         * @param A4_end_day ending day
         */
        public Data_Period(int N2_number_of_records_per_hour, String A1_description, String A2_start_day_of_week, String A3_start_day, String A4_end_day) {
            this.N2_number_of_records_per_hour = N2_number_of_records_per_hour;
            this.A1_description = A1_description;
            this.A2_start_day_of_week = A2_start_day_of_week.replace(" ", "");
            this.A3_start_day = A3_start_day.replace(" ", "");
            this.A4_end_day = A4_end_day.replace(" ", "");
        }

        /**
         * Returns the number of records per hour.
         *
         * @return number of records
         */
        public int getN2_number_of_records_per_hour() {
            return N2_number_of_records_per_hour;
        }

        /**
         * Returns the description of the data period.
         *
         * @return description
         */
        public String getA1_description() {
            return A1_description;
        }

        /**
         * Returns start day of the week
         *
         * @return start day of the week
         */
        public String getA2_start_day_of_week() {
            return A2_start_day_of_week;
        }

        /**
         * Returns the starting day.
         *
         * @return string of the starting day
         */
        public String getA3_start_day() {
            return A3_start_day;
        }

        /**
         * Returns the ending day.
         *
         * @return string of the ending day
         */
        public String getA4_end_day() {
            return A4_end_day;
        }

        /**
         * Number of records per hour.
         *
         * @param N2_number_of_records_per_hour number
         */
        public void setN2_number_of_records_per_hour(int N2_number_of_records_per_hour) {
            this.N2_number_of_records_per_hour = N2_number_of_records_per_hour;
        }

        /**
         * Data period name or description.
         *
         * @param A1_description text
         */
        public void setA1_description(String A1_description) {
            this.A1_description = A1_description;
        }

        /**
         * Data period start day of the week (Sunday, Monday, Tuesday,
         * Wednesday, Thursday, Friday, and Saturday).
         *
         * @param A2_start_day_of_week text
         */
        public void setA2_start_day_of_week(String A2_start_day_of_week) {
            this.A2_start_day_of_week = A2_start_day_of_week;
        }

        /**
         * Start day.
         *
         * @param A3_start_day day
         */
        public void setA3_start_day(String A3_start_day) {
            this.A3_start_day = A3_start_day;
        }

        /**
         * End day.
         *
         * @param A4_end_day day
         */
        public void setA4_end_day(String A4_end_day) {
            this.A4_end_day = A4_end_day;
        }

    }

}
