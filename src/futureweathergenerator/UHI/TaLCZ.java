/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.UHI;

/**
 *
 * @author eugenio
 */
public class TaLCZ {
    
    private final int LCZ;
    private final float SEM1_TMAX;
    private final float SEM1_TMIN;
    private final float SEM2_TMAX;
    private final float SEM2_TMIN;

    public TaLCZ(int LCZ, float SEM1_TMAX, float SEM1_TMIN, float SEM2_TMAX, float SEM2_TMIN) {
        this.LCZ = LCZ;
        this.SEM1_TMAX = SEM1_TMAX;
        this.SEM1_TMIN = SEM1_TMIN;
        this.SEM2_TMAX = SEM2_TMAX;
        this.SEM2_TMIN = SEM2_TMIN;
    }

    public int getLCZ() {
        return LCZ;
    }
    
    public float getSEM1_TMAX() {
        return SEM1_TMAX;
    }

    public float getSEM1_TMIN() {
        return SEM1_TMIN;
    }

    public float getSEM2_TMAX() {
        return SEM2_TMAX;
    }

    public float getSEM2_TMIN() {
        return SEM2_TMIN;
    }
    
}
