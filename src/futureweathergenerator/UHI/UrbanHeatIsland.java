/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.UHI;

import futureweathergenerator.EPW.EPW;
import static futureweathergenerator.File.deleteDirectory;
import static futureweathergenerator.File.unzip;
import futureweathergenerator.FutureWeatherGenerator;
import futureweathergenerator.functions.Design_Conditions;
import futureweathergenerator.functions.Ground_Temperatures;
import futureweathergenerator.functions.N12_Horizontal_Infrared_Radiation_Intensity;
import futureweathergenerator.functions.N30_Snow_Depth;
import futureweathergenerator.functions.N6_Dry_Bulb_Temperature;
import futureweathergenerator.functions.N8_Relative_Humidity;
import futureweathergenerator.functions.Typical_Extreme_Periods;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;

/**
 *
 * @author eugenio
 */
public class UrbanHeatIsland {

    public static void calculate(String PATH_OUTPUT_FOLDER, EPW epw, EPW uhiEPW, int EPW_ORIGINAL_LCZ, int TARGET_UHI_LCZ, boolean doEcho) {
        
        // Sets info in EPW.
        uhiEPW.setUHIInfo(EPW_ORIGINAL_LCZ, TARGET_UHI_LCZ);
        
        // Read Air Temperature UHI dataset.
        ArrayList<UrbanHeatIslandDataPoint> uhi_dataset = readUHIDataset();

        // Find nearest location of the UHI dataset to the EPW weather station
        // and retrive uhi daytime and nightime differences.
        UrbanHeatIslandDataPoint nearest_uhi_location = findNearestLocation(PATH_OUTPUT_FOLDER, uhiEPW.getEpw_location().getN3_longitude(), uhiEPW.getEpw_location().getN2_latitude(), uhi_dataset);
        
        if (doEcho) {
            System.out.println(String.format(Locale.ROOT,
                    "Urban Heat Island - Nearest Urban Area: %s (%s), ID %s, Lon: %s°, Lat: %s°",
                    new Object[]{
                        nearest_uhi_location.getName(),
                        nearest_uhi_location.getIso3(),
                        nearest_uhi_location.getIsourbid(),
                        nearest_uhi_location.getLongitude(),
                        nearest_uhi_location.getLatitude()
                    }
            ));
        }
        
        // Removes design conditions.
        Design_Conditions.reset(uhiEPW);
        
        // Morphes variables.
        N6_Dry_Bulb_Temperature.morph(nearest_uhi_location, uhiEPW, EPW_ORIGINAL_LCZ, TARGET_UHI_LCZ, doEcho);
        // Snow Cover
        N30_Snow_Depth.morph(nearest_uhi_location, uhiEPW, EPW_ORIGINAL_LCZ, TARGET_UHI_LCZ, doEcho);
        
        // Recalculates variables.
        N8_Relative_Humidity.calculate(epw, uhiEPW);
        // All radiation, irradiation, and illuminances variables.
        
        // Calculates extraterrestrial radiation.
        // N10_Extraterrestrial_Horizontal_Radiation.calculate(uhiEPW);
        // N11_Extraterrestrial_Direct_Normal_Radiation.calculate(uhiEPW);
        
        // Calculates surface solar irradiation.
        N12_Horizontal_Infrared_Radiation_Intensity.calculate(uhiEPW);
        // N15_Diffuse_Horizontal_Radiation.calculate(uhiEPW);
        // N14_Direct_Normal_Radiation.calculate(uhiEPW);
        
        // Calculates illuminances.
        // N16_Global_Horizontal_Illuminance.calculate(uhiEPW);
        // N17_Direct_Normal_Illuminance.calculate(uhiEPW);
        // N18_Diffuse_Horizontal_Illuminance.calculate(uhiEPW);
        // N19_Zenith_Luminance.calculate(uhiEPW);
        
        // Calculates ground temperatures.
        Ground_Temperatures.morph(uhiEPW);
        
        // Determines typical and extreme periods.
        Typical_Extreme_Periods.calculate(uhiEPW);
    }
    
    public static ArrayList<UrbanHeatIslandDataPoint> readUHIDataset() {
        ArrayList<UrbanHeatIslandDataPoint> uhi_dataset = new ArrayList<>();
        int count_lines = 0;
        
        InputStream in = FutureWeatherGenerator.class.getResourceAsStream(FutureWeatherGenerator.PATH_TO_UHI_DATASET);
        try (Scanner reader = new Scanner(in)) {
            while (reader.hasNextLine()) {
                String line = reader.nextLine();
                if (count_lines > 0) {
                    String[] values = line.split(",");
                    // "ID", "ISO3", "ISOURBID", "NAME",
                    // "LONGITUDE", "LATITUDE", "LCZs"
                    int id = Integer.parseInt(values[0]);
                    String iso3 = values[1];
                    String isourbid = values[2];
                    String name = values[3];
                    float longitude = Float.parseFloat(values[4]);
                    float latitude = Float.parseFloat(values[5]);
                    String lczs = values[6];
                    uhi_dataset.add(new UrbanHeatIslandDataPoint(id, iso3, isourbid, name, longitude, latitude, lczs));
                }
                count_lines++;
            }
        }
        return uhi_dataset;
    }

    public static UrbanHeatIslandDataPoint findNearestLocation(String PATH_OUTPUT_FOLDER, float longitude, float latitude, ArrayList<UrbanHeatIslandDataPoint> uhi_dataset) {
        int nearest_location_list_id = 0;
        UrbanHeatIslandDataPoint dp = uhi_dataset.get(nearest_location_list_id);
        float uhi_longitude = dp.getLongitude();
        float uhi_latitude = dp.getLatitude();
        float distance = (float) Math.sqrt(Math.pow(uhi_longitude - longitude, 2) + Math.pow(uhi_latitude - latitude, 2));
        for (int i = 1; i < uhi_dataset.size(); i++) {
            dp = uhi_dataset.get(i);
            uhi_longitude = dp.getLongitude();
            uhi_latitude = dp.getLatitude();
            float calculate_distance = (float) Math.sqrt(Math.pow(uhi_longitude - longitude, 2) + Math.pow(uhi_latitude - latitude, 2));
            if (distance > calculate_distance) {
                distance = calculate_distance;
                nearest_location_list_id = i;
            }
        }
        
        
        int interval = (int) (Math.floor(uhi_dataset.get(nearest_location_list_id).getId() / 2500f) * 2500f) + 2500;
        // Ta - Air Temperature
        // Unzip data
        try {
            // Unzip climate model
            unzip(FutureWeatherGenerator.class.getResourceAsStream(FutureWeatherGenerator.PATH_TO_UHI_AIR_TEMPERATURE + interval + ".zip"), new File(PATH_OUTPUT_FOLDER));
        } catch (IOException ex) {
            throw new UnsupportedOperationException();
        }
        // Read Ta LCZs.
        uhi_dataset.get(nearest_location_list_id).readTa_LCZs(PATH_OUTPUT_FOLDER, interval);
        // Clean folder
        deleteDirectory(new File(futureweathergenerator.File.verifyAndAddTrailingRightSlah(PATH_OUTPUT_FOLDER) + "UHI_AirTemperature_" + interval));
        
        // Scf - Snow Cover Fraction
        // Unzip data
        try {
            // Unzip climate model
            unzip(FutureWeatherGenerator.class.getResourceAsStream(FutureWeatherGenerator.PATH_TO_UHI_SNOW_COVER + interval + ".zip"), new File(PATH_OUTPUT_FOLDER));
        } catch (IOException ex) {
            throw new UnsupportedOperationException();
        }
        // Read buffer clusters.
        uhi_dataset.get(nearest_location_list_id).readBufferScfClusters(PATH_OUTPUT_FOLDER, interval);
        // Read urban clusters.
        uhi_dataset.get(nearest_location_list_id).readUrbanScfClusters(PATH_OUTPUT_FOLDER, interval);
        // Clean folder
        deleteDirectory(new File(futureweathergenerator.File.verifyAndAddTrailingRightSlah(PATH_OUTPUT_FOLDER) + "UHI_SnowCover_" + interval));
        deleteDirectory(new File(futureweathergenerator.File.verifyAndAddTrailingRightSlah(PATH_OUTPUT_FOLDER) + "__MACOSX"));
        
        return uhi_dataset.get(nearest_location_list_id);
    }
    
}
