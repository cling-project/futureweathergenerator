/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.UHI;

/**
 *
 * @author eugenio
 */
public enum LCZ {

    LCZ_1_Compact_highrise(1),
    LCZ_2_Compact_midrise(2),
    LCZ_3_Compact_lowrise(3),
    LCZ_4_Open_highrise(4),
    LCZ_5_Open_midrise(5),
    LCZ_6_Open_lowrise(6),
    LCZ_7_Lightweight_lowrise(7),
    LCZ_8_Large_lowrise(8),
    LCZ_9_Sparsely_built(9),
    LCZ_10_Heavy_industry(10),
    LCZ_11_Dense_trees(11),
    LCZ_12_Scattered_trees(12),
    LCZ_13_Bush_scrub(13),
    LCZ_14_Low_plants(14),
    LCZ_15_Bare_rock_or_paved(15),
    LCZ_16_Bare_soil_or_sand(16),
    LCZ_17_Water(17);

    private final int id;

    private LCZ(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
    
    public static LCZ fromId(int id) {
        for (LCZ lcz : LCZ.values()) {
            if (lcz.getId() == id) {
                return lcz;
            }
        }
        throw new IllegalArgumentException("No enum constant with id " + id);
    }
}
