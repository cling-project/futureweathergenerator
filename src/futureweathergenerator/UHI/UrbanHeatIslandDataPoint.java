/*
 * CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
 * and build upon the material in any medium or format for noncommercial
 * purposes only, and only so long as attribution is given to the creator.
 * If you remix, adapt, or build upon the material, you must license the
 * modified material under identical terms.
 */
package futureweathergenerator.UHI;

import futureweathergenerator.EPW.EPW;
import futureweathergenerator.Months;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;

/**
 *
 * @author eugenio
 */
public class UrbanHeatIslandDataPoint {
    
    private final int id;
    private final String iso3;
    private final String isourbid;
    private final String name;
    private final float longitude;
    private final float latitude;
    private final int[] lczs;
    
    
    private float Ta_DIFF_SEM1_TMAX;
    private float Ta_DIFF_SEM1_TMIN;
    private float Ta_DIFF_SEM2_TMAX;
    private float Ta_DIFF_SEM2_TMIN;

    private int epw_original_Ta_lcz_id;
    private ArrayList<TaLCZ> Ta_LCZs;
    
    private float[] Scf_DIFF_MONTHS;
    
    private int selected_buffer_Scf_cluster_id;
    private ArrayList<ScfCluster> buffer_Scf_clusters;
    private ArrayList<ScfCluster> urban_Scf_clusters;

    public UrbanHeatIslandDataPoint(int id, String iso3, String isourbid, String name, float longitude, float latitude, String lczs) {
        this.id = id;
        this.iso3 = iso3;
        this.isourbid = isourbid;
        this.name = name;
        this.longitude = longitude;
        this.latitude = latitude;
        String[] lczs_strings = lczs.split(":");
        this.lczs = new int[lczs_strings.length];
        for(int i = 0; i < lczs_strings.length; i++) {
            this.lczs[i] = Integer.parseInt(lczs_strings[i]);
        }
        this.Scf_DIFF_MONTHS = new float[12];
    }

    public int getId() {
        return id;
    }
    
    public String getIso3() {
        return iso3;
    }

    public String getIsourbid() {
        return isourbid;
    }

    public String getName() {
        return name;
    }

    public float getLongitude() {
        return longitude;
    }

    public float getLatitude() {
        return latitude;
    }

    public float getTa_DIFF_SEM1_TMAX() {
        return Ta_DIFF_SEM1_TMAX;
    }

    public float getTa_DIFF_SEM1_TMIN() {
        return Ta_DIFF_SEM1_TMIN;
    }

    public float getTa_DIFF_SEM2_TMAX() {
        return Ta_DIFF_SEM2_TMAX;
    }

    public float getTa_DIFF_SEM2_TMIN() {
        return Ta_DIFF_SEM2_TMIN;
    }

    private ArrayList<TaLCZ> readTa_LCZs(String PATH_OUTPUT_FOLDER, int interval, String filename) {
        ArrayList<TaLCZ> clusters = new ArrayList<>();
        String filepath = futureweathergenerator.File.verifyAndAddTrailingRightSlah(PATH_OUTPUT_FOLDER) + "UHI_AirTemperature_" + interval + "/" + filename;
        File file = new File(filepath);
        Scanner reader;
        try {
            reader = new Scanner(file);
        } catch (FileNotFoundException ex) {
            throw new UnsupportedOperationException(" ** ERROR ** Unknown UHI Air Temperature file name: " + filepath);
        }
        while (reader.hasNextLine()) {
            String line = reader.nextLine();
            if(!"".equals(line)) {
                String[] values = line.split(",");
                clusters.add(
                        new TaLCZ(
                                Integer.parseInt(values[0]),
                                Float.parseFloat(values[1]),
                                Float.parseFloat(values[2]),
                                Float.parseFloat(values[3]),
                                Float.parseFloat(values[4])
                        )
                );
            }
        }
        return(clusters);
    }
    
    private ArrayList<ScfCluster> readScfCluster(String PATH_OUTPUT_FOLDER, int interval, String filename) {
        ArrayList<ScfCluster> clusters = new ArrayList<>();
        String filepath = futureweathergenerator.File.verifyAndAddTrailingRightSlah(PATH_OUTPUT_FOLDER) + "UHI_SnowCover_" + interval + "/" + filename;
        File file = new File(filepath);
        Scanner reader;
        try {
            reader = new Scanner(file);
        } catch (FileNotFoundException ex) {
            throw new UnsupportedOperationException(" ** ERROR ** Unknown UHI Snow Cover file name: " + filepath);
        }
        while (reader.hasNextLine()) {
            String line = reader.nextLine();
            if(!"".equals(line)) {
                String[] values = line.split(",");
                float[] months = new float[values.length];
                for(int i = 0; i < values.length; i++) {
                    months[i] = Float.parseFloat(values[i]);
                }
                clusters.add(new ScfCluster(months));
            }
        }
        return(clusters);
    }

    public void readTa_LCZs(String PATH_OUTPUT_FOLDER, int interval) {
        String filename = isourbid + ".csv";
        this.Ta_LCZs = this.readTa_LCZs(PATH_OUTPUT_FOLDER, interval, filename);
    }
    
    public void readBufferScfClusters(String PATH_OUTPUT_FOLDER, int interval) {
        String filename = isourbid + "_buffer.csv";
        this.buffer_Scf_clusters = readScfCluster(PATH_OUTPUT_FOLDER, interval, filename);
    }

    public void readUrbanScfClusters(String PATH_OUTPUT_FOLDER, int interval) {
        String filename = isourbid + "_urban.csv";
        this.urban_Scf_clusters = readScfCluster(PATH_OUTPUT_FOLDER, interval, filename);
    }

    public ArrayList<TaLCZ> getTa_LCZs() {
        return Ta_LCZs;
    }

    public ArrayList<ScfCluster> getBuffer_Scf_clusters() {
        return buffer_Scf_clusters;
    }

    public float[] getScf_DIFF_MONTHS() {
        return Scf_DIFF_MONTHS;
    }

    public int getSelected_buffer_Scf_cluster_id() {
        return selected_buffer_Scf_cluster_id;
    }

    public int getEpw_original_Ta_lcz_id() {
        return epw_original_Ta_lcz_id;
    }

    public ArrayList<ScfCluster> getUrban_Scf_clusters() {
        return urban_Scf_clusters;
    }
    
    
    public String getAvailableLCZs() {
        StringBuilder list_of_available_lcz = new StringBuilder();
        
        for(int i = 0; i < this.Ta_LCZs.size(); i++) {
            list_of_available_lcz.append(
                    String.format(Locale.ROOT, "\t%s%n", LCZ.fromId(this.Ta_LCZs.get(i).getLCZ()).toString().replace("_", " "))
            );
        }

        return list_of_available_lcz.toString();
    }
    
    public void selectBufferTaCluster(EPW uhiEPW, int EPW_ORIGINAL_LCZ) {
        int epw_original_lcz_id = -1;
        for(int i = 0; i < this.Ta_LCZs.size(); i++) {
            if(this.Ta_LCZs.get(i).getLCZ() ==  EPW_ORIGINAL_LCZ) {
                epw_original_lcz_id = i;
                break;
            }
        }
        if(epw_original_lcz_id == -1) {
            System.err.println(String.format(Locale.ROOT, "%n** ERROR ** The EPW LCZ %d is not available for this location.", EPW_ORIGINAL_LCZ));
            System.err.println(String.format(Locale.ROOT, "\tThe LCZs available are:"));
            System.err.println(String.format(Locale.ROOT, getAvailableLCZs() + "%n"));
            System.exit(1);
        }
        this.epw_original_Ta_lcz_id = epw_original_lcz_id;
    }
    
    public void calculateTaDifferences(int TARGET_UHI_LCZ) {
        int target_uhi_lcz_id = -1;
        for(int i = 0; i < this.Ta_LCZs.size(); i++) {
            if(this.Ta_LCZs.get(i).getLCZ() ==  TARGET_UHI_LCZ) {
                target_uhi_lcz_id = i;
                break;
            }
        }
        if(target_uhi_lcz_id == -1) {
            System.err.println(String.format(Locale.ROOT, "%n** ERROR ** The target LCZ %d is not available for this location.", TARGET_UHI_LCZ));
            System.err.println(String.format(Locale.ROOT, "\tThe LCZs available are:"));
            System.err.println(String.format(Locale.ROOT,getAvailableLCZs() + "%n"));
            System.exit(1);
        }
        
        this.Ta_DIFF_SEM1_TMAX = this.Ta_LCZs.get(target_uhi_lcz_id).getSEM1_TMAX() - this.Ta_LCZs.get(this.epw_original_Ta_lcz_id).getSEM1_TMAX();
        this.Ta_DIFF_SEM1_TMIN = this.Ta_LCZs.get(target_uhi_lcz_id).getSEM1_TMIN() - this.Ta_LCZs.get(this.epw_original_Ta_lcz_id).getSEM1_TMIN();
        this.Ta_DIFF_SEM2_TMAX = this.Ta_LCZs.get(target_uhi_lcz_id).getSEM2_TMAX() - this.Ta_LCZs.get(this.epw_original_Ta_lcz_id).getSEM2_TMAX();
        this.Ta_DIFF_SEM2_TMIN = this.Ta_LCZs.get(target_uhi_lcz_id).getSEM2_TMIN() - this.Ta_LCZs.get(this.epw_original_Ta_lcz_id).getSEM2_TMIN();
    }
    
    public void selectBufferScfCluster(EPW uhiEPW, int BUFFER_AREA_TEMPERATURE_LEVEL) {
        switch(BUFFER_AREA_TEMPERATURE_LEVEL) {
            default:
            case 0:
                float[] EPW_MONTHS = new float[12];
                for(int i = 0; i < 12; i++) {
                    int[] month_row_ids = Months.getMonthRowIds(Months.Abbreviation.Jan);
                    int first_row_id = month_row_ids[1];
                    int last_row_id = month_row_ids[2];
                    for(int row_id = first_row_id; row_id < last_row_id; row_id++) {
                        float snow_depth = uhiEPW.getEpw_data_fields().get(row_id).getN30_snow_depth();
                        if(snow_depth > 0f) {
                            EPW_MONTHS[i]++;
                        }
                    }
                    EPW_MONTHS[i] /= (float) month_row_ids[0];
                    EPW_MONTHS[i] *= 100f;
                }
                
                // Check which cluster has temperatures nearer to EPW.
                float minMSE = Float.POSITIVE_INFINITY;
                int minMSEID = -1;
                for(int i = 0; i < this.buffer_Scf_clusters.size(); i++) {
                    float mse = (float)
                            (Math.pow(EPW_MONTHS[0] - Math.ceil(this.buffer_Scf_clusters.get(i).getMonths()[0]), 2) +
                            Math.pow(EPW_MONTHS[1] - Math.ceil(this.buffer_Scf_clusters.get(i).getMonths()[1]), 2) +
                            Math.pow(EPW_MONTHS[2] - Math.ceil(this.buffer_Scf_clusters.get(i).getMonths()[2]), 2) +
                            Math.pow(EPW_MONTHS[3] - Math.ceil(this.buffer_Scf_clusters.get(i).getMonths()[3]), 2) +
                            Math.pow(EPW_MONTHS[4] - Math.ceil(this.buffer_Scf_clusters.get(i).getMonths()[4]), 2) +
                            Math.pow(EPW_MONTHS[5] - Math.ceil(this.buffer_Scf_clusters.get(i).getMonths()[5]), 2) +
                            Math.pow(EPW_MONTHS[6] - Math.ceil(this.buffer_Scf_clusters.get(i).getMonths()[6]), 2) +
                            Math.pow(EPW_MONTHS[7] - Math.ceil(this.buffer_Scf_clusters.get(i).getMonths()[7]), 2) +
                            Math.pow(EPW_MONTHS[8] - Math.ceil(this.buffer_Scf_clusters.get(i).getMonths()[8]), 2) +
                            Math.pow(EPW_MONTHS[9] - Math.ceil(this.buffer_Scf_clusters.get(i).getMonths()[9]), 2) +
                            Math.pow(EPW_MONTHS[10] - Math.ceil(this.buffer_Scf_clusters.get(i).getMonths()[10]), 2) +
                            Math.pow(EPW_MONTHS[11] - Math.ceil(this.buffer_Scf_clusters.get(i).getMonths()[11]), 2));
                    mse = (float) Math.sqrt(mse / 4f);
                    if(minMSE > mse) {
                        minMSE = mse;
                        minMSEID = i;
                    }
                }
                this.selected_buffer_Scf_cluster_id = minMSEID;
                break;
            case 1:
                this.selected_buffer_Scf_cluster_id = 0;
                break;
            case 2:
                this.selected_buffer_Scf_cluster_id = Math.round((this.buffer_Scf_clusters.size() - 1) * 0.25f);
                break;
            case 3:
                this.selected_buffer_Scf_cluster_id = Math.round((this.buffer_Scf_clusters.size() - 1) * 0.5f);
                break;
            case 4:
                this.selected_buffer_Scf_cluster_id = Math.round((this.buffer_Scf_clusters.size() - 1) * 0.75f);
                break;
            case 5:
                this.selected_buffer_Scf_cluster_id = this.buffer_Scf_clusters.size() - 1;
                break;
        }
    }
    
    private float getScfDiff(float urban, float buffer) {
        float fraction = urban / buffer;
        if(Float.isInfinite(fraction)) {
            return 0f;
        }
        if(Float.isNaN(fraction)) {
            return 0f;
        }
        return fraction < 0.3f ? 0f : 1f;
    }
    
    public void calculateScfDifferences(int URBAN_DENSITY) {
        int urban_cluster_id;
        switch(URBAN_DENSITY) {
            default:
            case 0:
                urban_cluster_id = 0;
                break;
            case 1:
                urban_cluster_id = Math.round((this.urban_Scf_clusters.size() - 1) * 0.25f);
                break;
            case 2:
                urban_cluster_id = Math.round((this.urban_Scf_clusters.size() - 1) * 0.5f);
                break;
            case 3:
                urban_cluster_id = Math.round((this.urban_Scf_clusters.size() - 1) * 0.75f);
                break;
            case 4:
                urban_cluster_id = this.urban_Scf_clusters.size() - 1;
                break;
        }
        for(int i = 0; i < 12; i++) {
            this.Scf_DIFF_MONTHS[i] = getScfDiff(
                this.urban_Scf_clusters.get(urban_cluster_id).getMonths()[i], 
                this.buffer_Scf_clusters.get(this.selected_buffer_Scf_cluster_id).getMonths()[i]);
        }
    }
}
