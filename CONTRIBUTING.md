# Reporting issues and suggesting enhancements
Before reporting any issue and enhancements, guarantee that you have installed the latest version of the 'Future Weather Generator', Java, and JDK. Then, read the already reported issues in the Bitbucket repository not to double report the same issue.

In case of reporting a bug, please provide:

- Operating system
- Java and JDK versions
- Version of the ‘Future Weather Generator’
- Description of the issue
- Provide all the data needed to reproduce the problem

When suggesting an enhancement, provide detailed description with as many details as possible:

- Summary of the enhancement
- Motivation
- Describe alternatives
- Additional context