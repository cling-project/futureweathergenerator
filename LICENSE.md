Copyright (c) 2022 Eugénio Rodrigues, David Carvalho, and Marco S. Fernandes.

# Licensing
'Future Weather Generator' by Eugénio Rodrigues, David Carvalho, and Marco S. Fernandes is licensed under CC BY-NC-SA (Creative Commons Attribution 4.0 International license).

CC BY-NC-SA: This license allows reusers to distribute, remix, adapt, and build upon the material in any medium or format for noncommercial purposes only, and only so long as attribution is given to the creator. If you remix, adapt, or build upon the material, you must license the modified material under identical terms.