<#if licenseFirst??>
${licenseFirst}
</#if>
${licensePrefix}CC BY-NC-SA: This license allows reusers to distribute, remix, adapt,
${licensePrefix}and build upon the material in any medium or format for noncommercial
${licensePrefix}purposes only, and only so long as attribution is given to the creator.
${licensePrefix}If you remix, adapt, or build upon the material, you must license the
${licensePrefix}modified material under identical terms.
<#if licenseLast??>
${licenseLast}
</#if>

