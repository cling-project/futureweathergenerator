# Overview
The ‘Future Weather Generator’ is a Java app for researchers and building design professionals. The app morphs hourly EPW files to match future climate change scenarios used in dynamic simulation programs such as EnergyPlus.

The app is free, open-source, and cross-platform. Researchers can use the app as a standalone tool or integrate it into their research code. Building design professionals can also use this app to create future weather data to evaluate a building’s future energy performance.

The app currenlty moprhs EPW files according to the experiments of several CMIP6 general circulation models (contributed to the 6th IPCC Report).

You may visit the 'Future Weather Generator' [webiste](https://adai.pt/future-weather-generator/).

# Requirements
- Java
- JDK

‘Future Weather Generator’ was tested in machines running ‘Java 8 Update 321’ in mac OS and MS Windows, and ‘JDK 14’ and ‘JDK 17’ in macOS and MS Windows, respectively.

# Quick start
After guaranteeing the latest version of Java and JDK, download the desired version of the 'Future Weather Generator'. Run the app by double clicking the file or running the command `java -jar FutureWeatherGenerator.jar` in the terminal.

# Troubleshooting
Visit the app [troubleshooting webpage](https://adai.pt/future-weather-generator/troubleshooting/).

# Use it in your project
You can use 'Future Weather Generator' as a library in your project. Also, you can download the project repository and added it to your code. Licensing requirements apply.

Visit the app [documentation webpage](https://adai.pt/future-weather-generator/documentation/) for examples of coding.

# Reporting issues and suggesting enhancements
See CONTRIBUTING.md file or visit the app [documentation webpage](https://adai.pt/future-weather-generator/documentation/#contributing).

# Code of conduct
See CODE_OF_CONDUCT.md or visit the app [documentation webpage](https://adai.pt/future-weather-generator/documentation/#contributing).

# Technical description
Visit the app [documentation webpage](https://adai.pt/future-weather-generator/documentation/#technical-description) for technical description of the morphing procedures.

# Disclaimer
The software is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the software.

# Licensing
See LICENSE.md or visit the app [licensing webpage](https://adai.pt/future-weather-generator/download/).

# Acknowledgments
This work is framed under the ‘[Energy for Sustainability Initiative](https://www.uc.pt/en/efs)’ of the [University of Coimbra](http://www.uc.pt). The authors are grateful to all Ph.D. students that have tested the app.

We acknowledge the World Climate Research Programme, which, through its Working Group on Coupled Modelling, coordinated and promoted CMIP6. We thank the climate modeling groups for producing and making available their model output, the Earth System Grid Federation (ESGF) for archiving the data and providing access, and the multiple funding agencies who support CMIP6 and ESGF.

This app took advantage of [netCDF software](http://doi.org/10.5065/D6H70CW6) developed by UCAR/Unidata.